(* We call sure_diff x x' when x <> x' has been proved by CryptoVerif,
   so we admit that x <> x' has been proved on the F* side *)
let sure_diff (#t: Type0) (x: t) (x':t) : Lemma (ensures (~(x == x'))) =
  admit(); ()

let reachreveal_<var> (n:nat) = CVST?.reflect (fun s1 ->
  State.reach_reveal s1 vn_<var> vnqueried_<var> n)
  
let reach_<var> (n:nat) (v:<t>) = CVST?.reflect (fun s1 ->
  sure_diff (State.reach_test s1 vn_<var> vnqueried_<var> n) (Some v);
  (s1, ()))

let reachonesession_<var> (n:nat) (v:<t>) = CVST?.reflect (fun s1 ->
  sure_diff (State.var_get s1 vn_<var> n) (Some v);
  (s1, ()))

(* Indistinguishability - Real side *)

let indist_<var> (n:nat) = CVST?.reflect (fun s1 ->
  State.indist_real s1 vn_<var> n)

let indistonession_<var> (n:nat) = CVST?.reflect (fun s1 ->
  State.indist_one_session_real s1 vn_<var> vnqueried_<var> n)

(* Indistinguishability - Ideal side *)

let indist_<var> (n:nat) = CVST?.reflect (fun s1 ->
  State.indist_ideal s1 vn_<var> vnqueried_<var> <rand> n)

let indistonession_<var> (n:nat) = CVST?.reflect (fun s1 ->
  State.indist_one_session_ideal s1 vn_<var> vnqueried_<var> <rand> n)


val reachreveal_<var>: (n:nat) -> CVST (option <t>) (fun s1 res s2 ->
  (s2,res) == State.reach_reveal s1 vn_<var> vnqueried_<var> n)

val reach_<var>: (n:nat) -> (v:<t>) -> CVST (unit) (fun s1 res s2 ->
  (s2 == s1) /\ (res == ()) /\
  ~(State.reach_test s1 vn_<var> vnqueried_<var> n == Some v))

val reachonesession_<var>: (n:nat) -> (v:<t>) -> CVST (unit) (fun s1 res s2 ->
  (s2 == s1) /\ (res == ()) /\
  ~(State.var_get s1 vn_<var> n == Some v))

(* For other functions *)

val <f>: <args of f with type> -> CVST (option <t>) (fun s1 res s2 -> (s2, res) == State.<f'> s1 <args of f'>)


(* For correspondences *)

type evseq = Seq.seq <PROTOCOL>.Events.<protocol>_event

type interv (e: evseq) = x: nat { x < Seq.length e }

type intervopt (e: evseq) = option (interv e)

(* Dummy code with ideas similar to what we need

type injective (e: evseq) (f: interv e -> intervopt e)
  = forall (x: interv e) (y: interv e).
     ((f x == f y) /\ ~(f x == None)) ==> (x == y)
     
type corresp (e: evseq) =
    exists (f: interv e -> interv e).
       injective e f /\ (forall (x: interv e).
          (Seq.index e x = 1) ==> (Seq.index e (f x) = 2))
*)	  
type <corresp> (e: evseq) =
  

val <f>: unit -> CVST unit (fun s1 _ s2 -> s1 == s2 && <corresp> (Seq.seq_of_list (State.events_of s2)))

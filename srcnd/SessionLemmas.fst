module SessionLemmas

open CVTypes
open RandomHelper
open State
module Map = SidMap
module Set = FStar.Set

(**
 * There are a couple of predicates related to sessions that we did not describe
 * in the thesis manuscript because they are ongoing work. We mention in the
 * section on future work that we would like to prove that oracle functions
 * add correct session entries. These predicates are a step in this direction.
**)
val is_existing_sess_id: #stt: state_type -> state stt -> id: SidMap.sidt -> bool

let is_existing_sess_id #stt st id =
  match Map.get (sessions_of st) id with
  | None -> true
  | Some _ -> false

val is_matching_sess_id_entry
      (#stt: state_type)
      (st: state stt)
      (on: oracle_name (st_of stt))
      (id: SidMap.sidt)
    : Pure (bool)
      (requires True)
      (ensures
        fun b ->
          match b with
          | false -> True
          | true -> is_existing_sess_id st id)

let is_matching_sess_id_entry #stt st sn id =
  if is_existing_sess_id st id
  then
    (let seo = Map.get (sessions_of st) id in
      match seo with
      | None -> false // being in the true branch of is_existing, we know it is not None
      | Some se -> not (List.Tot.existsb (fun sess -> not (is_valid_entry sn sess)) se))
  else false
  
let same_sessions (#stt: state_type) (st0 st1: state stt) =
  (forall (id: SidMap.sidt) (on: oracle_name (st_of stt)).
      is_matching_sess_id_entry st0 on id ==> is_matching_sess_id_entry st1 on id)

(* three more predicates on our way to more guarantees on session entries *)
let sessions_of_other_modules_unchanged (#stt: state_type) (st0 st1: state stt) =
  (forall (id: SidMap.sidt) (on: oracle_name (st_of stt)).
      is_matching_sess_id_entry st0 on id ==> is_matching_sess_id_entry st1 on id)

let sessions_other_of_module_unchanged (#stt: state_type) (st0 st1: state stt) (id: SidMap.sidt) =
  (forall (other_id: SidMap.sidt{other_id <> id}) (on: oracle_name (st_of stt)).
      is_matching_sess_id_entry st0 on other_id ==> is_matching_sess_id_entry st1 on other_id)

let sessions_unaffected (#stt: state_type) (st0 st1: state stt) (id: SidMap.sidt) =
  sessions_of_other_modules_unchanged st0 st1 /\ sessions_other_of_module_unchanged st0 st1 id

(* Lemmas about various state functions *)

val state_upd_entropy_lemma (#stt: state_type) (st0: state stt) (e: entropy)
    : Lemma (same_sessions st0 (state_upd_entropy st0 e))

let state_upd_entropy_lemma st0 e = ()

val call_with_entropy_lemma (#stt: state_type) (#a: Type) (st0: state stt) (f: entropy -> entropy * a)
    : Lemma (same_sessions st0 (call_with_entropy st0 f)._1)

let call_with_entropy_lemma st0 f = ()

val state_upd_tables_lemma (#stt: state_type) (st0: state stt) (tabs: tt_of stt)
    : Lemma (same_sessions st0 (state_upd_tables st0 tabs))

let state_upd_tables_lemma st0 tabs = ()

val insert_lemma (#stt: state_type) (#te: Type0) (st0: state stt) (tn: table_name (tt_of stt) te) (x:te)
    : Lemma (same_sessions st0 (insert st0 tn x))

let insert_lemma st0 tn x = ()

type table_filter_full_same_session (stt: state_type) (te: Type0) (rt: Type0) =
  filter: table_filter_full stt te rt
  { forall (st0: state stt) (x:te) . same_sessions st0 (filter st0 x)._1 }

type table_filter_same_session (stt: state_type) (te: Type0) (rt: Type0) =
  f : table_filter stt te rt
  { match f with
    | TableFilterSimple _ -> True
    | TableFilterFull filter ->
        forall (st0: state stt) (x:te) . same_sessions st0 (filter st0 x)._1 }

val get_inner_full_lemma (#stt: state_type) (#te: Type0)
      (#rt: Type0)
      (st0: state stt)
      (matches: Seq.seq rt)
      (tab: table te)
      (filter: table_filter_full_same_session stt te rt)
      : Lemma
      (ensures (same_sessions st0 (get_inner_full st0 matches tab filter)._1))
      (decreases tab)

let rec get_inner_full_lemma st matches tab f = 
  match tab with
  | [] -> ()
  | x :: r -> 
    match f st x with
    | st, Some e -> get_inner_full_lemma st (Seq.cons e matches) r f
    | st, None -> get_inner_full_lemma st matches r f

val get_lemma (#stt: state_type) (#te: Type0)
      (#rt: Type0)
      (st0: state stt)
      (tn: table_name (tt_of stt) te)
      (f: table_filter_same_session stt te rt)
      : Lemma (same_sessions st0 (get st0 tn f)._1)

let get_lemma st tn tf =
  let tab = return_table st tn in
  match tf with
  | TableFilterSimple _ -> ()
  | TableFilterFull f -> get_inner_full_lemma st Seq.empty tab f

val get_unique_inner_full_lemma (#stt: state_type) (#te: Type0)
      (#rt: Type0)
      (st0: state stt)
      (tab: table te)
      (filter: table_filter_full_same_session stt te rt)
      : Lemma 
      (ensures (same_sessions st0 (get_unique_inner_full st0 tab filter)._1))
      (decreases tab)

let rec get_unique_inner_full_lemma st tab f =
  match tab with
  | [] -> ()
  | x :: r -> 
    match f st x with
    | st, Some e -> ()
    | st, None -> get_unique_inner_full_lemma st r f

val get_unique_full_lemma
    (#stt: state_type) (#te: Type0) (#rt: Type0)
    (st0: state stt) (tn: table_name (tt_of stt) te)
    (filter: table_filter_full_same_session stt te rt)
    : Lemma (same_sessions st0 (get_unique_full st0 tn filter)._1)

let get_unique_full_lemma st tn f =
  let tab = return_table st tn in
  get_unique_inner_full_lemma st tab f

val entry_exists_full_lemma
    (#stt: state_type) (#te: Type0)
    (#rt: Type0)
    (st0: state stt)
    (tn: table_name (tt_of stt) te)
    (filter: table_filter_full_same_session stt te rt)
    : Lemma (same_sessions st0 (entry_exists_full st0 tn filter)._1)

let entry_exists_full_lemma st tn f = 
  get_unique_full_lemma st tn f

val state_upd_variables_lemma (#stt: state_type) (st0: state stt) (vars: vt_of stt)
    : Lemma (same_sessions st0 (state_upd_variables st0 vars))

let state_upd_variables_lemma st0 vars = ()

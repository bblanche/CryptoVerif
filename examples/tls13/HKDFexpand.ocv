(* This file proves that, when l_1, l_2, l_3 are pairwise distinct
labels and s is a fresh random value, HKDF-expand-label(s, l_i, "") for 
i = 1,2,3 are indistinguishable from independent fresh random values. *)

type key [large, fixed].
type label.

(* HMAC is a PRF *)

proba Pprf.
expand PRF(key, bitstring, key, HMAC, Pprf).

(* HKDF_expand_label_empty_bytes(Secret, Label) = 
   HKDF_expand_label(Secret, Label, "", Length) =
   HKDF_expand(Secret, [Length, tls13_string + Label, ""], Length) =
   Truncate(HMAC(Secret, [Length, tls13_string + Label, "", 0x00]), Length)

tls13_string is "TLS 1.3, " in draft 18 and "tls13 " in the RFC.
The Length is supposed to be fixed from the Label (true for a given configuration).
Length <= Hash.length
We ignore the final truncation; we assume that the caller will use only
the first Length bits.
We define build_arg(Label) = [Length, tls13_string + Label, "", 0x00].
*)

fun build_arg(label): bitstring [data].

letfun HKDF_expand_label_empty_bytes(Secret: key, Label: label) =
       HMAC(Secret, build_arg(Label)).

(* Three different constant labels
   Typical labels: "finished", "key", "iv" for the handshake_traffic_secret
   or "application traffic secret" in draft 18 resp. "traffic upd" in the RFC,
   "key", "iv" for the traffic_secret_N *)

const l1: label.
const l2: label.
const l3: label.

letfun HKDF_expand_l1(Secret: key) =
       HKDF_expand_label_empty_bytes(Secret, l1).
letfun HKDF_expand_l2(Secret: key) =
       HKDF_expand_label_empty_bytes(Secret, l2).
letfun HKDF_expand_l3(Secret: key) =
       HKDF_expand_label_empty_bytes(Secret, l3).


(* Prove equivalence between processLeft and processRight *)

param N.

let processLeft =
    !N Ok() := new k: key; return;
    ((O1() := return(HKDF_expand_l1(k))) |
     (O2() := return(HKDF_expand_l2(k))) |
     (O3() := return(HKDF_expand_l3(k)))).

let processRight =
    !N Ok() := return;
    ((O1() := new r1: key; return(r1)) |
     (O2() := new r2: key; return(r2)) |
     (O3() := new r3: key; return(r3))).
    
equivalence
	run processLeft
	run processRight

(* EXPECTED
All queries proved.
0.052s (user 0.052s + system 0.000s), max rss 20272K
END *)

(* Common assumptions used by TLS models

   When USE_KeySchedule1 is defined, the beginning of the key schedule
   is used. Useful for most versions with pre-shared key.
   In this case, the type kex_shared_secret must be defined, and the function
   Derive_handshake_secret_DHE will be defined.
   This type may be renamed by defining it as a macro.
 *)


type key [large, fixed].
type extracted [large, fixed].

param N, N2, N3, N4, Nk.

type two_keys [large,fixed].

ifdef(`USE_KeySchedule1',`
(* We use the lemma proved in KeySchedule1-*.ocv *)

fun Derive_Secret_cets_eems(extracted, bitstring): two_keys.
fun Derive_Secret_psk_binder_key(extracted): key.
fun Derive_handshake_secret_DHE(extracted, kex_shared_secret): extracted.
fun Derive_handshake_secret_zero(extracted): extracted.

proba Pprf1.

equiv(prf1)
    !Nk new es: extracted;
    (!N O1(log1: bitstring) := return(Derive_Secret_cets_eems(es, log1)) |
     O2() := return(Derive_Secret_psk_binder_key(es)) |
     !N3 O3(e1: kex_shared_secret) := return(Derive_handshake_secret_DHE(es, e1)) |
     O4() := return(Derive_handshake_secret_zero(es)))
  <=(Nk * Pprf1(time + (Nk-1)*(time(Derive_Secret_psk_binder_key) + N * time(Derive_Secret_cets_eems, maxlength(log1)) + N3 * time(Derive_handshake_secret_DHE) + time(Derive_handshake_secret_zero)), N, N3))=>
    !Nk
    (!N O1(log1: bitstring) :=
    	 find[unique] j <= N suchthat defined(log1[j], r[j]) && log1 = log1[j] then
	      return(r[j])
	 else
	      new r: two_keys; return(r) |
     O2() := new r2: key; return(r2) |
     !N3 O3(e1: kex_shared_secret) :=
    	 find[unique] j3 <= N3 suchthat defined(e1[j3], r3[j3]) && e1 = e1[j3] then
	      return(r3[j3])
	 else
	      new r3: extracted; return(r3) |
     O4() := new r4: extracted; return(r4)).

')

(* We use the lemma proved in KeySchedule2-*.ocv *)

fun Derive_Secret_cs_hts(extracted,bitstring):two_keys.
fun Derive_master_secret(extracted):extracted.


proba Pprf2.

equiv(prf2)
     !Nk new hs: extracted;
     (!N O1(log: bitstring) := return(Derive_Secret_cs_hts(hs, log)) |
      O2() := return(Derive_master_secret(hs)))
  <=(Nk * Pprf2(time + (Nk-1)*(time(Derive_master_secret) + N * time(Derive_Secret_cs_hts, maxlength(log))), N))=>
     !Nk
    (!N O1(log: bitstring) :=
    	 find[unique] j <= N suchthat defined(log[j], r[j]) && log = log[j] then
	      return(r[j])
	 else
	      new r: two_keys; return(r) |
     O2() := new r2: extracted; return(r2)).

expand random_split_2(two_keys, key, key, tuple2keys).

(* We use the lemma proved in KeySchedule3-*.ocv *)

type three_keys [large, fixed].
fun Derive_Secret_cs_ats_exp(extracted, bitstring): three_keys.
fun Derive_Secret_rms(extracted, bitstring): key.

proba Pprf3.

equiv(prf3)
    !Nk new ms: extracted;
    (!N O1(log1: bitstring) := return(Derive_Secret_cs_ats_exp(ms, log1)) |
     !N2 O2(log2: bitstring) := return(Derive_Secret_rms(ms, log2)))
  <=(Nk * Pprf3(time + (Nk-1)*(N2 * time(Derive_Secret_rms, maxlength(log2)) + N * time(Derive_Secret_cs_ats_exp, maxlength(log1))), N, N2))=>
    !Nk
    (!N O1(log1: bitstring) :=
    	 find[unique] j <= N suchthat defined(log1[j], r[j]) && log1 = log1[j] then
	      return(r[j])
	 else
	      new r: three_keys; return(r) |
     !N2 O2(log2: bitstring) :=
     	  find[unique] j2 <= N2 suchthat defined(log2[j2], r2[j2]) && log2 = log2[j2] then
	      return(r2[j2])
	 else
	      new r2: key; return(r2)).

expand random_split_3(three_keys, key, key, key, tuple3keys).

(* We use the lemma proved in HKDFexpand.ocv *)

fun HKDF_expand_fin_label(key): key.
fun HKDF_expand_key_label(key): key.
fun HKDF_expand_iv_label(key): key.

proba Pprf_fin_key_iv.

equiv(prf_fin_key_iv)
      !N new r: key; (O1() := return(HKDF_expand_fin_label(r)) |
      	      	       O2() := return(HKDF_expand_key_label(r)) |
		       O3() := return(HKDF_expand_iv_label(r)))
    <=(N * Pprf_fin_key_iv(time + (N-1)*(time(HKDF_expand_fin_label) + time(HKDF_expand_key_label) + time(HKDF_expand_iv_label))))=>
      !N (O1() := new r1: key; return(r1) |
      	   O2() := new r2: key; return(r2) |
	   O3() := new r3: key; return(r3)).

(* SUF-CMA MAC
   The MAC is actually a combination of a hash followed by a MAC.
   It is easy to see that the combination is SUF-CMA provided the MAC is SUF-CMA 
   and the hash is collision resistant. *)

proba Pmac.

expand SUF_CMA_det_mac(key, bitstring, bitstring, mac, check, Pmac).

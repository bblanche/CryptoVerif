(* This file models a variant of TLS 1.3 draft 18
in which the random nonces in the Hello messages are
replaced with any function of the ephemeral public key
(possibly even a constant function).

It proves properties of the initial handshake (without
pre-shared key), including 0.5-RTT data.
It proves secrecy, authentication, and unique channel identifier
properties.

Regarding secrecy, the secrecy of sats is proved on the server side
when the server sends ServerFinished, before it receives
ClientFinished. This property is useful for proving security of 0.5RTT
messages, by composition with the record protocol. The authentication
property ClientTerm ==> ServerAccept shows that the client uses the
same keys, so that implies secrecy on the client side as well.

The secrecy of cats, ems and resumption_secret (denoted psk' in the
paper) is proved on the client side. The authentication property
ServerTerm ==> ClientAccept shows that the server uses the same keys,
so that implies secrecy on the server side as well, when the server
has received ClientFinished. *)

channel io1, io2, io3, io4, io5, io6, io7, io8, io9, io10,
	io11, io12, io13, io14, io15, io16, io17, io18, io19, io20,
	io21, io22, io23, io24, io25, io26, io27, io28, io29, io30,
	io31, io32, io33, io34, io35, io36, io37, io38, io39, io40,
	io25', io26', io27', io28', io31', io32',
	cCorruptS1, cCorruptC1, cCorruptS2, cCorruptC2.

param N1,N5,N6,
      N7,N8,N9,N10,N11,N12,
      N13,N14,N15,N16.

set casesInCorresp = false.
    (* Distinguishing cases in the proof of correspondences
       is too slow with the "unique channel id" property,
       and not necessary *)

proof {
      crypto uf_cma_corrupt(sign) kseedS;
      crypto exclude_weak_keys(Z) **;
      out_game "g1.out" occ;
      (* After the first input in the server (which defines sgx), and new sr_???: nonce.
         gx is the value of gx in the client.  *)
      insert after "cr_5: " "find j' <= N1 suchthat defined(gx_1[j']) && sgx = gx_1[j'] then";
      (* gy_1 is the value of gy in the server *)
      SArename gy_1;
      out_game "g2.out" occ;
      (* In the client, after "let sil_??? = ..." and before "let s_??? = exp(cgy, x_???)"
      	 gy_2 is the first variable obtained by renaming gy_1 in the server *)
      insert after_nth 1 "sil_2:" "find j'' <= N6 suchthat defined(gy_2[j''], sgx[j'']) && gx_1 = sgx[j''] && cgy = gy_2[j''] then";
      simplify;
      crypto prf_odh(Derive_handshake_secret_DHE) [variables: x_6 -> a, x_8 -> b .];
      crypto prf2 *;
      crypto splitter(tuple2keys) *;
      crypto prf_fin_key_iv *;
      crypto prf3 *;
      crypto splitter(tuple3keys) *;
      crypto suf_cma(mac) *; 
      simplify;
      success;
      crypto uf_cma_corrupt(sign) kseedC;
      success
}

include(`tls13-core-common.ocvl')

type Z [large, bounded, nonuniform].
type elt [large, bounded, nonuniform].

type nonce [large, fixed].
fun fnonce(elt): nonce.

(* PRF-ODH assumption. Moreover, 
   the probability that exp(g, x) = Y for random x and Y independent of x 
   is at most PCollKey1, and
   the probability that exp(g, mult(x,y)) = Y where x and y are independent 
   random private keys and Y is independent of x or y is at most PCollKey2.
   We exclude weak secret keys for Curve448. (PWeakKey = 2^-445 for Curve448,
   0 for Curve25519 and prime order groups.) *)

type Znw [large, bounded, nonuniform].

expand DH_basic(elt, Znw, G, expnw, expnw', mult).

proba PWeakKey.
expand DH_exclude_weak_keys(elt, Z, Znw, ZnwtoZ, exp, expnw, PWeakKey).

proba PCollKey1.
proba PCollKey2.
expand DH_proba_collision(elt, Znw, G, expnw, expnw', mult, PCollKey1, PCollKey2).

(* Assuming HMAC satisfies the PRF-ODH assumption,
- in draft-18, Derive_handshake_secret_DHE(DHE, k) = HKDF_extract(k, DHE) =
HMAC(k, DHE) also satisfies the PRF-ODH assumption
- in the RFC, Derive_handshake_secret_DHE(DHE, k) =
HKDF_extract(Derive_secret(k, "derived", ""), DHE) =
HMAC(Derive_secret(k, "derived", ""), DHE) also satisfies the PRF-ODH assumption
because Derive_secret is collision-resistant.

In this file, we can reason that Derive_handshake_secret_DHE(DHE, k)
is used only with a single constant k = EarlySecret = HKDF_extract(0,0),
so it is equivalent to calling HMAC(k', DHE) with a single constant k'
(k' = k in draft 18 and k' = Derive_secret(k, "derived", "") in the RFC).
We can then rely on the PRF-ODH assumption on HMAC. The reasoning above
is needed in tls13-core-PSKDHE_corruptPSK.cv. *)

proba pPRF_ODH.
expand snPRF_ODH(elt, Znw, extracted, extracted, G, expnw, expnw', mult, Derive_handshake_secret_DHE, pPRF_ODH).

letfun dh_keygen() =
   new x:Z;
   (x,exp(G,x)).

const HKDF_extract_zero_zero:extracted.

(* UF-CMA signatures
   I suppose that signatures are probabilistic, and
   I generate the public key and private key from a common seed
   (instead of generating the public key from the private key).
   Verify returns true when the verification succeeds 
   (instead of returning the message)
   
   The signature is actually a combination of a hash and a signature.
   It is easy to see that the combination is UF-CMA provided the
   signature is UF-CMA and the hash is collision-resistant.

   If desired, we could also allow different signature algorithms 
   on the client and server sides.
 *)

type keyseed [large, bounded].
type skey [bounded].
type certificate [bounded].

proba Psign.
proba Psigncoll.
expand UF_CMA_proba_signature(keyseed, certificate, skey, bitstring, bitstring, 
       		        skgen, pkcert, sign, verify, Psign, Psigncoll).

(* Message formats *)

fun ClientHello(nonce, elt): bitstring [data].
fun ServerHelloIn(nonce, elt, bitstring): bitstring [data].
fun ServerHelloOut(nonce, elt): bitstring [data].
fun ServerCertificateIn(certificate,bitstring): bitstring [data].
fun ServerFinishedIn(certificate,bitstring,bitstring,bitstring): bitstring [data].
fun ServerCertificateVerifyOut(bitstring): bitstring [data].
fun ServerFinishedOut(bitstring): bitstring [data].

fun ClientCertificateVerifyOut(bitstring): bitstring [data].
fun ClientFinishedOut(bitstring): bitstring  [data].
fun ClientFinishedIn(bitstring): bitstring  [data].
fun ClientFinishedAuthIn(bitstring,certificate,bitstring,bitstring): bitstring  [data].
(*
equation forall cfin1: bitstring, log2: bitstring, cert: certificate, ccv: bitstring, cfin2: bitstring;
       ClientFinishedIn(cfin1) <> ClientFinishedAuthIn(log2, cert, ccv, cfin2).
*)
(* Logs *)

fun ServerHelloLogInfo(nonce,elt,nonce,elt,bitstring): bitstring [data].
fun ServerCertificateLogInfo(bitstring,certificate,bitstring): bitstring [data].
fun ServerCertificateVerifyLogInfo(bitstring,bitstring): bitstring [data].
fun ServerFinishedLogInfo(bitstring,bitstring): bitstring [data].
fun ClientCertificateLogInfo(bitstring, bitstring, certificate): bitstring [data].
fun ClientCertificateVerifyLogInfo(bitstring, bitstring): bitstring [data].
fun ClientFinishedLogInfo(bitstring, bitstring): bitstring [data].

(* To make sure that a client MAC with client authentication cannot
   mixed with a client MAC without client authentication. *)

equation forall scvl: bitstring, m: bitstring, ccl: bitstring, ccv: bitstring;
       ServerFinishedLogInfo(scvl, m) <> ClientCertificateVerifyLogInfo(ccl, ccv).

(* Secrecy of the keys
   The keys are cats, sats, ems, resumption_secret.
   They are stored in the variables:
     - client side: 
        c_sats
   	c_cats, c_ems, c_resumption_secret
     - server side: 
        s_sats
        s_cats, s_ems, s_resumption_secret
   All these variables are considered public, except that when we prove the secrecy
   of one such variable, this variable and the corresponding 
   variable on the other side are not public. *)

(* We prove secrecy of sats server side when the ServerFinished message is sent. *)
query secret s_sats public_vars c_cats, c_ems, c_resumption_secret, 
      	     	    		s_cats, s_ems, s_resumption_secret.

(* We prove secrecy of cats, ems, resumption_secret client side at the end of the protocol. *)
query secret c_cats public_vars c_sats, c_ems, c_resumption_secret, 
      	     	    		s_sats, s_ems, s_resumption_secret.
query secret c_ems public_vars c_cats, c_sats, c_resumption_secret, 
      	     	   	       s_cats, s_sats, s_resumption_secret.
query secret c_resumption_secret public_vars c_cats, c_sats, c_ems, 
      	     			 	     s_cats, s_sats, s_ems.

(* Authentication of the server to the client *)

event ClientTerm(bitstring,bitstring).
event ServerAccept(bitstring,bitstring,N6).

query log4: bitstring, keys: bitstring, i <= N6;
      inj-event(ClientTerm(log4, keys)) ==> inj-event(ServerAccept(log4, keys, i))
      public_vars c_cats, c_sats, c_ems, c_resumption_secret, 
      	     	  s_cats, s_sats, s_ems, s_resumption_secret.

query log4: bitstring, s_keys: bitstring, s_keys': bitstring, i <= N6, i' <= N6;
      event(ServerAccept(log4, s_keys, i)) &&
      event(ServerAccept(log4, s_keys', i')) ==>
	    i = i'
      public_vars c_cats, c_sats, c_ems, c_resumption_secret, 
      	     	  s_cats, s_sats, s_ems, s_resumption_secret.

(* Correspondence client to server *)

event ServerTerm(bitstring,bitstring).
event ClientAccept(bitstring,bitstring, N1).

query log7: bitstring, keys: bitstring, i <= N1;
      inj-event(ServerTerm(log7, keys)) ==> inj-event(ClientAccept(log7, keys, i))
      public_vars c_cats, c_sats, c_ems, c_resumption_secret, 
      	     	  s_cats, s_sats, s_ems, s_resumption_secret.

query log7: bitstring, c_keys: bitstring, c_keys': bitstring, i <= N1, i' <= N1;
      event(ClientAccept(log7, c_keys, i)) &&
      event(ClientAccept(log7, c_keys', i')) ==>
	    i = i'
      public_vars c_cats, c_sats, c_ems, c_resumption_secret, 
      	     	  s_cats, s_sats, s_ems, s_resumption_secret.

(* Unique channel identifier 
   As explained in the paper, we manually prove that 
   when the cid is the same, sfl (log until ServerFinished)
   is the same. CryptoVerif proves that the full log and the
   keys are then the same, by the query below.  *)

event ClientTerm1(bitstring, bitstring, bitstring).
event ServerTerm1(bitstring, bitstring, bitstring).

query sfl: bitstring, c_cfl: bitstring, s_cfl: bitstring, c_keys: bitstring, s_keys: bitstring;
      event(ClientTerm1(sfl, c_cfl, c_keys)) && event(ServerTerm1(sfl, s_cfl, s_keys)) ==>
      c_cfl = s_cfl && c_keys = s_keys
      public_vars c_cats, c_sats, c_ems, c_resumption_secret, 
      	     	  s_cats, s_sats, s_ems, s_resumption_secret.

type send_client_hello_res_t.
fun send_client_hello_succ(nonce, Z, elt): send_client_hello_res_t [data].

letfun send_client_hello() = 
       new x:Z;
       let gx = exp(G,x) in
       let cr = fnonce(gx) in
       send_client_hello_succ(cr,x,gx).

type keys_res_t.
fun keys_succ(extracted, key, key, key, key, key, key): keys_res_t [data].
const keys_fail: keys_res_t.
equation forall x1: extracted, x2: key, x3: key, x4: key, x5: key, x6: key, x7: key;
	 keys_succ(x1,x2,x3,x4,x5,x6,x7) <> keys_fail.
	 
letfun recv_server_hello(ES: extracted, sil:bitstring, x:Z) = 
  let ServerHelloLogInfo(cr,gx,sr,gy,l) = sil in
  (  let s = exp(gy,x) in
     let handshakeSecret = Derive_handshake_secret_DHE(s, ES) in
     let tuple2keys(client_hts, server_hts) = Derive_Secret_cs_hts(handshakeSecret,sil) in
     let client_hk = HKDF_expand_key_label(client_hts) in
     let server_hk = HKDF_expand_key_label(server_hts) in
     let client_hiv = HKDF_expand_iv_label(client_hts) in
     let server_hiv = HKDF_expand_iv_label(server_hts) in
     let cfk = HKDF_expand_fin_label(client_hts) in
     let sfk = HKDF_expand_fin_label(server_hts) in
     let masterSecret = Derive_master_secret(handshakeSecret) in
     keys_succ(masterSecret,client_hk,server_hk,client_hiv,server_hiv,cfk,sfk)
  )
  else keys_fail.

type recv_server_finished_res_t.
fun recv_server_finished_succ(three_keys): recv_server_finished_res_t [data].
const recv_server_finished_fail: recv_server_finished_res_t.
equation forall x1: three_keys;
	 recv_server_finished_succ(x1) <> recv_server_finished_fail.

letfun recv_server_finished(si:bitstring, masterSecret:extracted, sfk: key,
       		            cert:certificate, s:bitstring, m:bitstring, 
			    log1:bitstring) =
   let scl = ServerCertificateLogInfo(si,cert,log1) in
   let scvl = ServerCertificateVerifyLogInfo(scl,s) in
   let sfl = ServerFinishedLogInfo(scvl,m) in
   let cs_ats_exp = Derive_Secret_cs_ats_exp(masterSecret,sfl) in
   if verify(scl,cert,s) && mac(scvl,sfk) = m then
      recv_server_finished_succ(cs_ats_exp)
   else
      recv_server_finished_fail.
			   
letfun send_client_certificate_verify(ccl:bitstring, sk:skey) = 
   sign(ccl, sk).

letfun send_client_finished(log:bitstring, cfk:key) = 
   mac(log, cfk).

letfun get_resumption_secret(masterSecret: extracted, cfl: bitstring) =
   Derive_Secret_rms(masterSecret, cfl).

type recv_client_hello_res_t.
fun recv_client_hello_succ(nonce, elt, extracted): recv_client_hello_res_t [data].

letfun recv_client_hello(ES: extracted, cr:nonce, gx:elt) = 
   new y: Z;
   let gy = exp(G,y) in
   let sr = fnonce(gy) in
   let s = exp(gx,y) in
   let handshakeSecret = Derive_handshake_secret_DHE(s, ES) in   
   recv_client_hello_succ(sr,gy,handshakeSecret).

letfun onertt_hs_keys(sil:bitstring,handshakeSecret:extracted) = 
   let tuple2keys(client_hts, server_hts) = Derive_Secret_cs_hts(handshakeSecret,sil) in
   let client_hk = HKDF_expand_key_label(client_hts) in
   let server_hk = HKDF_expand_key_label(server_hts) in
   let client_hiv = HKDF_expand_iv_label(client_hts) in
   let server_hiv = HKDF_expand_iv_label(server_hts) in
   let cfk = HKDF_expand_fin_label(client_hts) in
   let sfk = HKDF_expand_fin_label(server_hts) in
   let masterSecret = Derive_master_secret(handshakeSecret) in
   keys_succ(masterSecret, client_hk, server_hk, client_hiv, server_hiv, cfk, sfk).

letfun send_server_certificate_verify(scl:bitstring,sk:skey) = 
   sign(scl, sk).

letfun send_server_finished(scvl:bitstring,sfk:key) = 
   mac(scvl, sfk).

letfun onertt_data_keys(masterSecret: extracted, sfl:bitstring) = 
   Derive_Secret_cs_ats_exp(masterSecret,sfl).

type check_client_finished_res_t.
fun check_client_finished_succ(key): check_client_finished_res_t [data].
const check_client_finished_fail: check_client_finished_res_t.
equation forall x1: key;
	 check_client_finished_succ(x1) <> check_client_finished_fail.


letfun check_client_finished_no_auth(masterSecret: extracted, sfl:bitstring,cfin:bitstring,cfk:key) = 
   if mac(sfl, cfk) = cfin then
   (
       let cfl = ClientFinishedLogInfo(sfl, cfin) in
       let resumption_secret = Derive_Secret_rms(masterSecret, cfl) in
       check_client_finished_succ(resumption_secret)
   )
   else
       check_client_finished_fail.

letfun check_client_finished_client_auth(masterSecret: extracted, sfl:bitstring,
				    log2: bitstring, 
				    certC:certificate,
				    ccv:bitstring,cfin:bitstring,
			    	    cfk:key) = 
   let ccl = ClientCertificateLogInfo(sfl,log2,certC) in
   let ccvl = ClientCertificateVerifyLogInfo(ccl,ccv) in
   if verify(ccl,certC,ccv) && mac(ccvl, cfk) = cfin then
   (
      let cfl = ClientFinishedLogInfo(ccvl, cfin) in
      let resumption_secret = Derive_Secret_rms(masterSecret, cfl) in   
      check_client_finished_succ(resumption_secret)
   )
   else
      check_client_finished_fail.

fun Leak(key,key,key,key):bitstring [data].
const Ok : bitstring.
fun ServLeak(key) : bitstring [data].

let Client(ES: extracted, skC: skey, pkC: certificate, pkS: certificate) = 
!i1 <= N1
    in(io1,());
    let send_client_hello_succ(cr:nonce,x:Z,cgx:elt) = send_client_hello() in
    (* for 0-rtt  insert clientEphemeralKeys(cr,x,cgx); *)
    out(io2,ClientHello(cr,cgx));
    in(io3,ServerHelloIn(sr,cgy,log0));
    let sr = fnonce(cgy) in
    let sil = ServerHelloLogInfo(cr,cgx,sr,cgy,log0) in
    let keys_succ(masterSecret:extracted,client_hk:key,server_hk:key,client_hiv:key,server_hiv:key,cfk:key,sfk:key) = recv_server_hello(ES,sil,x) in
    out(io4,(client_hk, server_hk, client_hiv, server_hiv));
    in(io5,(ServerFinishedIn(certS,scv,m,log1), ClientAuth: bool, log2: bitstring)); 
    let recv_server_finished_succ(tuple3keys(cats, c_sats : key, ems)) =
    	recv_server_finished(sil,masterSecret,sfk,certS,scv,m,log1)
    in   
    let scl = ServerCertificateLogInfo(sil,certS,log1) in
    let scvl = ServerCertificateVerifyLogInfo(scl,scv) in
    let c_sfl : bitstring = ServerFinishedLogInfo(scvl,m) in
    let (resumption_secret: key, final_mess: bitstring, final_log: bitstring) = 
        if ClientAuth then
        (
            let certC = pkC in
            let ccl = ClientCertificateLogInfo(c_sfl,log2,certC) in
            let ccv = send_client_certificate_verify(ccl,skC) in
            let ccvl = ClientCertificateVerifyLogInfo(ccl,ccv) in
            let cfin = send_client_finished(ccvl,cfk) in
	    let c_cfl: bitstring = ClientFinishedLogInfo(ccvl, cfin) in
	    let resumption_secret = get_resumption_secret(masterSecret, c_cfl) in
	    let final_mess = (ClientCertificateVerifyOut(ccv),ClientFinishedOut(cfin)) in
	    let final_log = ClientFinishedAuthIn(log2, certC, ccv, cfin) in
	    (resumption_secret, final_mess, final_log)
	)
	else
	(
            let cfin = send_client_finished(c_sfl,cfk) in
	    let cfl = ClientFinishedLogInfo(c_sfl, cfin) in
	    let resumption_secret = get_resumption_secret(masterSecret, cfl) in
	    let final_mess = ClientFinishedOut(cfin) in
	    let final_log = ClientFinishedIn(cfin) in
	    (resumption_secret, final_mess, final_log)
	)
    in
    let honestsession =
	if certS = pkS then
	(
            (* The client is talking to the honest server. *)
	    if defined(corruptedServer) then
            (
               (* The server long-term secret key is corrupted,
	          so verifying the certificate does not give security.
	          We have security only if the messages really come from the server. *)
	       find j2 <= N6 suchthat defined(s_sfl[j2]) && c_sfl = s_sfl[j2] then
	          (* The server has the same cs_ats_exp in session j2.  *)
	          true
               else
                  false
            )
	    else true
        )
        else false
    in
    event ClientTerm1((cr,cgx,sr,cgy,log0,certS,log1,scv,m),final_log,(client_hk,server_hk,client_hiv,server_hiv,cfk,sfk,cats, c_sats, ems,resumption_secret));
    if honestsession then
    (
        (* The client is talking to the honest server, which is not corrupted
           or corrupted but really sent the messages.
           Check that cats, ems, and resumption_secret are secret.
	   The secrecy of sats is proved at the server.

	   We perform 3 compositions here:
	   - composition with the record protocol with secret s_sats (ClientTerm is event e2)
	   - composition with the record protocol with secret c_cats (ClientAccept is event e1)
	   - composition with the handshake with pre-shared key with secret c_resumption_secret (ClientAccept is event e1)
	    *)
        event ClientTerm((cr,cgx,sr,cgy,log0,log1,scv,m),(client_hk,server_hk,client_hiv,server_hiv,cfk,sfk,cats, c_sats, ems));
	event ClientAccept((cr,cgx,sr,cgy,log0,certS,log1,scv,m,final_log),(client_hk,server_hk,client_hiv,server_hiv,cfk,sfk,cats, c_sats, ems,resumption_secret),i1);
	let c_cats: key = cats in
	let c_ems: key = ems in
	let c_resumption_secret: key = resumption_secret in
	out(io6, (final_mess, Ok))
    )
    else
	(* Output the final message, and the secrets.
	   We do not prove security for these sessions

	   We compose with a process that publishes the final message and runs 
	   - the record protocol sender-side for cats,
	   - the record protocol receiver side for c_sats and 
	   - the client-side handshake with pre-shared key for resumption_secret,
	   with no security claim, by eliminating private communications *)
	out(io6, (final_mess, Leak(resumption_secret, cats, c_sats, ems))).

let ServerAfter05RTT1(pkC: certificate, masterSecret: extracted, sfl: bitstring, cr: nonce, sgx: elt, sr: nonce, sgy: elt, log0: bitstring, certS: certificate, log1: bitstring, scv: bitstring, m: bitstring, client_hk: key, server_hk: key, client_hiv: key, server_hiv: key, cfk: key, sfk: key, cats: key, sats: key, ems: key) =
   in(io26,clientfinished: bitstring);
   let (check_client_finished_succ(resumption_secret: key), honestsession: bool) = 
       let ClientFinishedAuthIn(log2, certC, ccv, cfin) = clientfinished in
       (
       	   let res = check_client_finished_client_auth(masterSecret,sfl,log2,certC,ccv,cfin,cfk) in
           let ccl = ClientCertificateLogInfo(sfl,log2,certC) in
           let ccvl = ClientCertificateVerifyLogInfo(ccl,ccv) in
           let s_cfl = ClientFinishedLogInfo(ccvl, cfin) in
           let honestsession =
               if certC = pkC then
               (
	           if defined(corruptedClient) then
       	               find j <= N1 suchthat defined(c_cfl[j]) && s_cfl = c_cfl[j] then
	      	           (* The client has the same key in session j. *)
		    	   true
                       else
		           false
                   else true
               )
               else false
            in
           (res, honestsession)
       )
       else let ClientFinishedIn(cfin) = clientfinished in
       (
	   let res = check_client_finished_no_auth(masterSecret,sfl,cfin,cfk) in
	   let honestsession =
	       find j <= N1 suchthat defined(cgx[j]) && sgx = cgx[j] then
	           true
	       else
	           false
           in
           (res, honestsession)
       )
       else
           (check_client_finished_fail, false)
   in
   let s_resumption_secret: key = resumption_secret in
   let s_cats : key = cats in
   let s_ems : key = ems in
   event ServerTerm1((cr,sgx,sr,sgy,log0,certS,log1,scv,m),clientfinished,(client_hk,server_hk,client_hiv,server_hiv,cfk,sfk,s_cats, sats, s_ems,s_resumption_secret));
   if honestsession then
	  (* By the correspondence ServerTerm ==> ClientAccept, the client has the same key.
	     The secrecy is proved at the client. 

	     We perform 2 compositions here:
	       - composition with the record protocol with secret c_cats (ServerTerm is event e2)
	       - composition with the handshake with pre-shared key with secret c_resumption_secret (ServerTerm is event e2) *)
	  event ServerTerm((cr,sgx,sr,sgy,log0,certS,log1,scv,m,clientfinished),(client_hk,server_hk,client_hiv,server_hiv,cfk,sfk,s_cats, sats, s_ems,s_resumption_secret));
	  out(io27, Ok)
   else
	  (* Output the secrets.
	     We do not prove security for these sessions

	     We compose with a process that runs 
	       - the record protocol receiver-side for s_cats,
	       - the record protocol sender side for sats and 
	       - the server-side handshake with pre-shared key for s_resumption_secret,
	     with no security claim, by eliminating private communications *)
	  out(io27, Leak(s_resumption_secret, s_cats, sats, s_ems)).

let ServerAfter05RTT2(pkC: certificate, masterSecret: extracted, sfl: bitstring, cr: nonce, sgx: elt, sr: nonce, sgy: elt, log0: bitstring, certS: certificate, log1: bitstring, scv: bitstring, m: bitstring, client_hk: key, server_hk: key, client_hiv: key, server_hiv: key, cfk: key, sfk: key, cats: key, sats: key, ems: key) =
   in(io26',clientfinished: bitstring);
   let (check_client_finished_succ(resumption_secret: key), honestsession: bool) = 
       let ClientFinishedAuthIn(log2, certC, ccv, cfin) = clientfinished in
       (
       	   let res = check_client_finished_client_auth(masterSecret,sfl,log2,certC,ccv,cfin,cfk) in
           let ccl = ClientCertificateLogInfo(sfl,log2,certC) in
           let ccvl = ClientCertificateVerifyLogInfo(ccl,ccv) in
           let s_cfl = ClientFinishedLogInfo(ccvl, cfin) in
           let honestsession =
               if certC = pkC then
               (
	           if defined(corruptedClient) then
       	               find j <= N1 suchthat defined(c_cfl[j]) && s_cfl = c_cfl[j] then
	      	           (* The client has the same key in session j. *)
		    	   true
                       else
		           false
                   else true
               )
               else false
            in
           (res, honestsession)
       )
       else let ClientFinishedIn(cfin) = clientfinished in
       (
	   let res = check_client_finished_no_auth(masterSecret,sfl,cfin,cfk) in
	   let honestsession =
	       find j <= N1 suchthat defined(cgx[j]) && sgx = cgx[j] then
	           true
	       else
	           false
           in
           (res, honestsession)
       )	   
       else
           (check_client_finished_fail, false)
   in
   let s_resumption_secret: key = resumption_secret in
   let s_cats: key = cats in
   let s_ems: key = ems in
   event ServerTerm1((cr,sgx,sr,sgy,log0,certS,log1,scv,m),clientfinished,(client_hk,server_hk,client_hiv,server_hiv,cfk,sfk,s_cats, sats, s_ems,s_resumption_secret));
   if honestsession then
	  (* By the correspondence ServerTerm ==> ClientAccept, the client has the same key.
	     The secrecy is proved at the client. 

	     We perform 2 compositions here:
	       - composition with the record protocol with secret c_cats (ServerTerm is event e2)
	       - composition with the handshake with pre-shared key with secret c_resumption_secret (ServerTerm is event e2) *)
	  event ServerTerm((cr,sgx,sr,sgy,log0,certS,log1,scv,m,clientfinished),(client_hk,server_hk,client_hiv,server_hiv,cfk,sfk,s_cats, sats, s_ems,s_resumption_secret));
	  out(io27', Ok)
   else
	  (* Output the secrets.
	     We do not prove security for these sessions

	     We compose with a process that runs 
	       - the record protocol receiver-side for s_cats,
	       - the record protocol sender side for sats and 
	       - the server-side handshake with pre-shared key for s_resumption_secret,
	     with no security claim, by eliminating private communications *)
	  out(io27', Leak(s_resumption_secret, s_cats, sats, s_ems)).

let Server(ES: extracted, skS: skey, pkS: certificate, pkC: certificate) = 
 !i6 <= N6
   in(io20,ClientHello(cr,sgx));
   let cr = fnonce(sgx) in
   let recv_client_hello_succ(sr:nonce,sgy:elt,handshakeSecret:extracted) = recv_client_hello(ES,cr,sgx) in
   out(io21,ServerHelloOut(sr,sgy));
   in(io22,log0:bitstring);
   let sil = ServerHelloLogInfo(cr,sgx,sr,sgy,log0) in
   let keys_succ(masterSecret: extracted, client_hk:key, server_hk: key, client_hiv: key, server_hiv: key, cfk: key, sfk: key) = onertt_hs_keys(sil,handshakeSecret) in
   out(io23,(client_hk, server_hk, client_hiv, server_hiv));
   in(io24,log1:bitstring);
   (* The server is the honest server -- but the client can talk to other servers *)
   let certS = pkS in
   let scl = ServerCertificateLogInfo(sil,certS,log1) in
   let scv = send_server_certificate_verify(scl,skS) in
   let scvl = ServerCertificateVerifyLogInfo(scl,scv) in
   let m = send_server_finished(scvl,sfk) in
   let s_sfl: bitstring = ServerFinishedLogInfo(scvl,m) in
   let tuple3keys(cats, sats, ems) = onertt_data_keys(masterSecret,s_sfl) in
   (* 0.5 RTT. 
      We have security when the Diffie-Hellman share comes from the client *)
   find j <= N1 suchthat defined(cgx[j]) && sgx = cgx[j] then
   (
      (* We compose with the record protocol with secret s_sats (ServerAccept is event e1) *)
      event ServerAccept((cr,sgx,sr,sgy,log0,log1,scv,m),(client_hk,server_hk,client_hiv,server_hiv,cfk,sfk,cats, sats, ems), i6);
      let s_sats: key = sats in
      out(io25,(ServerCertificateVerifyOut(scv), ServerFinishedOut(m), Ok));
      ServerAfter05RTT1(pkC, masterSecret, s_sfl, cr,sgx,sr,sgy,log0,certS,log1,scv,m, client_hk,server_hk,client_hiv,server_hiv,cfk,sfk,cats,sats,ems)
   )
   else
      (* We leak sats.
      	 We compose with a process that outputs the ServerCertificateVerify and	
	 ServerFinished messages, and runs the record protocol sender-side with sats,
	 with no security claim, by eliminating private communications *)
      out(io25, (ServerCertificateVerifyOut(scv), ServerFinishedOut(m), ServLeak(sats)));
      ServerAfter05RTT2(pkC, masterSecret, s_sfl, cr,sgx,sr,sgy,log0,certS,log1,scv,m, client_hk,server_hk,client_hiv,server_hiv,cfk,sfk,cats,sats,ems).


(* Corruption for forward secrecy *)

let corruptS(skS: skey) = 
  in(cCorruptS1, ()); 
  let corruptedServer:bool = true in
  out(cCorruptS2, skS).

let corruptC(skC: skey) = 
  in(cCorruptC1, ()); 
  let corruptedClient:bool = true in
  out(cCorruptC2, skC).

process 
  in(io33,());
  new kseedS:keyseed;
  let skS = skgen(kseedS) in
  let pkS = pkcert(kseedS) in
  new kseedC:keyseed;
  let skC = skgen(kseedC) in
  let pkC = pkcert(kseedC) in
  let ES = HKDF_extract_zero_zero in
  out(io34,(pkS, pkC));
  (Client(ES, skC, pkC, pkS)  | Server(ES, skS, pkS, pkC) | corruptS(skS) | corruptC(skC))


(* EXPECTED FILENAME: examples/tls13/cTLS/tls13-core-InitialHandshake-norandom.m4.cv TAG: 1
Warning: identifier sr rebound
Warning: identifier cr rebound
All queries proved.
229.449s (user 228.981s + system 0.468s), max rss 578440K
END *)

(* Definition of a hybrid Diffie-Hellman - KEM key exchange for PQ-TLS *)

(* A key exchange module defines

- types kex_public_client, kex_secret_client, kex_public_server,
  kex_shared_secret
- functions
fun kex_server_succ(kex_shared_secret, kex_public_server): bitstring [data].
fun kex_client_succ(kex_shared_secret): bitstring [data].
const kex_error: bitstring.

equation forall kex_ss: kex_shared_secret, kex_ps: kex_public_server;
   kex_server_succ(kex_ss, kex_ps) <> kex_error.
equation forall kex_ss: kex_shared_secret;
   kex_client_succ(kex_ss) <> kex_error.

fun kex_client_gen(): (kex_public_client, kex_secret_client)
fun kex_server(kex_public_client):
       kex_server_succ(kex_shared_secret,kex_public_server)/kex_error
fun kex_client_shared(kex_secret_client, kex_public_server):
       kex_client_succ(kex_shared_secret)/kex_error

- macros
  kex_security defines a security assumption on Derive_handshake_secret_DHE.
  kex_match_session defines the condition under which the current
  session of the server matches the session $1 of the client

In this hybrid key exchange, the security assumption and the session matching
depend on whether we rely on the Diffie-Hellman exchange (PRF_ODH is defined)
or on the KEM (PRF_OKEM is defined).

*)

(* KEM definitions *)
type kempkey [bounded].
type kemskey [bounded].
type ciphertext [bounded].
type kem_seed [large,fixed].

type kemsec [fixed].

type encaps_return [bounded].

proba Pkemcca.
proba Pkemcoll1.
proba Pkemcoll2.

ifdef(`PRF_OKEM',`
expand IND_CCA2_KEM(kem_seed,kempkey,kemskey,kemsec,ciphertext,encaps_return,kempkgen,kemskgen,encaps,kempair,decap,injbot,Pkemcca,Pkemcoll1,Pkemcoll2).
',`
expand KEM(kem_seed,kempkey,kemskey,kemsec,ciphertext,encaps_return,kempkgen,kemskgen,encaps,kempair,decap,injbot,Pkemcoll1,Pkemcoll2).
')

(* DH definitions *)
type Z [large, bounded, nonuniform].
type Znw [large, bounded, nonuniform].
type elt [large, bounded, nonuniform].

(* The probability that exp(g, x) = Y for random x and Y independent of x 
   is at most PCollKey1, and
   the probability that exp(g, mult(x,y)) = Y where x and y are independent 
   random private keys and Y is independent of x or y is at most PCollKey2.
   We exclude weak secret keys for Curve448. (PWeakKey = 2^-445 for Curve448,
   0 for Curve25519 and prime order groups.) *)

expand DH_basic(elt, Znw, G, expnw, expnwp, mult).

proba PWeakKey.
expand DH_exclude_weak_keys(elt, Z, Znw, ZnwtoZ, exp, expnw, PWeakKey).

proba PCollKey1.
proba PCollKey2.
expand DH_proba_collision(elt, Znw, G, expnw, expnwp, mult, PCollKey1, PCollKey2).

type kex_public_client [bounded].
type kex_secret_client [bounded].
type kex_public_server [bounded].
type kex_shared_secret [bounded].

fun concat_public_client(elt,kempkey):kex_public_client [data].
fun concat_secret_client(Z,kemskey):kex_secret_client [data].
fun concat_public_server(elt,ciphertext):kex_public_server [data].
fun concat_shared_secret(elt,kemsec):kex_shared_secret [data].

fun kex_server_succ(kex_shared_secret, kex_public_server): bitstring [data].
fun kex_client_succ(kex_shared_secret): bitstring [data].
const kex_error: bitstring.

equation forall kex_ss: kex_shared_secret, kex_ps: kex_public_server;
   kex_server_succ(kex_ss, kex_ps) <> kex_error.
equation forall kex_ss: kex_shared_secret;
   kex_client_succ(kex_ss) <> kex_error.


letfun kex_client_gen() =
   x <-R Z;
   cgx: elt <- exp(G,x);
   seed <-R kem_seed;
   cpk: kempkey <- kempkgen(seed);
   sk <- kemskgen(seed);
   (concat_public_client(cgx,cpk), concat_secret_client(x,sk)).

letfun kex_server(kex_pc: kex_public_client) =
   let concat_public_client(sgx: elt, spk: kempkey) = kex_pc in
   (
      y <-R Z;
      gy <- exp(G,y);
      s1 <- exp(sgx,y);
      let kempair(s2,sencap) = encaps(spk) in
      kex_server_succ(concat_shared_secret(s1,s2), concat_public_server(gy,sencap))
   )
   else
      kex_error. (* does not happen in correct runs *)

letfun kex_client_shared(kex_sc: kex_secret_client, kex_ps: kex_public_server) =
   let concat_secret_client(x,sk) = kex_sc in
   (
      let concat_public_server(gy,sencap) = kex_ps in
      (
         s1 <- exp(gy,x);
    	 let injbot(s2) = decap(sencap,sk) in
	    kex_client_succ(concat_shared_secret(s1,s2))
	 else
	    kex_error
      )
      else
         kex_error (* does not happen in correct runs *)
   )
   else
      kex_error. (* never happens *)


(* Security assumption on Derive_handshake_secret_DHE *)

define(kex_security, `

fun Derived_ES(extracted): key.

(* We assume that
   tPRF(derived_es, e, k) = HKDF_extract(derived_es, concat_shared_secret(e,k))
   is a PRF when the key is the 1st or the 3rd argument and
   satisfies the PRF-ODH assumption when the key is the 2nd argument.
   We have Derive_handshake_secret_DHE(es, concat_shared_secret(e,k)) =
   tPRF(e, Derived_ES(es), k).
   We reorganize the arguments to be able to use standard macros,
   by putting the key in first position:
   dh_prf(e,concat1(derived_es, k)) = tPRF(derived_es, e, k)
   prf(k,concat2(derived_es,e)) = tPRF(derived_es, e, k)
   and the case in which the key is derived_es is used in KeySchedule1.ocv. *)

ifdef(`PRF_ODH',`
(* The PRF-ODH assumption *)

proba pPRF_ODH.
type key_kemsec [bounded].

expand snPRF_ODH(elt, Znw, key_kemsec, extracted, G, expnw, expnwp, mult, dh_prf, pPRF_ODH).

fun concat1(key, kemsec): key_kemsec [data].

(* Rewrite Derive_handshake_secret_DHE(es, concat_shared_secret(e,k))
   into dh_prf(e,concat1(Derived_ES(es),k))
   triggered manually by use rew_hs_dh_prf *)
equation(rew_hs_dh_prf)
  forall es: extracted, e: elt, k: kemsec;
    Derive_handshake_secret_DHE(es, concat_shared_secret(e,k)) =
    dh_prf(e,concat1(Derived_ES(es),k)) [manual].

')


ifdef(`PRF_OKEM',`

proba Pprf.
type key_elt [bounded].

expand PRF(kemsec, key_elt, extracted, prf, Pprf).

fun concat2(key, elt): key_elt [data].

(* Rewrite Derive_handshake_secret_DHE(es, concat_shared_secret(e,k))
   into prf(k,concat2(Derived_ES(es),e))
   triggered manually by use rew_hs_prf *)
equation(rew_hs_prf)
  forall es: extracted, e: elt, k: kemsec;
    Derive_handshake_secret_DHE(es, concat_shared_secret(e,k)) =
    prf(k,concat2(Derived_ES(es),e)) [manual].

')

')

(* Condition for matching sessions *)

ifdef(`PRF_ODH',`
define(kex_match_session, `defined(sgx,cgx[$1]) && sgx = cgx[$1]')
')

ifdef(`PRF_OKEM',`
define(kex_match_session, `defined(spk,cpk[$1]) && spk = cpk[$1]')
')

# TLS 1.3: Computational Verification with CryptoVerif

The section numbers refer to the long version of the paper at IEEE S&P'17
on the TLS 1.3 draft-18 case study available at
https://inria.hal.science/hal-01528752
The files have been modified to improve and extend them since that paper
was written, so they may not match the paper exactly.

## Lemmas on the key schedule (Section 6.3)

* KeySchedule.m4.ocv proves properties of the key schedule.
  It generates KeySchedule<i>-<version>.ocv where
  <i> can be 1, 2, 3 for the three parts of the key schedule and
  <version> can be draft18 or RFC, depending on which version of TLS 1.3
  we consider.
* HKDFexpand.ocv

## The protocol

* tls13-core-common.ocvl: common assumptions on the key schedule and the MAC,
  included by other tls13-core-* files.

### Initial handshake (Section 6.4)

* tls13-core-InitialHandshake.m4.cv
* tls13-core-InitialHandshake-1RTT-only.m4.cv

The first file deals with 0.5-RTT and 1-RTT messages. The second one supports only 1-RTT (but proves stronger properties from server to client messages).

### Handshake with pre-shared key (Section 6.5)

* tls13-core-PSKandPSKDHE-NoCorruption.m4.cv
* tls13-core-PSKDHE-CorruptPSK.m4.cv proves forward secrecy of PSK-DHE
  with respect to the compromise of the pre-shared key. This file
  is an addition presented at CSF'24.

### Record Protocol (Section 6.6)

* tls13-core-RecordProtocol.cv
* tls13-core-RecordProtocol-0RTT.cv
* tls13-core-RecordProtocol-0RTT-badkey.cv

The first file is the normal record protocol. The last two are variants for 0-RTT messages: one with a replicated receiver, and one with no sender.
	
### Other files

The subdirectory pq-tls contains models that rely on the PRFODH assumption
instead of using the random oracle model and the gap Diffie-Hellman assumption
both for TLS 1.3 and for a post-quantum version of TLS, described
in https://datatracker.ietf.org/doc/draft-ietf-tls-hybrid-design/

The subdirectory cTLS (for compressed TLS) contains a model of the
initial handshake that does not use random numbers, in order reduce
the size of the messages. The security is still proved because the
Diffie-Hellman public keys bring randomness.


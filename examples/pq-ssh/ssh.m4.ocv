(*
We  model in this file the hybrid SSH proposal from
https://datatracker.ietf.org/doc/draft-kampanakis-curdle-ssh-pq-ke/
draft version 1.
*)

ifdef(`PRF_ODH',`

proof {
      crypto uf_cma_corrupt(sign);
      crypto exclude_weak_keys(Z) *;
      out_game "g1-odh.ocv";
      insert before "x_3 <-R" "find v <= NC suchthat defined(eC[v]) && eC[v] = eS  then";
      out_game "g2-odh.ocv";      
      SArename fS;
      out_game "g3-odh.ocv";            
      insert before_nth 2 "let injbotkem(ssC: kemsec) =" "find w <= NS suchthat defined( eS[w], fS_1[w]) &&  eC = eS[w] && fC = fS_1[w] then";      
      simplify;      
      out_game "g4-odh.ocv";
      SArename x_3;
      out_game "g5-odh.ocv";
  
      crypto prf_odh(dPRF) [variables: x_2 -> a, x_4 -> b.];
      (* Rewrite the key computation to use PRFc *)
      out_game "g6-odh.ocv";
      insert before_nth 1 "HS: hash <-" "HS0 <- h(sidS, ca2_1); tuple_keysS <- tuple(HS0, ffinal(ca2_1, HS0)); ";
      replace at_nth 1 1  "HS: hash <- {[0-9]+}" "1-proj-tuple(tuple_keysS)";
      replace at_nth 1 1 "six_hash <- {[0-9]+}" "2-proj-tuple(tuple_keysS)";

      insert before_nth 1 "HC: hash <-" "HC0 <- h(sidC, ca2_1[jS_1]); tuple_keysC0 <- tuple(HC0, ffinal(ca2_1[jS_1], HC0)); ";
      replace at_nth 1 1 "HC: hash <- {[0-9]+}" "1-proj-tuple(tuple_keysC0)";
      replace at_nth 3 1 "six_hash <- {[0-9]+}" "2-proj-tuple(tuple_keysC0)";
      
      insert before_nth 2 "HC: hash <-" "HC1 <- h(sidC, ca2_1[u_10]); tuple_keysC1 <- tuple(HC1, ffinal(ca2_1[u_10], HC1)); ";
      replace at_nth 2 1 "HC: hash <- {[0-9]+}" "1-proj-tuple(tuple_keysC1)";
      replace at_nth 4 1 "six_hash <- {[0-9]+}" "2-proj-tuple(tuple_keysC1)";

      insert before_nth 5 "HC: hash <-" "HC2 <- h(sidC, ca2_1[w]); tuple_keysC2 <- tuple(HC2, ffinal(ca2_1[w], HC2)); ";
      replace at_nth 5 1 "HC: hash <- {[0-9]+}" "1-proj-tuple(tuple_keysC2)";
      replace at_nth 7 1 "six_hash <- {[0-9]+}" "2-proj-tuple(tuple_keysC2)";
      crypto rewrite_PRFc;

      crypto prf(PRFc) *;
      auto
}

')

ifdef(`PRF_OKEM',`
  
proof {
      crypto uf_cma_corrupt(sign);
      crypto exclude_weak_keys(Z) *;
      out_game "g1-kem.ocv";
      insert before "x_3 <-R" "find v <= NC suchthat defined(kempkC[v]) && kempkC[v] = kempkS  then";
      out_game "g1-kem.ocv";
      SArename ctS;
      out_game "g1-kem.ocv";            
      insert before_nth 2 "let injbotkem(ssC: kemsec) = " "find w <= NS suchthat defined( kempkS[w], ctS_1[w]) &&  ctC = ctS_1[w] && kempkS[w] = kempkC then";      
      simplify;
      out_game "g1-kem.ocv";

      replace at_nth 1 1 "ssC: kemsec <- {[0-9]+}" "ssS[jS_1]";  
      replace at_nth 2 1 "ssC: kemsec <- {[0-9]+}" "ssS[w]";  

      SArename kseed;
      crypto ind_cca2(encaps);
      crypto prf(dPRF) *;

      out_game "g2-kem.ocv";
      (* Rewrite the key computation to use PRFc *)
      insert before_nth 1 "HS: hash <-" "HS0 <- h(sidS, r_42); tuple_keysS <- tuple(HS0, ffinal(r_42, HS0)); ";
      replace at_nth 1 1  "HS: hash <- {[0-9]+}" "1-proj-tuple(tuple_keysS)";
      replace at_nth 1 1 "six_hash <- {[0-9]+}" "2-proj-tuple(tuple_keysS)";

      insert before_nth 1 "HC: hash <-" "HC0 <- h(sidC, r_42[jS_1]); tuple_keysC0 <- tuple(HC0, ffinal(r_42[jS_1], HC0)); ";
      replace at_nth 1 1 "HC: hash <- {[0-9]+}" "1-proj-tuple(tuple_keysC0)";
      replace at_nth 3 1 "six_hash <- {[0-9]+}" "2-proj-tuple(tuple_keysC0)";
      
      insert before_nth 2 "HC: hash <-" "HC1 <- h(sidC, r_42[u_6]); tuple_keysC1 <- tuple(HC1, ffinal(r_42[u_6], HC1)); ";
      replace at_nth 2 1 "HC: hash <- {[0-9]+}" "1-proj-tuple(tuple_keysC1)";
      replace at_nth 4 1 "six_hash <- {[0-9]+}" "2-proj-tuple(tuple_keysC1)";

      insert before_nth 5 "HC: hash <-" "HC2 <- h(sidC, r_42[w]); tuple_keysC2 <- tuple(HC2, ffinal(r_42[w], HC2)); ";
      replace at_nth 5 1 "HC: hash <- {[0-9]+}" "1-proj-tuple(tuple_keysC2)";
      replace at_nth 7 1 "six_hash <- {[0-9]+}" "2-proj-tuple(tuple_keysC2)";

      crypto rewrite_PRFc;

      crypto prf(PRFc) *;      
      auto
}

')

param NC,NS.

type pad_rand [fixed]. (*size 255 bytes or less depending on how the padding is done*)
type cookie [fixed, large]. (* 16 bytes *)
type tag [fixed]. (* 1 byte *)
type uint32 [bounded]. (* 4 bytes *)
type D [fixed].
type hashkey [bounded].
type hash [fixed, large].
type six_hash [fixed, large]. (* Concatenation of six hashes *)
type signature [bounded].
type block [fixed].
type skeyseed [large, fixed].
type sskey [bounded, large].
type spkey [bounded, large].
type mkey [bounded, large].
type iv [fixed, large].
type symkey [fixed, large].
type extracted [large, fixed].

fun skp(spkey,sskey): bitstring [data].

type Z [large, bounded, nonuniform]. (*mpint = ssh string (size+string payload). MSB. 8 bits per byte. where 0=empty string, >0=first bit=0, <0=first bit=1,at beginning no useless 00 or FF. *)
type G [large, bounded, nonuniform]. (*mpint*)

fun bitstring_of_G(G): bitstring [data].

fun padr(bitstring,pad_rand): bitstring.
letfun pad(m:bitstring) = r <-R pad_rand; padr(m, r).
fun unpad(bitstring): bitstringbot.

fun injbot(bitstring): bitstringbot [data].
equation forall x: bitstring; injbot (x) <> bottom.
equation forall x: bitstring, r: pad_rand;
       unpad(padr(x, r)) = injbot(x).

(* KEM definitions *)

type kempkey [bounded].
type kemskey [bounded].
type ciphertext.
type kem_seed [large,fixed].
type kemsec [fixed].
type encaps_return.

proba Pkemcca.
proba Pkemkeycoll.
proba Pkemctxtcoll.

ifdef(`PRF_OKEM',`
expand IND_CCA2_KEM(kem_seed,kempkey,kemskey,kemsec,ciphertext,encaps_return,kempkgen,kemskgen,encaps,kempair,decap,injbotkem,Pkemcca,Pkemkeycoll,Pkemctxtcoll).',
`expand KEM(kem_seed,kempkey,kemskey,kemsec,ciphertext,encaps_return,kempkgen,kemskgen,encaps,kempair,decap,injbotkem,Pkemkeycoll,Pkemctxtcoll).')

(* DH basics *)

type Znw [large, bounded, nonuniform].
expand DH_basic(G, Znw, g, expnw, expnwp, mult).

proba PWeakKey.
expand DH_exclude_weak_keys(G, Z, Znw, ZnwtoZ, exp, expnw, PWeakKey).

proba PCollKey1.
proba PCollKey2.
expand DH_proba_collision(G, Znw, g, expnw, expnwp, mult, PCollKey1, PCollKey2).

(* utils *)

fun get_size(bitstring): uint32.

fun concatm(tag, bitstring): bitstring [data].
fun concatm2(tag, bitstring, kempkey): bitstring [data].
fun concatKEXINIT(cookie, bitstring): bitstring [data].
fun concat9(bitstring, bitstring, bitstring, bitstring, spkey, G, kempkey, G, ciphertext): bitstring [data].
fun concatKEXHYBRIDREPLY(spkey, G, ciphertext, signature): bitstring [data].
fun check_algorithms(bitstring): bool.
fun block_of_hash(hash): block [data].

const negotiation_string: bitstring.
const tag_kexhybrid_init: tag.
const tag_kexhybrid_reply: tag.
const tag_kex_init: tag.
const tag_newkeys: tag.
const uint32_zero: uint32.
const null_string: bitstring. (* the string of length 0 *)

(* hash function *)

(* we are forced to do a collision resistance assumption on the hash function, see 
https://d1kjwivbowugqa.cloudfront.net/files/research/papers/CCS-BDKSS14-full.pdf
*)

proba colH.
expand FixedCollisionResistant_hash_2(bitstring, extracted, hash, h, colH).

type concat_type [fixed]. (* concatenation of 7 hashes *)

proba Pprf2.
expand PRF(extracted, bitstring, concat_type, PRFc, Pprf2).

expand random_split_2(concat_type, hash, six_hash, tuple).

(*
let ffinal(K,H) = 
   h(K || H || 'A' || H) ||
   h(K || H || 'B' || H) ||
   h(K || H || 'C' || H) ||
   h(K || H || 'D' || H) ||
   h(K || H || 'E' || H) ||
   h(K || H || 'F' || H) 

We have
PRFc(K,x) =
   let H = h(x, K) in
   H || ffinal(K,H)

tuplefinal(IVC, IVS, EKC, EKS, MKC, MKS, rest) = ffinal(K,H)
*)

fun ffinal(extracted, hash): six_hash.

type six_hash_rest [fixed].
expand random_split_7(six_hash, iv, iv, symkey, symkey, mkey, mkey, six_hash_rest, tuplefinal).

equiv(rewrite_PRFc)
    O(K: extracted, x: bitstring) := let H = h(x, K) in
      return(tuple(H, ffinal(K, H))) [all]
<=(0)=> [manual]
    O(K: extracted, x: bitstring) :=
      return(PRFc(K,x)).
      
(* signature *)

proba Psign.
proba Psigncoll.

expand UF_CMA_proba_signature(skeyseed, spkey, sskey, block, signature, sskgen, spkgen, sign, check, Psign, Psigncoll).

letfun kgen_s () = r <-R skeyseed; skp(spkgen(r), sskgen(r)).

(* key table *)

table trusted_hosts(spkey).

(* Secret *)

query secret IVCS public_vars IVSS, EKCS, EKSS, MKCS, MKSS.
query secret IVSS public_vars IVCS, EKCS, EKSS, MKCS, MKSS.
query secret EKCS public_vars IVCS, IVSS, EKSS, MKCS, MKSS.
query secret EKSS public_vars IVCS, IVSS, EKCS, MKCS, MKSS.
query secret MKCS public_vars IVCS, IVSS, EKCS, EKSS, MKSS.
query secret MKSS public_vars IVCS, IVSS, EKCS, EKSS, MKCS.

(* Authentication property *)

event endC(bitstring, bitstring).
event endS(bitstring, bitstring, NS).

query forall sid: bitstring, keys: bitstring;
      inj-event(endC(sid, keys)) ==> exists i <= NS; inj-event(endS(sid, keys, i))
      public_vars IVCS, IVSS, EKCS, EKSS, MKCS, MKSS.

query forall sid: bitstring, keys, keys2: bitstring, i<=NS, i2<=NS;
      event(endS(sid, keys, i)) && event(endS(sid, keys2, i2)) ==>  i=i2
      public_vars IVCS, IVSS, EKCS, EKSS, MKCS, MKSS.

ifdef(`PRF_ODH',`

proba pPRF_ODH.
expand snPRF_ODH(G, Znw, kemsec, extracted, g, expnw, expnwp, mult, dPRF, pPRF_ODH).

')

ifdef(`PRF_OKEM',`

fun dPRF(G, kemsec): extracted.
proba Pprf.

equiv(prf(dPRF)) special prf("key_last", dPRF, Pprf, (k, r, x, y, z, u)).

equiv(prf_partial(dPRF)) special prf_partial("key_last", dPRF, Pprf, (k, r, x, y, z, u)) [manual].

')

const Ok: bitstring.
fun Leak(iv, iv, symkey, symkey, mkey, mkey): bitstring [data].

equation forall ivc, ivs: iv, ekc, eks: symkey, mkc, mks: mkey;
  Leak(ivc, ivs, ekc, eks, mkc, mks) <> Ok.

(* Server *)

let processS(pkS: spkey, skS: sskey) =
   foreach iS <= NS do 
      negotiationS() :=
	  (* Returning KEX_INIT to client *)
          cookieSS <-R cookie;
          initSS <- concatm(tag_kex_init, concatKEXINIT(cookieSS, negotiation_string));
          return (pad(initSS));

          key_exchange2S(idCS: bitstring, idSS: bitstring, m1: bitstring, m2: bitstring) :=
	  (* Expecting KEX_INIT and KEX_HYBRID_INIT from client, returning KEX_HYBRID_REPLY *)
          let injbot(initCS) = unpad(m1) in
          let concatm(=tag_kex_init, concatKEXINIT(cookieCS, nsCS)) = initCS in
          if (check_algorithms(nsCS)) then
          let injbot(concatm2(=tag_kexhybrid_init, bitstring_of_G(eS: G), kempkS: kempkey)) = unpad(m2) in
	  (* DH part *)
          yS <-R Z;
          fS:G <- exp(g,yS);
	  (* KEM part *)
	  let kempair(ssS, ctS: ciphertext) = encaps(kempkS) in
	  (* Key and exchange hash *)
	  sDHS <- exp(eS, yS);
          KS: extracted <- dPRF(sDHS, ssS);
	  let sidS: bitstring = concat9(idCS, idSS, initCS, initSS, pkS, eS, kempkS, fS, ctS) in
          HS <- h(sidS, KS);
	  let tuplefinal(IVCS0, IVSS0, EKCS0, EKSS0, MKCS0, MKSS0, restS) = ffinal(KS,HS) in
	  let leakval =
	    find jC <= NC suchthat
ifdef(`PRF_ODH',`defined(eC[jC]) && eS=eC[jC]')
ifdef(`PRF_OKEM',`defined(kempkC[jC]) && kempkS=kempkC[jC]')
	    then
	      event endS(sidS, (IVCS0, IVSS0, EKCS0, EKSS0, MKCS0, MKSS0), iS);
	      IVCS: iv <- IVCS0;
 	      IVSS: iv <- IVSS0;
	      EKCS: symkey <- EKCS0;
	      EKSS: symkey <- EKSS0;
	      MKCS: mkey <- MKCS0;
	      MKSS: mkey <- MKSS0;
	      Ok
	    else
	      Leak(IVCS0, IVSS0, EKCS0, EKSS0, MKCS0, MKSS0)
	  in
          sS <- sign(block_of_hash(HS), skS);
          return (HS, leakval, pad(concatm(tag_kexhybrid_reply, concatKEXHYBRIDREPLY(pkS, fS, ctS, sS))));
	     (* We leak HS because it is used in public-key authentication inside the tunnel
	     	We leak leakval because we need to leak the keys in case the server talks to a dishonest client *)

	 key_exchange4S(m: bitstring) :=
	   (* Expecting NEW_KEYS from client, returning NEW_KEYS *)
	   let injbot(nkCS) = unpad(m) in
	   let concatm(=tag_newkeys, =null_string) = nkCS in
	   return (pad(concatm(tag_newkeys, null_string))).

(* Client *)

let processC(pkSC:spkey) =
   foreach iC <= NC do
             negotiationC() :=
	     (* Returning KEX_INIT to the server *)
             cookieCC <-R cookie;
             initCC <- concatm(tag_kex_init, concatKEXINIT (cookieCC, negotiation_string));
             return (pad(initCC));
        
        key_exchange1C(m: bitstring) :=
             (* Expecting KEX_INIT from the server, returning KEX_HYBRID_INIT *)
             let injbot(initSC) = unpad(m) in
             let concatm(=tag_kex_init, concatKEXINIT(cookieSC, nsSC)) = initSC in
             if (check_algorithms(initSC)) then
             xC <-R Z;
             eC:G <- exp(g,xC);
             seed <-R kem_seed;
       	     kempkC:kempkey <- kempkgen(seed);
             return (pad(concatm2(tag_kexhybrid_init, bitstring_of_G(eC), kempkC)));

        key_exchange3C(idCC: bitstring, idSC: bitstring, m2: bitstring) :=
	     (* Expecting KEX_HYBRID_REPLY from the server, returning NEW_KEYS *)
             let injbot(concatm(=tag_kexhybrid_reply, concatKEXHYBRIDREPLY(=pkSC, fC, ctC, sC))) = unpad(m2) in
	     let sidC = concat9(idCC, idSC, initCC, initSC, pkSC, eC, kempkC, fC, ctC) in
	     honestSession :bool <-
	        if defined(corruptedServer) then
		  find jS <= NS suchthat defined(sidS[jS]) && sidC = sidS[jS]
                  then true
		  else false
		else true;
	     let injbotkem(ssC) = decap(ctC,kemskgen(seed)) in
	     sDHC <- exp(fC,xC);
	     KC: extracted  <- dPRF(sDHC, ssC );
             HC <- h(sidC,KC);
             if check(block_of_hash(HC),pkSC,sC) then
             let tuplefinal(IVCC, IVSC, EKCC, EKSC, MKCC, MKSC, restC) = ffinal(KC, HC) in
	     let leakval =
	       if honestSession then
                 event endC(sidC, (IVCC, IVSC, EKCC, EKSC, MKCC, MKSC));
		 Ok
	       else
	         Leak(IVCC, IVSC, EKCC, EKSC, MKCC, MKSC)
	     in
	     return (HC, leakval, pad(concatm(tag_newkeys, null_string)));
	        (* We leak HC because it is used in public-key authentication inside the tunnel
		   We leak leakval because we need to leak the keys in case the client talks to a dishonest server *)	     
	
	get_keysC(mf: bitstring) :=
	  (* Expecting NEW_KEYS from the server, then start the tunnel *)
  	  let injbot(nkSC) = unpad(mf) in
	  let concatm(=tag_newkeys, =null_string) = nkSC in
 	  return.

process
        (* Key generation *)
	Ostart() :=
        let skp(pkS0: spkey,skS0: sskey) = kgen_s() in
        insert trusted_hosts(pkS0);
        return (pkS0);
        (run processS(pkS0,skS0) | run processC(pkS0)
	| Corrupt() := corruptedServer : bool <- true; return(skS0))

ifdef(`PRF_ODH',`
(* EXPECTED FILENAME: examples/pq-ssh/ssh.m4.ocv TAG: 1
All queries proved.
12.149s (user 12.121s + system 0.028s), max rss 49012K
END *)
')

ifdef(`PRF_OKEM',`
(* EXPECTED FILENAME: examples/pq-ssh/ssh.m4.ocv TAG: 2
All queries proved.
10.437s (user 10.409s + system 0.028s), max rss 46564K
END *)
')

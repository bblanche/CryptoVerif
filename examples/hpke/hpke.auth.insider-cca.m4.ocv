(* Analysing the HPKE Standard - Supplementary Material
   Joël Alwen; Bruno Blanchet; Eduard Hauck; Eike Kiltz; Benjamin Lipp; 
   Doreen Riepel

This is supplementary material accompanying the paper:

Joël Alwen, Bruno Blanchet, Eduard Hauck, Eike Kiltz, Benjamin Lipp,
and Doreen Riepel. Analysing the HPKE Standard. In Anne Canteaut and
Francois-Xavier Standaert, editors, Eurocrypt 2021, Lecture Notes in
Computer Science, pages 87-116, Zagreb, Croatia, October 2021. Springer.
Long version: https://eprint.iacr.org/2020/1499 *)

proof {
  out_game "g00.out.cv";
  remove_assign binder the_sk;
  remove_assign binder the_pk;
  crypto insider_cca(AuthEncap) [variables: kseed->s_1];
  out_game "g01.out.cv";
  crypto prf(KeySchedule_mode) k'_1;
  out_game "g03.out.cv";
  crypto splitter(tuple_key_nonce) **;
  out_game "g04.out.cv";
  crypto int_ctxt(Seal_inner) part1;
  crypto ind_cpa(Seal_inner) **;
  out_game "g05.out.cv";
  success
}

(** Key Encapsulation Mechanism **)
type keypairseed_t [bounded,large].
type kemseed_t [fixed,large].
type skey_t [bounded,large].
type pkey_t [bounded,large].
type kemkey_t [fixed,large].
type kemciph_t [fixed,large].
proba P_pk_coll.
proba Adv_Insider_CCA.
expand Authenticated_KEM(keypairseed_t, pkey_t, skey_t, kemseed_t, AuthEncap_res_t, AuthDecap_res_t, kemkey_t, kemciph_t, skgen, pkgen, AuthEncap, AuthEncap_r, AuthEncap_tuple, AuthDecap, AuthDecap_Some, AuthDecap_None, P_pk_coll).
expand Insider_CCA_Secure_Authenticated_KEM(keypairseed_t, pkey_t, skey_t, kemseed_t, AuthEncap_res_t, AuthDecap_res_t, kemkey_t, kemciph_t, skgen, pkgen, AuthEncap, AuthEncap_r, AuthEncap_tuple, AuthDecap, AuthDecap_Some, AuthDecap_None, Adv_Insider_CCA).

include(`common.hpke.ocvl')

fun ChallEnc(kemciph_t, bitstring) : bitstringbot.

(* a set E used within the proof,
   containing 6-tuples of the following type: *)
table E(
  pkey_t,    (* sender's public key *)
  pkey_t,    (* receiver's public key *)
  kemciph_t, (* KEM ciphertext *)
  bitstring, (* AEAD ciphertext *)
  bitstring, (* AEAD additional authenticated data *)
  bitstring  (* application info string *)
).

param N.   (* number of honest keypairs/users *)
param Qeperuser. (* number of calls to the Oaenc() oracle per keypair *)
param Qdperuser. (* number of calls to the Oadec() oracle per keypair *)
param Qcperuser.  (* number of calls to the Ochall() oracle per keypair *)

(* This is the proof goal: prove that the adversary cannot guess
   the boolean b with probability better than 1/2.
   b is defined as a fresh random boolean inside the Ostart() oracle. *)
query secret b [cv_bit].

process
  Ostart() := b <-R bool; return();

  (* The adversary can generate up to N honest keypairs/users by calling
     the Osetup() oracle. The nested oracles Oaenc(), Oadec(), Ochall()
     will be available for each keypair. *)
  (foreach i <= N do
   Osetup() :=
     kseed <-R keypairseed_t;
     the_sk <- skgen(kseed);
     the_pk <- pkgen(kseed);
     (* The public key of each honest keypair is made available
        to the adversary. *)
     return(the_pk);

     (
       (* This defines the Oaenc() oracle with up to Qeperuser calls per keypair *)
       (foreach iae <= Qeperuser do
        Oaenc(pk: pkey_t, m: bitstring, aad: bitstring, info: bitstring) :=
          return(SealAuth(pk, info, aad, m, the_sk))
       )|

       (* This defines the Oadec() oracle with up to Qdperuser calls per keypair *)
       (foreach iad <= Qdperuser do
        Oadec(pk: pkey_t, enc: kemciph_t, c: bitstring, aad: bitstring, info: bitstring) :=
          get E(=pk, =the_pk, =enc, =c, =aad, =info) in (
            return(Open_None)
          ) else (
            return(OpenAuth(enc, the_sk, info, aad, c, pk))
          )
       )|

       (* This defines the Ochall() oracle with up to Qcperuser calls per keypair *)
       (foreach ich <= Qcperuser do
        Ochall(s': keypairseed_t, m0: bitstring, m1: bitstring, aad: bitstring, info: bitstring) :=
          (* only accept challenge queries for m0 and m1 of same length *)
          if Length(m0) = Length(m1) then (
            let Seal_Some(enc_star, c_star) = SealAuth(the_pk, info, aad, if_fun(b, m0, m1), skgen(s')) in
            insert E(pkgen(s'), the_pk, enc_star, c_star, aad, info);
            return(ChallEnc(enc_star, c_star))
          ) else return(bottom) (* ends the condition on m0 and m1 lengths *)
       ) (* ends the definition of the Ocall() oracle *)
     ) (* This ends the block of oracles that are defined for each keypair *)
  ) (* This ends the definition of the Osetup() oracle and its nested oracles *)

(* EXPECTED FILENAME: examples/hpke/hpke.auth.insider-cca.m4.ocv TAG: 1
Warning: due to the bijective function AuthEncap_tuple, the type AuthEncap_res_t must be bounded. You should declare it as such (or revise the other declarations if it is not bounded).
Warning: due to the bijective function Context, the type context_t must be bounded. You should declare it as such (or revise the other declarations if it is not bounded).
All queries proved.
0.244s (user 0.232s + system 0.012s), max rss 29456K
END *)

(* Analysing the HPKE Standard - Supplementary Material
   Joël Alwen; Bruno Blanchet; Eduard Hauck; Eike Kiltz; Benjamin Lipp; 
   Doreen Riepel

This is supplementary material accompanying the paper:

Joël Alwen, Bruno Blanchet, Eduard Hauck, Eike Kiltz, Benjamin Lipp,
and Doreen Riepel. Analysing the HPKE Standard. In Anne Canteaut and
Francois-Xavier Standaert, editors, Eurocrypt 2021, Lecture Notes in
Computer Science, pages 87-116, Zagreb, Croatia, October 2021. Springer.
Long version: https://eprint.iacr.org/2020/1499 *)

type eae_output_t [fixed,large].
type Ctx_t [bounded].
type G_t [bounded].
fun Serialize(G_t): bitstring [data].
type Z_t [bounded,nonuniform].
type Znw_t [bounded,nonuniform].
proba PCollKey.
proba PWeakKey.

ifdef(`subgroup', `

type subG_t [bounded].
expand DH_subgroup(G_t, Znw_t, g, expnw, mult, subG_t, g_k,
       		   exp_div_k, exp_div_k2, pow_k , subGtoG).

proba PCollKey2.
expand DH_proba_collision(subG_t, Znw_t, g_k, exp_div_k, exp_div_k2,
       			  mult, PCollKey, PCollKey2).

proba pPRF_ODH.
expand mnPRF_ODH(
  (* types *)
  subG_t,  (* Group elements *)
  Znw_t,  (* Exponents *)
  bitstring, (* PRF argument *)
  eae_output_t, (* PRF output *)
  (* variables *)
  g_k,    (* a generator of the group *)
  exp_div_k,  (* exponentiation function *)
  exp_div_k,  (* exponentiation function (same function after game transformation) *)
  mult, (* multiplication function for exponents *)
  prf_subG, (* prf *)
  (* probabilities *)
  pPRF_ODH, (* probability of breaking the PRF-ODH assumption *)
  PCollKey
).

fun prf(G_t, bitstring): eae_output_t.
equation forall x: subG_t, y: bitstring; prf(subGtoG(x), y) = prf_subG(x, y).

', `

expand DH_proba_collision_minimal(
  G_t,
  Znw_t,
  g,
  expnw,
  mult,
  PCollKey
).

ifdef(`square', `

proba pPRF_ODH.
proba pSQPRF_ODH.
expand square_mPRF_ODH(
  (* types *)
  G_t,  (* Group elements *)
  Znw_t,  (* Exponents *)
  bitstring, (* PRF argument *)
  eae_output_t, (* PRF output *)
  (* variables *)
  g,    (* a generator of the group *)
  expnw,  (* exponentiation function *)
  expnw,  (* exponentiation function (same function after game transformation) *)
  mult, (* multiplication function for exponents *)
  prf, (* prf *)
  (* probabilities *)
  pPRF_ODH, (* probability of breaking the PRF-ODH assumption *)
  pSQPRF_ODH (* probability of breaking the square PRF-ODH assumption *)
).

',`

proba pPRF_ODH.
expand mnPRF_ODH(
  (* types *)
  G_t,  (* Group elements *)
  Znw_t,  (* Exponents *)
  bitstring, (* PRF argument *)
  eae_output_t, (* PRF output *)
  (* variables *)
  g,    (* a generator of the group *)
  expnw,  (* exponentiation function *)
  expnw,  (* exponentiation function (same function after game transformation) *)
  mult, (* multiplication function for exponents *)
  prf, (* prf *)
  (* probabilities *)
  pPRF_ODH, (* probability of breaking the PRF-ODH assumption *)
  PCollKey
).

')
')

expand DH_exclude_weak_keys(
  G_t,
  Z_t,
  Znw_t,
  ZnwtoZ,
  exp,
  expnw,
  PWeakKey
).

(* For a group of prime order q:
   PColl1Rand(Z_t) = PColl2Rand(Z_t) = 1/(q-1)
   PCollKey1 = PCollKey2 = 1/(q-1)
   PDistRerandom = 0

   For Curve25519:
   PColl1Rand(Z_t) = PColl2Rand(Z_t) = 2^{-251}
   PCollKey1 = PCollKey2 = 2 PColl1Rand(Z_t) = 2^{-250}
   PDistRerandom = 2^{-125}

   For Curve448:
   PColl1Rand(Z_t) = PColl2Rand(Z_t) = 2^{-445}
   PCollKey1 = PCollKey2 = 2 PColl1Rand(Z_t) = 2^{-444}
   PDistRerandom = 2^{-220}
*)

expand BijectivePair(Z_t, G_t, keypair_t, keypair).

letfun DH(exponent: Z_t, group_element: G_t) =
  exp(group_element, exponent).
letfun pkgen(exponent: Z_t) =
  exp(g, exponent).
letfun skgen(exponent: Z_t) =
  exponent.
letfun GenerateKeyPair() =
  z <-R Z_t;
  keypair(skgen(z), pkgen(z)).


(* KDF *)

fun concatContextAuth(G_t, G_t, G_t): Ctx_t [data].
fun concatContext(G_t, G_t): Ctx_t [data].
fun eae2b(eae_output_t): bitstring [data].
fun ctx2b(Ctx_t): bitstring [data].

equation forall x: eae_output_t, y: Ctx_t; eae2b(x) <> ctx2b(y).

(* prf(x,eae2b(k)) is a PRF with key k
   Specified by converting it to prf2(k,x). *)

proba Pprf.
expand PRF(eae_output_t, G_t, eae_output_t, prf2, Pprf).

equation(prf_convert(prf))
  forall k: eae_output_t, x: G_t; prf(x, eae2b(k)) = prf2(k,x) [manual].

(* prf is collision-resistant *)

equation forall x, x': G_t, y, y': bitstring;
         (prf(x,y) = prf(x',y')) = ((x = x') && (y = y')).

(* prf(G_t, bitstring): eae_output_t satisfies (square) mmPRF-ODH *)

letfun ExtractAndExpandAuth(dh1: G_t, dh2: G_t, ctx: Ctx_t) =
   let prf1 = prf(dh2, ctx2b(ctx)) in
   prf(dh1, eae2b(prf1)).
   
letfun ExtractAndExpand(dh: G_t, ctx: Ctx_t) =
   prf(dh, ctx2b(ctx)).
   
(* Detailed justification that the following usage of ExtractAndExpandAuth
   satisfies the assumptions of above-mentioned Lemma 6, given it is
   implemented by HKDF:

- keyspace and info||i_0 do not collide, because info has two bytes and
  then RFCXXXX, and RFCXXXX does not collide with itself when moved by
  two bytes.

For long Nsecret, we also need to consider this:
- keyspace and hmac_output_space||info||i_j with j>0
  Length(keyspace) - Length(hmac_output_space||info||i_j)
  = -2 +Length(label_eae_prk) -Length(label_shared_secret) +Length(dh) -Length(kem_context) -Nh
  = -2 +7 -13 +Ndh -Nenc -Npk -Npk -Nh
  = -8 +Ndh -Nenc -Npk -Npk -Nh
  Assuming that Ndh is pretty close to Npk, we can argue that
  keyspace and hmac_output_space||info||i_j with j>0
  have clearly different lengths, and thus, do not collide.

Therefore the following usage of ExtractAndExpandAuth satisfies the
assumptions of Lemma 6.
*)

(* Authenticated DH KEM *)

expand BijectivePair(eae_output_t, bitstring, Encap_res_t, Encap_tuple).

letfun AuthEncap(pkR: G_t, skS: Z_t) =
  let keypair(skE, pkE) = GenerateKeyPair() in
  let dh1 = DH(skE, pkR) in
  let dh2 = DH(skS, pkR) in
  let enc: bitstring = Serialize(pkE) in
  let pkS = pkgen(skS) in
  let kemContext: Ctx_t = concatContextAuth(pkE, pkR, pkS) in
  let zz: eae_output_t = ExtractAndExpandAuth(dh1, dh2, kemContext) in
  Encap_tuple(zz, enc).

expand OptionType_1(Decap_res_t, Decap_Some, Decap_None, eae_output_t).

letfun AuthDecap(enc: bitstring, skR: Z_t, pkS: G_t) =
  let Serialize(pkE: G_t) = enc in
  (
    let dh1 = DH(skR, pkE) in
    let dh2 = DH(skR, pkS) in
    let pkR = pkgen(skR) in
    let kemContext: Ctx_t = concatContextAuth(pkE, pkR, pkS) in
    let zz: eae_output_t = ExtractAndExpandAuth(dh1, dh2, kemContext) in
    Decap_Some(zz)
  ) else (
    Decap_None
  ).

(* Non-authenticated DH KEM *)

letfun Encap(pkR: G_t) =
  let keypair(skE,pkE) = GenerateKeyPair() in
  let dh: G_t = DH(skE, pkR) in
  let enc: bitstring = Serialize(pkE) in
  let kem_context = concatContext(pkE, pkR) in
  let zz: eae_output_t = ExtractAndExpand(dh, kem_context) in
  Encap_tuple(zz, enc).

letfun Decap(enc: bitstring, skR: Z_t) =
  let Serialize(pkE: G_t) = enc in
  (
    let dh: G_t = DH(skR, pkE) in
    let kem_context = concatContext(pkE, pkgen(skR)) in
    let zz: eae_output_t = ExtractAndExpand(dh, kem_context) in
    Decap_Some(zz)
  ) else (
    Decap_None
  ).

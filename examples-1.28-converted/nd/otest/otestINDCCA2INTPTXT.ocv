(* Trivial running example for conference paper. *)

proof {
crypto ind_cca2(enc);
auto
}

param N.
param N2.
param N3.

type mkey [bounded].
type mkeyseed [fixed].
type key [fixed,large].
type keyseed [fixed].
type seed [fixed].

fun keyToBitstring(key):bitstring [data].

(* Shared-key encryption (CPA Stream cipher) *)

proba Penc.
proba Pencptxt.

expand IND_CCA2_INT_PTXT_sym_enc(keyseed, key, bitstring, bitstring, seed, kgen, enc, dec, injbot, Z, Penc, Pencptxt).

(* The function Z returns for each bitstring, a bitstring
   of the same length, consisting only of zeroes. *)
const Zkey:bitstring.
equation forall y:key; 
	Z(keyToBitstring(y)) = Zkey.

(* Queries *)

query secret k2 [cv_onesession].
query secret k3 [cv_onesession].

query secret k2.
query secret k3.





process 
	Ostart() :=
rKab <-R keyseed;
Kab: key <- kgen(rKab);
return();
((
  foreach iA <= N do
  OA() :=
  k2 <-R key;
  s1 <-R seed;
  ea1: bitstring <- enc(keyToBitstring(k2), Kab, s1);
  return(ea1)
) | (
  foreach iB <= N do
  OB(ea: bitstring) :=
  let injbot(keyToBitstring(k3: key)) = dec(ea, Kab) in
  return()
))



(* EXPECTED
Warning: Foreach uses the same parameter N with oracle OB as foreach at line 51, characters 17-18 with oracle OA. Avoid reusing parameters for multiple foreach with different oracles to avoid losing precision in the probability bound.
RESULT Could not prove secrecy of k3; one-session secrecy of k3.
0.024s (user 0.024s + system 0.000s), max rss 14012K
END *)

{
  inputs = {
    fstar.url = "github:fstarlang/fstar";
    flake-utils.follows = "fstar/flake-utils";
    nixpkgs.follows = "fstar/nixpkgs";
    karamel = {
      url = "github:fstarlang/karamel";
      inputs.fstar.follows = "fstar";
      inputs.flake-utils.follows = "flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    hacl = {
      url = "github:hacl-star/hacl-star";
      inputs = {
        fstar.follows = "fstar";
        flake-utils.follows = "flake-utils";
        nixpkgs.follows = "nixpkgs";
        karamel.follows = "karamel";
      };
    };
  };

  outputs = inputs: let
    system = "x86_64-linux";
    pkgs = import inputs.nixpkgs {inherit system;};
  in {
    packages.${system}.default = pkgs.stdenv.mkDerivation {
      name = "cryptoverif";
      src = ./.;
      buildInputs = pkgs.lib.attrValues {
        inherit
          (pkgs)
          dune_3
          ocaml
          which
          time
          ;
        inherit
          (pkgs.ocamlPackages)
          findlib
          cryptokit
          batteries
          menhirLib
          mtime
          pprint
          ppx_deriving
          ppx_deriving_yojson
          process
          sedlex
          stdint
          ;
        inherit (inputs.fstar.packages.${system}) fstar;
      };
      FSTAR_HOME = inputs.fstar.packages.${system}.fstar;
      KRML_HOME = inputs.karamel.packages.${system}.karamel.home;
      HACL_HOME = inputs.hacl.packages.${system}.hacl;
      buildPhase = ''
        ./build
        make -C cv2fstar/cv2fstar
      '';
      doCheck = true;
      checkPhase = ''
        cd cv2fstar/nspk
        ../../cryptoverif -impl FStar -o src nsl.ocv
        make
      '';
      installPhase = "cp -r . $out";
    };
  };
}

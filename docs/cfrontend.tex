\def\iprocess{\nonterm{iprocess}}
\def\oprocess{\nonterm{oprocess}}

CryptoVerif also as an a \texttt{channel} frontend, that enables
cross-compatibility with ProVerif. This frontend is only for the input
file, the input process will be translated as an oracle definition and
all subsequent games will be displayed in the oracle syntax.

The \texttt{channels} front-end is similar to the  \texttt{oracles} one
with the following differences.
The keyword \texttt{newOracle} is replaced with \texttt{newChannel},
\texttt{channel} and \texttt{out} are keywords,
and \texttt{run} is not a keyword.
\begin{figure}[tp]
\def\phop{\phantom{\oprocess = }\mid}
\def\phip{\phantom{\iprocess = }\mid}

\begin{align*}
&\nonterm{channel} ::= \nonterm{ident}[\texttt{[}\seq{ident}\texttt{]}]\\
&\oprocess ::= \nonterm{ident}[\texttt{(}\seq{term}\texttt{)}]\\
&\phop \texttt{(} \oprocess \texttt{)}\\
&\phop \yield\\
&\phop \texttt{event }\nonterm{ident}[\texttt{(}\seq{term}\texttt{)}]\ [\texttt{; }\oprocess]\\
&\phop \texttt{event\string_abort }\nonterm{ident}\\
&\phop \Resavt[\texttt{; }\oprocess]\\
&\phop \Resbvt[\texttt{; }\oprocess]\\
&\phop \nonterm{basicpat} \texttt{ <- }\nonterm{term}[\texttt{; }\oprocess]\\
&\phop \texttt{let }\nonterm{pattern} \texttt{ = }\nonterm{term}\ 
[\texttt{in }\oprocess\ [\texttt{else }\oprocess]]\\
&\phop \texttt{if }\nonterm{cond}\texttt{ then }\oprocess\ [\texttt{else }\oprocess]\\
&\phop \texttt{find}[\texttt{[unique]}]\ \nonterm{findbranch}\ (\texttt{orfind }\nonterm{findbranch})^* \ [\texttt{else }\oprocess]\\
&\phop \texttt{insert }\nonterm{ident}\texttt{(}\seq{term}\texttt{)}\ [\texttt{; }\oprocess]\\
&\phop \texttt{get}[\texttt{[unique]}]\ \nonterm{ident}\texttt{(}\seq{pattern}\texttt{)}\ [\texttt{suchthat}\ \nonterm{term}]\texttt{ in }\oprocess\ [\texttt{else }\oprocess]\\
&\phop \texttt{out(}\nonterm{channel}\texttt{, }\nonterm{term}\texttt{)}[\texttt{; }\iprocess]\\
&\nonterm{findbranch} ::= \seq{identbound} \texttt{ suchthat }\nonterm{cond}\texttt{ then }\oprocess\\
&\iprocess ::= \nonterm{ident}[\texttt{(}\seq{term}\texttt{)}]\\
&\phip \texttt{(} \iprocess \texttt{)}\\
&\phip \texttt{0}\\
&\phip \iprocess \texttt{ | } \iprocess\\
&\phip \texttt{!} [\nonterm{ident}\texttt{ <=}]\ \nonterm{ident}\ \iprocess\\
&\phip \texttt{foreach }\nonterm{ident}\texttt{ <= } \nonterm{ident}\texttt{ do }\iprocess\\
&\phip \texttt{in(}\nonterm{channel}\texttt{,}\nonterm{pattern}\texttt{)}[\texttt{; }\oprocess]
\end{align*}
\caption{Grammar for processes (\texttt{channels} front-end)}
\label{fig:syntax3ch}
\end{figure}



The input file consists of a list of declarations followed by 
an input process or an equivalence query:
\begin{align*}
&\nonterm{declaration}^*\ {\tt process}\ \iprocess\\[2mm]
&\nonterm{declaration}^*\ {\tt equivalence}\ \iprocess\ \iprocess\ [\texttt{public\_vars}\ \seq{ident}]\\[2mm]
&\nonterm{declaration}^*\ {\tt query\_equiv}[\texttt{(}\nonterm{ident}[\texttt{(}\nonterm{ident}\texttt{)}]\texttt{)}]\\
&\qquad \nonterm{omode}\ [\texttt{|}\ \ldots\ \texttt{|}\nonterm{omode}]\texttt{ <=(?)=> }
[\texttt{[}n\texttt{]}]\ [\texttt{[}\neseq{option}\texttt{]}]\ \nonterm{ogroup}\ [\texttt{|}\ \ldots\ \texttt{|}\nonterm{ogroup}]
\end{align*}

In addition to the declarations of the \texttt{oracles} front-end, the \texttt{channels} front-end allows the declaration $\texttt{channel}\ c_1, \ldots, c_n\texttt{.}$, which declares communication channels $c_1, \ldots, c_n$. 

The syntax of processes is given in Figure~\ref{fig:syntax3ch}.

The calculus distinguishes two kinds of processes: input processes
$\iprocess$ are ready to receive a message on a channel; output
processes $\oprocess$ output a message on a channel after executing
some internal computations.  When an input or output process is an
identifier, it is substituted with its value defined by a \texttt{let}
declaration.
%
The input process $\mathit{proc}(M_1, \dots, M_n)$ is replaced with $Q\{M_1/x_1, \dots, M_n/x_n\}$ when $\mathit{proc}$ is declared by $\texttt{let}\ \mathit{proc}(x_1:T_1, \dots, x_n:T_n)\texttt{ = }Q\texttt{.}$
where $Q$ is an input process.
The terms $M_1, \dots, M_n$ must contain only variables, replication indices, and function applications.
%
The output process $\mathit{proc}(M_1, \dots, M_n)$ is replaced with $\texttt{let}$ $x_1 = M_1$ $\texttt{in}$ \dots $\texttt{let}$ $x_n = M_n$ $\texttt{in}$ $P$ when $\mathit{proc}$ is declared by $\texttt{let}\ \mathit{proc}(x_1:T_1, \dots, x_n:T_n)\texttt{ = }P\texttt{.}$ where $P$ is an output process.

In this front-end, communications are made over \emph{channels}.

The input process
%$\cinput{c[M_1, \ldots, M_l]}{x_1[i_1, \ldots, i_m]:T_1, \ldots, x_k[i_1, \ldots, i_m]:T_k};P$ 
$\texttt{in(}c\texttt{[}i_1, \dots, i_l\texttt{]},p\texttt{);}P$ declares a new process expecting an input from the attacker over the channel $c[i_1,\dots,i_l]$ where $c$ is declared by $\texttt{channel }c$ and $i_1, \ldots, i_n$ are the current
replication indices at the considered input. When the received message matches the given pattern $p$, the output process $P$ is executed with the variables in the pattern $p$ bound according to the received message.
Otherwise, the input fails and controls returns to the attacker immediately,
as if \texttt{yield} had been executed.
%
Patterns $p$ are as in the \texttt{let} process, except that
variables in $p$ that are not under a function symbol $f(\ldots)$
must be declared with their type.

The output process
$\texttt{out(}c\texttt{[}i_1, \ldots, i_l\texttt{],}N\texttt{);}Q$
sends the output value $N$ (truncated
to the maximum length of bitstrings on channel $c$) to the attacker over a channel $c[i_1, \dots, i_l]$ where $c$ is declared by $\texttt{channel }c$ and $i_1, \ldots, i_n$ are the current replication indices at the considered output.
The inputs declared in the input process $Q$ that
follows the output are then made available: the attacker can send messages to them.

In inputs and outputs, the channels $c\texttt{[}i_1, \ldots, i_n\texttt{]}$
where $i_1, \ldots, i_n$ are the current replication indices at the
considered input or output, can be abbreviated as $c$. CryptoVerif
automatically adds the current replication indices.

Due to the translation to oracles, there are multiple restrictions
over the input and output process definitions:
\begin{itemize}
\item Each input must use a different channel, except in disjoint
  execution branches. The channel name is used in the translation to
  name the corresponding oracle.
\item There cannot be two directly consecutive outputs. If one needs to
  output several messages consecutively, one can simply insert
  fictitious inputs between the outputs. The adversary can then
  schedule the outputs by sending messages to these inputs.
\item All inputs on a given channel must have current replication
  indices of the same types and patterns of the same types, and all
  outputs below these inputs must be of the same type, so that the
  resulting oracle is well typed. Tuples are considered of the same
  type when they are of the same arity and have elements of the same
  type.
\item An output channel and an input channel both available at the
  same time can never be equal. It is recommended to use disjoint
  channels in parallel processes to ensure this.
\end{itemize}

Note that the construct $\textbf{newChannel }c;Q$  used in research papers
is absent from the implementation: this construct is useful in the proof
of soundness of CryptoVerif, but not essential for encoding games
that CryptoVerif manipulates.


In probability formulas (Figure~\ref{fig:syntax2}), 
\texttt{time(out $\dots$)} and \texttt{time(in $n$)} are added and
\texttt{time(newOracle)} is replaced with \texttt{time(newChannel)}.
$\texttt{time(newChannel)}$ is the maximum time to create a new
private channel.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "manual"
%%% End:

require import AllCore List Finite Distr DBool DInterval DList FSet SmtMap.

(* * Preliminaries *)

(* Arithmetic *)

lemma distr_lossL (x' x y r1 r2 : real) :
  `| x' - y | <= r2 => `| x - x' | <= r1 => `| x - y | <= r1 + r2.
proof. smt(). qed.

lemma distr_lossR (x y y' r1 r2 : real) :
  `| x - y' | <= r2 => `| y - y' | <= r1  => `| x - y | <= r1 + r2.
proof. smt(). qed.

(* Logic.ec *)

type ('a, 'b) sum = [Left of 'a | Right of 'b].

op is_left ['a 'b] (s : ('a,'b) sum) =
  with s = Left _  => true
  with s = Right _ => false.

op left ['a 'b] (s : ('a,'b) sum) =
  with s = Left x  => x
  with s = Right _ => witness.

op right ['a 'b] (s : ('a,'b) sum) =
  with s = Right x => x
  with s = Left _  => witness.

(* really useful ? *)
lemma omap_some (ox : 'a option) (y : 'b) (f : 'a -> 'b) :
  omap f ox = Some y => exists x, ox = Some x /\ f x = y.
proof. by case: ox => //= x <-; exists x. qed.

(* List.ec *)

lemma uniq_mem_rem (y x : 'a) (s : 'a list) :
  uniq s => y \in rem x s <=> y \in s /\ y <> x.
proof. by elim: s => //= /#. qed.

(* FSet.ec *)

(* PR #411 *)
op product (A : 'a fset) (B : 'b fset) =
  oflist (allpairs (fun x y => (x,y)) (elems A) (elems B))
axiomatized by productE.

lemma productP (A : 'a fset) (B : 'b fset) (a : 'a) (b : 'b):
  (a, b) \in product A B <=> (a \in A) /\ (b \in B).
proof.
by rewrite productE mem_oflist allpairsP; split => [/#|*]; exists (a,b) => /#.
qed.

lemma card_product (A B : 'a fset):
  card (product A B) = card A * card B.
proof.
rewrite productE uniq_card_oflist ?size_allpairs -?cardE //.
apply allpairs_uniq; smt(uniq_elems).
qed.

(* SmtMap.ec *)
lemma find_map (m : ('a, 'b) fmap) (f : 'a -> 'b -> 'c) P :
  find P (map f m) = find (fun x y => P x (f x y)) m.
proof.
rewrite !findE fdom_map; congr; apply find_eq_in => x /=.
by rewrite -memE fdomP /= mapE; case(m.[x]).
qed.

lemma find_not_none (P : 'a -> 'b -> bool) (m : ('a,'b) fmap) :
  find P m <> None => exists x y, find P m = Some x /\ m.[x] = Some y /\ P x y.
proof. by case: (findP P m) => // x y ? ? ? _; exists x y. qed.


lemma rem_filterE (m : ('a,'b) fmap) x (p : 'a -> 'b -> bool) :
  (forall y, !p x y) => rem (filter p m) x = filter p m.
proof.
move => Hpx; apply/fmap_eqP => z; rewrite remE.
by case(z = x) => // ->; rewrite filterE /#.
qed.


lemma find_none (p : 'a -> 'b -> bool) (m : ('a,'b) fmap):
  (forall x, x \in m => !p x (oget m.[x])) => find p m = None.
proof. by move=> np; apply contraT => /find_not_none /#. qed.

lemma nosmt uniq_find_some z (P : 'a -> 'b -> bool) (m : ('a, 'b) fmap) :
  (forall (x : 'a) (y : 'b), m.[x] = Some y => P x y => x = z) =>
  z \in m => P z (oget m.[z]) => find P m = Some z.
proof.
move => uniq_m z_m p_z; case (findP P m) => [/#|x y fmx mx p_xy].
by have <- := find_some_unique _ _ _ _ uniq_m fmx.
qed.

lemma nosmt uniq_find_set (p : 'a -> 'b -> bool) (m : ('a,'b) fmap) x y :
  (forall x1 x2, x1 \in m => x2 \in m =>
    p x1 (oget m.[x1]) => p x2 (oget m.[x2]) => x1 = x2) =>
  x \notin m => !p x y => find p m.[x <- y] = find p m.
proof.
move=> x_m uniq_m p_xy. case (findP p m) => [-> npm|a b E m_a p_ab].
- apply find_none; smt(mem_set get_setE).
rewrite E. apply uniq_find_some; smt(mem_set get_setE).
qed.


lemma mem_filterE (m : ('a,'b) fmap) (p : 'a -> 'b -> bool) x :
  x \in filter p m => (filter p m).[x] = m.[x].
proof. smt(filterE). qed.

lemma filter_empty (p:'a -> 'b -> bool) : filter p empty = empty.
proof. by apply/fmap_eqP => x; rewrite filterE emptyE. qed.

(* fsize *)

op fsize (m : ('a,'b) fmap) : int = FSet.card (fdom m).

lemma fsize_empty ['a 'b] : fsize<:'a,'b> empty = 0.
proof. by rewrite /fsize fdom0 fcards0. qed.

lemma fsize_set (m : ('a, 'b) fmap) k v :
  fsize m.[k <- v] = b2i (k \notin m) + fsize m.
proof. by rewrite /fsize fdom_set fcardU1 mem_fdom. qed.

lemma mu_le_fsize (m : ('a, 'b) fmap) (d : 'c distr) p bd :
  (forall u, u \in m => mu d (p u) <= bd) =>
  mu d (fun r => exists u, u \in m /\ p u r) <= (fsize m)%r * bd.
proof.
elim/fmapW : m; first by rewrite mu0_false; smt(mem_empty fsize_empty).
move => m k v fresh_k IH H.
rewrite (@mu_eq _ _ (predU (p k)
  (fun (r : 'c) => exists (u : 'a), (u \in m) /\ p u r))); 1: smt(mem_set).
rewrite mu_or; apply StdOrder.RealOrder.ler_naddr; 1: smt(mu_bounded).
have -> : (fsize m.[k <- v])%r * bd = bd + (fsize m)%r * bd.
  smt(fsize_set mem_fdom).
smt(mem_set).
qed.


(* Distr.MRat *)
import MRat.
lemma drat_map (s : 'a list) (f : 'a -> 'b) :
  drat (map f s) = dmap (drat s) f.
proof.
by apply/eq_distr => x; rewrite dmapE !prratE size_map count_map.
qed.

(* end preliminaries *)


(* This theory develops a variant of [fmap] where update ("_.[_<-_]")
is cumulative and get ("_.[_]") is a distribution that uniformly
samles a result from among the stored key-value pairs.

The type [('a, 'b) rmap] of random maps is implemented as [('a * 'b)
list. We define a [dom] operation using "\in" and "\notin" the same as
for [fmap]. Consequently, we have the following syntax (where
[m : ('a, 'b) rmap], [x : 'a] and [y : 'b] [oy : option y]):

x \in m      : x has some binding in y          (domain operation)
oy \in m.[x] : oy is in the support of m.[x]    (support of distribution)
(x,y) \in m  : the pair (x,y) is a binding in m (list membership)
*)

lemma nilP (s : 'a list) : s <> [] <=> exists x, x \in s by smt().

lemma has_filter p (s : 'a list) :
  has p s <=> filter p s <> [].
proof. by rewrite hasP nilP; smt(List.mem_filter). qed.

theory RMap.

type ('a,'b) rmap = ('a*'b) list.

op rempty : ('a,'b) rmap = [].

op dom (m : ('a,'b) rmap) = (fun x => has (fun e => fst e = x) m).

(* range of possible results for lookup *)
op ran (m : ('a,'b) rmap) x =
  unzip2 (filter (fun e => fst e = x) m).

lemma ranP (m : ('a,'b) rmap) x y :
  y \in ran m x <=> (x,y) \in m.
proof.
split => [|xy_m @/ran]; first by case/mapP => -[x' y']; rewrite mem_filter /#.
by rewrite (@map_f (fun p => snd p) _ (x,y)) mem_filter.
qed.

op "_.[_]" (m : ('a,'b) rmap) (x : 'a) : ('b option) distr =
  let r = ran m x in
  if r = [] then dunit None else dmap (drat r) Some.

op "_.[_<-_]" (m : ('a,'b) rmap) (x : 'a) (y : 'b) : ('a,'b) rmap =
  (x,y) :: m.

abbrev (\in)    ['a 'b] x (m : ('a, 'b) rmap) = (dom m x).
abbrev (\notin) ['a 'b] x (m : ('a, 'b) rmap) = ! (dom m x).

lemma domP (m : ('a,'b) rmap) x :
  x \in m <=> exists y, (x,y) \in m.
proof. by rewrite /dom hasP /= /#. qed.

lemma domE (m : ('a, 'b) rmap, x : 'a) :
  x \in m <=> ran m x <> [].
proof.
split; last by case/nilP => y; rewrite ranP; smt(domP).
by case/domP => y xy_m; apply/nilP; exists y; apply/ranP.
qed.

lemma get_none (m : ('a, 'b) rmap, x : 'a) y :
  x \notin m => y \in m.[x] => is_none y.
proof.
by rewrite domE negbK /("_.[_]") => -> /=; smt(supp_dunit).
qed.

lemma get_some (m : ('a, 'b) rmap, x : 'a) y :
  x \in m => y \in m.[x] => is_some y.
proof.
by rewrite domE /("_.[_]") /= => ?; rewrite ifF // supp_dmap.
qed.

lemma get_ll (m : ('a,'b) rmap) x : is_lossless (m.[x]).
proof. smt(dunit_ll dmap_ll drat_ll). qed.

lemma get1E_some (m : ('a,'b) rmap) x y :
  mu1 m.[x] (Some y) =
  if y \in ran m x then mu1 (drat (ran m x)) y else 0%r.
proof.
rewrite /("_.[_]") /=; case (y \in ran m x) => [y_m|yNm].
- by rewrite ifF 1:/# dmapE (mu_eq _ _ (pred1 y)).
case (ran m x = []) => rN0 ; first by rewrite dunitE.
rewrite dmapE (mu_eq _ _ (pred1 y)) //; smt(supp_drat mu_bounded).
qed.

lemma get1E_none (m : ('a,'b) rmap) x :
  mu1 m.[x] None = b2r (x \notin m).
proof.
rewrite /("_.[_]") /= -if_neg -domE.
by case (x \in m) => [|]; rewrite ?dunitE // dmapE mu0_false.
qed.

lemma supp_get (m : ('a, 'b) rmap) (x : 'a) oy :
  oy \in m.[x] <=> if is_some oy then oget oy \in ran m x else x \notin m.
proof.
case: oy => [|y] /= @/support; rewrite ?get1E_none ?get1E_some; 1:smt().
case (y \in ran m x) => //; smt(supp_drat mu_bounded).
qed.

lemma get_valid (m : ('a, 'b) rmap) (x : 'a) oy :
  x \in m => oy \in m.[x] => exists y, (x,y) \in m /\ oy = Some y.
proof.
move => x_m oy_m; have := get_some m x oy x_m oy_m.
case: oy oy_m => // y y_m _; exists y; smt(supp_get ranP).
qed.

lemma ran_set_same (m : ('a, 'b) rmap) x y :
  ran m.[x<-y] x = y :: ran m x.
proof. done. qed.

lemma ran_set (m : ('a, 'b) rmap) x y x' :
  ran m.[x<-y] x' = if x = x' then y :: ran m x else ran m x'.
proof. smt(). qed.

lemma ran_rempty x : ran rempty<:'a,'b> x = [] by done.

lemma supp_get_some (m : ('a, 'b) rmap) (x : 'a) y :
  Some y \in m.[x] <=>  y \in ran m x.
proof. smt(supp_get). qed.

lemma supp_get_non (m : ('a, 'b) rmap) x :
  None \in m.[x] <=> x \notin m.
proof. smt(supp_get). qed.

lemma mem_set (m : ('a, 'b) rmap) x y (z : 'a) :
  z \in m.[x <- y] <=> z \in m \/ (z = x).
proof. smt(). qed.


lemma mem_rempty (x : 'a) : x \notin rempty<:'a,'b> by done.

op filter ['a, 'b] (p : 'a -> 'b -> bool) (m : ('a, 'b) rmap) : ('a, 'b) rmap =
  List.filter (fun xy : 'a * 'b => p xy.`1 xy.`2) m.

lemma filter_set (p : 'a -> 'b -> bool) (m : ('a, 'b) rmap) (x : 'a) (b : 'b) :
  filter p m.[x <- b] =
  (if p x b then [(x,b)] else []) ++ filter p m.
proof. smt(). qed.

lemma mem_filter_pair (p : 'a -> 'b -> bool) (m : ('a, 'b) rmap) (x : 'a*'b) :
  x \in filter p m <=> p x.`1 x.`2 /\ (x \in m).
proof. smt(List.mem_filter). qed.

lemma mem_filter (p : 'a -> 'b -> bool) (m : ('a, 'b) rmap) (x : 'a) :
  x \in filter p m <=> exists y, p x y /\ (x,y) \in m.
proof. smt(List.mem_filter domP). qed.

(* smt seems unable to use this at all *)
lemma nosmt dom_mem_filter (p : 'a -> bool) (m : ('a, 'b) rmap) (x : 'a) :
  x \in filter (fun x _ => p x) m <=> p x /\ x \in m.
proof. by rewrite RMap.mem_filter; smt(domP). qed.

lemma dom_filter (p : 'a -> 'b -> bool) (m : ('a, 'b) rmap) (x : 'a) :
 x \in filter p m => x \in m.
proof. smt(mem_filter domP). qed.

(* if the filter acts only on the domain, it does not affect the
probabilities of the parts of the domain it preserves *)
(* TOTHINK: is there a reasonably simple statement for general predicates? *)
lemma dom_filterE (p : 'a -> bool) (m : ('a, 'b) rmap) (x : 'a) E :
  p x => mu (filter (fun z _ => p z) m).[x] E = mu m.[x] E.
proof.
move => p_x; congr; rewrite /("_.[_]") /=; pose m' := filter _ _.
suff -> : ran m x = ran m' x by done.
by rewrite /ran /m' -filter_predI; congr; apply List.eq_in_filter => />.
qed.

(** also holds for general predicates, but this is good enough for now
and provable using the lemma above *)
lemma mem_supp_filter (p : 'a -> bool) (m : ('a, 'b) rmap) (x : 'a) oy :
  x \in filter (fun z _ => p z) m =>
  oy \in (filter (fun z _ => p z) m).[x] => oy \in m.[x].
proof.
by move/dom_mem_filter => [px x_m]; rewrite /support dom_filterE.
qed.

end RMap.

import StdOrder.RealOrder.
import RField.

op h = fun x n => (1%r / (x * (1%r - x)^n)).

op up = fun n => 1 + 3 * n.

lemma upper_bound n : 0 <= n => h (1%r / (n + 1)%r) n <= (up n)%r.
proof.
case: (n = 0) => [->|n_neq0 n_gt0].
- by rewrite /h /up StdBigop.Bigreal.Num.Domain.expr0 mulr0 addr0 add0r !mulr1.
have {n_neq0 n_gt0} n_gt0 : 0 < n by rewrite StdOrder.IntOrder.ltr_def.
have -> : h (1%r / (n + 1)%r) n = ((n + 1)%r / n%r) ^ (n + 1) * n%r.
- rewrite /h -{3}(divrr (n + 1)%r); 1: by smt().
  have -> : (n + 1)%r / (n + 1)%r - 1%r / (n + 1)%r = n%r / (n + 1)%r
    by algebra; smt().
  rewrite !exprMn ?exprVn; 1..4: by smt().
  rewrite !fromintXn ?exprSr; 1..6: by smt().
  by algebra; smt(fromintXn unitrX).
pose h := fun n => ((n + 1) %r / n%r) ^ (n + 1).
suff /# : h n * n%r <= (up n)%r.
have up1 : up 1 = 4 by rewrite /up; algebra.
have h1  : h 1 * 1%r = 4%r by rewrite /h !mulr1 exprSr // expr1 //; algebra.
suff fhP : forall m n, 0 < m <= n => 0%r < (up m)%r / (h m * m%r) =>
                       (up m)%r / (h m * m%r) <= (up n)%r / (h n * n%r)
  by move: (fhP 1 n); rewrite up1 h1 divrr; smt().
move => {n n_gt0 up1 h1} m n [m_gt0 mnP].
suff ihn : forall n, 0 < n => 0%r < (up n)%r / (h n * n%r) =>
                     (up n)%r / (h n * n%r) <=
                     (up (n + 1))%r / (h (n + 1) * (n + 1)%r).
- case: (m = n) => [/#|mn_neq]; have {mnP mn_neq} mnP : m < n by smt().
  have {mnP} [q] [] mnqP : (exists q, n - m = q /\ 0 < q) by smt().
  have -> {mnqP} : n = m + q by smt().
  by move: q; apply/natind => /#.
move => {m n m_gt0 mnP} n n_gt0 fhnP.
have -> : (up (n + 1))%r / (h (n + 1) * (n + 1)%r) =
          ((up n)%r / (h n * n%r)) * ((up (n + 1))%r / (up n)%r) *
          (n%r / (n + 1)%r) * ((n + 1)%r / (n + 2)%r) *
          ((n + 1)%r ^ 2 / (n * (n + 2))%r) ^ (n + 1).
- have -> : (up n)%r / (h n * n%r) * ((up (n + 1))%r / (up n)%r) =
            (up (n + 1))%r / (h n * n%r).
  + rewrite mulrACA mulrCA mulrAC divrr; 1: by smt().
    by rewrite mul1r mulrC.
  rewrite -!mulrA; congr; rewrite !mulrA.
  rewrite 2?invrM; 1..4: by smt(unitrX).
  have -> : inv n%r / h n * n%r / (n + 1)%r = inv (n + 1)%r / h n
    by rewrite -mulrA mulrACA mulVr ?mul1r 1?mulrC /#.
  rewrite -!mulrA; congr; rewrite /h.
  rewrite -!exprVn; 1, 2: by smt().
  rewrite ?StdBigop.Bigreal.Num.Domain.invrM; 1..4: by smt().
  rewrite !StdBigop.Bigreal.Num.Domain.invrK.
  rewrite StdBigop.Bigreal.Num.Domain.exprSr; 1: by smt().
  have -> : n + 1 + 1 = n + 2 by done.
  rewrite !mulrA -mulrA -(mulrA _ (n + 1)%r).
  pose k := (n + 1)%r / (n + 2)%r; rewrite mulrAC; congr.
  rewrite -exprMn; [smt()|congr; rewrite expr2 /k].
  have -> : (n * (n + 2))%r = n%r * (n + 2)%r by smt().
  by field; smt(expr2).
rewrite -!mulrA mulrA ler_pmulr // !mulrA => {fhnP}.
have -> : (up (n + 1))%r / (up n)%r * n%r / (n + 1)%r * (n + 1)%r / (n + 2)%r =
          (up (n + 1))%r / (up n)%r * n%r / (n + 2)%r by field; smt().
apply (ler_trans ((up (n + 1))%r / (up n)%r * n%r / (n + 2)%r *
                  ((1%r + (n + 1)%r / (n%r * (n + 2)%r) +
                   (n + 1)%r / (2%r * n%r * (n + 2)%r ^ 2))))); last first.
- rewrite ler_pmul2l; 1: by smt().
  have -> : (n + 1)%r ^ 2 / (n * (n + 2))%r = 1%r + 1%r / (n%r * (n + 2)%r)
    by algebra; smt(expr2).
  rewrite Binomial.BCR.binomial; 1: by smt().
  have -> : n + 1 + 1 = n - 1 + 1 + 1 + 1 by algebra.
  rewrite ?rangeSr; 1..3: smt().
  rewrite !StdBigop.Bigreal.BRA.big_rcons /predT /=.
  rewrite -addrA Binomial.binn; 1: smt().
  rewrite -!addrA; apply ler_paddl.
  + apply StdBigop.Bigreal.sumr_ge0_seq.
    move => a aP _; rewrite mulr_ge0; 1: smt(Binomial.ge0_bin).
    by rewrite expr1z mul1r; smt(expr_gt0).
  + have -> : n + (1 - n) = 1 by algebra.
    have -> : n + (1 - (n - 1)) = 2 by algebra.
    have binP : forall n, 0 <= n => (Binomial.bin (n + 1) n)%Binomial = n + 1.
    * move => {n n_gt0} n; elim n => [|n n_ge0 nP]; 1: by rewrite Binomial.bin0.
      by rewrite Binomial.binSn ?nP ?Binomial.binn; smt().
    rewrite !expr1z expr0 expr1 !mul1r addrC !addrA ler_add2r addrC.
    rewrite ler_add; 2: by smt().
    suff -> : (Binomial.bin (n + 1) (n - 1))%Binomial%r =
              ((n + 1)%r * n%r) / 2%r.
    * rewrite exprVn // ler_eqVlt !expr2 -!mulrA; left; congr.
      have -> : inv 2%r / (n%r * ((n + 2)%r * (n%r * (n + 2)%r))) =
                inv n%r * inv 2%r / (((n + 2)%r * (n%r * (n + 2)%r)))
        by smt(mulrA mulrC).
      rewrite !mulrA divrr -?mulrA ?mul1r; 1: by smt().
      by rewrite invrM 1?mulrC; smt(mulrA mulrC).
    move: n n_gt0; apply/natind => [/#|n].
    elim n => [|n n_ge0 _ ihn n_gt0]; 1: by rewrite Binomial.bin0.
    have -> : n + 1 + 1 - 1 = n + 1 by done.
    rewrite Binomial.binSn ?binP; 1..3: by smt().
    case: (0 < n + 1) => [nP|]; 2: by smt(Binomial.bin0).
    move: (ihn nP); rewrite -!addrA addrN addr0 !addrA !fromintD => ->.
    by field; smt().
- have -> : 1%r + (n + 1)%r / (n%r * (n + 2)%r) +
            (n + 1)%r / (2%r * n%r * (n + 2)%r ^ 2) =
            (2%r * n%r ^ 3 + 10%r * n%r ^ 2 + 15%r * n%r + 5%r) /
            (2%r * n%r * (n + 2)%r ^ 2).
  + field; 1: by smt(expr2).
    suff -> : n%r ^ 3 = n%r * n%r * n%r by smt(expr2).
    by rewrite -expr2 -exprSr.
  rewrite !mulrA ler_pdivl_mulr ?mul1r; 1: by smt(expr2).
  rewrite -!mulrA (mulrCA (up (n + 1))%r) (mulrC (inv ((up n)%r))).
  rewrite ler_pdivl_mulr; 1: by smt().
  rewrite -!mulrA (mulrCA _ (inv (n + 2)%r)) (mulrCA (up (n + 1))%r).
  rewrite (mulrC (inv (n + 2)%r)) ler_pdivl_mulr /f; 1: by smt().
  have -> : 2%r * (n%r * ((n + 2)%r ^ 2 * (1 + 3 * n)%r)) * (n + 2)%r =
            6%r * n%r ^ 5 + 38%r * n%r ^ 4 + 84%r * n%r ^ 3 +
            72%r * n%r ^ 2 + 16%r * n%r by ring.
  have -> : (1 + 3 * (n + 1))%r *
            (n%r * (2%r * n%r ^ 3 + 10%r * n%r ^ 2 + 15%r * n%r + 5%r)) =
            6%r * n%r ^ 5 + 38%r * n%r ^ 4 + 85%r * n%r ^ 3 +
            75%r * n%r ^ 2 + 20%r * n%r by ring.
  rewrite -!addrA !ler_add2l ?ler_add ?ler_pmul2r //; 2, 3: by smt(expr2).
  suff /# : n%r ^ 3 = n%r * n%r * n%r.
  by rewrite -expr2 -exprSr.
qed.

lemma unique_choice x (p : 'a -> bool) x0 :
  p x => (forall x1 x2, p x1 => p x2 => x1 = x2) =>
  choiceb p x0 = x.
proof. by move=> p_x unique_p; smt(choicebP). qed.

op finv p (f : 'a -> 'b) (y : 'b) =
  choiceb (fun x => p x /\ f x = y) witness.

op injective_on (p : 'a -> bool) (f : 'a -> 'b) =
  forall x1 x2, p x1 => p x2 => f x1 = f x2 => x1 = x2.

op cancel_on (p : 'a -> bool) (f : 'a -> 'b) (g : 'b -> 'a) =
  forall x, p x => g (f x) = x.

op image (f : 'a -> 'b) (p : 'a -> bool) y =
  exists x, p x /\ f x = y.

lemma finvK (f : 'a -> 'b) (p : 'a -> bool) :
  injective_on p f => cancel_on p f (finv p f).
proof.
move=> f_inj x @/finv p_x. apply unique_choice => // /#.
qed.

lemma invfK (f : 'a -> 'b) (p : 'a -> bool) :
  injective_on p f => cancel_on (image f p) (finv p f) f.
proof.
move=> f_inj y @/finv [x [p_x fx_y]].
by rewrite (unique_choice x) // /#.
qed.

op permutation_on p (f : 'a -> 'a) (g : 'a -> 'a) =
  cancel_on p f g /\ cancel_on p g f.

lemma have_permutation (F1 F2 : 'a -> 'b) (p : 'a -> bool) :
  injective_on p F1 => injective_on p F2 => image F1 p == image F2 p =>
  exists f g, permutation_on p f g
    /\ (forall x, p x => F2 x = F1 (g x))
    /\ (forall x, p x => F1 x = F2 (f x))
    /\ (forall x, p x => p (f x))
    /\ forall x, p x => p (g x).
proof.
move=> F1_inj F2_inj eq_image.
exists (finv p F2 \o F1) (finv p F1 \o F2).
do ! split.
by move=> x p_x @/(\o); rewrite invfK ?finvK // /#.
by move=> x p_x @/(\o); rewrite invfK ?finvK // /#.
by move=> x @/(\o) ?; rewrite invfK // /#.
by move=> x @/(\o) ?; rewrite invfK // /#.
- move=> x @/(\o) p_x.
  pose p' := (fun (x0 : 'a) => p x0 /\ F2 x0 = F1 x).
  suff: p' (choiceb p' witness) by smt().
  apply/choicebP.
  have [x' ?]: image F2 p (F1 x) by smt().
  by exists x'.
- move=> x @/(\o) p_x.
  pose p' := (fun (x0 : 'a) => p x0 /\ F1 x0 = F2 x).
  suff: p' (choiceb p' witness) by smt().
  apply/choicebP.
  have [x' ?]: image F1 p (F2 x) by smt().
  by exists x'.
qed.

lemma fimageE (f : 'a -> 'b) (A : 'a fset) :
  mem (FSet.image f A) == (image f (mem A)).
proof. smt(imageP). qed.

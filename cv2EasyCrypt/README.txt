This directory contains examples for the translation from
CryptoVerif assumptions to EasyCrypt.

- To generate the EasyCrypt files, run `make ec`. The files are put
  in directory `generated`.

- To check the generated files / proofs, you can you run `make
  test-$x` where $x can take the following values:

    - common: check the core EasyCrypt files located in common/

    - generated: check the generated files located in generated/

    - proofs: check the proofs located in proofs/

    - proofs-fast: same as `proofs` with the exception of GDH_RSR.
      (The checking time of this example is very long)

    - all: common+generated+proofs

    - all-fast: common+generated+proofs-fast

  The proofs have been verified with EasyCrypt commit
  068fdd9c984c344c1b4a44f9f6455f41ee437a30 [1] that was
  configured with the 3 following provers:

    - Alt-Ergo@2.4.2
    - Z3@4.12.2
    - CVC4@1.6

  using Why3@1.7.0
  
[1] https://github.com/EasyCrypt/easycrypt.git#068fdd9c984c344c1b4a44f9f6455f41ee437a30
To install that version of EasyCrypt, follow the instructions at
https://github.com/EasyCrypt/easycrypt
using opam with the command:
opam pin -yn add easycrypt https://github.com/EasyCrypt/easycrypt.git#068fdd9c984c344c1b4a44f9f6455f41ee437a30

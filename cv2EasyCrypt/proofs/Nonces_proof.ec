require import AllCore Bool Int List Distr SmtMap Finite CV2EC.

require (*--*) FinType Word Ring BitWord.
require (*--*) Nonces.

op n : {int | 0 < n} as gt0_n.

clone BitWord as Nonce with
  op n <- n,
  axiom gt0_n <- gt0_n.

type nonce = Nonce.word.

clone BitWord as Nonce2 with
  op n <- 2*n
proof gt0_n by smt(gt0_n).

type nonces = Nonce2.word.

op concat_nonces (ns : nonce * nonce) =
  let (n1,n2) = ns in
  Nonce2.mkword (Nonce.ofword n1 ++ Nonce.ofword n2).

op split_nonces (ns : nonces) =
  let ns = Nonce2.ofword ns in
  (Nonce.mkword (take n ns), Nonce.mkword (drop n ns)).

lemma concatK : cancel concat_nonces split_nonces.
proof.
move => [n1 n2] @/concat_nonces @/split_nonces /=.
rewrite  Nonce2.ofwordK ?size_cat ?Nonce.size_word 1:/#.
rewrite take_size_cat ?drop_size_cat ?Nonce.size_word //=.
by rewrite !Nonce.mkwordK.
qed.

lemma splitK : cancel split_nonces concat_nonces.
proof.
move => ns @/concat_nonces @/split_nonces /=.
rewrite !Nonce.ofwordK.
- rewrite size_takel ?Nonce2.size_word; smt(gt0_n).
- rewrite size_drop ?Nonce2.size_word; smt(gt0_n).
by rewrite cat_take_drop Nonce2.mkwordK.
qed.

clone import Nonces as CV with
  type nonce <- nonce,
  type nonces <- nonces,
  op cat <- concat_nonces
proof*.

realize nonce_fixed.
proof.
split; first by apply: Nonce.is_finite.
exists n; split; first by smt(gt0_n).
by rewrite -Nonce.word_card Nonce.card_size_to_seq.
qed.

realize nonces_fixed.
proof.
split; first by apply: Nonce2.is_finite.
exists (2*n); split; first by smt(gt0_n).
by rewrite -Nonce2.word_card Nonce2.card_size_to_seq.
qed.

realize cat_inj by apply: can_inj concatK.

module On1 = OL_n1.RO.
module On2 = OL_n2.RO.
module On = OR_n.RO.

lemma nonce_fin : finite_type<:nonce>.
proof.
apply fixed_fin.
by apply nonce_fixed.
qed.

lemma nonces_fin : finite_type<:nonces>.
proof.
apply fixed_fin.
by apply nonces_fixed.
qed.

equiv eq_LHS_RHS (A <: Adversary{-LHS,-RHS,-On1,-On2,-On}) :
   Game(LHS,A).main ~ Game(RHS,A).main : ={glob A} ==> ={glob A,res}.
proof.
proc; inline*.
call (: (dom On1.m = dom On2.m){1}
     /\ (dom On1.m){1} = (dom On.m){2}
     /\ ={m_r_i, m_Ononces}(LHS,RHS)
     /\ (forall i, i \in LHS.m_r_i{1} =>
          (oget On.m.[i]){2} = (concat_nonces(oget On1.m.[i],oget On2.m.[i])){1})
     /\ (forall i, i \notin LHS.m_r_i{1} =>
          i \notin On1.m{1} /\ i \notin On2.m{1} /\ i \notin On.m{2}  )
     /\ (forall i, i \in LHS.m_r_i{1} =>
          i \in On1.m{1} /\ i \in On2.m{1} /\ i \in On.m{2}  )
     ).
- proc. if; 1: smt(); 2: by auto.
  case (i{1} \in LHS.m_r_i{1}).
  + inline*.
    rcondf{1} ^if; 1: by auto => /#.
    rcondf{1} ^if; 1: by auto => /#.
    rcondf{2} ^if; 1: by auto => /#.
    swap{1} 1 6; swap{2} 1 3; wp; conseq (:_ ==> true); 1:smt(mem_set get_setE).
    islossless; smt(duni_ll nonce_fin nonces_fin).
  inline*.
  rcondt{1} ^if; 1: by auto => /#.
  rcondt{1} ^if; 1: by auto => /#.
  rcondt{2} ^if; 1: by auto => /#.
  swap{1} 4 -3 ; swap{1} 8 -6; swap{2} 4 -3.
  wp.
  conseq (: _ ==> r{2} = concat_nonces(r{1},r0{1})).
  + move => />; smt(mem_set get_setE).
  rnd concat_nonces split_nonces : *0 *0.
  skip; rewrite !andaE => /> &1 &2 *; do ! split.
  + smt(concatK splitK).
  + move => n. rewrite dmap_id.
    rewrite -(dmap_dprodE duni duni (fun x => x)) dmap_id => Hn.
    rewrite !mu1_uni ?dprod_uni ?duni_uni Hn /= ifT.
    - by rewrite dprod_fu_auto ?duni_full &(nonce_fin).
    rewrite weight_dprod !duni_ll.
    - by apply: nonces_fin. - by apply: nonce_fin.
    rewrite size_dprod ?finite_duni !supp_duni.
    - by apply: nonces_fin. - by apply: nonce_fin.
    rewrite -Nonce.card_size_to_seq -Nonce2.card_size_to_seq.
    smt(Nonce.word_card Nonce2.word_card gt0_n exprD_nneg).
  + move => n1 n2; rewrite dmap_id; smt(nonces_fin duni_full concatK splitK).
- proc; inline*; sp.
  if; 1,3: by auto => /#.
  rcondf{1} ^if; 1: by auto => /#.
  rcondf{1} ^if; 1: by auto => /#.
  rcondf{2} ^if; 1: by auto => /#.
  auto. rnd{1}. auto. rnd{1}. rnd{2}.
  auto => />; smt(duni_ll nonce_fin nonces_fin).
auto => />; smt(emptyE).
qed.

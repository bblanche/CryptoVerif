(* -------------------------------------------------------------------- *)
type 'a pp = Format.formatter -> 'a -> unit

(* -------------------------------------------------------------------- *)
module Format : sig
  include module type of Format with type formatter = Format.formatter

  val tsfprintf :
    formatter -> ('a, formatter, unit) format -> 'a

  val pp_print_list :
    sep:(unit, formatter, unit) format ->
    (formatter -> 'a -> unit) -> formatter -> 'a list -> unit
end

(* -------------------------------------------------------------------- *)
val pp_term     : Types.term     pp
val pp_mode     : Types.mode     pp
val pp_eqmember : Types.eqmember pp

(* -------------------------------------------------------------------- *)
open Types                      (* Cryptoverif AST *)
open Set

module Format = Dump.Format     (* Format extended with list formatting *)

(* -------------------------------------------------------------------- *)
let last xs = List.hd (List.rev xs)
let belast xs = List.rev (List.tl (List.rev xs))

(* -------------------------------------------------------------------- *)
module GenComparable(X : sig type t end)
  : OrderedType with type t = X.t
= struct
  type t = X.t

  let compare : t -> t -> int = Stdlib.compare
end

(* -------------------------------------------------------------------- *)
type etype =
  | T_Bool
  | T_Bitstring                  (* built-in "bitstring" type         *)
  | T_Int       of string option (* "repl index with bound" *)
  | T_User      of string        (* User-declared types from CV     *)
  | T_Option    of etype

(* names, replication indices, and variables *)
(* variables also store the (implicit) replication indices at creation *)
(* TOTHINK: There is some duplication here between [vargs] and [Env.get_binder] *)
type name      = string * int
type replindex = { rname : name; rintv : string; }
type variable  = { vname : name; vtype : etype; vargs: replindex list; }

(* For debugging purposes: *)
let name_to_string (v,_i) =
  if _i = 0 then v else Format.asprintf "%s_%d" v _i
let var_to_string v = name_to_string v.vname

(* -------------------------------------------------------------------- *)
module IntSet = Set.Make(Int)

(* -------------------------------------------------------------------- *)
module StringSet = Set.Make(String)
module StringMap = Map.Make(String)

(* -------------------------------------------------------------------- *)
module NameComparable = GenComparable(struct type t = name end)

module NameSet = Set.Make(NameComparable)
module NameMap = Map.Make(NameComparable)

(* -------------------------------------------------------------------- *)
module NamesComparable = GenComparable(struct type t = name list end)

module NamesSet = Set.Make(NamesComparable)
module NamesMap = Map.Make(NamesComparable)

(* -------------------------------------------------------------------- *)
type cname = name * replindex list

module CNameComparable = GenComparable(struct type t = cname end)

module CNameSet = Set.Make(CNameComparable)
module CNameMap = Map.Make(CNameComparable)

(* ==================================================================== *)
type fmode = [`Direct of replindex list | `Map]
type fkind = [`Direct | `Map]

module FModeComparable = GenComparable (struct type t = fmode end)

module FModeSet = struct
  include Set.Make(FModeComparable)

  let omerge m1 m2 =
    match m1, m2 with
    | None   , None    -> None
    | None   , Some m  -> Some m
    | Some m , None    -> Some m
    | Some m1, Some m2 -> Some (union m1 m2)

  let omerge1 mode m =
    Some (add mode (Option.value ~default:empty m))
end

(* ==================================================================== *)
(* type map_access = (string * int option) option  *)

type defkind =
  [ `OLocal                      (* locally introduced variables *)
  | `OArg of string * int option (* argument variables:
                                    optional int is position in multi-argument tuples *)
  | `Rnd of bool]                (* random variables;
                                    we create an RO if flag is true *)
let is_olocal (k : defkind) =
  match k with `OLocal -> true | _ -> false

let is_oarg (k : defkind) =
  match k with `OArg _ -> true | _ -> false

let is_rnd ?(exported : bool option) (k : defkind) =
  match k, exported with
  | `Rnd _, None   -> true
  | `Rnd b, Some e -> b = e
  | _     , _      -> false

(* ==================================================================== *)
(* Intermediate syntax: Stores the relevant parts of the CV AST
   enriched with all the information required to emit EC code. *)

type expr =
  | EVar   of (string * replindex list) option * name * replindex list
              (* oracle variable is defined in * name of var * ris at point of use *)
  | EApp   of string * expr list
  | EIndex of replindex

and instruction =
  | SFetch     of FModeSet.t NameMap.t                 (* fetch variable from RO *)
  | SAssign    of (variable * replindex list) * expr   (* assignment: v.[i,j] <- e *)
  | SSample    of variable                             (* v <$ duni *)

and find = {
  f_branches : find_branch list;
  f_else     : statement;
  f_info     : find_info;
  }

and find_branch = {
  f_indexes : variable list; (* variables defined in the [then] branch *)
  f_ris     : replindex list; (* corresponding repl-indices for the condition *)
  f_defined : (variable * replindex list) list;
  f_cond    : expr;
  f_then    : statement;
}

and defined = {
  d_defined : (variable * replindex list) list;
  d_cond    : expr;
  d_then    : statement;
  d_else    : statement;
}

and patassign = {
   pa_vars : variable list * string * replindex list;
   pa_expr : expr;
   pa_body : statement;
   pa_else : statement;
  }

and get_pat =
  | GVar  of variable (* get T(v,..) ... *)
  | GCond of expr     (* get T(=t,..) ... *)

and table_insert = {           (* insert t(v1,..,vn) in ... *)
  ti_table : string;
  ti_row   : expr list;
  ti_body  : statement;
}

and table_get = {              (* get t(v1,..,=e) in ... else ... *)
  tg_table   : string;
  tg_context : replindex list;
  tg_pattern : get_pat list;
  tg_cond    : expr option;
  tg_body    : statement;
  tg_else    : statement;
  tg_info    : find_info;
}

and statement =
  | SDone      of expr
  | SSeq       of instruction * statement
  | SIf        of expr * (statement * statement)
  | SFind      of find
  | SDefined   of defined
  | SPatAssign of patassign
  | STabInsert of table_insert
  | STabGet    of table_get * int ref
  | SAbort                               (* bad <- true *)

and equation = {
  eq_forall : variable list;
  eq_cond   : expr;
  eq_lhs    : expr;
  eq_rhs    : expr;
}

(* -------------------------------------------------------------------- *)
type oracle = {
  oname : string;
  octxt : replindex list;
  oargs : variable list;
  ores  : etype;
  obody : statement;
}

(* -------------------------------------------------------------------- *)
type eq_theories =
    NoEq
  | Commut
  | Assoc
  | AssocCommut
  | AssocN of string(*associative function*) * string(*neutral*)
  | AssocCommutN of string(*AC function*) * string(*neutral*)
  | Group of string(*mult*) * string(*inverse*) * string(*neutral*)
  | CommutGroup of string(*mult*) * string(*inverse*) * string(*neutral*)
  | ACUN of string(*xor*) * string(*neutral*)
  | Injective
  | Bijective

type funsig = {
    fname : string;
    fargs : etype list;
    fres  : etype;
    fopts : int;
    feqs  : eq_theories;
}

(* ==================================================================== *)
(* Global environment, collects everything that is needed to print the
   shared prelude: types, operators, replication bounds, ...  *)

module GEnv : sig
  type t

  val create : unit -> t

  val push_ri_bound : t -> string -> unit
  val ri_bounds     : t -> string list

  val push_type      : t -> string -> int -> unit
  val types          : t -> string list
  val types_with_opt : t -> (string * int) list
  val get_typeopts   : t -> string -> int

  val push_func    : t -> funsig -> unit
  val has_func     : t -> string -> bool
  val funcs        : t -> funsig list
  val fun_theories : t -> (string * eq_theories) list

  val push_ctor : t -> funsig -> unit
  val ctors     : t -> funsig list

  val push_rs   : t -> replindex -> unit
  val get_rs    : t -> replindex list

  val push_unchanged : t -> replindex list * variable -> unit
  val get_unchanged  : t -> (replindex list * variable) list

  val set_bitstring : t -> unit
  val uses_bitstring : t -> bool

  val push_equation : t -> equation -> unit
  val get_equations : t -> equation list

end = struct
  type t = {
      mutable ge_ri_bounds : StringSet.t;
      mutable ge_types     : int StringMap.t;
      mutable ge_funcs     : funsig StringMap.t;
      mutable ge_ctors     : funsig StringMap.t;
      mutable ge_rs        : replindex list;
      mutable ge_unchgd    : (replindex list * variable) list;
      mutable ge_bitstring : bool;
      mutable ge_equations : equation list
    }

  let create () =
    { ge_ri_bounds = StringSet.empty;
      ge_types     = StringMap.empty;
      ge_funcs     = StringMap.empty;
      ge_ctors     = StringMap.empty;
      ge_rs        = [];
      ge_unchgd    = [];
      ge_bitstring = false;
      ge_equations = [];
    }

  let push_ri_bound (env : t) (bound : string) =
    env.ge_ri_bounds <- StringSet.add bound env.ge_ri_bounds

  let ri_bounds (env : t) =
    StringSet.elements (env.ge_ri_bounds)

  let push_type (env : t) (name : string) (opts : int) =
    env.ge_types <- StringMap.add name opts env.ge_types

  let types (env : t) =
    List.map fst (StringMap.bindings env.ge_types)

  let types_with_opt (env : t) =
    StringMap.bindings env.ge_types

  let get_typeopts (env : t) (name : string) =
    StringMap.find name env.ge_types

  let has_func (env : t) (name : string) =
    StringMap.mem name env.ge_funcs

  let push_func (env : t) (fs : funsig) =
    if not (has_func env fs.fname) then
      env.ge_funcs <- StringMap.add fs.fname fs env.ge_funcs

  let funcs (env : t) =
    List.map snd (StringMap.bindings env.ge_funcs)

  let fun_theories (env : t) =
    List.filter_map
      (function (s,fs) ->
         if fs.feqs != NoEq then Some(s,fs.feqs) else None)
      (StringMap.bindings env.ge_funcs)

  let has_ctor (env : t) (name : string) =
    StringMap.mem name env.ge_ctors

  let push_ctor (env : t) (fs : funsig) =
    if not (has_ctor env fs.fname) then
      env.ge_ctors <- StringMap.add fs.fname fs env.ge_ctors

  let ctors (env : t) =
    List.map snd (StringMap.bindings env.ge_ctors)

  let push_rs (env : t) (ri : replindex) =
    env.ge_rs <- ri :: env.ge_rs

  let get_rs (env : t) =
    env.ge_rs

  let push_unchanged (env : t) (ctx_v : replindex list * variable) =
    env.ge_unchgd <- ctx_v :: env.ge_unchgd

  let get_unchanged (env : t) =
    List.rev env.ge_unchgd

  let set_bitstring (env : t) = env.ge_bitstring <- true
  let uses_bitstring (env : t) = env. ge_bitstring

  let push_equation (env : t) (eq : equation) =
    env.ge_equations <- eq :: env.ge_equations

  let get_equations (env : t) =
    List.rev env.ge_equations

end

(* ==================================================================== *)
(* Local environment, keeps track of everything that is needed to
   print one side of the equivalence: (global) variables, the current
   scope of replication indices, etc. *)

module Env : sig
  type t

  type bkind = defkind

  val create : GEnv.t -> t

  val genv : t -> GEnv.t

  val push_binder     : t -> bkind -> variable -> unit
  val get_binder      : t -> name -> (variable * replindex list * bkind) option
  val get_binder_kind : t -> name -> bkind option
  val binders         : t -> (variable * replindex list * bkind) list
  val rnds            : ?kind:[`All | `Global | `Local] -> t -> (variable * replindex list * bool) list
  val olocals         : t -> (variable * replindex list) list

  val needmap     : t -> name -> bool
  val set_needmap : t -> name -> unit

  val push_replindex : t -> replindex -> unit
  val pop_replindex  : t -> unit
  val context        : t -> replindex list

  val in_replindex : t -> replindex list -> (t -> 'a) -> 'a

  val in_oracle : t -> string -> (t -> 'a) -> 'a
  val oracle : t -> (string * replindex list) option

  val push_rndseq : t -> replindex list -> variable list -> unit
  val get_rndseqs : ?print:bool -> t -> (replindex list * variable list) list

  val push_table : t -> string -> etype list -> unit
  val tables : t -> (string * etype list) list
  val get_table : t -> string -> etype list option

  val set_abort : t -> unit
  val get_abort : t -> bool

  val set_unique : t -> unit
  val has_unique : t -> bool

  val push_odefcnd : t -> string -> name -> variable list -> unit
  val get_odefcnd  : t -> string -> (name * variable list) list
end = struct
  type bkind = defkind

  type t = {
    (*---*) genv    : GEnv.t;
    mutable binders : (variable * replindex list * bkind) NameMap.t;
    mutable needmap : NameSet.t;
    mutable context : replindex list;
    mutable oracle  : (string * replindex list) option;
    mutable odefcnd : ((name * variable list) list) StringMap.t;
    mutable rndseqs : (replindex list * variable list) list;
    mutable tables  : (etype list) StringMap.t;
    mutable abort   : bool;
    mutable unique  : bool;
  }

  let create (genv : GEnv.t) =
    { genv;
      binders = NameMap.empty;
      needmap = NameSet.empty;
      context = [];
      oracle  = None;
      odefcnd = StringMap.empty;
      rndseqs = [];
      tables  = StringMap.empty;
      abort   = false;
      unique  = false;
    }

  let genv (env : t) =
    env.genv

  let context (env : t) =
    List.rev env.context

  let binders (env : t) =
    List.map snd (NameMap.bindings env.binders)

  let rnds ?(kind = `All) (env : t) =
    let do1 (v, ris, k) =
      match k with
      | `Rnd b ->
         if
           match kind, b with
           | `All   , _     -> true
           | `Global, true  -> true
           | `Local , false -> true
           | _      , _     -> false
         then Some (v, ris, b) else None
      | _ -> None in
    List.filter_map do1 (binders env)

  let olocals (env : t) =
    List.filter_map
      (fun (v, ris, k) ->
        match k with `OLocal -> Some (v, ris) | _ -> None)
      (binders env)

  let needmap (env : t) (x : name) =
    NameSet.mem x env.needmap

  let set_needmap (env : t) (x : name) =
    env.needmap <- NameSet.add x env.needmap

  let push_binder (env : t) (bkind : bkind) (v : variable) =
    env.binders <-
      NameMap.add v.vname (v, context env, bkind) env.binders

  let get_binder (env : t) (v : name) =
    NameMap.find_opt v env.binders

  let get_binder_kind (env : t) (v : name) =
    Option.map (fun (_, _, k) -> k) (get_binder env v)

  let push_replindex (env : t) (ri : replindex) =
    env.context <- ri :: env.context

  let pop_replindex (env : t) =
    assert (env.context <> []);
    env.context <- List.tl env.context

  let in_replindex
    (env : t) (ris : replindex list) (f : t -> 'a)  : 'a
  =
    List.iter (push_replindex env) ris;
    let aout = f env in
    List.iter (fun _ -> pop_replindex env) ris;
    aout

  let in_oracle (env : t) (name : string) (f : t -> 'a) : 'a =
    assert (env.oracle = None);
    env.oracle <- Some (name, List.rev env.context);
    let aout = f env in
    env.oracle <- None; aout

  let oracle (env : t) =
    env.oracle

  let push_table (env : t) (name : string) (signature : etype list) =
    env.tables <- StringMap.add name signature env.tables

  let tables (env : t) =
    StringMap.bindings env.tables

  let get_table (env : t) (name : string) =
    StringMap.find_opt name env.tables

  let push_rndseq (env : t) (prev : replindex list) (seq : variable list) =
    env.rndseqs <- (prev,seq) :: env.rndseqs

  let get_rndseqs ?(print = false) (env : t) =
    if not print then env.rndseqs
    else List.filter (function
              ([],_) -> true
            | (c,_) -> List.mem (last c) (GEnv.get_rs (genv env))) env.rndseqs

  let set_abort (env : t) = env.abort <- true

  let get_abort (env : t) = env.abort

  let set_unique (env : t) = env.unique <- true

  let has_unique (env : t) = env.unique

  let push_odefcnd (env : t) (oracle : string)
        (rv : name) (args : variable list) =
    (* TOTHINK: use a set? *)
    env.odefcnd <-
      StringMap.update oracle (function
          | None -> Some [(rv,args)]
          | Some s -> Some ((rv,args)::s)) env.odefcnd

  let get_odefcnd (env : t) (oracle : string) =
    Option.fold ~none:[] ~some:(fun x -> x) (StringMap.find_opt oracle env.odefcnd)
end

type env = Env.t

(* -------------------------------------------------------------------- *)
(* function symbols that are translated directly to their counterpart in EasyCrypt *)
module Funcs = struct
  let ignore =
    (* The name "" denotes tuples *)
    StringSet.of_list [""; "="; "<>"; "&&"; "||"; "true"; "false"; "not"]

  let is_kept (f : funsymb) =
    not(StringSet.mem f.f_name ignore)
end

(* ==================================================================== *)
let convert_error (msg : string) =
  Parsing_helper.raise_user_error ("EC convert: " ^ msg)

let convert_lerror (msg : string) (ext : Parsing_helper.extent) =
  Parsing_helper.raise_error ("EC convert: " ^ msg) ext

let convert_warning (msg : string) (ext : Parsing_helper.extent) =
  Parsing_helper.input_warning ("EC convert: " ^ msg) ext

(* -------------------------------------------------------------------- *)

let var_of_ri ri = { vname = ri.rname; vtype = T_Int None; vargs = []; }

let convert_range_type (t : typet) =
  match t.tcat with
  | Interv p -> p.pname
  | _ -> assert false

(* -------------------------------------------------------------------- *)
let rec convert_typet ?(intv = false) (typ_ : typet) =
  match typ_.tcat with
  | BitString -> begin
      match typ_.tname with
      | "bool" -> T_Bool
      | "bitstring" -> T_Bitstring
      | "bitstringbot" -> T_Option (T_Bitstring)
      | s -> T_User s
    end

  | Interv p when intv -> T_Int (Some p.pname)

  | Interv _ ->
      convert_error "interval variable not supported at the root of defined conditions, at assignments, ..."

(* -------------------------------------------------------------------- *)
let convert_typet ?intv (env : env) (typ_ : typet) =
  let opts = typ_.toptions in
  let typ_ = convert_typet ?intv typ_ in

  let rec add_decls (typ_ : etype) =
    match typ_ with
    | T_Bool        -> ()
    | T_Int _       -> ()
    | T_Bitstring   -> GEnv.set_bitstring (Env.genv env)
    | T_User s -> GEnv.push_type (Env.genv env) s opts
    | T_Option t    -> add_decls t

  in add_decls typ_; typ_

(* ==================================================================== *)
(* Variable fetching: EasyCrypt does not allow procedure calls within
expressions. Thus, variables that are accessed via procedures (e.g.,
random values stored in random oracles) must be fetched before
evaluating an expression. We implemented as a preprocessing step
before translating an expression *)

(* -------------------------------------------------------------------- *)
(* smart constructor that avoids successive [SFetch] instructions *)
(* TOTHINK: can we avoid `Direct if `Map is used on the containing map? *)
let rec sseq (is : instruction list) s =
  List.fold_right (fun i s ->
    match i, s with
    | SFetch fs1, SSeq (SFetch fs2, s) ->
        SSeq (SFetch (NameMap.merge (fun _ -> FModeSet.omerge) fs1 fs2), s)

    | _ ->
       SSeq (i, s)) is s
(* -------------------------------------------------------------------- *)
type fvar =
  | Find  of bool(*true when the "find" has several branches*) * name list
  | Table of string * int * variable list

module FVarSet = Set.Make(struct
  type t = fvar

  let compare = Stdlib.compare
end)

(* -------------------------------------------------------------------- *)
module FetchEnv : sig
  type fenv

  val create : unit -> fenv
  val next_table_index : fenv -> string -> int
end = struct
  type fenv = {
    mutable fe_tables : int StringMap.t;
  }

  let create () : fenv =
    { fe_tables = StringMap.empty; }

  let next_table_index (env : fenv) (table : string) : int =
    let index = StringMap.find_opt table env.fe_tables in
    env.fe_tables <-
      StringMap.update
        table
        (fun i -> Some (Option.value ~default:0 i + 1))
        env.fe_tables;
    Option.value ~default:0 index
end

(* -------------------------------------------------------------------- *)
type fenv = FetchEnv.fenv

(* -------------------------------------------------------------------- *)
let rec stmt_fetch_fvar (env : fenv) (fs : FVarSet.t) (s : statement) =
  let stmt_fetch_fvar = stmt_fetch_fvar env in

  match s with
  | SDone _ ->
      fs

  | SSeq (_, s) ->
      (* BB: why no fetch in the first arg of SSeq? *)
      stmt_fetch_fvar fs s

  | SIf (_, (s1, s2)) ->
      let fs = stmt_fetch_fvar fs s1 in
      let fs = stmt_fetch_fvar fs s2 in
      fs

  | SFind f ->
      let multiple_branch =
	match f.f_branches with
	| [] | [_] -> false
	| _ -> true
      in
      let fs =
        List.fold_left (fun fs b ->
          let fs = stmt_fetch_fvar fs b.f_then in
          let fs = FVarSet.add (Find (multiple_branch, List.map (fun v -> v.vname) b.f_indexes)) fs in
          fs
        ) fs f.f_branches in
      let fs = stmt_fetch_fvar fs f.f_else in
      fs

  | SDefined f ->
      let fs = stmt_fetch_fvar fs f.d_then in
      let fs = stmt_fetch_fvar fs f.d_else in
      fs

  | SPatAssign pa ->
      let fs = stmt_fetch_fvar fs pa.pa_body in
      let fs = stmt_fetch_fvar fs pa.pa_else in
      fs

  | STabInsert f ->
     stmt_fetch_fvar fs f.ti_body

  | STabGet (get, index) ->
     let vars =
       List.filter_map
         (function GVar x -> Some x | _ -> None)
         get.tg_pattern in
     index := FetchEnv.next_table_index env get.tg_table;
     let fs = (FVarSet.add (Table (get.tg_table, !index, vars)) fs) in
     let fs = stmt_fetch_fvar fs get.tg_body in
     let fs = stmt_fetch_fvar fs get.tg_else in
     fs

  | SAbort -> fs

(* -------------------------------------------------------------------- *)
let filter_fetch_map (env : env) (fs : FModeSet.t NameMap.t) =
  let filter name _ =
    match Env.get_binder_kind env name with
    | None -> false (* indices of find are not pushed; they are not random anyway *)
    | Some k -> is_rnd k
  in NameMap.filter filter fs

(* -------------------------------------------------------------------- *)
let rec stmt_fetch_fmap (env : env) (fs : FModeSet.t NameMap.t) (s : statement) =
  match s with
  | SDone _ ->
      fs

  | SSeq (i, s) ->
      let fs = instr_fetch_fmap env fs i in
      let fs = stmt_fetch_fmap  env fs s in
      fs

  | SIf (_, (s1, s2)) ->
      let fs = stmt_fetch_fmap env fs s1 in
      let fs = stmt_fetch_fmap env fs s2 in
      fs

  | SFind f ->
      let fs =
        List.fold_left
          (fun fs b -> stmt_fetch_fmap env fs b.f_then)
          fs f.f_branches in
      let fs = stmt_fetch_fmap env fs f.f_else in
      fs

  | SDefined f ->
      let fs = stmt_fetch_fmap env fs f.d_then in
      let fs = stmt_fetch_fmap env fs f.d_else in
      fs

  | SPatAssign pa ->
      let fs = stmt_fetch_fmap env fs pa.pa_body in
      let fs = stmt_fetch_fmap env fs pa.pa_else in
      fs

  | STabInsert f ->
     stmt_fetch_fmap env fs f.ti_body

  | STabGet (f, _) ->
      let fs = stmt_fetch_fmap env fs f.tg_body in
      let fs = stmt_fetch_fmap env fs f.tg_else in
      fs

  | SAbort -> fs

(* -------------------------------------------------------------------- *)
and instr_fetch_fmap (env : env) (fs : FModeSet.t NameMap.t) (i : instruction) =
  match i with
  | SAssign _ ->
      fs

  | SSample _ ->
      fs

  | SFetch fs' ->
      let fs' = filter_fetch_map env fs' in
      NameMap.merge (fun _ -> FModeSet.omerge) fs fs'

(* -------------------------------------------------------------------- *)
let rec expr_fetch (fs : CNameSet.t) (e : expr) =
  match e with
  | EVar (_, name, ris) ->
      CNameSet.add (name, ris) fs

  | EApp (_, es) ->
      List.fold_left (fun fs e -> expr_fetch fs e) fs es

  | EIndex _ ->
      fs

(* -------------------------------------------------------------------- *)
let rec expr_fetch_with_mode ?(fs = NameMap.empty) (kind : fkind) (e : expr) =
  let fs0 = expr_fetch CNameSet.empty e in

  match kind with
  | `Map ->
      CNameSet.fold
        (fun (x, _) fs -> NameMap.update x (FModeSet.omerge1 `Map) fs)
        fs0 fs

  | `Direct ->
      CNameSet.fold
        (fun (x, ris) fs -> NameMap.update x (FModeSet.omerge1 (`Direct ris)) fs)
        fs0 fs

(* ==================================================================== *)
(* Helper functions for the conversion of binders, replication
   indices, function symbols, etc. *)

(* -------------------------------------------------------------------- *)
let name_of_binder (binder : binder) : name =
  (binder.sname, binder.vname)

(* -------------------------------------------------------------------- *)
let convert_replindex (ri : repl_index) =
  let rname = (ri.ri_sname, ri.ri_vname) in
  let rintv = convert_range_type ri.ri_type in
  { rname; rintv; }

(* -------------------------------------------------------------------- *)
let convert_binder ?intv (env : env) (binder : binder) =
  let vname = (binder.sname, binder.vname) in
  let vtype = convert_typet ?intv env binder.btype in
  let vargs = List.map convert_replindex binder.args_at_creation in
  { vname; vtype; vargs }

(* -------------------------------------------------------------------- *)
let convert_term_ri (ri : term) =
  match ri.t_desc with
  | ReplIndex ri ->
      convert_replindex ri

  | Var (b, idx) ->
      if not (Terms.is_args_at_creation b idx) then
	convert_lerror "nested array references not supported" ri.t_loc;
      { rname = name_of_binder b;
        rintv = convert_range_type ri.t_type; }

  | _ ->
      assert false

(* -------------------------------------------------------------------- *)
(* indices to variables that are oracle arguments *)
let check_index_args (env : env) (vris : term list) =
  let for1 (vr : term) =
    match vr.t_desc with
    | Var (b,_) -> begin
       let v = convert_binder ~intv:true env b in
       match Env.get_binder_kind env v.vname with
       | None ->
          (* TOTHINK: indices generated by "find i <= ..." are not pushed *)
          (* Not a problem here, as they wouldn't have kind `OArg *)
          (* Format.printf "var arg w/o kind: %s@\n" (var_to_string v); *)
          None
       | Some k -> if is_oarg k then Some v else None
      end
    | _ -> None
  in
  List.filter_map for1 (List.rev vris)

(* -------------------------------------------------------------------- *)

let rec convert_funsymb (new_symb_opt : funsymb list ref option) (env : env) (f0 : funsymb) : string =
  let f = Terms.unalias f0 in
  let convert_eq_theories (env : env) (eq : Types.eq_th) : eq_theories * funsymb list =
    match eq with
    | NoEq -> (NoEq, [])
    | Commut -> (Commut, [])
    | Assoc -> (Assoc, [])
    | AssocCommut -> (AssocCommut, [])
    | AssocN(f, n) -> (AssocN(f.f_name, n.f_name), [n])
    | AssocCommutN(f, n) -> (AssocCommutN(f.f_name, n.f_name), [n])
    | Group(f, inv, n) -> (Group(f.f_name, inv.f_name, n.f_name), [f; inv; n])
    | CommutGroup(f, inv, n) -> (CommutGroup(f.f_name, inv.f_name, n.f_name), [f; inv; n])
    | ACUN(f, n) -> (ACUN(f.f_name, n.f_name), [n])
  in
  if not (StringSet.mem f.f_name Funcs.ignore) then
    if not (GEnv.has_func (Env.genv env) f.f_name) then begin
        let fargs = List.map (convert_typet env) (fst f.f_type) in
        let fres  = convert_typet env (snd f.f_type) in
        let (feqs,fsbs)  = convert_eq_theories env f.f_eq_theories in
        let fsig  = { fargs; fres; fname = f.f_name; fopts = f.f_options; feqs = feqs } in
        GEnv.push_func (Env.genv env) fsig;
	match new_symb_opt with
	| None -> ()
	| Some new_symb ->
	    new_symb := f :: (!new_symb);
            List.iter (fun f -> let _ = convert_funsymb new_symb_opt env f in ()) fsbs;
      end;
  f.f_name

(* -------------------------------------------------------------------- *)
let rec convert_expr (new_symb_opt : funsymb list ref option) (env : env) (t : term) =
  match t.t_desc with
  | Var (b, vris) ->
      let name = name_of_binder b in
      let ris  = List.map convert_term_ri vris in

      (* Check if variable is applied to indices passed as `OArg *)
      (* If so, record oracle, variable and indices for definedness check *)
      (* This code makes sense only if either all indices are oracle
	 arguments or none of them. This is guaranteed by CryptoVerif. *)
      let vargs = check_index_args env vris in
      if not (vargs = []) then
        begin
          let oname,oris = Option.get (Env.oracle env) in
          Env.push_odefcnd env oname name vargs
        end;

      EVar (Env.oracle env, name, List.rev ris)

  | FunApp (f, es) ->
      let es = List.map (convert_expr new_symb_opt env) es in
      EApp (convert_funsymb new_symb_opt env f, es)

  | ReplIndex ri ->
      EIndex (convert_replindex ri)

  | _ ->
      convert_lerror "Please use only functions, variables, and replication indices in terms" t.t_loc

(* -------------------------------------------------------------------- *)
let convert_equation (new_symb : funsymb list ref) (env : env) (c : collision) =
  let eqf = List.map (fun b -> convert_binder ~intv:false env b) c.c_forall in
  let eqc = convert_expr (Some new_symb) env c.c_side_cond in
  let eql = convert_expr (Some new_symb) env c.c_redl in
  let eqr = convert_expr (Some new_symb) env c.c_redr in
  {eq_forall = eqf; eq_cond = eqc; eq_lhs = eql; eq_rhs = eqr;}

(* -------------------------------------------------------------------- *)
let convert_binderref (env : env) ((b, ris) : binderref) =
  let b    = convert_binder env b in
  let ris  = List.map convert_term_ri ris in
  (b, ris)

(* -------------------------------------------------------------------- *)
let pattern_as_patvar (b : pattern) =
  match b with
  | PatVar x -> x
  | _ -> convert_error "let f(p1,...,pn) = ... supported only when p1,...,pn are variables"

(* ==================================================================== *)
(* First main function: conversion of oracle bodies (i.e. statements    *)

(* -------------------------------------------------------------------- *)
let rec convert_statement (env : env) (t : term) =
  match t.t_desc with
  | FunApp _ | Var _ ->
      let e = convert_expr None env t in
      sseq [SFetch (expr_fetch_with_mode `Direct e)] (SDone e)

  (* if defined(...) then ... else ... *)
  | FindE ([([], d_defined, d_cond, d_then)], d_else, _) ->
      let d_defined = List.map (convert_binderref env) d_defined in
      let d_cond    = convert_expr None env d_cond in
      let d_then    = convert_statement env d_then in
      let d_else    = convert_statement env d_else in

      let fs =
        let fs = expr_fetch_with_mode `Map d_cond in
        List.fold_left (fun fs (x, _) ->
          NameMap.update x.vname (FModeSet.omerge1 `Map) fs
        ) fs d_defined
      in

      begin
        let vars = expr_fetch CNameSet.empty d_cond in
        List.iter (fun (x, _) -> Env.set_needmap env x.vname) d_defined;
        CNameSet.iter (fun (x, _) -> Env.set_needmap env x) vars
      end;

      sseq [SFetch fs] (SDefined { d_defined; d_cond; d_then; d_else; })

  (* find ... suchthat ... then ... [orfind ...] ... else *)
  | FindE (branches, f_else, find_info) ->
      (* Game contains [unique] tag *)
      if find_info = Unique then Env.set_unique env;

      let for_branch (bris, f_defined, f_cond, f_then) =
        let bs, bris  = List.split bris in
        let f_indexes = List.map (convert_binder ~intv:true env) bs in
        let f_ris     = List.map convert_replindex bris in
        let f_defined = List.map (convert_binderref env) f_defined in
        let f_cond    = convert_expr None env f_cond in
        let f_then    = convert_statement env f_then in
        { f_indexes; f_ris; f_defined; f_cond; f_then; } in

      let f_branches = List.map for_branch branches in
      let f_else     = convert_statement env f_else in

      let fs =
        let for_branch fs b =
          let fs = expr_fetch_with_mode `Map ~fs b.f_cond in
          List.fold_left (fun fs (x, _) ->
            NameMap.update x.vname (FModeSet.omerge1 `Map) fs
          ) fs b.f_defined
        in

        List.fold_left for_branch NameMap.empty f_branches
      in

      List.iter (fun { f_defined = defs; f_cond = cond; }->
        let vars = expr_fetch CNameSet.empty cond in
        List.iter (fun (x, _) -> Env.set_needmap env x.vname) defs;
        CNameSet.iter (fun (x, _) -> Env.set_needmap env x) vars
      ) f_branches;

     let find =
       { f_branches = f_branches;
         f_else     = f_else;
         f_info     = find_info;
       } in

      sseq [SFetch fs] (SFind find)

  | LetE (PatVar b, e1, e2, _(*else branch can be ignored*)) ->
      let b = convert_binder env b in

      Env.push_binder env `OLocal b;

      let e1 = convert_expr None env e1 in
      let s2 = convert_statement env e2 in

      sseq [
        SFetch (expr_fetch_with_mode `Direct e1);
        SAssign ((b, Env.context env), e1)
      ] s2

  | LetE (PatTuple (name, bs), t, body, None) ->
      assert (name.f_options land Settings.fopt_BIJECTIVE != 0);
      let bs = List.map pattern_as_patvar bs in
      let bs = List.map (convert_binder env) bs in

      List.iter (Env.push_binder env `OLocal) bs;
	
      let t     = convert_expr None env t     in
      let body  = convert_statement env body  in
      let projs = Settings.get_proj name      in
      
      sseq
        ((SFetch (expr_fetch_with_mode `Direct t)) ::
        (List.map2 (fun proj b -> SAssign ((b, Env.context env), EApp (convert_funsymb None env proj, [t]))) projs bs))
	body

  | LetE (PatTuple (name, bs), (t as cond), body, Some telse) ->
      let name = Terms.unalias name in
      let bs = List.map pattern_as_patvar bs in
      let bs = List.map (convert_binder env) bs in

      List.iter (Env.push_binder env `OLocal) bs;

      let t     = convert_expr None env t     in
      let body  = convert_statement env body  in
      let telse = convert_statement env telse in

      GEnv.push_ctor (Env.genv env) {
        fname = name.f_name;
        fargs = List.map (fun v -> v.vtype) bs;
        fres  = convert_typet env cond.t_type;
        fopts = 0;
        feqs  = NoEq
      };

      sseq [
          SFetch (expr_fetch_with_mode `Direct t);
      ] (SPatAssign {
           pa_vars = (bs, name.f_name, Env.context env);
           pa_expr = t;
           pa_body = body;
           pa_else = telse;
        })

  | LetE(PatEqual teq, e1, tthen, Some telse) ->
      convert_statement env
	(Terms.build_term t (TestE(Terms.make_equal teq e1, tthen, telse)))

  | LetE(_,_,_,None) -> assert false

  | TestE (cond, ift, iff) ->
      let cond = convert_expr None env cond in
      let ift  = convert_statement env ift in
      let iff  = convert_statement env iff in

      sseq [
        SFetch (expr_fetch_with_mode `Direct cond)
      ] (SIf (cond, (ift, iff)))

  | ResE (b, t) ->
      let b = convert_binder env b in
      Env.push_binder env (`Rnd false) b;
      sseq [
        SSample b
      ] (convert_statement env t)

  | InsertE (tab,row,tbody) ->
     let row_t = List.map (convert_typet ~intv:true env) tab.tbltype in
     let row   = List.map (convert_expr None env) row in
     let body  = convert_statement env tbody in

     (* remember encountered a table *)
     Env.push_table env tab.tblname row_t;

     sseq
       (List.map (fun e -> SFetch (expr_fetch_with_mode `Direct e)) row)
       (STabInsert {
         ti_table = tab.tblname;
         ti_row = row;
         ti_body = body;
       })

  | GetE (tab, pats, None, tbody, telse, find_info) ->
     (* Game contains [unique] tag *)
     if find_info = Unique then Env.set_unique env;

     let row_t = List.map (convert_typet ~intv:true env) tab.tbltype in

     (* check for binder/condition patterns *)
     let pts =
       let for1 (p : pattern) =
         match p with
         | PatVar v ->
            let v = convert_binder ~intv:true env v in
           Env.push_binder env `OLocal v;
            GVar v

         | PatEqual e ->
            GCond (convert_expr None env e)

         | _ -> convert_lerror "constructor pattern not supported in get. You can try the command-line option -EasyCryptRemoveTables to convert get into find first." t.t_loc
       in
       List.map for1 pats
     in
     (* fetch variables for condition patterns *)
     let fetch =
       let for1 (p : get_pat) =
         match p with
         | GCond e -> Some (SFetch (expr_fetch_with_mode `Direct e))
         | _ -> None
       in
       List.filter_map for1 pts
     in
     let body = convert_statement env tbody in
     let telse = convert_statement env telse in

     (* remember encoutered table *)
     Env.push_table env tab.tblname row_t;

     let get =
       { tg_table   = tab.tblname;
         tg_context = Env.context env;
         tg_pattern = pts;
         tg_cond    = None;
         tg_body    = body;
         tg_else    = telse;
         tg_info    = find_info;
       } in

     sseq fetch (STabGet (get, ref 0))

  | GetE(tab, pats, Some _, tbody, telse, _) ->
      convert_lerror "conditions not supported in get. You can try the command-line option -EasyCryptRemoveTables to convert get into find first." t.t_loc
  | EventAbortE _ ->
     Env.set_abort env;
     SAbort

  | EventE _ | ReplIndex _ ->
      Format.eprintf "%a@." Dump.pp_term t;
      assert false

(* ==================================================================== *)
(* Conversion of the fungroup prelude (replication and random sequences *)

(* -------------------------------------------------------------------- *)


let rec convert_fungroup_prelude (env : env) (fg : fungroup) =
  match fg with
  (* [foreach <ri> do] <binders> ; <fgs> *)
  (* <binders> is the, possibly empty, list of random samplings for this <ri> *)
  (* The <ri> may be missing at the top level *)
  | ReplRestr (ri, rnds, fgs) -> begin
      (* record the bound of <ri> in global env *)
      let ri = Option.map convert_replindex ri in
      Option.iter
        (fun x -> GEnv.push_ri_bound (Env.genv env) x.rintv)
        ri;

      (* Move underneath the <ri> to handle the sequence of random samplings *)
      Env.in_replindex env (Option.to_list ri) begin fun env ->
        (* convert binders and record [unchanged] tags (for [computational] equivs *)
        let rnds =
          List.map
            (fun (b,_,opt) ->
              let rv = convert_binder env b in
              if opt = Unchanged then GEnv.push_unchanged (Env.genv env) (Env.context env,rv);
              rv) rnds
        in
        (* push individual random variables into env to create ROs *)
        List.iter (Env.push_binder env (`Rnd true)) rnds;
        (* push the full sequence, to create sampling procs for adversary *)
        Env.push_rndseq env (Env.context env) rnds;

        (* Convert remaining fungroups *)
        List.flatten (List.map (convert_fungroup_prelude env) fgs)
      end
    end

  | Fun (o, binders, body, _) ->
      let oargs = List.map (convert_binder ~intv:true env) binders in

      begin match oargs with
      | [oarg] ->
          Env.push_binder env (`OArg (o.oname, None)) oarg
      | _ ->
          List.iteri
            (fun i -> Env.push_binder env (`OArg (o.oname, Some i)))
            oargs
      end;

      [((o.oname, oargs, body), Env.context env)]

(* -------------------------------------------------------------------- *)
let convert_fungroup (env : env) (fg : fungroup) =
  let oracles = convert_fungroup_prelude env fg in

  let for1 ((oname, oargs, body), ris) =
    Env.in_replindex env ris (fun env ->
      Env.in_oracle env oname (fun env ->
        let octxt = Env.context env in
        let obody = convert_statement env body in
        let ores  = convert_typet env body.t_type in
        { oname; octxt; ores; oargs; obody; })) in

  List.map for1 oracles

(* -------------------------------------------------------------------- *)
(* Top-level conversion function (eqmember = LHS/RHS of equiv           *)

let convert_eqmember (env : env) (eqm : eqmember) =
  let ora =
    List.map (fun (fg, _) -> convert_fungroup env fg) eqm
  in List.flatten ora

(* ==================================================================== *)
(* Pretty printing of the intermediate data structures (global
   environment, local environment, and list of oracles) as an
   EasyCrypt theory (i.e., a string written to a file *)

module PP = struct
  (* ------------------------------------------------------------------ *)
  let pp_string fmt (s : string) = Format.fprintf fmt "%s" s

  (* ------------------------------------------------------------------ *)
  let name_to_string ((v, _i) : name) =
    if _i = 0
    then v
    else Format.asprintf "%s_%d" v _i

  (* ------------------------------------------------------------------ *)
  let oracle_modtype_name = "Oracles"
  let oracle_modtype_init_name = "Oracles_i"
  let abort_name = "abort"
  let unique_name = "not_unique"
  let find_success_list_name = "find_success_list"
  let find_chosen_branch_name = "find_chosen_branch"
  let find_chosen_idx_name = "find_chosen_idx"

  let reserved_ids = [ oracle_modtype_name; oracle_modtype_init_name;
		       abort_name; unique_name; find_success_list_name;
		       find_chosen_branch_name; find_chosen_idx_name;
		       "list"; "fmap";
		       "unit"; "int"; "bool"; "bitstring"; "option" ;
		       "duni"; "oget"; "encode" ; "aout"; "iota_";
		       "allpairs"; "List"; "witness"; "drat";
		       "is_some"; "row"; "Some"; "None"; "fixed_type";
		       "finite_type"; "countableT"; "lossless";
		       "distr"; "associative"; "commutative";
		       "neutral"; "group"; "cancel"; "injective";
		       "LHS"; "RHS"; "LHS_"; "RHS_"; "unchanged";
		       "init"; "empty"; "LHS_RHS"; "RHS_LHS";
		       "islossless"; "A_ll"; "A_guess_ll" ]

  let easycrypt_kws = [ "forall"; "exists"; "fun"; "glob"; "let";
			"in"; "for"; "var"; "is"; "match"; "proc";
			"if"; "then"; "else"; "elif"; "while";
			"assert"; "return"; "res"; "equiv"; "hoare";
			"ehoare"; "choare"; "cost"; "phoare";
			"islossless"; "async"; "axiom"; "schema";
			"axiomatized"; "lemma"; "realize"; "proof";
			"qed"; "abort"; "goal"; "end"; "from";
			"import"; "export"; "include"; "local";
			"declare"; "hint"; "nosmt"; "module"; "of";
			"const"; "op"; "pred"; "inductive";
			"notation"; "abbrev"; "require"; "theory";
			"abstract"; "section"; "type"; "class";
			"instance"; "instantiate"; "print"; "search";
			"locate"; "as"; "Pr"; "clone"; "with";
			"rename"; "prover"; "timeout"; "why3"; "dump";
			"remove"; "exit"; "time"; "undo"; "debug";
			"pragma"; "Top"; "Self"; "beta"; "iota";
			"zeta"; "eta"; "logic"; "delta"; "simplify";
			"cbv"; "congr"; "change"; "split"; "left";
			"right"; "case"; "pose"; "gen"; "cut"; "have";
			"suff"; "elim"; "exlim"; "ecall"; "clear";
			"wlog"; "apply"; "rewrite"; "rwnormal";
			"subst"; "progress"; "trivial"; "auto";
			"idtac"; "move"; "modpath"; "field";
			"fieldeq"; "ring"; "ringeq"; "algebra";
			"replace"; "transitivity"; "symmetry"; "seq";
			"wp"; "sp"; "sim"; "skip"; "call"; "rcondt";
			"rcondf"; "swap"; "cfold"; "rnd"; "rnd_sem";
			"pr_bounded"; "bypr"; "byphoare"; "byehoare";
			"byequiv"; "fel"; "conseq"; "exfalso";
			"inline"; "interleave"; "alias"; "fission";
			"fusion"; "unroll"; "splitwhile"; "kill";
			"eager"; "try"; "first"; "last"; "do";
			"strict"; "expect"; "exact"; "assumption";
			"smt"; "by"; "reflexivity"; "done"; "solve";
			"admit"; "admitted" ]

  let init_used_ids = StringSet.of_list (reserved_ids @ easycrypt_kws)

  type first_char =
    | Lowercase
    | Uppercase
    | Anycase

  let fix_case fc s =
    match fc with
    | Anycase -> s
    | Lowercase -> String.uncapitalize_ascii s
    | Uppercase -> String.capitalize_ascii s

  let char_converter =
    "@@@@@@@@@@@@@@@@" ^
    "@@@@@@@@@@@@@@@@" ^
    "@@@@@@@'@@@@@_@@" ^
    "0123456789@@@@@@" ^
    "@ABCDEFGHIJKLMNO" ^
    "PQRSTUVWXYZ@@@@_" ^
    "@abcdefghijklmno" ^
    "pqrstuvwxyz@@@@@" ^
    "@@@@@@@@@@@@@@@@" ^
    "@@@@@@@@@@@@@@@@" ^
    "@@@@@@@@@@@@@@@@" ^
    "@@@@@@@@@@@@@@@@" ^
    "AAAAAAACEEEEIIII" ^
    "DNOOOOO@OUUUUYPS" ^
    "aaaaaaaceeeeiiii" ^
    "dnooooo@ouuuuypy"
   
  let remove_accents_char c =
    let c' = char_converter.[int_of_char c] in
    assert (c' <> '@'); 
    c'
	  
  let remove_accents s = String.map remove_accents_char s 

  let start_letter s =
    if String.length s = 0 || ('0' <= s.[0] && s.[0] <= '9') then "n"^s else s
      
  (* [sanitize fc code l] is guaranteed to be injective in the pair [(code,l)],
     to return a string that starts with a character of case specified by
     [first_char] and using ASCII letters, digits, _, and ',
     and not to return a string in [init_used_ids].
     If possible, it returns the concatenation of the strings in [l],
     with accents removed and with the first character changed to the case
     according to [fc].
     Otherwise, it modifies that string by changing the suffix _nnn if
     it has one, or by adding such a suffix. *)

  module IntStringListComparable = GenComparable(struct type t = int * string list end)

  module IntStringListMap = Map.Make(IntStringListComparable)
	
  let id_map = ref (IntStringListMap.empty)
  let used_ids = ref init_used_ids
      
  let sanitize (fc : first_char) (code : int) (l : string list) =
    try
      IntStringListMap.find (code, l) (!id_map)
    with Not_found ->
      let default = fix_case fc (start_letter (String.concat "" (List.map remove_accents l))) in
      let rec fresh s = 
	if StringSet.mem s (!used_ids) then
	  let (s0,n) = Terms.get_id_n s in
          fresh (name_to_string (s0,n+1))
	else
	  s
      in
      let result = fresh default in
      used_ids := StringSet.add result (!used_ids);
      id_map := IntStringListMap.add (code, l) result (!id_map);
      result
	
  let get_sanitize_state() = (!id_map, !used_ids)

  let set_sanitize_state (id_map',used_ids') =
    id_map := id_map';
    used_ids := used_ids'

  (* ------------------------------------------------------------------ *)
  let bound_name (x : string) =
    sanitize Lowercase 1 ["b_"; x]

  (* ------------------------------------------------------------------ *)
  let ro_name (v : name) =
    sanitize Uppercase 2 ["O_"; name_to_string v]

  (* ------------------------------------------------------------------ *)
  let ro_sig_name (modname : string) (v : name) =
    (* [modname] can be "L" or "R" *)
    sanitize Uppercase 3 ["O"; modname; "_"; name_to_string v]

  (* ------------------------------------------------------------------ *)
  let var_name (v : name) =
    sanitize Lowercase 4 [name_to_string v]

  (* ------------------------------------------------------------------ *)
  let rec form_name_list = function
    | [] -> []
    | [v] -> [name_to_string v]
    | v::r -> (name_to_string v)::"_"::(form_name_list r)

  let vars_name (vs : name list) =
    sanitize Lowercase 4 (form_name_list vs)
      (* use the same code as in [var_name], because when the list
	 contains a single variable name, we want the generated name
	 to be the same as in [var_name] *)
      
  let vars_name_list (vs : name list) =
    sanitize Lowercase 6 ((form_name_list vs) @ ["_list"])

  (* ------------------------------------------------------------------ *)
  let tmp_var_name (v : name) =
    sanitize Lowercase 7 ["tmp_"; name_to_string v]

  (* ------------------------------------------------------------------ *)
  let tmp_map_name (v : name) =
    sanitize Lowercase 8 ["tmp_map_"; name_to_string v]

  (* ------------------------------------------------------------------ *)
  let local_rnd_map_name (v : name) =
    sanitize Lowercase 9 ["lr_"; name_to_string v]

  (* ------------------------------------------------------------------ *)
  let var_fetch ((v, ris) : name * replindex list) =
    let ri_names = List.map (fun ri -> ri.rname) ris in
    sanitize Lowercase 10 ("tmp_"::(form_name_list (v::ri_names)))
      
  (* ------------------------------------------------------------------ *)
  let vmap_name (v : name) =
    sanitize Lowercase 11 ["v_"; name_to_string v]

  (* ------------------------------------------------------------------ *)
  let omap_name (v : string) =
    sanitize Lowercase 12 ["m_"; v]

  (* ------------------------------------------------------------------ *)
  let ri_suffix (ri_opt : replindex option) =
    Option.fold ~none:"" ~some:(fun ri -> "_" ^ (var_name ri.rname)) ri_opt

  let rmap_name_ri (ri_opt : replindex option) =
    sanitize Lowercase 13 ["m_r"; ri_suffix ri_opt]

  let rproc_name_ri (ri_opt : replindex option) =
    sanitize Lowercase 14 ["r"; ri_suffix ri_opt]

  let last_opt (ctx : replindex list) =
    List.nth_opt ctx (max 0 (List.length ctx - 1))
      
  let rmap_name (ctx : replindex list) =
    rmap_name_ri (last_opt ctx)

  let rproc_name (ctx : replindex list) =
    rproc_name_ri (last_opt ctx)
      
  (* ------------------------------------------------------------------ *)
  let type_name (s : string) =
    sanitize Anycase 15 [s]

  (* ------------------------------------------------------------------ *)
  let fun_name (s : string) =
    sanitize Anycase 16 [s]
      
  (* ------------------------------------------------------------------ *)
  let get_as_name (s : string) =
    sanitize Lowercase 17 ["get_as_";s]
      
  (* ------------------------------------------------------------------ *)
  let all_distinct = ref 100

  (* Make sure that all axiom names are distinct, by using a distinct
     value of the counter. This is ok because we call [axiom_name]
     once for each axiom, when it is generated. *)
  let axiom_name (l : string list) =
    incr all_distinct;
    sanitize Anycase (!all_distinct) l

  (* ------------------------------------------------------------------ *)
  let distrib_name (s : string) =
    sanitize Lowercase 19 ["d"; s]
      
  (* ------------------------------------------------------------------ *)
  let oracle_name (v : string) =
    sanitize Lowercase 20 ["p_"; v]

  (* ------------------------------------------------------------------ *)
  let table_name (v : string) =
    sanitize Lowercase 21 ["t_"; v]

  (* ------------------------------------------------------------------ *)
  let table_rows_name (v : string) (i : int) =
    sanitize Lowercase 22 ["r_"; string_of_int i; "_"; v]

  (* -------------------------------------------------------------------- *)
  let ri_map_key (ris : replindex list) =
    match ris with
    | []   -> "()"
    | [ri] -> var_name ri.rname
    | _    ->
        Format.sprintf "(%s)"
          (String.concat ", "
             (List.map (fun ri -> var_name ri.rname) ris))

  (* -------------------------------------------------------------------- *)
  let ri_arg_key (ris : replindex list) =
    String.concat ", "
       (List.map (fun ri -> var_name ri.rname) ris)

  (* -------------------------------------------------------------------- *)
  let rnd_getter (x : name) =
    Format.sprintf "%s.get" (ro_name x)

  (* -------------------------------------------------------------------- *)
  let rnd_map_getter (x : name) =
    Format.sprintf "%s.restrK" (ro_name x)

  (* ------------------------------------------------------------------ *)
  let rec pp_type fmt (t : etype) =
    match t with
    | T_Int _       -> Format.fprintf fmt "%s" "int"
    | T_Bool        -> Format.fprintf fmt "%s" "bool"
    | T_Bitstring   -> Format.fprintf fmt "%s" "bitstring"
    | T_User x      -> Format.fprintf fmt "%s" (type_name x)
    | T_Option x    -> Format.fprintf fmt "%a option" pp_type x

  (* ------------------------------------------------------------------ *)
  let pp_type_tuple fmt ?(paren = false) (ts : etype list) =
    if ts = [] then
      Format.fprintf fmt "%s" "unit"
    else
      Format.fprintf fmt "%s%a%s"
        (if paren then "(" else "")
        (Format.pp_print_list ~sep:" * " pp_type) ts
        (if paren then ")" else "")

  (* ------------------------------------------------------------------ *)
  let pp_sig fmt ((args, res) : etype list * etype) =
    if args = [] then
      Format.fprintf fmt "%a" pp_type res
    else
      Format.fprintf fmt "%a -> %a"
        (fun fmt -> pp_type_tuple fmt ~paren:false) args
        pp_type res

  (* ------------------------------------------------------------------ *)
  let pp_ctor_get_sig fmt ((args, res) : etype list * etype) =
    Format.fprintf fmt "%a -> (%a) option"
      pp_type res
      (Format.pp_print_list ~sep: " * " pp_type) args

  let pp_default_distr fmt ((env,v) : env * variable) =
    match v.vtype with
    | T_User x when
      (GEnv.get_typeopts (Env.genv env) x land Settings.tyopt_NONUNIFORM != 0)
      ->
	Format.fprintf fmt "%s" (distrib_name x)
    | T_Int _ | T_Option _ | T_Bitstring ->
	assert false
    | _ -> 
	Format.fprintf fmt "duni<:%a>"  pp_type v.vtype

  (* ------------------------------------------------------------------ *)
  let rec pp_expr ?(direct = false) (env : Env.t) fmt (e : expr) =
    let pp_expr = pp_expr ~direct in

    match e with
    | EVar (ctxt, name, ris) -> begin
        let eqctxt oname =
          match ctxt with
          | Some (oname', oris) when oname = oname' ->
              let oris = List.map (fun ri -> ri.rname) oris in
              let  ris = List.map (fun ri -> ri.rname)  ris in
              oris = ris
		
          | _ -> false in
	
        match Env.get_binder_kind env name with
	| None -> Format.fprintf fmt "%s" (var_name name)
	| Some k -> 
          match k with
          | `OArg (oname, _) when eqctxt oname ->
              Format.fprintf fmt "%s" (var_name name)
		
          | `OLocal ->
              Format.fprintf fmt "(oget %s.[%s])" (vmap_name name) (ri_map_key ris)
		
          | `OArg (oname, None) ->
              Format.fprintf fmt
                "(oget %s.[%s])" (omap_name oname) (ri_map_key ris)
		
          | `OArg (oname, Some i) ->
              Format.fprintf fmt
                "(oget %s.[%s]).`%d" (omap_name oname) (ri_map_key ris) (i+1)

          | `Rnd false ->
              Format.fprintf fmt "(oget %s.[%s])"
                (local_rnd_map_name name) (ri_map_key ris)
		
          | `Rnd true when direct ->
              Format.fprintf fmt "(oget %s.[%s])"
                (tmp_map_name name) (ri_map_key ris)
		
          | `Rnd true ->
              Format.fprintf fmt "%s" (var_fetch (name, ris))
    end
	  
    | EApp ("", es) -> (* the empty function denotes a tuple *)
        Format.fprintf fmt "encode (%a)"
          (Format.pp_print_list ~sep:", " (pp_expr env)) es
	  
    | EApp (f, []) ->
        Format.fprintf fmt "%s" (fun_name f)
	  
    | EApp (("=" | "<>" | "&&" | "||") as op, [e1; e2]) ->
        Format.fprintf fmt "(%a %s %a)"
          (pp_expr env) e1 op (pp_expr env) e2
	  
    | EApp (("not"), [e]) ->
        Format.fprintf fmt "%s%a"
          "!" (pp_expr env) e
	  
    | EApp (f, args) ->
        Format.fprintf fmt "%s (%a)" (fun_name f)
          (Format.pp_print_list ~sep:", " (pp_expr env)) args

    | EIndex ri ->
        Format.fprintf fmt "%s" (var_name ri.rname)


    (* ------------------------------------------------------------------ *)

  let pp_defcond env fmt defined_cond = 
    let for1 fmt (v, ris) =
      let map_name = 
	match Option.get (Env.get_binder_kind env v.vname) with
	| `OLocal -> vmap_name v.vname
	| `OArg (oname, _) -> omap_name oname
	| `Rnd true -> tmp_map_name v.vname
	| `Rnd false -> local_rnd_map_name v.vname
      in
      Format.fprintf fmt "%s \\in %s" (ri_map_key ris) map_name
    in
    if defined_cond = [] then
      Format.pp_print_string fmt "true"
    else
      Format.pp_print_list ~sep:" /\\ " for1 fmt defined_cond
    
    (* ------------------------------------------------------------------ *)

  let rec extract_let fmt (var_from, vars_to) =
    match vars_to with
    | [] -> ()
    | v::r ->
	Format.fprintf fmt "@\nlet %s = List.head witness %s in" v var_from;
	if r <> [] then
	  begin
	    Format.fprintf fmt "@\nlet %s = List.behead %s in" var_from var_from;
	    extract_let fmt (var_from, r)
	  end

  let rec extract_assign fmt (var_from, vars_to) =
    match vars_to with
    | [] -> ()
    | v :: r ->
	Format.fprintf fmt "@\n%s <- List.head witness %s;" v var_from;
	if r <> [] then
	  begin
	    Format.fprintf fmt "@\n%s <- List.behead %s;" var_from var_from;
	    extract_assign fmt (var_from, r)
	  end
	    
    (* ------------------------------------------------------------------ *)
  let rec pp_stmt (env : Env.t) fmt (s : statement) =
    match s with
    | SDone e ->
        Format.fprintf fmt "@\naout <- %a;" (pp_expr env) e
	  
    | SSeq (i, s) ->
        Format.fprintf fmt "%a%a" (pp_instr env) i (pp_stmt env) s
	  
    | SIf (c, (ift, iff)) ->
        Format.fprintf fmt
          "@\n@[<v 2>if (%a) {%a@;<0 -2>} else {%a@;<0 -2>}@]"
          (pp_expr env) c (pp_stmt env) ift (pp_stmt env) iff
	  
    | SFind f ->
	begin
	  match f.f_branches with
	  | [] ->
	      (* find without any "then" branch -> execute the "else" branch *)
	      pp_stmt env fmt f.f_else
	  | [b] -> begin
	      (* find with a single "then" branch *)
              let indices = List.map (fun v -> v.vname) b.f_indexes in
              let bounds = List.map (fun ri -> ri.rintv) b.f_ris in
              let rindices = List.map (fun ri -> ri.rname) b.f_ris in
	      
              let ll_name = vars_name_list indices in
	      
              let iota1 bound =
                Format.sprintf "iota_ 1 %s" (bound_name bound) in
	      
              let rec allpairs bounds =
                match bounds with
                | [] ->
		    (* find with a single branch and no index is translated
		       into [SDefined], so cannot occur here. *)
                    assert false
		      
                | [bound] ->
                    0, iota1 bound
		      
                | bound :: bounds ->
                    let i, bounds = allpairs bounds in
                    let bound = iota1 bound in
                    let mapap =
                      match i with
                      | _ when i <= 0 ->
                          "fun (x y : int) => (x, y)"
			    
                      | _ ->
                          Format.sprintf
                            "fun (x : int) (y : %s) => (x, %s)"
                            (String.concat " * " (List.init (i+1) (fun _ -> "int")))
                            (String.concat ", " (List.init (i+1) (fun j -> Format.sprintf "y.`%d" (j+1))))

                    in (i+1, Format.sprintf "allpairs (%s) (%s) (%s)" mapap bound bounds)
              in
	      
              let allpairs bounds = snd (allpairs bounds) in
	      
              Format.fprintf fmt
                "@\n@[<hov 2>%s <- List.filter (fun %s =>@;%t(%a) /\\@ (%a))@ (%s);@]"
                ll_name
                (vars_name rindices)
                (fun fmt ->
                  if List.length indices > 1 then
                    Format.fprintf fmt "let (%s) = %s in "
                      (String.concat ", " (List.map var_name rindices))
                      (vars_name rindices))
                (pp_defcond env) b.f_defined
                (pp_expr ~direct:true env) b.f_cond
                (allpairs bounds);

              if f.f_info = Unique
              then
                Format.fprintf fmt
                  "@\n@[<v 2>if (%s = []) {%a@]@;@[<v 2>} elif (List.behead %s = []) {%t@]@;@[<v 2>} else { %s <- true; }@]"
                  ll_name (pp_stmt env) f.f_else ll_name
                  (fun fmt ->
                    let lv = String.concat ", " (List.map var_name indices) in
                    let lv = if List.length indices > 1 then "(" ^ lv ^ ")" else lv in
                    Format.fprintf fmt "@\n%s <- List.head witness %s;%a"
                      lv ll_name (pp_stmt env) b.f_then)
                  unique_name
              else
                Format.fprintf fmt
                  "@\n@[<v 2>if (%s = []) {%a@;<0 -2>} else {%t@;<0 -2>}@]"
                  ll_name (pp_stmt env) f.f_else
                  (fun fmt ->
                    let lv = String.concat ", " (List.map var_name indices) in
                    let lv = if List.length indices > 1 then "(" ^ lv ^ ")" else lv in
                    Format.fprintf fmt "@\n%s <$ drat %s;%a"
                      lv ll_name (pp_stmt env) b.f_then)
          end

	  | branches -> begin
	      (* find with several branches *)
	      let one_branch_list branch_num fmt b =
		let bounds = List.map (fun ri -> ri.rintv) b.f_ris in
		let rindices = List.map (fun ri -> ri.rname) b.f_ris in

		let iota1 bound =
                  Format.sprintf "iota_ 1 %s" (bound_name bound) in

		let rec allpairs bounds =
                  match bounds with
                  | [] ->
		      "[[]]"
                  | bound :: bounds ->
                      let bounds = allpairs bounds in
                      let bound = iota1 bound in
                      Format.sprintf "allpairs (::) (%s) (%s)" bound bounds
		in

		Format.fprintf fmt
                  "@\n@[<hov 2>(List.map (fun l => (%d, l)) (List.filter (fun %s =>%t@;(%a) /\\@ (%a))@ (%s)))@]"
		  branch_num
                  (if List.length rindices >= 1 then vars_name rindices else "_")
                  (fun fmt ->
                    if List.length rindices >= 1 then
		      extract_let fmt (vars_name rindices,
				       List.map var_name rindices))
                  (pp_defcond env) b.f_defined
                  (pp_expr ~direct:true env) b.f_cond
                  (allpairs bounds)

	      in

	      let rec build_branches branch_num fmt = function
		| [] -> assert false
		| [b1] ->
		    one_branch_list branch_num fmt b1
		| b1::r ->
		    one_branch_list branch_num fmt b1;
		    Format.fprintf fmt " ++ ";
		    build_branches (branch_num+1) fmt r
	      in
	      Format.fprintf fmt "@\n@[<hov 2>%s <- %a;@]"
		find_success_list_name
		(build_branches 1) branches;

	      let rec choose_branch branch_num fmt = function
		| [] -> assert false
		| [b1] ->
		    (* This branch is always preceded by other branches,
		       so "se" completes "el" into "else" *)
		    Format.fprintf fmt "se {%a%a@]@\n}"
		      extract_assign (find_chosen_idx_name,
				      List.map (fun v -> var_name v.vname) b1.f_indexes)
		      (pp_stmt env) b1.f_then
		| b1::r ->
		    Format.fprintf fmt "if (%s = %d) {%a%a@]@\n@[<hov 2>} el"
		      find_chosen_branch_name branch_num
		      extract_assign (find_chosen_idx_name,
				      List.map (fun v -> var_name v.vname) b1.f_indexes)
		      (pp_stmt env) b1.f_then;
		    (* "el" is completed into either "elif" or "else" by the next branch *)
		    choose_branch (branch_num+1) fmt r
	      in

	      if f.f_info = Unique
              then
                Format.fprintf fmt
                  "@\n@[<v 2>if (%s = []) {%a@]@;@[<v 2>} elif (List.behead %s = []) {%t@]@;@[<v 2>} else { %s <- true; }@]"
                  find_success_list_name (pp_stmt env) f.f_else find_success_list_name
                  (fun fmt ->
                    Format.fprintf fmt "@\n(%s,%s) <- List.head witness %s;@\n@[<hov 2>%a"
                      find_chosen_branch_name find_chosen_idx_name find_success_list_name
		      (choose_branch 1) branches)
                  unique_name
              else
                Format.fprintf fmt
                  "@\n@[<v 2>if (%s = []) {%a@;<0 -2>} else {%t@;<0 -2>}@]"
                  find_success_list_name (pp_stmt env) f.f_else
                  (fun fmt ->
                    Format.fprintf fmt "@\n(%s,%s) <$ drat %s;@\n@[<hov 2>%a"
                      find_chosen_branch_name find_chosen_idx_name find_success_list_name
                      (choose_branch 1) branches)
	  end
	end
	  
    | SDefined f ->
        Format.fprintf fmt
          "@\n@[<v 2>if ((%a) /\\ (%a)) {%a@;<0 -2>} else {%a@;<0 -2>}@]"
          (pp_defcond env) f.d_defined
          (pp_expr ~direct:true env) f.d_cond
          (pp_stmt env) f.d_then
          (pp_stmt env) f.d_else

    | SPatAssign {
      pa_vars = (vs, name, ris);
        pa_expr = e;
        pa_body = body;
        pa_else = telse;
      } -> begin
        let pp_assign fmt =
          match vs with
          | [] ->
              ()
		
          | [v] ->
              Format.fprintf fmt "@\n%s.[%s] <- oget (%s (%a));"
                (vmap_name v.vname)
                (ri_map_key ris)
                (get_as_name name)
                (pp_expr env) e
		
          | _ ->
              Format.fprintf fmt "@\n%a"
                (Format.pp_print_list ~sep:"@\n"
                   (fun fmt (index, v) ->
                     Format.fprintf fmt "%s.[%s] <- (oget (%s (%a))).`%d;"
                       (vmap_name v.vname)
                       (ri_map_key ris)
                       (get_as_name name)
                       (pp_expr env) e
                       (1+index)))
                (List.mapi (fun i v -> (i, v)) vs)
        in
	
        Format.fprintf fmt
          "@\n@[<v 2>if (is_some (%s (%a))) {%t%a@;<0 -2>} else {%a@;<0 -2>}@]"
          (get_as_name name) (pp_expr env) e pp_assign (pp_stmt env) body (pp_stmt env) telse

      end
	  
    | STabInsert ins ->
        Format.fprintf fmt
          "@\n%s <- (%a) :: %s;"
          (table_name ins.ti_table)
          (Format.pp_print_list ~sep:", " (pp_expr env))
          ins.ti_row
          (table_name ins.ti_table);
        Format.fprintf fmt "%a" (pp_stmt env) ins.ti_body
	  
    | STabGet (get, { contents = index }) ->
        let binders = List.mapi (fun i p -> (i, p)) get.tg_pattern in
        let table = Option.get (Env.get_table env get.tg_table) in
	
        let vars = List.filter_map (fun (i, p) ->
          match p with GVar v -> Some (i, v) | _ -> None
              ) binders in
	
        let conds = List.filter_map (fun (i, p) ->
          match p with GCond e -> Some (i, e) | _ -> None
              ) binders in
	
        let vars_tuple =
          match vars with
          | [] ->
	      (* no variables bound in the pattern list *)
              convert_error "get without variable not supported. You can try the command-line option -EasyCryptRemoveTables to convert get into find first."

          | [_, v] ->
              var_name v.vname

          | _ ->
              Format.sprintf "(%s)"
                (String.concat
                   ", "
                   (List.map (fun (_, v) -> var_name v.vname) vars)) in

        let pp_lets fmt =
          let pp_let fmt (i, v) =
            match table with
            | [] | [_] ->
                Format.fprintf fmt "let %s = row in" (var_name v.vname)
            | _ ->
                Format.fprintf fmt "let %s = row.`%d in" (var_name v.vname) (i+1) in
	  
          Format.fprintf fmt "%a"
            (Format.pp_print_list ~sep:" " pp_let) vars in
	
        let pp_pat_conds fmt =
          let pp_pat_cond fmt (i, e) =
             Format.fprintf fmt "row.`%d = %a" (i+1) (pp_expr env) e in
          Format.fprintf fmt "%a"
            (Format.pp_print_list ~sep:" /\\ " pp_pat_cond)
            conds in
	
        let pp_cond fmt =
          match get.tg_cond with
          | None ->
              Format.fprintf fmt "true"
		
          | Some e ->
              Format.fprintf fmt "%a" (pp_expr env) e in

        Format.fprintf fmt
          "@\n@[<hov 2>%s <- List.pmap (fun row : %a => %t@ if (%t /\\ %t) then Some %s else None)@ %s;@]"
          (table_rows_name get.tg_table index)
          (Format.pp_print_list ~sep:" * " pp_type)
          table
          pp_lets
          pp_pat_conds
          pp_cond
          vars_tuple
          (table_name get.tg_table);
	
        let pp_sample fmt =
          Format.fprintf fmt
            "@\n%s <$ drat %s;"
            vars_tuple
            (table_rows_name get.tg_table index);
          List.iter
            (fun (_, v) ->
              Format.fprintf fmt "@\n%s.[%s] <- %s;"
                (vmap_name v.vname)
                (ri_map_key get.tg_context)
                (var_name v.vname))
            vars
        in
	
        if get.tg_info = Unique
        then
          let row_name = table_rows_name get.tg_table index in
          Format.fprintf fmt
            "@\n@[<v 2>if (%s = []) {%a@]@;@[<v 2>} elif (List.behead %s = []) {%t%a@]@;@[<v 2>} else { %s <- true; }@]"
            row_name
            (pp_stmt env) get.tg_else
            row_name
            pp_sample
            (pp_stmt env) get.tg_body
            unique_name
        else
          Format.fprintf fmt
            "@\n@[<v 2>if (%s = []) {%a@;<0 -2>} else {%t%a@;<0 -2>}@]"
            (table_rows_name get.tg_table index)
            (pp_stmt env) get.tg_else
            pp_sample
            (pp_stmt env) get.tg_body

    | SAbort ->
        Format.fprintf fmt "@\n%s <- true;" abort_name

    (* ------------------------------------------------------------------ *)
  and pp_instr (env : Env.t) fmt (i : instruction) =
    match i with
    | SAssign ((v, ris), e) ->
        Format.fprintf fmt "@\n%s.[%s] <- %a;"
          (vmap_name v.vname)
          (ri_map_key ris)
          (pp_expr env) e
	  
    | SSample b ->
        let v, ris, mode = Option.get (Env.get_binder env b.vname) in
        assert (mode = `Rnd false);
        Format.fprintf fmt "@\n%s <$ %a;@\n%s.[%s] <- %s;"
          (var_fetch (v.vname, ris))
          pp_default_distr (env,v)
          (local_rnd_map_name v.vname)
          (ri_map_key ris)
           (var_fetch (v.vname, ris))
	  
    | SFetch fs ->
        let fs = filter_fetch_map env fs in
        let fs = NameMap.bindings fs in
        let fs = List.flatten (List.map (fun (x, ms) ->
          List.map (fun m -> (x, m)) (FModeSet.elements ms)
            ) fs) in
        let fs = List.filter (fun (x, _) ->
          let _, _, kind = Option.get (Env.get_binder env x) in
          kind = `Rnd true
            ) fs in
	
        let pp_direct fmt (x, ris) =
          Format.fprintf fmt "%s <%@ %s(%s);"
            (var_fetch (x, ris)) (rnd_getter x)
            (ri_arg_key ris)
	    
        and pp_map fmt x =
          Format.fprintf fmt "%s <%@ %s();"
            (tmp_map_name x) (rnd_map_getter x)
        in
	
        let pp fmt (x, mode) =
          match mode with
          | `Direct ris -> pp_direct fmt (x, ris)
          | `Map        -> pp_map    fmt x
        in
	
        if fs <> [] then
          Format.fprintf fmt "@\n%a" (Format.pp_print_list ~sep:"@\n" pp) fs

  (* ------------------------------------------------------------------ *)
  let pp_prelude fmt (env : GEnv.t) =
    List.iter
      (fun line -> Format.fprintf fmt "%s@." line)
      [ "require import AllCore SmtMap List Distr Finite Discrete CV2EC."
      ; "require (*--*) PROM."
      ; "(*---*) import Distr.MRat."
      ; ""
      ];

    if GEnv.uses_bitstring env then
      Format.fprintf fmt "clone import Bitstring.@.@.";

    if GEnv.ri_bounds env <> [] then begin
      let pp_bound (name : string) =
        Format.fprintf fmt "op %s : int.@." (bound_name name)
      in
        List.iter pp_bound (GEnv.ri_bounds env);
        Format.fprintf fmt "@."
    end;

    if GEnv.types env <> [] then begin
      let pp_tydecl (name : string) =
        Format.fprintf fmt "type %s.@." (type_name name)
      in
        List.iter pp_tydecl (GEnv.types env);
        Format.fprintf fmt "@."
      end;

    if GEnv.types_with_opt env <> [] then begin
      let pp_axioms ((name,opts) : string * int) =
        if (opts land Settings.tyopt_FIXED != 0) then
          Format.fprintf fmt "axiom %s : fixed_type<:%s>.@." (axiom_name [name; "_fixed"]) (type_name name) 
        else if (opts land Settings.tyopt_BOUNDED != 0) then
          Format.fprintf fmt "axiom %s : finite_type<:%s>.@." (axiom_name [name; "_fin"]) (type_name name)
	else
	  Format.fprintf fmt "axiom %s : countableT<:%s>.@." (axiom_name [name; "_countable"]) (type_name name);
	(* In EasyCrypt, all types are non-empty by default, as
	   in CryptoVerif. *)
        if (opts land Settings.tyopt_NONUNIFORM != 0) then
          Format.fprintf fmt "op [lossless] %s : %s distr.@." (distrib_name name) (type_name name)
      in
        List.iter pp_axioms (GEnv.types_with_opt env);
        Format.fprintf fmt "@."
      end;

    if GEnv.funcs env <> [] then begin
      let builtin = ref [] in
      let joint   = ref [] in
      let pp_fs (fs : funsig) =
        Format.fprintf fmt "op %s : %a.@."
          (fun_name fs.fname) pp_sig (fs.fargs, fs.fres);
	if (fs.fopts land Settings.fopt_BIJECTIVE != 0) then
	  builtin := (fs.fname,Bijective) :: !builtin
        else if (fs.fopts land Settings.fopt_COMPOS != 0) then
          builtin := (fs.fname,Injective) :: !builtin;
        if (fs.feqs <> NoEq) then
          builtin := (fs.fname, fs.feqs) :: !builtin
      in
      let pp_builtin ((fname, eq) : string * eq_theories) =
        let pp_assoc s =
          Format.fprintf fmt "axiom %s : associative %s.@."
	    (axiom_name [s;"A"]) (fun_name s) in
        let pp_comm s =
          Format.fprintf fmt "axiom %s : commutative %s.@."
	    (axiom_name [s;"C"]) (fun_name s) in
        let pp_neutral f n =
          Format.fprintf fmt "axiom %s : neutral %s %s.@."
	    (axiom_name ["neutral_"; f; "_"; n]) (fun_name f) (fun_name n) in
        let pp_group f inv n =
          Format.fprintf fmt "axiom %s : group %s %s %s.@."
            (axiom_name ["group_"; f; "_"; inv; "_"; n]) (fun_name f)
	    (fun_name inv) (fun_name n) in
	let pp_cancel f n =
          Format.fprintf fmt "axiom %s : cancel %s %s.@."
	    (axiom_name ["cancel_"; f; "_"; n]) (fun_name f) (fun_name n) in
        match eq with
        | NoEq -> assert false
        | Assoc -> pp_assoc fname
        | Commut -> pp_comm fname
        | AssocCommut -> pp_assoc fname; pp_comm fname
        | Injective ->
            Format.fprintf fmt "axiom %s : injective %s.@."
              (axiom_name [fname; "_inj"]) (fun_name fname)
	| Bijective ->
            Format.fprintf fmt "axiom %s : bijective %s.@."
              (axiom_name [fname; "_bij"]) (fun_name fname)
        | _ when not (List.mem eq !joint) ->
           joint := eq :: !joint;
           begin match eq with
           | AssocN(f, n) -> pp_assoc f; pp_neutral f n
           | AssocCommutN(f,n) -> pp_assoc f; pp_comm f; pp_neutral f n
           | Group(f,inv,n) -> pp_group f inv n
           | CommutGroup(f,inv,n) -> pp_comm f; pp_group f inv n
           | ACUN(f,n) -> pp_assoc f; pp_comm f; pp_neutral f n; pp_cancel f n
           | _ -> assert false
           end
        | _ -> ()
      in
        List.iter pp_fs (GEnv.funcs env);
        Format.fprintf fmt "@.";
        if (!builtin <> []) then begin
          List.iter pp_builtin (List.rev !builtin);
          Format.fprintf fmt "@.";
        end;
      end;

    if GEnv.get_equations env <> [] then begin
      let pp_eq ((name,eq) : string * equation) =
        let env = Env.create env in
        let pp_forall fmt =
          let for1 fmt v =
            Format.fprintf fmt "(%s : %a)" (var_name v.vname) pp_type v.vtype
          in
          if eq.eq_forall <> [] then
            Format.fprintf fmt "forall %a,@ "
              (Format.pp_print_list ~sep:" " for1) eq.eq_forall in
        let pp_cond fmt =
          let prem = Format.asprintf "%a" (pp_expr env) eq.eq_cond in
          if prem <> "true" then
            Format.fprintf fmt "%a =>" (pp_expr env) eq.eq_cond in
	let ax_name = axiom_name ["eq_"; name] in
	let san_state = get_sanitize_state() in
	(* Since the function symbols are declared before, the only
	   new identifiers created here are the bound variables.
	   They are local, so we can reuse them later by removing
	   them from the sanitize state. *)
        Format.fprintf fmt "@[<hov 2>axiom %s : %t%t@[<hv>%a =@ %a@].@]@."
	  ax_name
          pp_forall pp_cond
          (pp_expr env) eq.eq_lhs
          (pp_expr env) eq.eq_rhs;
	set_sanitize_state san_state
      in
      (* try to get sensible names for equations *)
      let get_head_symbol (eq: equation) : string option =
        match eq.eq_lhs with
          EApp (s, _) when not(StringSet.mem s Funcs.ignore) -> Some s
        | _ -> None in
      let neqns = List.sort
          (function (n1,_) -> function (n2,_) -> compare n1 n2)
          (List.map (fun eq -> (get_head_symbol eq,eq))
                    (GEnv.get_equations env)) in
      (* obtain distinct and valid names for all equations *)
      let rec rename (xs : (string option * equation) list) n =
        match xs with
        | (None,eq1)::(((None,eq2)::_) as xr)->
           (string_of_int n,eq1)::rename xr (n+1)
        | (None,eq1)::xr->
           (string_of_int n,eq1)::rename xr 1
        | (Some s1,eq1)::(((Some s2,eq2)::_) as xr) when s1 = s2 ->
           (s1^"_"^string_of_int n,eq1)::rename xr (n+1)
        | (Some s1,eq1)::xr->
           let sn = if n > 1 then "_"^string_of_int n else "" in
           (s1^sn,eq1)::rename xr 1
        | [] -> []
      in
      List.iter pp_eq (rename neqns 1);
        Format.fprintf fmt "@."
    end;

    if GEnv.ctors env <> [] then begin
      let pp_ctor (ctor : funsig) =
        Format.fprintf fmt "op %s : %a.@."
          (get_as_name ctor.fname) pp_ctor_get_sig (ctor.fargs, ctor.fres);
	(* Axiom get_as_f(f(x1,...,xn)) = Some(x1,...,xn) *)
	let vars = List.mapi (fun i t -> (var_name ("x",i+1), t)) ctor.fargs in
        let pp_forall fmt =
          let for1 fmt (v,t) =
            Format.fprintf fmt "(%s : %a)" v pp_type t
          in
          if ctor.fargs <> [] then
            Format.fprintf fmt "forall %a,@ "
              (Format.pp_print_list ~sep:" " for1) vars in
	let pp_args fmt =
	  let arg1 fmt (v,t) = Format.fprintf fmt "%s" v in
	  Format.pp_print_list ~sep:", " arg1 fmt vars in
        Format.fprintf fmt "@[<hov 2>axiom %s : %t@ %s(%s(%t)) = Some (%t).@]@."
	  (axiom_name ["ax1"; "get_as_"; ctor.fname]) 
	  pp_forall
	  (get_as_name ctor.fname)
	  (fun_name ctor.fname)
	  pp_args
	  pp_args;
	(* Axiom let y = get_as_f(x) in
	   if is_some(y)
	   then f((oget y).`1,...,(oget y).`n) = x
	   else forall x1,...,xn, f(x1,...,xn) <> x *)
	let x = var_name ("x",0) in
	let y = var_name ("y",0) in
	let indices = List.mapi (fun i _ -> i+1) ctor.fargs in
	let pp_oget fmt =
	  match indices with
	  | [] -> ()
	  | [_] ->
	      (* When [ctor] has a single argument, [oget y] is not
		 a tuple, it is directly the argument of [ctor] *)
	      Format.fprintf fmt "oget %s" y
	  | _ -> 
	      let arg1 fmt i = Format.fprintf fmt "(oget %s).`%d" y i in
	      Format.pp_print_list ~sep:", " arg1 fmt indices
	in
        Format.fprintf fmt "@[<v 2>axiom %s : forall (%s : %a), @;let %s = %s(%s) in @;if is_some(%s) @;then %s(%t) = %s @;else %t %s(%t) <> %s.@]@."
	  (axiom_name ["ax2"; "get_as_"; ctor.fname])
	  x pp_type ctor.fres
	  y (get_as_name ctor.fname) x 
	  y
	  (fun_name ctor.fname) pp_oget x
	  pp_forall (fun_name ctor.fname) pp_args x
      in
        List.iter pp_ctor (GEnv.ctors env);
        Format.fprintf fmt "@."
    end


  (* ------------------------------------------------------------------ *)
  let pp_unchanged_type fmt (unchanged : (replindex list * variable) list) =
    let for1 fmt (ctx,v) =
      Format.fprintf fmt "(%a, %t) fmap"
        (pp_type_tuple ~paren:false)
        (List.map (fun _ -> T_Int None) ctx)
        (fun fmt -> pp_type fmt v.vtype)
    in
    Format.pp_print_list ~sep:" * " for1 fmt unchanged


  (* ------------------------------------------------------------------ *)
  let pp_eqmember fmt (ppmtypes : bool) (name : string) (env, lenv, oracles) =

    let mod_name =
      Format.sprintf "%sHS" name in
      
    let rec pp_ro (v, ris, _) =
      Format.fprintf fmt "\
clone PROM.FullRO as %s with
  type in_t    <- %a,
  type out_t   <- %a,
  op   dout    <- fun _ => %a,
  type d_in_t  <- unit,
  type d_out_t <- bool.@\n@."

        (* RO name *)
        (ro_sig_name name v.vname)
        (* in_t *)
        (pp_type_tuple ~paren:false) (List.map (fun _ -> T_Int None) ris)
        (* out_t *)
        pp_type v.vtype
        (* dout *)
        pp_default_distr (env, v)

    and pp_modtypes fmt =
      let pp_rndseqs_access fmt =
        let for1 (context, rnds) =
          Format.fprintf fmt "@\nproc %s(%s) : unit"
            (rproc_name context)
            (match context with
             | [] -> ""
             | _  -> Format.asprintf "_ : %a"
                       (pp_type_tuple ~paren:false)
                       (List.map (fun _ -> T_Int None) context))
        in List.iter for1 (List.rev (Env.get_rndseqs ~print:true env))

      and pp_ora_access fmt =
        let for1 (o : oracle) =
          let riargs = List.map var_of_ri o.octxt in

          let args = riargs @ o.oargs in

          Format.fprintf fmt "@\nproc %s(%a) : %a"
            (oracle_name o.oname) pp_args args pp_type o.ores
        in List.iter for1 oracles

      and pp_unchanged_access fmt =
        let unchanged = GEnv.get_unchanged (Env.genv env) in
        if unchanged = [] then () else
          Format.fprintf fmt "@;  proc unchanged() : %a"
            pp_unchanged_type unchanged
      in

      if ppmtypes then begin
        Format.fprintf fmt
          "@[<v 2>module type %s = {%t%t@;<0 -2>}.@]@\n@."
            oracle_modtype_name
            pp_rndseqs_access
            pp_ora_access;

        Format.fprintf fmt "\
module type %s = {
  proc init() : unit%t
  include %s
}.@."
          oracle_modtype_init_name
          pp_unchanged_access
          oracle_modtype_name
      end

    and pp_mod_params fmt =
      let for1 fmt (v, ris, _) =
        (* When printing the LHS, we still need to check whether the
           random variable was tagged as [unchanged] in the RHS *)
        let is_unchanged =
          List.exists (fun (_,v') -> v = v') (GEnv.get_unchanged (Env.genv env)) in

        Format.fprintf fmt "(%s : %s.RO%s)"
          (ro_name v.vname)
          (ro_sig_name name v.vname)
          (if is_unchanged || Env.needmap env v.vname then "map" else "") in

      List.iter (Format.fprintf fmt " %a" for1) (Env.rnds ~kind:`Global env)

    and pp_args fmt args =
      let pp_arg fmt v =
        Format.fprintf fmt "%s : %a"
          (var_name v.vname) pp_type v.vtype in

      Format.fprintf fmt "%a"
        (Format.pp_print_list ~sep:", " pp_arg) args

    and pp_var_maps fmt =
      let for1 fmt (v, ris) =
        Format.fprintf fmt "var %s : (%a, %a) fmap"
          (vmap_name v.vname)
          (pp_type_tuple ~paren:false) (List.map (fun _ -> T_Int None) ris)
          pp_type v.vtype in

      List.iter (Format.fprintf fmt "@\n%a" for1) (Env.olocals env)

    and pp_abort_var fmt =
      if Env.get_abort env then
        Format.fprintf fmt "@\nvar %s : bool" abort_name;
      if Env.has_unique env then
        Format.fprintf fmt "@\nvar %s : bool" unique_name


    and pp_local_rnd_map fmt =
      let for1 fmt (x, ris, _) =
        Format.fprintf fmt "var %s : (%a, %a) fmap"
          (local_rnd_map_name x.vname)
          (pp_type_tuple ~paren:false) (List.map (fun _ -> T_Int None) ris)
          pp_type x.vtype in

      List.iter (Format.fprintf fmt "@\n%a" for1) (Env.rnds ~kind:`Local env)

    and pp_tables fmt =
      let for1 fmt (table, types) =
        Format.fprintf fmt "var %s : (%a) list"
          (table_name table)
          (Format.pp_print_list ~sep:" * " pp_type)
          types in

      List.iter (Format.fprintf fmt "@\n%a" for1) (Env.tables env)

    and pp_rnd_maps fmt =
      let for1 fmt (context,rnds) =
        Format.fprintf fmt "var %s : (%a, unit) fmap"
          (rmap_name context)
          (pp_type_tuple ~paren:false) (List.map (fun _ -> T_Int None) context)
        in
      List.iter (Format.fprintf fmt "@\n%a" for1) (Env.get_rndseqs ~print:true env)

    and pp_oracle_maps fmt =
      let for1 fmt o =
        Format.fprintf fmt "var %s : (%a, %a) fmap"
        (omap_name o.oname)
        (pp_type_tuple ~paren:false) (List.map (fun _ -> T_Int None) o.octxt)
        (pp_type_tuple ~paren:false) (List.map (fun x -> x.vtype) o.oargs)

      in List.iter (Format.fprintf fmt "@\n%a" for1) oracles

    and tuple_var ?(paren = false) v =
      match List.map (fun x -> var_name x.vname) v with
      | []  -> "()"
      | [x] -> if paren then "("^x^")" else x
      | xs  -> Format.asprintf "(%s)" (String.concat ", " xs)

    and pp_rndseqs fmt =
      let print_rs = (GEnv.get_rs (Env.genv env)) in
      let for1 fmt (context,rnds) =
        let rvis = List.map var_of_ri context in
        (* check, whether there is a previous rndseq,
           possibly with the empty context *)
        let ctx = List.filter (fun ri -> List.mem ri print_rs) context in
        let rnd0 = List.exists (function ([],_) -> true | _ -> false) (Env.get_rndseqs env) in
        let prnd =
          (* top-level rndseq never has a preceding rndseq *)
          if context = [] then []
          (* if there is no more ri, check for a top-level rndseq *)
          else if belast ctx = [] then (if rnd0 then [None] else [])
          (* take the previous rndseq *)
          else [Some (last (belast ctx))]
        in

        let pp_body fmt =
          let pp_body_cond fmt =
            let for1 fmt ri =
              match ri with
                `Rnd ri ->
                 Format.fprintf fmt "1 <= %s <= %s"
                   (var_name ri.rname) (bound_name ri.rintv)
              | `Rmap ((Some p) as ri_opt) ->
                 Format.fprintf fmt "%s \\in %s"
                   (* TOTHINK: is there a circumstance where we need
                      to remove more than one index?
		      BB: by construction of [prnd], it should never happen. *)
                   (tuple_var (belast rvis))
                   (rmap_name_ri ri_opt)
              | `Rmap (None as ri_opt) ->
                 Format.fprintf fmt "() \\in %s" (rmap_name_ri ri_opt)
              | `RNmap ->
                 Format.fprintf fmt "%s \\notin %s"
                   (tuple_var rvis) (rmap_name context)
            in
            Format.fprintf fmt "%a"
              (Format.pp_print_list ~sep:" /\\ " for1)
              (List.map (fun i -> `Rnd i) context @
                 List.map (fun c -> `Rmap c) prnd   @ [`RNmap])
          in
          let pp_body_stmt fmt =
            let for1 fmt rnd =
              Format.fprintf fmt "@\n%s.sample%s;"
                (ro_name rnd.vname) (tuple_var ~paren:true rvis)
            in
            Format.fprintf fmt "@\n%s.[%s] <- ();"
              (rmap_name context) (tuple_var rvis);
            List.iter (Format.fprintf fmt "%a" for1) rnds
          in
          Format.fprintf fmt "@[<v 2>if (%t) {%t@;<0 -2>}@]"
            pp_body_cond
            pp_body_stmt
        in
        Format.fprintf fmt
          "@\n@\n@[<v 2>proc %s(%a) = {@\n%t@;<0 -2>}@]"
          (rproc_name context)
          pp_args rvis
          pp_body
      in
      (* Filter out rndseqs for which we do not need to output a sampling oracle *)
      List.iter (for1 fmt) (List.rev (Env.get_rndseqs ~print:true env))

    and pp_init_proc fmt =
      let pp_init_maps fmt =
        let for1 fmt name =
          Format.fprintf fmt "%s <- empty;" name in
        List.iter
          (Format.fprintf fmt "@\n%a" for1)
          (List.map (fun (v, _) -> vmap_name v.vname) (Env.olocals env) @
           List.map (fun (c, _) -> rmap_name c) (Env.get_rndseqs ~print:true env) @
           List.map (fun o -> omap_name o.oname) (oracles)) in

      let pp_init_tables fmt =
        let for1 fmt (name, _) =
          Format.fprintf fmt "%s <- [];" (table_name name) in
        List.iter
          (Format.fprintf fmt "@\n%a" for1)
          (Env.tables env) in

      let pp_init_local_rnd_maps fmt =
        let for1 fmt (v, _, _) =
          Format.fprintf fmt "%s <- empty;" (local_rnd_map_name v.vname) in
        List.iter
          (Format.fprintf fmt "@\n%a" for1)
          (Env.rnds ~kind:`Local env) in

      let pp_init_ro fmt =
        let for1 fmt (v, _, _) =
          Format.fprintf fmt "%s.init();" (ro_name v.vname) in
        List.iter (Format.fprintf fmt "@\n%a" for1) (Env.rnds ~kind:`Global env) in

      let pp_init_abort fmt =
        if Env.get_abort env then
          Format.fprintf fmt "@\n%s <- false;" abort_name in

      Format.fprintf fmt
        "@\n@\n@[<v 2>proc init() = {%t%t%t%t%t@;<0 -2>}@]"
        pp_init_maps pp_init_tables pp_init_local_rnd_maps pp_init_ro pp_init_abort

    and pp_oracle_defs fmt =
      let print_rs = (GEnv.get_rs (Env.genv env)) in
      (* [rnd0] is true when there is a top-level random sequence without replication *) 
      let rnd0 = List.exists (function ([],_) -> true | _ -> false) (Env.get_rndseqs env) in
      let for1 fmt (o : oracle) =
        let ofvar = stmt_fetch_fvar (FetchEnv.create ()) FVarSet.empty o.obody in
        let ofmap = stmt_fetch_fmap env NameMap.empty o.obody in

        let ctx = List.filter (fun ri -> List.mem ri print_rs) o.octxt in
        let riargs = List.map var_of_ri o.octxt in
        let rriargs = List.map var_of_ri ctx in
        let args = riargs @ o.oargs in

        let rituple = tuple_var riargs in
        let rrituple = tuple_var rriargs in
        let atuple  = tuple_var o.oargs in

        let pp_body fmt =
          let pp_body_cond fmt =
            let for1 fmt = function
              | `RI ri ->
                  Format.fprintf fmt "1 <= %s <= %s"
                    (var_name ri.rname) (bound_name ri.rintv)

              | `RMap ->
                 Format.fprintf fmt "%s \\in %s" rrituple (rmap_name ctx)

              | `Map ->
                  Format.fprintf fmt "%s \\notin %s" rituple (omap_name o.oname)

              | `Arg (v, intv) ->
                  Format.fprintf fmt "1 <= %s <= %s"
                    (var_name v.vname) (bound_name intv)
              | `Def (rs,vs) ->
                 Format.fprintf fmt "%s \\in %s"
                   (tuple_var vs) (rmap_name rs)

            in

            let args_cond =
              let for1 v =
                match v.vtype with
                | T_Int (Some p) -> Some (`Arg (v, p))
                | _ -> None
              in List.filter_map for1 o.oargs in

            let def_cond =
              let acc = Env.get_odefcnd lenv o.oname in
              let for1 (n, vs) =
                let v,_,_ = Option.get (Env.get_binder lenv n) in
                let rs = v.vargs in
                `Def (rs,vs)
              in
              List.map for1 acc
            in

            Format.fprintf fmt "%a"
              (Format.pp_print_list ~sep:"@ /\\ " for1)
              (List.map (fun ri -> `RI ri) o.octxt @
	       (if ctx = [] && not rnd0 then [] else [`RMap ]) @
	       `Map :: args_cond @ def_cond) in

          let pp_real_body fmt =
            Format.fprintf fmt "@\n%s.[%s] <- %s;"
              (omap_name o.oname) rituple atuple in

          Format.fprintf fmt
            "@[<v 2>if (@[<hov>%t@]) {%t%a@;<0 -2>}@]"
            pp_body_cond pp_real_body (pp_stmt env) o.obody in

        Format.fprintf fmt
          "@[<v 2>proc %s(%a) = {@\n%t@\n@\n%t@\n%t@;<0 -2>}@]"
          (oracle_name o.oname) pp_args args
          (* local variable declarations *)
          (fun fmt ->
            let ofvar    = FVarSet.elements ofvar in
            let findvars = List.filter_map (function Find (_,x) -> Some x | _ -> None) ofvar in
            let allidx   = NameSet.of_list (List.flatten findvars) in
	    let find_single_branch_vars = List.filter_map (function Find (false,x) -> Some x | _ -> None) ofvar in
            let find_single_branch_vars = NamesSet.of_list find_single_branch_vars in
	    let has_multiple_branch_find = List.exists (function Find(true,_) -> true | _ -> false) ofvar in
            let rowvars  = List.filter_map (function Table (x, i, v) -> Some (x, i, v) | _ -> None) ofvar in
            let rowvars  =
              List.fold_left (fun map (name, i, v) ->
                  StringMap.update
                    name
                    (fun logs -> Some ((i, v) :: (Option.value ~default:[] logs)))
                    map
                ) StringMap.empty rowvars in
            let tblvars  = List.filter_map
                             (function Table (_, _, x) -> Some x | _ -> None) ofvar in
            let tblvars  =
              List.fold_left
                (fun acc v -> NameMap.add v.vname v acc)
                NameMap.empty (List.flatten tblvars) in

            Format.fprintf fmt "var aout : %a <- witness;" pp_type o.ores;
            NameSet.iter (fun fv ->
                 Format.fprintf fmt "@\nvar %s : int;" (var_name fv)
              ) allidx;
            NamesSet.iter (fun fvs ->
                 Format.fprintf fmt "@\nvar %s : %a list;"
                   (vars_name_list fvs)
                   (pp_type_tuple ~paren:true) (List.map (fun _ -> T_Int None) fvs)
		) find_single_branch_vars;
	    if has_multiple_branch_find then
	      Format.fprintf fmt "@\nvar %s : (int * int list) list;@\nvar %s : int;@\nvar %s : int list;" find_success_list_name find_chosen_branch_name find_chosen_idx_name;
            NameMap.iter (fun _ v ->
                 Format.fprintf fmt "@\nvar %s : %a;"
                   (var_name v.vname) pp_type v.vtype
               ) tblvars;
            StringMap.iter (fun table logs ->
                List.iter (fun (i, vs) ->
                    Format.fprintf fmt "@\nvar %s: (%a) list;"
                    (table_rows_name table i)
                    (Format.pp_print_list ~sep:" * " pp_type)
                    (List.map (fun v -> v.vtype) vs)
                  ) logs
              ) rowvars;
            (NameMap.iter (fun fv modes ->
                 let { vtype = ty }, dris, kind = Option.get (Env.get_binder env fv) in
                 let exported = match kind with `Rnd b -> b | _ -> assert false in
                 FModeSet.iter (fun mode ->
                   match mode with
                   | `Direct ris ->
                       Format.fprintf fmt "@\nvar %s : %a;"
                         (var_fetch (fv, ris)) pp_type ty
                   | `Map when exported ->
                       Format.fprintf fmt "@\nvar %s : (%a, %a) fmap;"
                         (tmp_map_name fv)
                         (pp_type_tuple ~paren:false) (List.map (fun _ -> T_Int None) dris)
                         pp_type ty
                   | _ -> ()
                 ) modes
               ) ofmap))
          pp_body
          (fun fmt -> Format.fprintf fmt "return aout;")

      in List.iter (Format.fprintf fmt "@\n@\n%a" for1) oracles
    in

    let pp_unchanged fmt =
      let unchanged = GEnv.get_unchanged (Env.genv env) in
      let uvars = List.map snd unchanged in
      let pp_var fmt =
        let for1 (v: variable) =
          Format.fprintf fmt "@\nvar %s;" (tmp_map_name v.vname) in
        List.iter for1 uvars
      in
      let pp_body fmt =
        let for1 (v: variable) =
          Format.fprintf fmt "@\n%s <%@ %s();"
            (tmp_map_name v.vname) (rnd_map_getter v.vname) in
        List.iter for1 uvars
      in
      let pp_ret fmt =
        Format.fprintf fmt "return (%a);"
          (Format.pp_print_list ~sep:", " pp_string)
          (List.map (fun v -> tmp_map_name v.vname) uvars)
      in
      if unchanged = [] then () else
        Format.fprintf fmt "@\n@\n@[<v 2>proc unchanged() = {%t@\n%t@\n%t@;<0 -2>}@]"
          pp_var
          pp_body
          pp_ret
    in

    let oracles =
      List.map
        (fun (v,_,_) -> ro_sig_name name v.vname ^ ".RO")
        (Env.rnds ~kind:`Global env)
    in

    let side =
        Format.asprintf "%s_%s" mod_name
          (match oracles with [] -> "" | _ -> "("^String.concat ", " oracles^")")
    in

    Format.fprintf fmt "%t@." pp_modtypes;

    Format.fprintf fmt "\
(******************************************************************************)
(*                                 %s module                                 *)
(******************************************************************************)

"
      mod_name;

    (* Clone PROM for every "exported" random variable *)
    List.iter pp_ro (Env.rnds ~kind:`Global env);

    Format.tsfprintf fmt "@[<v 2>module %s_%t : %s = {%t%t%t%t%t%t%t%t%t%t@;<0 -2>}.@]@\n@?@."
      (* module name *)
      mod_name

      (* module parameters *)
      pp_mod_params

      (* module type *)
      oracle_modtype_init_name

      (* maps for var/oracle maps *)
      pp_var_maps pp_rnd_maps pp_oracle_maps pp_abort_var

      (* maps for local rnds *)
      pp_local_rnd_map

      (* tables *)
      pp_tables

      (* init. proc *)
      pp_init_proc

      (* sampling oracles *)
      pp_rndseqs

      (* oracle defs *)
      pp_oracle_defs

      (* unchanged oracle for [computational] equivs *)
      pp_unchanged;

    Format.fprintf fmt "module %s = %s.@." mod_name side

  (* ------------------------------------------------------------------ *)
  let pp_game fmt (env : GEnv.t) =

    Format.fprintf fmt "
(******************************************************************************)
(*                                    Game                                    *)
(******************************************************************************)

";
    let unchanged = GEnv.get_unchanged env in

    if unchanged = [] then
      Format.fprintf fmt "\
module type Adversary (S : %s) = {
  proc distinguish() : bool
}.

module Game (S : %s, A : Adversary) = {
  proc main() = {
    var r : bool;
    S.init();
    r <%@ A(S).distinguish();
    return r;
  }
}.
" oracle_modtype_name oracle_modtype_init_name
    else
      Format.fprintf fmt "\
module type Adversary (S : %s) = {
  proc distinguish() : unit
  proc guess(_ : %a) : bool {}
}.

module Game (S : %s, A : Adversary) = {
  proc main() = {
    var r : bool;
    var rnds;
    S.init();
    A(S).distinguish();
    rnds <%@ S.unchanged();
    r <%@ A(S).guess(rnds);
    return r;
  }
}.
" oracle_modtype_name pp_unchanged_type unchanged oracle_modtype_init_name 

  (* ------------------------------------------------------------------ *)

  and pp_goals fmt oracles renv p =
    let pp_mem_restr fmt =
      let for1 fmt o = Format.fprintf fmt "-%s" o in
      Format.pp_print_list ~sep:", " for1 fmt (["LHS";"RHS"]) in
    let pp_pr res fmt side =
        Format.fprintf fmt "Pr[CV.Game(%s, A).main() %@ &m : %s]" side res in
    let pp_side side = Format.asprintf "%s" side in

    let pp_indist has_unique fmt =
      let res_u = if has_unique then " \\/ RHS."^unique_name else "" in
      let pp_body fmt =
        Format.fprintf fmt "`| %a -@;   %a | <= <replace with bound>"
          (pp_pr "res") (pp_side "LHS")
          (pp_pr ("res"^res_u)) (pp_side "RHS")
      in
      Format.fprintf fmt
        "@[<v 2>lemma LHS_RHS &m:@;%t.@]@;"
        pp_body
    in

    let pp_indist_abort has_unique fmt =
      let pp_body_1 fmt =
        let res_u = if has_unique then " /\\ !RHS."^unique_name else "" in
        Format.fprintf fmt "%a <=@;%a + <replace with bound>"
          (pp_pr "res") (pp_side "LHS")
          (pp_pr ("res"^res_u^" \\/ RHS."^abort_name)) (pp_side "RHS") in
      let pp_body_2 fmt =
        let res_u = if has_unique then " \\/ RHS."^unique_name else "" in
        Format.fprintf fmt "%a <=@;%a + <replace with bound>"
          (pp_pr ("res"^res_u^" /\\ !RHS."^abort_name)) (pp_side "RHS")
          (pp_pr "res") (pp_side "LHS")
      in
      Format.fprintf fmt
        "@[<v 2>lemma LHS_RHS &m:@;%t.@]@;"
        pp_body_1;
      if not has_unique then
        Format.fprintf fmt
          "@[<v 2>lemma RHS_LHS &m:@;%t.@]@;"
          pp_body_2
    in

    let pp_lossless fmt unchanged =
      let pp_rndseq fmt =
        let for1 fmt o = Format.fprintf fmt "islossless O.%s" (rproc_name o) in
        let rnds = List.map fst (List.rev (Env.get_rndseqs ~print:true renv)) in
        Format.pp_print_list ~sep:" =>@;" for1 fmt rnds;
        if rnds <> [] then Format.fprintf fmt " =>@;"
      in

      let pp_oracles fmt = 
        let for1 fmt o = Format.fprintf fmt "islossless O.%s" (oracle_name o.oname) in
        Format.pp_print_list ~sep:" =>@;" for1 fmt oracles
      in 
      Format.fprintf fmt 
        "@[<v 2>declare axiom A_ll : forall (O <: CV.Oracles{-A}),@;%t%t => islossless A(O).distinguish.@]@;" 
        pp_rndseq pp_oracles;
      if unchanged <> [] then
        Format.fprintf fmt
          "\n@[<v 2>declare axiom A_guess_ll : forall (O <: CV.Oracles{-A}),@;islossless A(O).guess.@]@;"
    in
      
    let unchanged = GEnv.get_unchanged (Env.genv renv) in
    let has_abort = Env.get_abort renv in
    let has_unique = Env.has_unique renv in
    (* Format.tsfprintf fmt "@\n@[<v 2>abstract theory Goals.@;%t(\* %s *\)@;@;%t@;<0 -2>@]end Goals." *)
    Format.fprintf fmt "\

(*

CV value of the bound:

%s

Indistinguishability lemma that should be proved
(assuming the current theory has been cloned as CV):

section.

declare module A <: CV.Adversary{%t}.
%a
%t
end section.
*)"
      (* CV value of the bound *)
      (Display.string_out (fun () -> Display.display_set p))

      pp_mem_restr

      pp_lossless unchanged

      (* indistinguishability of LHS and RHS *)
      (if has_abort then pp_indist_abort has_unique else pp_indist has_unique)
end

let find_rndseqs (env : GEnv.t) ((lhs, rhs) : eqmember * eqmember) =
  let rec find_rndseqs (env : GEnv.t) (lhs : fungroup list) (rhs : fungroup list) =
    let for1 (lhs : fungroup) (rhs : fungroup) =
      match lhs, rhs with
        ReplRestr (None,rnds1,fgs1), ReplRestr(None,rnds2,fgs2) ->
         find_rndseqs env fgs1 fgs2;
      | ReplRestr (Some ri1,rnds1,fgs1), ReplRestr(Some ri2,rnds2,fgs2) ->
         let (ri1,ri2) = (convert_replindex ri1, convert_replindex ri2) in
         if not (ri1 = ri2) then convert_error "repl indices do not align";
         if not (rnds1 = []) || not (rnds2 = []) then GEnv.push_rs env ri1;
         find_rndseqs env fgs1 fgs2;
      | Fun _,Fun _ -> ();
      | _ -> assert false
    in
    List.iter2 for1 lhs rhs;
  in find_rndseqs env (List.map fst lhs) (List.map fst rhs)

(* -------------------------------------------------------------------- *)
let convert_equations (env : env) (lhs : eqmember) (rhs : eqmember) =
  let eq_used (c : collision) (fs : funsymb list) =
    let eq_uses_f (c : collision) (f : funsymb) =
         Terms.uses_f f c.c_side_cond
      || Terms.uses_f f c.c_redl
      || Terms.uses_f f c.c_redr in
    List.exists (eq_uses_f c) fs in
  let funs = ref [] in
  Terms.collect_eqmember_f funs lhs;
  Terms.collect_eqmember_f funs rhs;
  let funs = List.filter Funcs.is_kept !funs in
  let new_symb_ref = ref funs in
  let eqs = List.map (fun eq -> (ref false, eq)) (!Settings.initial_statements) in
  (* repeat until no new function symbol is found *)
  while !new_symb_ref != [] do
    let new_symb = !new_symb_ref in
    new_symb_ref := [];
    List.iter (fun f -> ignore (convert_funsymb (Some new_symb_ref) env f))
      new_symb;
    let new_eqs =
      List.filter (fun (already_converted, c) ->
	(not (!already_converted)) && (eq_used c new_symb)
	  ) eqs
    in
    List.iter (fun (already_converted, c) ->
      already_converted := true;
      GEnv.push_equation (Env.genv env)
	(convert_equation new_symb_ref env c)
	) new_eqs
  done

(* ==================================================================== *)
let convert (fmt : Format.formatter)
            ((lhs, rhs) : eqmember * eqmember) (p : setf list) =
  (* Format.printf "%a@\n@." Dump.pp_eqmember lhs; *)
  (* Format.printf "%a@\n@." Dump.pp_eqmember rhs; *)

  let env  = GEnv.create () in
  let lenv = Env.create env in
  let renv = Env.create env in

  (* It does not matter whether we use lenv or renv *)
  convert_equations lenv lhs rhs;

  (* Find repl-indices whose rndseqs are nonempty on at least one side *)
  find_rndseqs env (lhs,rhs);

  let lhs = convert_eqmember lenv lhs in
  let rhs = convert_eqmember renv rhs in

  PP.pp_prelude fmt env;

  (* definedness conditions always use [lenv], even for RHS *)
  PP.pp_eqmember fmt true  "L" (lenv, lenv, lhs);
  PP.pp_eqmember fmt false "R" (renv, lenv, rhs);

  PP.pp_game fmt env;
  PP.pp_goals fmt rhs renv p

(* -------------------------------------------------------------------- *)
let find_equiv_by_name (name : string) (eq : equiv_gen) =
  match eq.eq_name with
  | NoName ->
      false

  | CstName (s, _) ->
      name = s

  | ParName ((s1, _), (s2, _)) ->
      name = Format.sprintf "%s(%s)" s1 s2

(* -------------------------------------------------------------------- *)
let find_and_convert (fmt : Format.formatter) (name : string) =
  let equivs = List.filter (find_equiv_by_name name) !Settings.equivs in

  match equivs with
  | [{ eq_fixed_equiv = Some (lhs, rhs, bound, _) }] ->
     convert fmt (lhs, rhs) bound

  | [_] ->
    Parsing_helper.user_error (Format.sprintf
      "Error: The equivalence %s is generated on-demand by CryptoVerif,
it cannot be converted to EasyCrypt\n" name)
  | [] ->
    Parsing_helper.user_error (Format.sprintf
      "Error: No equivalence corresponds to the one you mentioned on the
command-line (%s)\n" name)
  | _::_::_ ->
    Parsing_helper.user_error (Format.sprintf
      "Error: Several equivalences correspond to the one you mentioned on
the command-line (%s)\n" name)

(* -------------------------------------------------------------------- *)
let parse_easycrypt_convert s =
  if String.contains s ':' then
    let i = String.index s ':' in
    let filename = String.sub s 0 i in
    let entry = String.sub s (i+1) (String.length s - i - 1) in
    (Some filename, entry)
  else
    (None, s)

let main s0 =
  let (filename, s) = parse_easycrypt_convert s0 in
  let toclose, stream =
    match filename with
    | None ->
        None, Format.std_formatter
    | Some filename ->
        let outc =
	  try
	    open_out filename
	  with Sys_error s ->
	    Parsing_helper.raise_user_error s
	in
        Some outc, Format.formatter_of_out_channel outc in

  begin try
    Format.pp_set_margin stream 81;
    Format.fprintf stream "%a@." find_and_convert s
  with e ->
    Option.iter close_out toclose;
    Option.iter
      (fun filename -> try Unix.unlink filename with _ -> ())
      filename;
    raise e end;
  Option.iter close_out toclose

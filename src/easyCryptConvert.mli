(* [convert out eqv] converts the equiv [eqv] into EasyCrypt and *
   write the result to [out]. *)

val convert : Format.formatter -> Types.eqmember * Types.eqmember -> Types.setf list -> unit

(* [main s]
 * When the string [s] is <filename>:<equiv>, finds the equiv specified 
 * by <equiv> and converts it into EasyCrypt. The result is written to 
 * <filename>.
 * When the string [s] is <equiv>, the result is written to the standard output.
 * 
 * See [convert] for more information. *)

val main : string -> unit

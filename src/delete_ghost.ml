open Types

let is_ghost ty =
  ty.toptions land Settings.tyopt_GHOST != 0
  
let g_cst ty =
  assert (is_ghost ty);
  let c = Stringmap.cst_for_type ty in
  begin
    match c.t_desc with
    | FunApp(f,[]) -> f.f_impl <- Const "()"
    | _ -> assert false
  end;
  c

let proved_queries = ref []
let query_vars = ref []
let current_opt = ref []
let tables_to_remove = ref []
let tables_with_get = ref []
let tables_with_insert = ref []

let has_write b =
  List.exists (function
    | Read _ -> false
    | Write(b',_) -> b' == b) (!current_opt)

let must_keep b =
  has_write b || List.memq b (!query_vars)

let must_keep_event t =
  match t.t_desc with
  | FunApp(e,_) ->
      List.exists (fun q -> Settings.event_occurs_in_query e q) (!proved_queries)
  | _ -> assert false
    
(* There are no array accesses, so I can remove variable definitions
   if I remove the term in their syntactic scope *)
let rec no_state t =
  match t.t_desc with
  | ReplIndex _ -> true
  | Var(_,l) -> List.for_all no_state l
  | FunApp(f,l) -> 
      (not f.f_impl_needs_state) &&
      (List.for_all no_state l)
  | TestE(t1,t2,t3) ->
      (no_state t1) && (no_state t2) && (no_state t3)
  | LetE(pat, t1,t2,topt) ->
      (no_state_pat pat) && (no_state t1) && (no_state t2) &&
      (match topt with
      | None -> true
      | Some t3 -> no_state t3)
  | ResE(_,t) -> no_state t
  | GetE(tbl,patl,topt,t1,t2,_) ->
      (List.for_all no_state_pat patl) &&
      (match topt with
      | None -> true
      | Some t3 -> no_state t3)	&&
      (no_state t1) && (no_state t2)
  | InsertE(tbl,tl,t) ->
      (List.memq tbl (!tables_to_remove)) &&
      (List.for_all no_state tl) &&
      (no_state t)
  | EventE(t1, t2) ->
      (not (must_keep_event t1)) && (no_state t1) && (no_state t2)
  | FindE _ ->
      Parsing_helper.input_error "Find not supported (implementation)" t.t_loc
  | _ -> false

and no_state_pat = function
  | PatVar _ -> true
  | PatTuple(_,l) -> List.for_all no_state_pat l
  | PatEqual t -> no_state t

let rec compute cur_array t t' =
  let b = Terms.create_binder Settings.underscore_var_name t.t_type cur_array in  
  Terms.build_term_type t'.t_type 
    (LetE(PatVar b, delete_ghost_strict_subterm cur_array t, t', None))

(* [ignore_result cur_array final_t t] builds a term that
   - evaluates the term [t] and ignores its result;
   - finally evaluates [final_t] and returns its result *)
and ignore_result_list cur_array final_t = function
  | [] -> final_t
  | t::l ->
    ignore_result cur_array (ignore_result_list cur_array final_t l) t
    
and ignore_result cur_array final_t t =
  if no_state t then final_t else
  match t.t_desc with
  | ReplIndex _ -> final_t
  | Var(_,l) -> ignore_result_list cur_array final_t l
  | FunApp(f,l) ->
      if f.f_impl_needs_state then
	compute cur_array t final_t
      else
	ignore_result_list cur_array final_t l
  | LetE(pat, t1, t2, _) when Terms.is_full_pattern pat ->
      let r = ignore_result cur_array final_t t2 in
      let vars = Terms.vars_from_pat [] pat in
      if List.for_all (fun b -> is_ghost b.btype || not (Terms.refers_to b r || must_keep b)) vars then
	(* Find is not supported => there are no array accesses *)
	ignore_result cur_array r t1
      else
	Terms.new_term r.t_type t.t_loc (LetE(pat, delete_ghost_t cur_array t1, r, None))
  | FindE _ ->
      Parsing_helper.input_error "Find not supported (implementation)" t.t_loc
  | GetE (tbl, patl,topt,t1,t2,_)  ->
      if List.memq tbl (!tables_to_remove) then
	(* There is no insert in the table, only the else branch can be executed *)
	ignore_result cur_array final_t t2
      else
	begin
	  Terms.addq_ref tables_with_get tbl;
	  compute cur_array t final_t
	end
  | TestE _ | LetE _ ->
      compute cur_array t final_t
  | ResE(b,t1) ->
      let r = ignore_result cur_array final_t t1 in
      if is_ghost b.btype || not (Terms.refers_to b r || must_keep b) then
	(* Find is not supported => there are no array accesses *)
	r
      else
	Terms.new_term r.t_type t.t_loc (ResE(b,r))
  | EventAbortE f ->
      Terms.new_term final_t.t_type t.t_loc(EventAbortE f)
  | EventE(t1,t2) ->
      let r = ignore_result cur_array final_t t2 in
      if must_keep_event t1 then
	Terms.new_term r.t_type t.t_loc (EventE(delete_ghost_t cur_array t1,r))
      else
	ignore_result cur_array r t1
  | InsertE(tbl,tl,t2) ->
      let r = ignore_result cur_array final_t t2 in
      if List.memq tbl (!tables_to_remove) then
	(* tbl has not get, no need to insert *)
	ignore_result_list cur_array r tl
      else
	begin
	  Terms.addq_ref tables_with_insert tbl;
	  Terms.new_term r.t_type t.t_loc
	    (InsertE(tbl,List.map (delete_ghost_t cur_array) tl,r))
	end

and delete_ghost_t cur_array t =
  if is_ghost t.t_type then
    ignore_result cur_array (g_cst t.t_type) t
  else
    match t.t_desc with
    | ResE(b,t2) ->
	let r = delete_ghost_t cur_array t2 in
	if is_ghost b.btype || not (Terms.refers_to b r || must_keep b) then
	  (* Find is not supported => there are no array accesses *)
	  r
	else
	  Terms.new_term r.t_type t.t_loc (ResE(b,r))
    | LetE(pat,t1,t2,_) when Terms.is_full_pattern pat ->
	let r = delete_ghost_t cur_array t2 in
	let vars = Terms.vars_from_pat [] pat in
	if List.for_all (fun b -> is_ghost b.btype || not (Terms.refers_to b r || must_keep b)) vars then
	  (* Find is not supported => there are no array accesses *)
	  ignore_result cur_array r t1
	else
	  Terms.new_term r.t_type t.t_loc (LetE(pat, delete_ghost_t cur_array t1, r, None))
    | FindE _ ->
	Parsing_helper.input_error "Find not supported (implementation)" t.t_loc
    | GetE(tbl, patl,topt,t1,t2,_)  ->
	if List.memq tbl (!tables_to_remove) then
	  (* There is no insert in the table, only the else branch can be executed *)
	  delete_ghost_t cur_array t2
	else
	  begin
	    Terms.addq_ref tables_with_get tbl;
	    delete_ghost_strict_subterm cur_array t
	  end
    | InsertE(tbl, tl, t2) ->
	if List.memq tbl (!tables_to_remove) then
	  ignore_result_list cur_array (delete_ghost_t cur_array t2) tl
	else
	  begin
	    Terms.addq_ref tables_with_insert tbl;
	    delete_ghost_strict_subterm cur_array t
	  end
    | EventE(t1,t2) ->
	if must_keep_event t1 then
	  delete_ghost_strict_subterm cur_array t
	else
	  ignore_result cur_array (delete_ghost_t cur_array t2) t1
    | _ ->
	delete_ghost_strict_subterm cur_array t

and delete_ghost_strict_subterm cur_array t =
  Terms.build_term t 
    (Terms.map_subterm (delete_ghost_t cur_array) (fun def_list -> def_list)
       (delete_ghost_pat cur_array) t)

and delete_ghost_pat cur_array = function
  | PatVar b -> PatVar b
  | PatTuple(f,l) ->
      if is_ghost (snd f.f_type) then
	Parsing_helper.user_error ("Error: cannot match pattern of ghost type "^(snd f.f_type).tname^"\n");
      PatTuple(f,List.map (delete_ghost_pat cur_array) l)
  | PatEqual t ->
      if is_ghost t.t_type then
	Parsing_helper.input_error ("Error: cannot match pattern of ghost type "^t.t_type.tname^"\n") t.t_loc;
      PatEqual (delete_ghost_t cur_array t)
      
let compute_p cur_array t p' =
  let b = Terms.create_binder Settings.underscore_var_name t.t_type cur_array in  
  Terms.oproc_from_desc
    (Let(PatVar b, delete_ghost_strict_subterm cur_array t, p',
	 Terms.oproc_from_desc Yield))

let rec ignore_result_plist cur_array final_p = function
  | [] -> final_p
  | t::l ->
      ignore_result_p cur_array (ignore_result_plist cur_array final_p l) t
    
and ignore_result_p cur_array final_p t =
  if no_state t then final_p else
  match t.t_desc with
  | ReplIndex _ -> final_p
  | Var(_,l') -> ignore_result_plist cur_array final_p l'
  | FunApp(f,l') ->
      if f.f_impl_needs_state then
	compute_p cur_array t final_p
      else
	ignore_result_plist cur_array final_p l'
  | LetE(pat, t1, t2, _) when Terms.is_full_pattern pat ->
      let r = ignore_result_p cur_array final_p t2 in
      let vars = Terms.vars_from_pat [] pat in
      if List.for_all (fun b -> is_ghost b.btype || not (Terms.refers_to_oprocess b r || must_keep b)) vars then
	(* Find is not supported => there are no array accesses *)
	ignore_result_p cur_array r t1
      else
	Terms.new_oproc (Let(pat, delete_ghost_t cur_array t1, r, Terms.oproc_from_desc Yield)) t.t_loc
  | FindE _ ->
      Parsing_helper.input_error "Find not supported (implementation)" t.t_loc
  | GetE(tbl,patl,topt,t1,t2,_) ->
      if List.memq tbl (!tables_to_remove) then
	(* There is no insert in the table, only the else branch can be executed *)
	ignore_result_p cur_array final_p t2
      else
	begin
	  Terms.addq_ref tables_with_get tbl;
	  compute_p cur_array t final_p
	end
  | TestE _ | LetE _ ->
      compute_p cur_array t final_p
  | ResE(b,t1) ->
      let r = ignore_result_p cur_array final_p t1 in
      if is_ghost b.btype || not (Terms.refers_to_oprocess b r || must_keep b) then
	(* Find is not supported => there are no array accesses *)
	r
      else 
	Terms.new_oproc (Restr(b,r)) t.t_loc
  | EventAbortE f ->
      Terms.new_oproc (EventAbort f) t.t_loc
  | EventE(t1,t2) ->
      let r = ignore_result_p cur_array final_p t2 in
      if must_keep_event t1 then
	Terms.new_oproc (EventP(delete_ghost_t cur_array t1,r)) t.t_loc
      else
	ignore_result_p cur_array r t1
  | InsertE(tbl,tl,t2) ->
      let r = ignore_result_p cur_array final_p t2 in
      if List.memq tbl (!tables_to_remove) then
	ignore_result_plist cur_array r tl
      else
	begin
	  Terms.addq_ref tables_with_insert tbl;
	  Terms.new_oproc
	    (Insert(tbl,List.map (delete_ghost_t cur_array) tl,r)) t.t_loc
	end

let rec delete_ghost_i cur_array p =
  match p.i_desc with
  | Nil -> p
  | Par(p1,p2) ->
      Terms.iproc_from_desc_loc p (Par(delete_ghost_i cur_array p1,
				       delete_ghost_i cur_array p2))
  | Repl(b,p1) ->
      Terms.iproc_from_desc_loc p (Repl(b, delete_ghost_i (b::cur_array) p1))
  | OracleDecl(o,patl,p1) ->
      Terms.iproc_from_desc_loc p
	(OracleDecl(o, List.map (delete_ghost_pat cur_array) patl,
		    delete_ghost_o cur_array p1))

and delete_ghost_o cur_array p =
  match p.p_desc with
  | Restr(b,p1) ->
      let r = delete_ghost_o cur_array p1 in
      if is_ghost b.btype || not (Terms.refers_to_oprocess b r || must_keep b) then
	(* Find is not supported => there are no array accesses *)
	r
      else
	Terms.oproc_from_desc_loc p (Restr(b,r))
  | Let(pat,t,p1,_) when Terms.is_full_pattern pat ->
      let r = delete_ghost_o cur_array p1 in
      let vars = Terms.vars_from_pat [] pat in
      if List.for_all (fun b -> is_ghost b.btype || not (Terms.refers_to_oprocess b r || must_keep b)) vars then 
	(* Find is not supported => there are no array accesses *)
	ignore_result_p cur_array r t
      else
	Terms.oproc_from_desc_loc p (Let(pat, delete_ghost_t cur_array t,
					 r, Terms.oproc_from_desc Yield))
  | Find _ ->
      Parsing_helper.input_error "Find not supported (implementation)" p.p_loc
  | Get(tbl,patl,topt,p1,p2,_) ->
      if List.memq tbl (!tables_to_remove) then
	(* There is no insert in the table, only the else branch can be executed *)
	delete_ghost_o cur_array p2
      else
	begin
	  Terms.addq_ref tables_with_get tbl;
	  delete_ghost_strict_subproc cur_array p
	end
  | Insert(tbl,tl,p2) ->
      if List.memq tbl (!tables_to_remove) then
	ignore_result_plist cur_array (delete_ghost_o cur_array p2) tl
      else
	begin
	  Terms.addq_ref tables_with_insert tbl;
	  delete_ghost_strict_subproc cur_array p
	end
  | EventP(t1,p2) ->
      if must_keep_event t1 then
	delete_ghost_strict_subproc cur_array p
      else
	ignore_result_p cur_array (delete_ghost_o cur_array p2) t1
  |_ ->
      delete_ghost_strict_subproc cur_array p

and delete_ghost_strict_subproc cur_array p = 
  Terms.oproc_from_desc
    (Terms.map_suboproc (delete_ghost_o cur_array)
       (delete_ghost_t cur_array)
       (fun def_list -> def_list)
       (delete_ghost_pat cur_array)
       (delete_ghost_i cur_array) p)


let delete_ghost_letfun l =
  List.map (fun (f, bl, t) ->
    let t' = delete_ghost_t [] t in
    if !Settings.debug_ghost then
      begin
	print_string ("letfun "^f.f_name^" after removing ghost:\n");
	Display.display_term t';
	print_string "\n\n"
      end;
    (f, bl, t')) l

let delete_ghost_procs l =
  List.map (fun (s,opt,p) ->
    current_opt := opt;
    let p' = delete_ghost_i [] p in
    if !Settings.debug_ghost then
      begin
	print_string ("module "^s^" after removing ghost:\n");
	Display.display_process p'
      end;
    (s, opt, p')) l

let rec delete_ghost_impl tables_remove (letfuns,procs) =
  tables_to_remove := tables_remove;
  tables_with_get := [];
  tables_with_insert := [];
  let i' = (delete_ghost_letfun letfuns, delete_ghost_procs procs) in
  let tables_remove =
    (* Tables with [get] only *)
    (List.filter (fun tbl -> not (List.memq tbl (!tables_with_insert))) (!tables_with_get)) @
    (* Tables with [insert] only *)
    (List.filter (fun tbl -> not (List.memq tbl (!tables_with_get))) (!tables_with_insert))
  in
  tables_with_get := [];
  tables_with_insert := [];
  (* Repeat until there are no tables to remove *)
  if tables_remove != [] then delete_ghost_impl tables_remove i' else i'
    
let delete_ghost queries i =
  proved_queries := queries;
  query_vars := Settings.get_public_vars0 ~reach_1s:true queries;
  let r =
    match i with
    | Impl0 -> Impl0
    | Impl1(i) ->
	Impl1(delete_ghost_impl [] i)
    | Impl2(i1, i2) -> 
	Impl2(delete_ghost_impl [] i1, delete_ghost_impl [] i2)
  in
  query_vars := [];
  r

(* Check that the top oracles, before a replication, are all ghost, 
   for F* implementations *)
	
let rec deletable_pat = function
  | PatVar b -> is_ghost b.btype
  | PatTuple(_,l) -> List.for_all deletable_pat l
  | PatEqual _ -> false

(* The adversary can compute the term itself and it does not
   impact the state *)
let rec deletable_term t =
  match t.t_desc with
  | Var _ | ReplIndex _ -> true
  | FunApp(f,l) ->
      (not f.f_impl_needs_state) && (List.for_all deletable_term l)
  | TestE(t1,t2,t3) ->
      (deletable_term t1) && (deletable_term t2) && (deletable_term t3)
  | _ -> false

(* I cannot exploit that a variable [b] is not referenced later
   in the process, because, even if the variable is not referenced
   in the process obtained here after expanding [letfun]s,
   it may (theoretically) be referenced as an unused argument of a 
   [letfun] which is kept in the implementation, so it would be 
   referenced in the process passed to [implementationFStar].
   That would lead to an error in the F* implementation. *)
   
let rec check_top_ghost_p p =
  match p.p_desc with
  | Restr(b,p) when is_ghost b.btype ->
      check_top_ghost_p p
  | Let(pat, t1, p,_) when Terms.is_full_pattern pat &&
      List.for_all (fun b -> is_ghost b.btype) (Terms.vars_from_pat [] pat) &&
      no_state t1 ->
      check_top_ghost_p p
  | Return(tl, p1) when List.for_all deletable_term tl ->
      check_top_ghost p1
  | _ -> 
      Parsing_helper.input_error "For F* implementations, oracles above replication should be removed when removing ghost computations" p.p_loc
	
and check_top_ghost p =
  match p.i_desc with
  | Par(p1,p2) ->
      check_top_ghost p1;
      check_top_ghost p2
  | OracleDecl(o, patl, p1) ->
      if List.for_all deletable_pat patl then
	check_top_ghost_p p1
      else
	Parsing_helper.input_error "For F* implementations, oracles above replication should be removed when removing ghost computations" p.i_loc
  | Repl _ | Nil -> ()



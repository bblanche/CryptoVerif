open Ptree
open Parsing_helper
open Types
open Terms
open Stringmap

let parse_from_string parse ?(lex = Lexer.token) (s, ext_s) =
  let lexbuf = Lexing.from_string s in
  Parsing_helper.set_start lexbuf ext_s;
  try
    parse lex lexbuf
  with
    Parsing.Parse_error -> raise (Error("Syntax error", extent lexbuf))

(* Parse a file *)

let parse filename =
  try
    let ic = open_in filename in
    let lexbuf = Lexing.from_channel ic in
    lexbuf.Lexing.lex_curr_p <- { lexbuf.Lexing.lex_curr_p with 
                                  Lexing.pos_fname = filename };    
    let ptree =
      try
	if (!Settings.front_end) == Settings.Channels then
          Parser.all Lexer.token lexbuf
	else
	  Parser.oall Lexer.token lexbuf
      with Parsing.Parse_error ->
        raise_error "Syntax error" (extent lexbuf)
    in
    close_in ic;
    ptree
  with Sys_error s ->
    raise_user_error s

let parse_lib filename =
  let filename =
    if StringPlus.case_insensitive_ends_with filename ".cvl" then
      begin
	if (!Settings.front_end) != Settings.Channels then
	  raise_user_error "You are mixing a library for channel front-end with a file for the oracle front-end";
	filename
      end
    else if StringPlus.case_insensitive_ends_with filename ".ocvl" then
      begin
	if (!Settings.front_end) == Settings.Channels then
	  raise_user_error "You are mixing a library for oracle front-end with a file for the channel front-end";
	filename
      end
    else
      filename ^ (if (!Settings.front_end) == Settings.Channels then ".cvl" else ".ocvl")
  in
  try
    let ic = open_in filename in
    let lexbuf = Lexing.from_channel ic in
    lexbuf.Lexing.lex_curr_p <- { lexbuf.Lexing.lex_curr_p with 
                                  Lexing.pos_fname = filename };    
    let ptree =
      try
	if (!Settings.front_end) == Settings.Channels then
          Parser.lib Lexer.token lexbuf
	else
	  Parser.olib Lexer.token lexbuf
      with Parsing.Parse_error ->
        raise_error "Syntax error" (extent lexbuf)
    in
    close_in ic;
    ptree
  with Sys_error s ->
    raise_user_error s 

let parse_with_lib filename =
  let libs =
    match !Settings.lib_name with
    | [] ->
	(* Use default library *)
	let filename = "default" ^ (if (!Settings.front_end) == Settings.Channels then ".cvl" else ".ocvl") in
	if Sys.file_exists filename then
	  [filename]
	else
	  (* Look for the default library also in the CryptoVerif directory *)
	  let filename' = Filename.concat (Filename.dirname Sys.executable_name) filename in
	  if Sys.file_exists filename' then
	    [filename']
	  else
	    raise_user_error ("Could not find default library of primitives "^filename)
    | l ->
        (* In [Settings.lib_name], the librairies are in the reverse order
	   in which they should be included *)
	List.rev l
  in
  let rec parse_all_lib = function
    | [] -> parse filename
    | lib::q ->
	let decl_lib = parse_lib lib in
	let (decl_other,p) = parse_all_lib q in
	(decl_lib @ decl_other, p)
  in
  parse_all_lib libs


(* Environment.
   May contain function symbols, variables, ...
   Is a map from strings to the description of the ident *)

let init_env () =
  Terms.record_id "true" dummy_ext;
  Terms.record_id "false" dummy_ext;
  Terms.record_id "not" dummy_ext;
  Terms.record_id "bitstring" dummy_ext;
  Terms.record_id "bitstringbot" dummy_ext;
  Terms.record_id "bool" dummy_ext;
  let m = StringMap.empty in
  let m1 = StringMap.add "true" (EFunc Settings.c_true)
      (StringMap.add "false" (EFunc Settings.c_false)
	 (StringMap.add "not" (EFunc Settings.f_not) m)) in
  let m2 = StringMap.add "bitstring" (EType Settings.t_bitstring)
      (StringMap.add "bitstringbot" (EType Settings.t_bitstringbot)
	 (StringMap.add "bool" (EType Settings.t_bool) m1)) in
  StringMap.add "if_fun" (EFunc dummy_if_fun) m2

type location_type =
    InProcess
  | InEquivalence
  | InLetFun
      
let current_location = ref InProcess

let unique_to_prove = ref false

let in_impl_process () =
  !Settings.get_implementation <> Prove && !current_location <> InEquivalence

let set_role1 = ref false
let set_role2 = ref false
    
exception CannotSeparateLetFun

(* Declarations *)

type macro_elem =
    Macro of Ptree.ident list * Ptree.decl list * string list * macro_elem Stringmap.StringMap.t
    
let statements = ref ([]: statement list)
let collisions = ref ([]: collision list)
let equivalences = ref ([]: equiv_gen list)
let queries_parse = ref ([]: Ptree.query_e list)
let proof = ref (None : Ptree.command list option)
let non_unique_events = ref ([] : funsymb list)
let diff_bit = ref (None : binder option)
    
let implementation = ref ([]: Ptree.impl list)
let impl_roles1 = ref StringMap.empty
let impl_roles2 = ref StringMap.empty
let impl_letfuns1 = ref []
let impl_letfuns2 = ref []

let unused_type = { tname = "error: this type should not be used";
		    tcat = BitString;
		    toptions = 0;
		    tsize = None;
		    tpcoll = None;
                    timplsize = None;
                    tpredicate = None;
                    timplname = None;
                    tserial = None;
                    tserial_lemmas = None;
                    trandom = None;
		    tequal = None }
    
let merge_types t1 t2 ext =
  try
    Terms.merge_types t1 t2
  with IncompatibleTypes ->
    raise_error "All branches of if/let/find/get should yield the same type" ext
      
(**** First pass: build binder_env ****)

(* Check terms *)

let pinstruct_name = function
    PIdent _ -> "ident"
  | PArray _ -> "array reference"
  | PFunApp _ | PEqual _ | PDiff _ | PAnd _ | POr _ -> "function application"
  | PTuple _ -> "tuple"
  | PTestE _ -> "if"
  | PLetE _ -> "let"
  | PFindE _ -> "find"
  | PResE _ -> "new"
  | PEventAbortE _ -> "event_abort"
  | PEventE _ -> "event"
  | PGetE _ -> "get"
  | PInsertE _ -> "insert"
  | PQEvent _ -> "query event"
  | PIndepOf _ -> "independent of"
  | PBefore _ -> "==>"
  | PExists _ -> "exists"
  | PForall _ -> "forall"
  | PDiffIndist _ -> "diff"
    
let add_var_list env in_find_cond cur_array bindl =
  List.fold_left (fun env (s, tyopt) ->
    if in_find_cond then
      add_in_env1error env error_find_cond s
    else
      match tyopt with
	None -> add_in_env1error env error_no_type s
      | Some ty -> add_in_env1 env s ty cur_array
	    ) env bindl

(* [check_term1 binder_env in_find_cond cur_array env t] returns
   a binder environment containing the variables of [binder_env]
   plus those defined by [t]. 
   [in_find_cond] is true when [t] is in a condition of find or get. *)

(* [check_term1]/[check_process1] use the environment [env] only to lookup types,
   parameters, processes, and letfun. We still add variables and replication indices
   to [env], so that the right [letfun]s are visible, but we use dummy
   variables. *)
let dummy_var = Terms.create_binder "@dummy" Settings.t_any []

let rec check_args1 env = function
    [] -> env
  | ((s1, ext1), tyb)::rvardecl ->
      let env' = StringMap.add s1 (EVar dummy_var) env in
      check_args1 env' rvardecl  

let rec check_term1 binder_env in_find_cond cur_array env = function
    PIdent (s, ext), ext2 -> 
      begin
	try 
	  match StringMap.find s env with
	  | ELetFun(f, env', vardecl, t) ->
	      if fst (f.f_type) = [] then
		begin
		  assert (vardecl == []);
                  (*expand letfun functions: we always inline letfuns here, 
		    even when we generate an implementation, so that [binder_env]
		    is correctly set. This is useful in case a find (in the part 
		    of code that is not translated into an implementation) 
		    references variables defined in the letfun. *)
                  check_term1 binder_env in_find_cond cur_array env' t
		end
	      else
		raise_error (s ^ " has no arguments but expects some") ext
	  | _ -> binder_env
	with Not_found -> binder_env
      end
  | (PArray(_, tl) | PTuple(tl)), ext -> 
      check_term_list1 binder_env in_find_cond cur_array env tl
  | PFunApp((s,ext), tl), ext2 -> 
      let env_args = check_term_list1 binder_env in_find_cond cur_array env tl in
      begin
      try 
	match StringMap.find s env with
	  EFunc(f) -> env_args
	| ELetFun(f, env', vardecl, t) ->
            (*expand letfun functions: we always inline letfuns here, 
	      even when we generate an implementation, so that [binder_env]
	      is correctly set. This is useful in case a find (in the part 
	      of code that is not translated into an implementation) 
	      references variables defined in the letfun. *)
	    if List.length vardecl != List.length tl then
	      raise_error ("Letfun "^s^" expects "^(string_of_int (List.length vardecl))^" argument(s), but is here given "^(string_of_int (List.length tl))^" argument(s)") ext;
	    let env'' = check_args1 env' vardecl in
	    let env_args_vars =
	      List.fold_left (fun binder_env ((s1,ext1), ty) ->
		let (ty',_) = get_ty env' ty in
		add_in_env1 binder_env s1 ty' cur_array
		  ) env_args vardecl
	    in
	    check_term1 env_args_vars in_find_cond cur_array env'' t 
	| d -> raise_error (s ^ " was previously declared as a "^ (decl_name d)^". Expected a function.") ext
      with Not_found ->
	raise_error (s ^ " not defined. Expected a function.") ext
      end
  | PTestE(t1, t2, t3), ext ->
      union_both
	(check_term1 binder_env in_find_cond cur_array env t1)
	(union_exclude
	   (check_term1 empty_binder_env in_find_cond cur_array env t2)
	   (check_term1 empty_binder_env in_find_cond cur_array env t3))
  | PFindE(l0,t3,_), ext ->
      if !current_location = InLetFun then
	raise CannotSeparateLetFun;
      let env_branches = ref (check_term1 empty_binder_env in_find_cond cur_array env t3) in
      let env_common = ref binder_env in
      List.iter (fun (bl_ref, bl,def_list,t1,t2) ->
	let rec add env_cond env_then = function
	    [] -> (env_cond,env_then,[])
	  | ((s0,ext0),(s1,ext1),(s2,ext2))::bl ->
	    let p = get_param env s2 ext2 in
	    let t = type_for_param p in
	    (* Create a replication index *)
	    let b = Terms.create_repl_index s1 t in
	    let env_cond' = StringMap.add s1 (EReplIndex b) env_cond in
	    let env_then' = StringMap.add s0 (EVar dummy_var) env_then in
	    let (env_cond'',env_then'',bl') = add env_cond' env_then' bl in
	    (env_cond'',env_then'',b::bl')
	in
	let (env_cond, env_then, bl_repl_index) = add env env bl in 
	bl_ref := bl_repl_index;
	let cur_array' = bl_repl_index @ cur_array in
	(* The defined condition defines no variable.
	   However, in case there is a letfun in it, it is complicated to
	   know whether we will manage to expand it into a simple term.
	   Moreover, the next pass requires the binders to be in binder_env,
	   otherwise, it causes an internal error, because it checks that the
	   term is simple after converting it, not before. So we add the binders
	   in binder_env in that case. Referencing them is an error since [in_find_cond = true]. *)
	List.iter (fun (b,l) ->
	  env_common := check_term_list1 (!env_common) true cur_array' env_cond l) def_list;
	(* The condition is evaluated in all branches *)
	env_common := check_term1 (!env_common) true cur_array' env_cond t1;
	(* The then branch and the variables storing the found indices
           are used only in the successful branch *)
	env_branches := union_exclude (!env_branches)
	     (List.fold_left2 (fun env ri ((s0,ext0),_,_) ->
	       let t = ri.ri_type in
	       if in_find_cond then
		 add_in_env1error env error_find_cond s0
	       else 
		 add_in_env1 env s0 t cur_array
		   ) (check_term1 empty_binder_env in_find_cond cur_array env_then t2) bl_repl_index bl)
	     ) l0;
      union_both (!env_common) (!env_branches)
  | PLetE(pat, t1, t2, topt), ext ->
      let (binder_env_pat, env_pat, bindl) = check_pattern1 binder_env in_find_cond cur_array env false pat in
      let binder_env_cond_pat = check_term1 binder_env_pat in_find_cond cur_array env t1 in
      let binder_env_in = check_term1 empty_binder_env in_find_cond cur_array env_pat t2 in
      let binder_env_else = 
	match topt with
	  None -> empty_binder_env
	| Some t3 -> check_term1 empty_binder_env in_find_cond cur_array env t3
      in
      union_both binder_env_cond_pat
	(union_exclude
	   (add_var_list binder_env_in in_find_cond cur_array bindl)
	   binder_env_else)
  | PResE((s1,ext1),(s2,ext2),t), ext ->
      let ty' = get_type env s2 ext2 in
      let binder_env_new = 
	if in_find_cond then
	  add_in_env1error binder_env error_find_cond s1
	else
	  add_in_env1 binder_env s1 ty' cur_array
      in
      let env_new = StringMap.add s1 (EVar dummy_var) env in
      check_term1 binder_env_new in_find_cond cur_array env_new t
  | PEventAbortE(id), ext -> binder_env
  | PEventE((PFunApp((s,ext0),tl), ext),p), _ ->
      let binder_env_tl = check_term_list1 binder_env in_find_cond cur_array env tl in
      check_term1 binder_env_tl in_find_cond cur_array env p
  | PEventE _, ext2 ->
      raise_error "events should be function applications" ext2
  | PGetE(tbl, patlist, topt, p1, p2,_), _ ->
      (* After conversion of get into find, patlist and topt will
	 appear in conditions of find. 
	 We must appropriately forbid array accesses to the variables they define,
	 so we set [in_find_cond] to true for them. *)
      let (binder_env_pat, env_pat, bindl) =
	check_pattern_list1 binder_env true cur_array env false patlist in
      let binder_env_cond_pat = 
	match topt with
	  Some t -> check_term1 binder_env_pat true cur_array env_pat t
	| None -> binder_env_pat
      in
      let binder_env_in = check_term1 empty_binder_env in_find_cond cur_array env_pat p1 in
      let binder_env_else = check_term1 empty_binder_env in_find_cond cur_array env p2 in
      union_both
	binder_env_cond_pat
	(union_exclude
	   (add_var_list binder_env_in true cur_array bindl)
	   binder_env_else)
  | PInsertE(tbl,tlist,p),_ ->
      let env_tlist = check_term_list1 binder_env in_find_cond cur_array env tlist in
      check_term1 env_tlist in_find_cond cur_array env p
  | (PEqual(t1,t2) | PDiff(t1,t2) | PAnd(t1,t2) | POr(t1,t2)), ext ->
      let env_t1 = check_term1 binder_env in_find_cond cur_array env t1 in
      check_term1 env_t1 in_find_cond cur_array env t2
  | PDiffIndist(t1,t2), _ ->
      union_both binder_env
	(union_exclude
	   (check_term1 empty_binder_env in_find_cond cur_array env t1)
	   (check_term1 empty_binder_env in_find_cond cur_array env t2))
  | PQEvent _,ext -> 
      raise_error "event(...) and inj-event(...) allowed only in queries" ext
  | PBefore _,ext ->
      raise_error "==> allowed only in queries" ext
  | PExists _, ext ->
      raise_error "exists allowed only in queries" ext
  | PForall _, ext ->
      raise_error "forall allowed only in queries" ext
  | PIndepOf _, ext ->
      raise_error "independent-of allowed only in side-conditions of collisions" ext
	
and check_term_list1 binder_env in_find_cond cur_array env = function
    [] -> binder_env
  | t::l ->
      let env_t = check_term1 binder_env in_find_cond cur_array env t in
      check_term_list1 env_t in_find_cond cur_array env l

(* Check pattern
   [check_pattern1 binder_env in_find_cond cur_array needtype pat] returns
   a pair containing:
   - a binder environment containing the variables of [binder_env]
   plus those defined by terms [t] in subpatterns [=t] of [pat].
   - the list of variables bound by [pat], with their type [Some ty]
   or [None] when the type is not mentioned in the pattern.

   [in_find_cond] is true when [t] is in a condition of find or get.

   [needtype] is true when the type of the pattern cannot be inferred
   from the outside; in this case, when the pattern is a variable,
   its type must be explicitly given. *)

and check_pattern1 binder_env in_find_cond cur_array env needtype = function
    PPatVar (id_underscore, tyopt), _ ->
      begin
	match id_underscore with
	| Underscore ext1 -> (binder_env, env, [])
	| Ident(s1,ext1) ->
	    let env' = StringMap.add s1 (EVar dummy_var) env in
	    match tyopt with
	      None -> 
		if needtype then
		  raise_error "type needed for this variable" ext1
		else
		  (binder_env, env', [s1, None])
	    | Some ty ->
		let (ty',_) = get_ty env ty in
		(binder_env, env', [s1, Some ty'])
      end
  | PPatTuple l, _ ->
      check_pattern_list1 binder_env in_find_cond cur_array env true l
  | PPatFunApp(f,l), _ ->
      check_pattern_list1 binder_env in_find_cond cur_array env false l
  | PPatEqual t, _ ->
      (check_term1 binder_env in_find_cond cur_array env t, env, [])

and check_pattern_list1 binder_env in_find_cond cur_array env needtype = function
    [] -> (binder_env, env, [])
  | pat1::patl ->
      let (binder_env1, env1, bind1) = check_pattern1 binder_env in_find_cond cur_array env needtype pat1 in
      let (binder_env1l, env1l, bindl) = check_pattern_list1 binder_env1 in_find_cond cur_array env1 needtype patl in
      (binder_env1l, env1l, bind1 @ bindl)
	
(* Check equivalence statements *)

let check_binder1 cur_array binder_env ((s1,ext1),(s2,ext2),opt) = 
  let t = get_type (!env) s2 ext2 in
  add_in_env1 binder_env s1 t cur_array

let check_binderi1 cur_array binder_env ((s1,ext1),tyb) =
  let (ty, _) = get_ty (!env) tyb in
  add_in_env1 binder_env s1 ty cur_array

(* On the right-hand side, reuse the same names of variables as in 
   the left-hand side for the arguments of oracles *)
let check_binder_rhsi1 cur_array binder_env b0 ((s1,ext1),tyb) =
  let (ty, _) = get_ty (!env) tyb in
  if b0.btype != ty then
    raise_error "Incompatible types of arguments between left and right members of equivalence" ext1;	
  add_in_env1reusename binder_env s1 b0 ty cur_array
    
let rec check_lm_fungroup1 cur_array env binder_env = function
    PReplRestr(repl_opt, restrlist, funlist) ->
      let (cur_array', env') =
	match repl_opt with
	| Some (repl_index_ref, idopt, (rep,ext)) ->
	    let pn = get_param env rep ext in
	    let t = type_for_param pn in 
	    let b = Terms.create_repl_index
		(match idopt with 
		  None -> "i" 
		| Some(id,ext) -> id) t
	    in
	    repl_index_ref := Some b;
	    let cur_array' = b :: cur_array in
	    let env' =
	      match idopt with
		None -> env
	      | Some(id,ext) -> StringMap.add id (EReplIndex b) env
	    in
	    (cur_array', env')
	| None ->
	    (cur_array, env)
      in
      let env'' = List.fold_left (fun env ((s1,ext1),(s2,ext2),opt) ->
	StringMap.add s1 (EVar dummy_var) env) env' restrlist
      in
      let env_funlist = List.fold_left (check_lm_fungroup1 cur_array' env'') binder_env funlist in
      List.fold_left (check_binder1 cur_array') env_funlist restrlist
  | PFun(_, arglist, tres, _) ->
      let env' = List.fold_left (fun env ((s1,ext1),tyb) ->
	StringMap.add s1 (EVar dummy_var) env) env arglist
      in
      List.fold_left (check_binderi1 cur_array) 
	(check_term1 binder_env false cur_array env' tres) arglist

let check_rm_restr1 cur_array restrlist0 binder_env ((s1,ext1),(s2,ext2),opt) =
  let t = get_type (!env) s2 ext2 in
  if t.toptions land Settings.tyopt_CHOOSABLE == 0 then
    raise_error ("Cannot choose randomly a bitstring from " ^ t.tname) ext2;
  let (unchanged, ext) = 
    match opt with
      [] -> (false, Parsing_helper.dummy_ext)
    | ["unchanged", ext] -> (true, ext)
    | (_,ext)::_ -> 
	raise_error "The only allowed option for random choices is [unchanged]" ext
  in
  try
    (* When there is variable in the left-hand side with the same name, try to reuse that name *)
    let (_,(b0,_,_)) =
      List.find (fun (((s1',_),_,_), (b0,_,_)) ->
	s1' = s1 && b0.btype == t) restrlist0
    in
    add_in_env1reusename binder_env s1 b0 t cur_array
  with Not_found ->
    (* List.find failed *)
    if unchanged then 
      raise_error "When a random choice is marked [unchanged] in the right-hand side,\nthere should exist a corresponding random choice of the same name in the\nleft-hand side" ext
    else
      add_in_env1 binder_env s1 t cur_array

let rec combine_first l1 l2 =
  match l1,l2 with
  | [],_ | _,[] -> []
  | a1::r1, a2::r2 -> (a1,a2)::(combine_first r1 r2)
	  
let rec check_rm_fungroup1 cur_array env binder_env plm_fg lm_fg rm_fg =
  match (plm_fg, lm_fg, rm_fg) with
    PReplRestr(repl_opt0, prestrlist0, pfunlist0),
    ReplRestr(lhs_repl_opt, restrlist0, funlist0),
    PReplRestr(repl_opt, restrlist, funlist) ->
      let (cur_array', env') =
	match repl_opt0, lhs_repl_opt, repl_opt with
	| Some _, Some lhs_repl, Some (repl_index_ref, idopt, (rep,ext)) ->
	    let pn = get_param env rep ext in
	    let t = type_for_param pn in 
	    if t != lhs_repl.ri_type then
	      raise_error "Different number of repetitions in left and right members of equivalence" ext;
	    let b = lhs_repl in
	    repl_index_ref := Some b;
	    let cur_array' = b :: cur_array in
	    if List.length funlist != List.length funlist0 then
	      raise_error "Different number of functions in left and right sides of equivalence" ext;
	    let env' =
	      match idopt with
		None -> env
	      | Some(id,ext) -> StringMap.add id (EReplIndex b) env
	    in
	    (cur_array', env')
	| Some _, None, Some _ ->
	    Parsing_helper.internal_error "LHS has a replication that disappeared"
	| None, _, None ->
	    (cur_array, env)
	| Some (_, _, (rep,ext)), _, None ->
	    raise_error "Left member is a replication, right member has no replication" ext
	| None, _, Some(_, _, (rep,ext)) ->
	    raise_error "Right member is a replication, left member has no replication" ext	    
      in
      let env'' = List.fold_left (fun env ((s1,ext1),(s2,ext2),opt) ->
	StringMap.add s1 (EVar dummy_var) env) env' restrlist
      in
      List.fold_left (check_rm_restr1 cur_array' (combine_first prestrlist0 restrlist0)) 
	(check_rm_fungroup_list1 cur_array' env'' binder_env pfunlist0 funlist0 funlist) restrlist
  | _, Fun(_, arglist0,_,_), PFun(_, arglist, tres, _) ->
      if List.length arglist != List.length arglist0 then
	raise_error "Argument lists have different lengths in left and right members of equivalence" (snd tres);
      let env' = List.fold_left (fun env ((s1,ext1),tyb) ->
	StringMap.add s1 (EVar dummy_var) env) env arglist
      in
      List.fold_left2 (check_binder_rhsi1 cur_array) 
	(check_term1 binder_env false cur_array env' tres) arglist0 arglist
  | _, _, PReplRestr(Some(_, _, (_,ext)), _,_) ->
      raise_error "Left member is a function, right member is a replication" ext
  | _, _, PReplRestr(None, (((s1,ext1),_,_)::_),_) ->
      raise_error "Left member is a function, right member is a random number generation" ext1
  | _, _, PReplRestr(None, [],_) ->
      Parsing_helper.internal_error "Left member is a function, right member is PReplRestr with no replication and no new"
  | _,_, PFun(_, arglist, tres, _) ->
      raise_error "Left member is a replication, right member is a function" (snd tres)
      

and check_rm_fungroup_list1 cur_array env binder_env pfunlist0 funlist0 funlist =
  match pfunlist0, funlist0, funlist with
    [],[],[] -> binder_env
  | a1::r1, a2::r2, a3::r3 ->
      let env_a = check_rm_fungroup1 cur_array env binder_env a1 a2 a3 in
      check_rm_fungroup_list1 cur_array env env_a r1 r2 r3
  | _ -> Parsing_helper.internal_error "Lists should have same length in check_rm_fungroup_list1"
	   
let rec check_rm_funmode_list binder_env pfunlist0 funlist0 funlist =
  match pfunlist0, funlist0, funlist with
    [],[],[] -> binder_env
  | (plm_fg,_,_) ::r1, (lm_fg,_)::r2, (fg, _, _):: r3 ->
      let env_a = check_rm_fungroup1 [] (!env) binder_env plm_fg lm_fg fg in
      check_rm_funmode_list env_a r1 r2 r3
  | _ -> Parsing_helper.internal_error "Lists should have same length in check_rm_funmode_list"

(* Check process *)

let rec check_process1 binder_env cur_array env = function
  | PBeginModule (_,p),_ ->
      check_process1 binder_env cur_array env p
  | PNil, _ -> binder_env
  | PPar(p1,p2), _ -> 
      let env_p1 = check_process1 binder_env cur_array env p1 in
      check_process1 env_p1 cur_array env p2
  | PRepl(repl_index_ref,idopt,(s2,ext2),p), _ ->
      let pn = get_param env s2 ext2 in
      let t = type_for_param pn in 
      let b = Terms.create_repl_index
	  (match idopt with 
	      None -> "i" 
	    | Some(id,ext) -> id) t 
      in
      repl_index_ref := Some b;
      let env' =
	match idopt with
	  None -> env
	| Some(id,ext) -> StringMap.add id (EReplIndex b) env
      in
      check_process1 binder_env (b::cur_array) env' p
  | PInput(c, pat, p), _ ->
      let (binder_pat_env, env_pat, bindl) = check_pattern1 binder_env false cur_array env true pat in
      let binder_env_cont_pat = check_oprocess1 binder_pat_env cur_array env_pat p in
      add_var_list binder_env_cont_pat false cur_array bindl
  | PLetDef((s,ext), args), _ ->
      let (env', vardecl, p) = get_process env s ext in
      let binder_env' = check_term_list1 binder_env false cur_array env args in
      let env'' = check_args1 env' vardecl in
      (* I will not be able to make array references to the arguments of the process. That's too tricky because we need to move the definition of these variables to an output process above or below. *)
      let binder_env_var = 
	List.fold_left (fun binder_env ((s1,ext1), ty) ->
	  let _ = get_ty env' ty in
	  add_in_env1error binder_env error_in_input_process s1
	    ) binder_env' vardecl
      in
      check_process1 binder_env_var cur_array env'' p
  | _, ext ->
      raise_error "input process expected" ext

and check_oprocess1 binder_env cur_array env = function
  | PYield, _ | PEventAbort(_), _ -> binder_env
  | PRestr((s1,ext1),(s2,ext2),p), _ ->
      let t = get_type env s2 ext2 in
      let binder_env_new = add_in_env1 binder_env s1 t cur_array in
      let env_new = StringMap.add s1 (EVar dummy_var) env in
      check_oprocess1 binder_env_new cur_array env_new p
  | PLetDef((s,ext), args), _ ->
      let (env', vardecl, p) = get_process env s ext in
      let env'' = check_args1 env' vardecl in
      let env_args = check_term_list1 binder_env false cur_array env args in
      let env_args_vars =
	List.fold_left (fun binder_env ((s1,ext1), ty) ->
	  let (ty',_) = get_ty env' ty in
	  add_in_env1 binder_env s1 ty' cur_array
	    ) env_args vardecl
      in
      check_oprocess1 env_args_vars cur_array env'' p
  | PTest(t,p1,p2), _ ->
      union_both
	(check_term1 binder_env false cur_array env t)
	(union_exclude
	   (check_oprocess1 empty_binder_env cur_array env p1)
	   (check_oprocess1 empty_binder_env cur_array env p2))
  | PFind(l0,p2,_), _ ->
      let env_branches = ref (check_oprocess1 empty_binder_env cur_array env p2) in
      let env_common = ref binder_env in
      List.iter (fun (bl_ref,bl,def_list,t,p1) ->
	let rec add env_cond env_then = function
	    [] -> (env_cond,env_then,[])
	  | ((s0,ext0),(s1,ext1),(s2,ext2))::bl ->
	    let p = get_param env s2 ext2 in
	    let t = type_for_param p in
	    (* Create a replication index *)
	    let b = Terms.create_repl_index s1 t in
	    let env_cond' = StringMap.add s1 (EReplIndex b) env_cond in
	    let env_then' = StringMap.add s0 (EVar dummy_var) env_then in
	    let (env_cond'',env_then'',bl') = add env_cond' env_then' bl in
	    (env_cond'',env_then'',b::bl')
	in
	let (env_cond, env_then, bl_repl_index) = add env env bl in 
	bl_ref := bl_repl_index;
	let cur_array' = bl_repl_index @ cur_array in
	(* The defined condition defines no variable.
	   However, in case there is a letfun in it, it is complicated to
	   know whether we will manage to expand it into a simple term.
	   Moreover, the next pass requires the binders to be in binder_env,
	   otherwise, it causes an internal error, because it checks that the
	   term is simple after converting it, not before. So we add the binders
	   in binder_env in that case. Referencing them is an error since [in_find_cond = true]. *)
	List.iter (fun (b,l) ->
	  env_common := check_term_list1 (!env_common) true cur_array' env_cond l) def_list;
	(* The condition is evaluated in all branches *)
	env_common := check_term1 (!env_common) true cur_array' env_cond t;
	(* The then branch and the variables storing the found indices
           are used only in the successful branch *)
	env_branches := union_exclude (!env_branches)
	     (List.fold_left2 (fun env ri ((s0,ext0),_,_) ->
	       let t = ri.ri_type in
	       add_in_env1 env s0 t cur_array
		 ) (check_oprocess1 empty_binder_env cur_array env_then p1) bl_repl_index bl)
	     ) l0;
      union_both (!env_common) (!env_branches)
  | POutput(b,c,t2,p), _ ->
      let env_t = check_term1 binder_env false cur_array env t2 in
      check_process1 env_t cur_array env p
  | PLet(pat, t, p1, p2), _ ->
      let (binder_env_pat, env_pat, bindl) = check_pattern1 binder_env false cur_array env false pat in
      let binder_env_cond_pat = check_term1 binder_env_pat false cur_array env t in
      let binder_env_in = check_oprocess1 empty_binder_env cur_array env_pat p1 in
      let binder_env_else = check_oprocess1 empty_binder_env cur_array env p2  in
      union_both binder_env_cond_pat
	(union_exclude
	   (add_var_list binder_env_in false cur_array bindl)
	   binder_env_else)
  | PEvent((PFunApp((s,ext0),tl), ext),p), _ ->
      let env_tl = check_term_list1 binder_env false cur_array env tl in
      check_oprocess1 env_tl cur_array env p
  | PEvent _, ext2 ->
      raise_error "events should be function applications" ext2
  | PGet(tbl, patlist, topt, p1, p2, _), _ ->
      (* After conversion of get into find, patlist and topt will
	 appear in conditions of find. 
	 We must appropriately forbid array accesses to the variables they define,
	 so we set [in_find_cond] to true for them. *)
      let (binder_env_pat, env_pat, bindl) = check_pattern_list1 binder_env true cur_array env false patlist in
      let binder_env_cond_pat = 
	match topt with
	  Some t -> check_term1 binder_env_pat true cur_array env_pat t
	| None -> binder_env_pat
      in
      let binder_env_in = check_oprocess1 empty_binder_env cur_array env_pat p1 in
      let binder_env_else = check_oprocess1 empty_binder_env cur_array env p2 in
      union_both
	binder_env_cond_pat
	(union_exclude
	   (add_var_list binder_env_in true cur_array bindl)
	   binder_env_else)
  | PInsert(tbl,tlist,p),_ ->
      let env_tlist = check_term_list1 binder_env false cur_array env tlist in
      check_oprocess1 env_tlist cur_array env p
  | PDiffIndistProc(p1,p2),_ ->
      union_both binder_env
	(union_exclude
	   (check_oprocess1 empty_binder_env cur_array env p1)
	   (check_oprocess1 empty_binder_env cur_array env p2))
  | _, ext -> 
      raise_error "non-input process expected" ext

(**************************************************************)

(* I decided to do checks one after the other to easily disable just one of
   them. *)

(* Build a list of returns corresponding to an oracle/channel name.
   [h] is a hash table containing bindings from oracle names to returns.
   [name] is the current oracle/channel name. *)
let rec build_return_list_aux h name = function
  | PNil, _ | PYield, _ | PEventAbort _, _ -> ()
  | PPar (p1, p2), _ | PTest (_, p1, p2), _ | PLet (_, _, p1, p2), _
  | PGet(_, _, _, p1, p2, _), _ | PDiffIndistProc(p1,p2), _ ->
    build_return_list_aux h name p1;
    build_return_list_aux h name p2
  | PRepl (_, _, _, p), _ | PRestr (_, _, p), _ | PEvent(_, p), _
  | PInsert(_, _, p),_ | PBeginModule (_, p),_ ->
    build_return_list_aux h name p
  | PLetDef((s,ext), _), _ ->
    let (env', vardecl, p) = get_process (!env) s ext in
    build_return_list_aux h name p 
  | PFind(l, p, _), _ ->
    build_return_list_aux h name p;
    List.iter (fun (_, _, _, _, p) -> build_return_list_aux h name p) l
  | POutput(_, _, _, p), ext as o ->
    begin
      match name with
        | Some name ->
          Hashtbl.add h name o;
          build_return_list_aux h None p
        | None ->
            (* This error should be catched by [check_process] *)
            match !Settings.front_end with
              | Settings.Channels ->
                  raise_error "Out present in input process part (implementation)" ext
              | Settings.Oracles ->
                  raise_error "Return present in oracle description part (implementation)" ext
    end
  | PInput(((name, _), _), _, p), _ ->
    build_return_list_aux h (Some name) p

let build_return_list p =
  let h = Hashtbl.create 10 in
  build_return_list_aux h None p;
  h

(* Check that the previous oracle before a role declaration has at most one
   return. *)
let rec check_role_aux error h name = function
  | PNil, _ | PYield, _ | PEventAbort _, _ -> ()
  | PPar (p1, p2), _ | PTest (_, p1, p2), _ | PLet (_, _, p1, p2), _
  | PGet(_, _, _, p1, p2, _), _ | PDiffIndistProc(p1,p2), _ ->
    check_role_aux error h name p1;
    check_role_aux error h name p2
  | PRepl (_, _, _, p), _ | PRestr (_, _, p), _ | PEvent(_, p), _
  | PInsert(_, _, p),_ | POutput(_, _, _, p), _ ->
    check_role_aux error h name p
  | PLetDef((s,ext),_), _ ->
    let (env', vardecl, p) = get_process (!env) s ext in
    check_role_aux error h name p 
  | PFind(l, p, _), _ ->
    check_role_aux error h name p;
    List.iter (fun (_, _, _, _, p) -> check_role_aux error h name p) l
  | PBeginModule (((role, _), _), p), ext ->
      ( match name with
      | Some name ->
          let returns = Hashtbl.find_all h name in
          if List.length returns > 1 then
            let oracle = match !Settings.front_end with
              | Settings.Channels -> "in-out block"
              | Settings.Oracles -> "oracle"
            in
            let return = match !Settings.front_end with
              | Settings.Channels -> "out construct"
              | Settings.Oracles -> "return"
            in
            error
              (Printf.sprintf
                 "Role %s is defined after %s %s that has \
                  more than one %s (implementation)"
                 role
                 oracle
                 name
                 return)
              ext
      | None -> ()
      );
      check_role_aux error h name p
  | PInput (((name, _), _), _, p), _ -> check_role_aux error h (Some name) p

let check_role error h p = check_role_aux error h None p

(* Check that an out followed by a role declaration closes the current
   oracle. This ensures that no oracle is between two roles.
   The boolean [role_possible] indicates whether a role declaration is
   possible here. *)
let rec check_role_continuity_aux error role_possible = function
  | PNil, _ | PYield, _ | PEventAbort _, _ -> ()
  | PPar (p1, p2), _ ->
    check_role_continuity_aux error role_possible p1;
    check_role_continuity_aux error role_possible p2;
  | PTest (_, p1, p2), _ | PLet (_, _, p1, p2), _
  | PGet(_, _, _, p1, p2, _), _ | PDiffIndistProc(p1,p2), _ ->
    check_role_continuity_aux error false p1;
    check_role_continuity_aux error false p2
  | PRepl (_, _, _, p), _ ->
    check_role_continuity_aux error role_possible p
  | PRestr (_, _, p), _ | PEvent(_, p), _
  | PInsert(_, _, p),_ | PInput(_, _, p), _ ->
    check_role_continuity_aux error false p
  | PLetDef((s,ext),_), _ ->
    let (env', vardecl, p) = get_process (!env) s ext in
    check_role_continuity_aux error role_possible p
  | PFind(l, p, _), _ ->
    check_role_continuity_aux error false p;
    List.iter
      (fun (_, _, _, _, p) -> check_role_continuity_aux error false p)
      l
  | POutput(role_end, _, _, p), _ ->
    check_role_continuity_aux error role_end p
  | PBeginModule (((role, _), _), p), ext ->
      ( if not role_possible then
        let return =
          match !Settings.front_end with
          | Settings.Channels -> "an out construct"
          | Settings.Oracles -> "a return"
        in
        error
          (Printf.sprintf
             "Role %s is defined after %s that does not end the previous \
              role/is not in a role (implementation)"
             role return)
          ext );
      check_role_continuity_aux error role_possible p

let check_role_continuity error p = check_role_continuity_aux error true p


let rec check_fstar_no_event_abort error = function
  | PNil, _ | PYield, _ -> ()
  | PEventAbort _, ext ->
      error
        (Printf.sprintf
           "event_abort is not supported in F* implementations (implementation)")
        ext
  | PPar (p1, p2), _
  | PTest (_, p1, p2), _
  | PLet (_, _, p1, p2), _
  | PGet (_, _, _, p1, p2, _), _
  | PDiffIndistProc(p1, p2), _ ->
      check_fstar_no_event_abort error p1;
      check_fstar_no_event_abort error p2
  | PRepl (_, _, _, p), _
  | PRestr (_, _, p), _
  | PEvent (_, p), _
  | PInsert (_, _, p), _
  | PInput (((_, _), _), _, p), _
  | POutput (_, _, _, p), _ ->
      check_fstar_no_event_abort error p
  | PLetDef ((s, ext), _), _ ->
      let env', vardecl, p = get_process !env s ext in
      check_fstar_no_event_abort error p
  | PFind (l, p, _), _ ->
      check_fstar_no_event_abort error p;
      List.iter (fun (_, _, _, _, p) -> check_fstar_no_event_abort error p) l
  | PBeginModule (((role, _), _), p), ext ->
      check_fstar_no_event_abort error p


(* For FStar, check that we have mod1 { !N1 ... } | ... | modn { !Nn ... } *)

(*  *)
let rec check_no_module_closure_open error = function
  | PNil, _ | PYield, _ | PEventAbort _, _ -> ()
  | PPar (p1, p2), _
  | PTest (_, p1, p2), _
  | PLet (_, _, p1, p2), _
  | PGet (_, _, _, p1, p2, _), _
  | PDiffIndistProc(p1, p2), _ ->
      check_no_module_closure_open error p1;
      check_no_module_closure_open error p2
  | PRepl (_, _, _, p), _
  | PRestr (_, _, p), _
  | PEvent (_, p), _
  | PInsert (_, _, p), _
  | PInput (((_, _), _), _, p), _ ->
      check_no_module_closure_open error p
  | POutput (role_end, _, _, p), ext ->
      if role_end then
	match p with
	| PNil, _ -> ()
	| _ ->
	    error
	      "A role ends before the end of the process. (implementation)"
              ext
      else
	check_no_module_closure_open error p
  | PLetDef ((s, ext), _), _ ->
      let env', vardecl, p = get_process !env s ext in
      check_no_module_closure_open error p
  | PFind (l, p, _), _ ->
      check_no_module_closure_open error p;
      List.iter (fun (_, _, _, _, p) -> check_no_module_closure_open error p) l
  | PBeginModule (((role, _), _), p), ext ->
      (* In fact, this error will always be detected earlier:
         - if there is a role closure above, the role closure will trigger
           an error
         - if there is no role closure above, it will trigger a 
	   "Roles cannot be nested" error in [check_process] *)
      error
        (Printf.sprintf
           "The role %s is defined under some other role. (implementation)"
           role)
        ext

(* check if the module is under replication *)
let rec check_start_repl role error = function
  | PPar(p1,p2), _ ->
      check_start_repl role error p1;
      check_start_repl role error p2
  | PRepl (_, _, _, p), _ ->
      check_no_module_closure_open error p
  (* processes defined using let should be transparent for this structure check *)
  | PLetDef ((s, ext), _), _ ->
      let env', vardecl, p = get_process !env s ext in
      check_start_repl role error p
  | _, ext ->
      let str_process =
        match !Settings.front_end with
        | Settings.Channels -> "in process"
        | Settings.Oracles -> "oracle"
      in
      error
        (Printf.sprintf
           "The top-level %s of role %s must be under \
           replication. (implementation)"
           str_process role)
        ext

let rec check_fstar_toplevel_structure allow_ghost_oracles error = function
  (* Nil is always ok for this structure check. *)
  | PNil, _ -> ()
  (* We either need to start by a parallel composition, ... *)
  | PPar (p1, p2), _ ->
      check_fstar_toplevel_structure allow_ghost_oracles error p1;
      check_fstar_toplevel_structure allow_ghost_oracles error p2
  (* a replication, *)	
  | PRepl (_, _, _, p), _ ->
      check_fstar_toplevel_structure false error p
  (* ... or directly by a module declaration. *)
  | PBeginModule (((role, _), _), p), ext ->
      check_start_repl role error p
  (* processes defined using let should be transparent for this structure check *)
  | PLetDef ((s, ext), _), _ ->
      let env', vardecl, p = get_process !env s ext in
      check_fstar_toplevel_structure allow_ghost_oracles error p
  (* Tolerate ghost oracles above replications. 
     We will check via [Delete_ghost.check_top_ghost] that they are removed
     when deleting ghost computations *)
  | PInput(_, _, p), ext
  | PLet(_, _, p, _), ext
  | PRestr(_, _, p), ext
  | POutput(false,_,_, p), ext when allow_ghost_oracles ->
      check_fstar_toplevel_structure allow_ghost_oracles error p
  (* If we encounter the following at this point, the structure requirement is violated *)
  | PYield, ext
  | PTest (_, _, _), ext
  | PLet (_, _, _, _), ext
  | PGet (_, _, _, _, _, _), ext
  | PDiffIndistProc(_,_), ext
  | PRestr (_, _, _), ext
  | PEvent (_, _), ext
  | PInsert (_, _, _), ext
  | PInput (((_, _), _), _, _), ext
  | POutput (_, _, _, _), ext
  | PFind (_, _, _), ext
  | PEventAbort _, ext ->
      let oracle =
	match !Settings.front_end with
              | Settings.Channels -> "in-out block"
              | Settings.Oracles -> "oracle"
      in
      error
        (Printf.sprintf
           "For an FStar implementation, the main process must start with a \
            parallel composition of modules, or only one module, except \
	    that one may have %ss that perform only ghost computations \
	    above replications and modules (implementation)" oracle) 
        ext


let check_process2 p =
  (* Do not check implementation-based requirements when not compiling
     the specification into an implementation. *)
  match !Settings.get_implementation with
  | OCaml ->
      let h = build_return_list p in
      check_role raise_error h p;
      check_role_continuity raise_error p
  | FStar ->
      check_fstar_toplevel_structure true raise_error p;
      check_fstar_no_event_abort raise_error p
  | Prove -> ()

(* We could have a warning when we do not generate an implementation,
      as follows. However, in this case, we should also have a warning
      for other errors that happen at implementation time (e.g. type errors)
   let error_function =
     if !Settings.get_implementation then
       raise_error
     else
       input_warning
   in
   let h = build_return_list p in
   check_role error_function h p;
   check_role_continuity error_function p *)

(* Check the form of process p to signal inefficiencies.

   The check is done on the parse tree instead of processes in order to
   get locations for warnings, including location of replication bounds. *)

let warn_process_form i =
  let repl, repl', repl'', oname = match !Settings.front_end with
    | Settings.Channels -> "Replication", "replication", "replications", "channel" 
    | Settings.Oracles -> "Foreach", "foreach", "foreach", "oracle"
  in
  let warn_parallel_after_replication locp locr =
    Parsing_helper.input_warning
      (Printf.sprintf "Parallel at %s after %s. To avoid \
         losing precision in the probability bound, you should \
         rather put a distinct replication above each component \
         of the parallel composition."
         (in_file_position locr locp)
         repl')
      locr
  in
  let warn_replication_after_replication locr1 locr2 =
    Parsing_helper.input_warning
      (Printf.sprintf "Useless %s at %s after %s. Avoid this to \
         avoid losing precision in the probability bound."
         repl'
         (in_file_position locr2 locr1)
         repl')
      locr2
  in
  let param_tbl = Hashtbl.create 20 in
  let add_and_warn param ch loc =
    begin
      try
        let (ch', loc') = Hashtbl.find param_tbl param in
	if ch <> ch' then
          Parsing_helper.input_warning
            (Printf.sprintf "%s uses the same parameter %s with %s %s as %s at %s with %s %s. \
               Avoid reusing parameters for multiple %s with different %ss to avoid losing precision \
               in the probability bound." repl param oname ch repl' (in_file_position loc loc') oname ch' repl'' oname)
          loc
      with Not_found -> ()
    end;
    Hashtbl.add param_tbl param (ch,loc)
  in
  let rec aux after_repl = function
    | PRepl(_, _, bound_loc, p), loc ->
      begin
        match after_repl with
          | Some (_,r) -> warn_replication_after_replication loc r
          | None -> ()
      end;
      aux (Some (bound_loc,loc)) p

    | PPar(p1, p2), loc ->
      begin
        match after_repl with
          | Some (_,r) -> warn_parallel_after_replication loc r
          | None -> ()
      end;
      aux after_repl p1;
      aux after_repl p2

    | PInput (((ch,_),_), _, p), loc ->
	begin
	  match after_repl with
	  | Some ((bound, loc),_) -> add_and_warn bound ch loc
	  | None -> ()
	end;
	aux None p

    | PNil, _ | PYield, _ | PEventAbort _, _ -> ()
    | PTest(_, p1, p2), _ | PLet (_, _, p1, p2), _
    | PGet(_, _, _, p1, p2, _), _ | PDiffIndistProc(p1,p2), _ ->
      aux after_repl p1;
      aux after_repl p2
    | PRestr (_, _, p), _ | PEvent(_, p), _
    | PInsert(_, _, p),_ | PBeginModule (_, p),_ 
    | POutput(_, _, _, p), _ ->
      aux after_repl p
    | PLetDef((s,ext),_), _ ->
      let (env', vardecl, p) = get_process (!env) s ext in
      aux after_repl p
    | PFind(l, p, _), _ ->
      aux after_repl p;
      List.iter
        (fun (_, _, _, _, p) -> aux after_repl p)
        l
  in
  aux None i


(**** Second pass: type check everything ****)

(* Add a binder in the environment *)

let add_in_env env s ext t cur_array =
  if (StringMap.mem s env) then
    input_warning ("identifier " ^ s ^ " rebound") ext;
  match get_global_binder_if_possible s with
    Some b -> 
      (StringMap.add s (EVar b) env, b)
  | None ->
      let b = Terms.create_binder s t cur_array in
      (StringMap.add s (EVar b) env, b)
	
(* Add a binder in the environment of a letfun. These binders are to be replaced by new binders when used *)

let add_in_env_letfun (tl,bl,env) s ext t =
  if (StringMap.mem s env) then
    input_warning ("identifier " ^ s ^ " rebound") ext;
  let b = Terms.create_binder0 s t [] in
  (t::tl,b::bl,StringMap.add s (EVar b) env)

(* Check that t does not contain if/find/let/new/event/get/insert *)

let instruct_name t =
  match t.t_desc with
    Var _ -> "variable"
  | ReplIndex _ -> "replication index"
  | FunApp _ -> "function application"
  | TestE _ -> "if"
  | LetE _ -> "let"
  | FindE _ -> "find"
  | ResE _ -> "new"
  | EventAbortE _ -> "event_abort"
  | EventE _ -> "event"
  | GetE _ -> "get"
  | InsertE _ -> "insert"
    
let rec check_no_iffindletnewevent ref ext t =
  match t.t_desc with
  | Var (_,l) | FunApp(_,l) ->
      List.iter (check_no_iffindletnewevent ref ext) l
  | ReplIndex _ -> ()
  | TestE _ | LetE _ | FindE _ | ResE _ | EventAbortE _ | EventE _ | GetE _ | InsertE _ ->
      raise_error ((instruct_name t) ^ " at " ^ (in_file_position ext t.t_loc) ^
				  " should not occur in "^ref) ext

(* Check that t does not contain event nor insert *)

let rec check_no_event_insert ext is_get t =
  match t.t_desc with
  | Var (_,l) | FunApp(_,l) ->
      List.iter (check_no_event_insert ext is_get) l
  | ReplIndex _ -> ()
  | TestE(t1,t2,t3) ->
      check_no_event_insert ext is_get t1;
      check_no_event_insert ext is_get t2;
      check_no_event_insert ext is_get t3
  | LetE(pat,t1,t2,topt) ->
      check_no_event_insert_pat ext is_get pat;
      check_no_event_insert ext is_get t1;
      check_no_event_insert ext is_get t2;
      begin
	match topt with
	  None -> ()
	| Some t3 -> check_no_event_insert ext is_get t3
      end
  | FindE(l0,t3,_) ->
   (*   if is_get then
	begin
	  match l0 with
	    [([],def_list,_,_)] -> ()
	      (* This find is in fact a if, so ok *)
	  | _ ->
	      raise_error ("find at " ^ (in_file_position ext t.t_loc) ^
					  " is not allowed in condition of get") ext
	end; *)
      List.iter (fun (bl,def_list,t1,t2) ->
	(* def_list will be checked by check_no_iffindletnew
	   when translating this find *)
	check_no_event_insert ext is_get t1;
	check_no_event_insert ext is_get t2) l0;
      check_no_event_insert ext is_get t3
  | GetE(table, patl, topt, t1,t2, _) ->
      List.iter (check_no_event_insert_pat ext is_get) patl;
      begin
	match topt with
	  None -> ()
	| Some t -> check_no_event_insert ext is_get t
      end;
      check_no_event_insert ext is_get t1;
      check_no_event_insert ext is_get t2
  | ResE(b,t) -> check_no_event_insert ext is_get t
  | EventAbortE _ -> ()
  | EventE _ | InsertE _ -> 
      raise_error ((instruct_name t) ^ " at " ^ (in_file_position ext t.t_loc) ^
				  " should not occur in condition of " ^
				  (if is_get then "get" else "find")) ext

and check_no_event_insert_pat ext is_get = function
    PatVar _ -> ()
  | PatTuple(_,l) -> List.iter (check_no_event_insert_pat ext is_get) l
  | PatEqual t -> check_no_event_insert ext is_get t

(* Check terms *)

(* when t is a variable b0 with current repl. ind. and
   b has no array accesses, use b0 instead of b *)
let add_in_env_reuse_var env s ext ty cur_array (t0,t1,t2) =
  match t0.t_desc, t1.t_desc, t2.t_desc with
    Var(b0,l0), Var(b1,l1), Var(b2,l2) when b0 == b1 && b0 == b2 &&
    Terms.is_args_at_creation b0 l0 &&
    Terms.is_args_at_creation b1 l1 &&
    Terms.is_args_at_creation b2 l2 ->
      begin
	if (StringMap.mem s env) then
	  input_warning ("identifier " ^ s ^ " rebound") ext;
	match get_global_binder_if_possible s with
	  Some b -> 
	    (StringMap.add s (EVar b) env, [PatVar b, t0], [PatVar b, t1], [PatVar b, t2])
	| None ->
	    (StringMap.add s (EVar b0) env, [], [], [])
      end
  | ReplIndex b0, ReplIndex b1, ReplIndex b2 when b0 == b1 && b0 == b2 ->
      begin
	if (StringMap.mem s env) then
	  input_warning ("identifier " ^ s ^ " rebound") ext;
	match get_global_binder_if_possible s with
	  Some b -> 
	    (StringMap.add s (EVar b) env, [PatVar b, t0], [PatVar b, t1], [PatVar b, t2])
	| None ->
	    (StringMap.add s (EReplIndex b0) env, [], [], [])
      end
  | _ ->
      let (env', b) = add_in_env env s ext ty cur_array in
      (env', [PatVar b, t0], [PatVar b, t1], [PatVar b, t2])
    

let rec check_args cur_array env vardecl (args0, args1, args2) =
  match (vardecl, args0, args1, args2) with
    [], [], [], [] -> (env, [], [], [])
  | [], _,_,_ | _, [], _, _ | _, _, [], _ | _, _, _, [] ->
      Parsing_helper.internal_error "Syntax.check_args vardecl and args should have the same length"
  | ((s1, ext1), tyb)::rvardecl, t0::rargs0, t1::rargs1, t2::rargs2 ->
      let ty = t0.t_type in 
      let (ty', ext2) = get_ty env tyb in
      if ty != ty' then
	raise_error ("Process or letfun expects an argument of type " ^ ty'.tname ^ " but is here given an argument of type " ^ ty.tname) t0.t_loc;
      let (env',letopt0, letopt1, letopt2) = add_in_env_reuse_var env s1 ext1 ty' cur_array (t0,t1,t2) in
      (* when t is a variable b0 with current repl. ind. and
	 b has no array accesses, use b0 instead of b *)
      let (env'', rlets0, rlets1, rlets2) = check_args cur_array env' rvardecl (rargs0, rargs1, rargs2) in
      (env'', letopt0 @ rlets0, letopt1 @ rlets1, letopt2 @ rlets2)

exception RemoveFindBranch

let parse_unique construct opt =
  match opt with
  | [] -> Nothing, Nothing
  | ["unique", _] ->
      if !unique_to_prove then
	let e = Terms.create_nonunique_event() in
	non_unique_events := e :: (!non_unique_events);
	UniqueToProve e, Unique
      else
	Unique, Unique
  | (_, ext_s)::_ -> 
      raise_error ("The only option allowed for "^construct^" is unique") ext_s

let queries_for_unique queries =
  let pub_vars = Settings.get_public_vars0 queries in
  let u_queries =
    List.map (fun e -> Terms.build_event_query e pub_vars) (!non_unique_events)
  in
  u_queries @ queries

let get_diff_bit() =
  match !diff_bit with
  | Some b -> b
  | None ->
      let b = Terms.create_binder "diff_bit" Settings.t_bool [] in
      diff_bit := Some b;
      b

let rec list_map_3res f = function
  | [] -> [],[],[]
  | a::l ->
      let (a1,a2,a3) = f a in
      let (l1,l2,l3) = list_map_3res f l in
      a1::l1, a2::l2, a3::l3
	
let rec check_term defined_refs_opt cur_array env prog = function
    PIdent (s, ext), ext2 ->
      begin
      try 
	match StringMap.find s env with
	  EVar(b) ->
	    let t = Terms.new_term b.btype ext2 (Var(b,List.map Terms.term_from_repl_index b.args_at_creation)) in
	    (t,t,t)
	| EReplIndex(b) ->
	    let t = Terms.new_term b.ri_type ext2 (ReplIndex(b)) in
	    (t,t,t)
	| EFunc(f) -> 
	    if fst (f.f_type) = [] then
              let t = Terms.new_term (snd f.f_type) ext2 (FunApp(f, [])) in
	      (t,t,t)
	    else
	      raise_error (s ^ " has no arguments but expects some") ext
	| ELetFun(f, env', vardecl, t) ->
	    if fst (f.f_type) = [] then
	      begin
		assert (vardecl == []);
		let proginside =
	          (* If an implementation is provided for [f], I do not need
		     to record functions called inside [f] as used nor to 
                     generate code for the letfun.
		     (When [f.f_impl = SepFun], that is already done.) *)
		  if f.f_impl = No_impl then
		    prog
		  else
		    None
		in
		let (t0, t1, t2) = check_term (Some []) cur_array env' proginside t in
                (*expand letfun functions*)
		if in_impl_process() && (f.f_cat = SepLetFun || f.f_impl <> No_impl) then
		  begin
		    if (prog <> None) && (f.f_impl = No_impl) then
		      (* Mark the function as used *)
		      f.f_impl <- SepFun;
                    let t1 = Terms.new_term (snd f.f_type) ext2 (FunApp(f, [])) in
		    (t0, t1, t1)
		  end
		else
                  (t0,t1,t2)
	      end
	    else
	      raise_error (s ^ " has no arguments but expects some") ext
	| d -> raise_error (s ^ " was previously declared as a "^(decl_name d)^". Expected a variable, a replication index, or a function") ext
      with Not_found ->
	if in_impl_process() && prog <> None then
	  raise_error "Implementation does not support out-of-scope references" ext;
	if !current_location = InLetFun then
	  raise CannotSeparateLetFun;
	let b = get_global_binder "outside its scope" (s, ext) in
	let tl'' = check_array_type_list ext2 [] [] cur_array b.args_at_creation in
	if (!current_location) <> InEquivalence then
	  begin
	    match defined_refs_opt with
	      None -> () (* We are in a [defined] condition: all array accesses are accepted *)
	    | Some defined_refs ->
		if not (List.exists (Terms.equal_binderref (b, tl'')) defined_refs) then
		  raise_error ("Variable "^s^" is referenced outside its scope. It should be guarded by a defined condition") ext
	  end;
	let t = Terms.new_term b.btype ext2 (Var(b,tl'')) in
	(t,t,t)
      end
  | PArray(id, tl), ext2 ->
      if in_impl_process() && prog <> None then
	raise_error "Implementation does not support array references" ext2;
      if !current_location = InLetFun then
	raise CannotSeparateLetFun;
      let (tl0', tl1', tl2') = list_map_3res (check_term defined_refs_opt cur_array env prog) tl in
      let b = get_global_binder "in an array reference" id in
      let tl0'' = check_array_type_list ext2 tl tl0' cur_array b.args_at_creation in
      let tl1'' = check_array_type_list ext2 tl tl1' cur_array b.args_at_creation in
      let tl2'' = check_array_type_list ext2 tl tl2' cur_array b.args_at_creation in
      if (!current_location) <> InEquivalence then
	begin
	  match defined_refs_opt with
	    None -> () (* We are in a [defined] condition: all array accesses are accepted *)
	  | Some defined_refs ->
	      if not (List.exists (Terms.equal_binderref (b, tl0'')) defined_refs) then
		raise_error "Array reference should be guarded by a defined condition" ext2
	end;
      (Terms.new_term b.btype ext2 (Var(b,tl0'')),
       Terms.new_term b.btype ext2 (Var(b,tl1'')),
       Terms.new_term b.btype ext2 (Var(b,tl2'')))
  | PFunApp((s,ext), tl),ext2 ->
      let (tl0', tl1', tl2') = list_map_3res (check_term defined_refs_opt cur_array env prog) tl in
      begin
      try 
	match StringMap.find s env with
	  EFunc(f) ->
	    if f == dummy_if_fun then
	      let f = get_if_fun_tl ext tl0' in
	      (Terms.new_term (snd f.f_type) ext2 (FunApp(f, tl0')),
	       Terms.new_term (snd f.f_type) ext2 (FunApp(f, tl1')),
	       Terms.new_term (snd f.f_type) ext2 (FunApp(f, tl2')))
	    else
	      begin
		check_type_list ext2 tl tl0' (fst f.f_type);
		(Terms.new_term (snd f.f_type) ext2 (FunApp(f, tl0')),
		 Terms.new_term (snd f.f_type) ext2 (FunApp(f, tl1')),
		 Terms.new_term (snd f.f_type) ext2 (FunApp(f, tl2')))
	      end
	| ELetFun(f, env', vardecl, t) ->
	    check_type_list ext2 tl tl0' (fst f.f_type);
            (*expand letfun functions*)
	    (* Arity already checked by [check_type_list] *)
	    let (env'', lets0, lets1, lets2) = check_args cur_array env' vardecl (tl0',tl1',tl2') in
	    let proginside =
	      (* If an implementation is provided for [f], I do not need
		 to record functions called inside [f] as usednor to 
                 generate code for the letfun.
		 (When [f.f_impl = SepFun], that is already done.) *)
	      if f.f_impl = No_impl then
		prog
	      else
		None
	    in
	    let t0',t1',t2' = check_term (Some []) cur_array env'' proginside t in
	    let t0'' = Terms.put_lets_term lets0 t0' None in
	    let t1'' = Terms.put_lets_term lets0 t1' None in
	    let t2'' = Terms.put_lets_term lets0 t2' None in
            if in_impl_process() && (f.f_cat = SepLetFun || f.f_impl <> No_impl) then
	      begin
		if (prog <> None) && (f.f_impl = No_impl) then
		  (* Mark the function as used *)
		  f.f_impl <- SepFun;
		(t0'',
		 Terms.new_term (snd f.f_type) ext2 (FunApp(f, tl1')),
		 Terms.new_term (snd f.f_type) ext2 (FunApp(f, tl2')))
	      end
            else
	      (t0'',t1'',t2'')
	| d -> raise_error (s ^ " was previously declared as a "^(decl_name d)^". Expected a function.") ext
      with Not_found ->
	raise_error (s ^ " not defined. Expected a function.") ext
      end
  | PTuple(tl), ext2 ->
      let (tl0', tl1', tl2') = list_map_3res (check_term defined_refs_opt cur_array env prog) tl in
      let f = Settings.get_tuple_fun (List.map (fun t -> t.t_type) tl0') in
      check_type_list ext2 tl tl0' (fst f.f_type);
      (Terms.new_term (snd f.f_type) ext2 (FunApp(f, tl0')),
       Terms.new_term (snd f.f_type) ext2 (FunApp(f, tl1')),
       Terms.new_term (snd f.f_type) ext2 (FunApp(f, tl2')))
  | PTestE(t1, t2, t3), ext ->
      let (t10',t11',t12') = check_term defined_refs_opt cur_array env prog t1 in
      let (t20',t21',t22') = check_term defined_refs_opt cur_array env prog t2 in
      let (t30',t31',t32') = check_term defined_refs_opt cur_array env prog t3 in
      check_type (snd t1) t10' Settings.t_bool;
      let t_common = merge_types t20'.t_type t30'.t_type ext in
      (Terms.new_term t_common ext (TestE(t10', t20', t30')),
       Terms.new_term t_common ext (TestE(t11', t21', t31')),
       Terms.new_term t_common ext (TestE(t12', t22', t32')))
  | PLetE(pat, t1, t2, topt), ext ->
      let (t10',t11',t12') = check_term defined_refs_opt cur_array env prog t1 in
      let (env', pat0', pat1', pat2') = check_pattern defined_refs_opt cur_array env prog (Some t10'.t_type) pat in
      let (t20',t21',t22') = check_term defined_refs_opt cur_array env' prog t2 in
      let topt0', topt1', topt2' = 
	match topt, Terms.is_full_pattern pat0' with
	| Some _, true -> raise_error "When a let in an expression has an else part, the pattern must not match all values" ext
	| Some t3, false ->
	    let (t30', t31', t32') = check_term defined_refs_opt cur_array env prog t3 in
	    Some t30', Some t31', Some t32'
	| None, true -> None, None, None
	| None, false -> raise_error "When a let in an expression has no else part, the pattern must match all values" ext
      in
      let t_common = 
	match topt0' with
	  None -> t20'.t_type
	| Some t30' -> merge_types t20'.t_type t30'.t_type ext
      in
      (Terms.new_term t_common ext (LetE(pat0', t10', t20', topt0')),
       Terms.new_term t_common ext (LetE(pat1', t11', t21', topt1')),
       Terms.new_term t_common ext (LetE(pat2', t12', t22', topt2')))
  | PResE((s1,ext1),(s2,ext2),t), ext ->
      let ty = get_type env s2 ext2 in
      if ty.toptions land Settings.tyopt_CHOOSABLE == 0 then
	raise_error ("Cannot choose randomly a bitstring from " ^ ty.tname) ext2;
      let (env',b) = add_in_env env s1 ext1 ty cur_array in
      let (t0',t1',t2') = check_term defined_refs_opt cur_array env' prog t in
      (Terms.new_term t0'.t_type ext (ResE(b, t0')),
       Terms.new_term t0'.t_type ext (ResE(b, t1')),
       Terms.new_term t0'.t_type ext (ResE(b, t2')))
  | PFindE(l0,t3,opt), ext ->
      if in_impl_process() && prog <> None then
	raise_error "Implementation does not support find" ext;
        (* So we do not need to bother generating the second and third terms, useful
           for implementation only *)
      if !current_location = InLetFun then
	raise CannotSeparateLetFun;
      let find_info, _ = parse_unique "find" opt in
      let (t3', _,_) = check_term defined_refs_opt cur_array env prog t3 in
      let rec add env = function
	  [] -> (env,[])
	| ((s0,ext0),(s1,ext1),(s2,ext2))::bl ->
	    let p = get_param env s2 ext2 in
	    let (env',b) = add_in_env env s0 ext0 (type_for_param p) cur_array in
	    let (env'',bl') = add env' bl in
	    (env'',b::bl')
      in
      let t_common = ref t3'.t_type in
      let l0' = List.fold_left (fun accu (bl_ref,bl,def_list,t1,t2) ->
	try 
	  let (env', bl') = add env bl in
	  let bl'' = !bl_ref in (* recover replication indices *)
	  let env'' = List.fold_left2 (fun env (_,(s1, ext1),_) b -> StringMap.add s1 (EReplIndex b) env) env bl bl'' in
	  let bl_bin = List.combine bl' bl'' in
	  let cur_array' = bl'' @ cur_array in
	  let def_list' = List.map (check_br cur_array' env'' prog) def_list in
	  let (defined_refs_opt_t1, defined_refs_opt_t2) =
	    match defined_refs_opt with
	      None -> (None, None)
	    | Some defined_refs ->
		let (defined_refs_t1, defined_refs_t2) =
		  Terms.defined_refs_find bl_bin def_list' defined_refs
		in
		(Some defined_refs_t1, Some defined_refs_t2)
	  in
	  let (t1',_,_) = check_term defined_refs_opt_t1 cur_array' env'' prog t1 in
	  check_no_event_insert (snd t1) false t1';
	  let (t2',_,_) = check_term defined_refs_opt_t2 cur_array env' prog t2 in
	  check_type (snd t1) t1' Settings.t_bool;
	  t_common := merge_types (!t_common) t2'.t_type ext;
	  (bl_bin, def_list', t1', t2')::accu
	with RemoveFindBranch ->
	  accu
	    ) [] l0 
      in
      let t = Terms.new_term (!t_common) ext (FindE(List.rev l0', t3', find_info)) in
      (t,t,t)
  | PEventAbortE(s,ext2), ext ->
      let f = get_event env s ext2 in
      check_type_list ext2 [] [] (List.tl (fst f.f_type));
      let t = Terms.new_term Settings.t_any ext (EventAbortE(f)) in
      (t,t,t)
  | PEventE((PFunApp((s,ext0),tl), ext), p), ext2 ->
      let f = get_event env s ext0 in
      let tl0',tl1',tl2' = list_map_3res (check_term defined_refs_opt cur_array env prog) tl in
      check_type_list ext tl tl0' (List.tl (fst f.f_type));
      let tupf = Settings.get_tuple_fun (List.map (fun ri -> ri.ri_type) cur_array) in
      let tcur_array =
	Terms.new_term Settings.t_bitstring ext2
	  (FunApp(tupf, List.map Terms.term_from_repl_index cur_array))
      in
      let p0',p1',p2' = check_term defined_refs_opt cur_array env prog p in
      let event0 = Terms.new_term Settings.t_bool ext2 (FunApp(f, tcur_array::tl0')) in
      let event1 = Terms.new_term Settings.t_bool ext2 (FunApp(f, tcur_array::tl1')) in
      let event2 = Terms.new_term Settings.t_bool ext2 (FunApp(f, tcur_array::tl2')) in
      (Terms.new_term p0'.t_type ext2 (EventE(event0, p0')),
       Terms.new_term p0'.t_type ext2 (EventE(event1, p1')),
       Terms.new_term p0'.t_type ext2 (EventE(event2, p2')))
  | PEventE _, ext2 ->
      raise_error "events should be function applications" ext2
  | PGetE((id,ext),patl,topt,p1,p2,opt),ext2 ->
      let find_info0, find_infoi = parse_unique "get" opt in
      let tbl = get_table env id ext in
      if List.length patl != List.length tbl.tbltype then
	raise_error ("Table "^id^" expects "^
		     (string_of_int (List.length tbl.tbltype))^
		     " argument(s), but is here given "^
		     (string_of_int (List.length patl))^" argument(s)") ext;
      let (p20', p21', p22') = check_term defined_refs_opt cur_array env prog p2 in
      let (env', patl0', patl1', patl2') = check_pattern_list defined_refs_opt cur_array env prog (List.map (fun x->Some x) tbl.tbltype) patl in
      let topt0', topt1', topt2' = 
	match topt with 
	  None -> None, None, None 
	| Some t -> 
	    let (t0', t1', t2') = check_term defined_refs_opt cur_array env' prog t in
	    check_no_event_insert (snd t) true t0';
	    check_type (snd t) t0' Settings.t_bool;
	    Some t0', Some t1', Some t2'
      in
      let (p10', p11', p12') = check_term defined_refs_opt cur_array env' prog p1 in
      let t_common = merge_types p10'.t_type p20'.t_type ext2 in
      (Terms.new_term t_common ext2 (GetE(tbl, patl0',topt0',p10', p20', find_info0)),
       Terms.new_term t_common ext2 (GetE(tbl, patl1',topt1',p11', p21', find_infoi)),
       Terms.new_term t_common ext2 (GetE(tbl, patl2',topt2',p12', p22', find_infoi)))
          
  | PInsertE((id,ext),tl,p),ext2 ->
      let tbl = get_table env id ext in
      let (tl0', tl1', tl2') = list_map_3res (check_term defined_refs_opt cur_array env prog) tl in
      check_type_list ext2 tl tl0' tbl.tbltype;
      let (p0', p1', p2') = check_term defined_refs_opt cur_array env prog p in
      (Terms.new_term p0'.t_type ext2 (InsertE(tbl, tl0', p0')), 
       Terms.new_term p0'.t_type ext2 (InsertE(tbl, tl1', p1')),
       Terms.new_term p0'.t_type ext2 (InsertE(tbl, tl2', p2')))
	
  | PEqual(t1,t2), ext ->
      let (t10', t11', t12') = check_term defined_refs_opt cur_array env prog t1 in
      let (t20', t21', t22') = check_term defined_refs_opt cur_array env prog t2 in
      if (t10'.t_type != t20'.t_type) && (t10'.t_type != Settings.t_any) && (t20'.t_type != Settings.t_any) then
	raise_error "= expects expressions of the same type" ext;
      (Terms.make_equal_ext ext t10' t20',
       Terms.make_equal_ext ext t11' t21',
       Terms.make_equal_ext ext t12' t22')
  | PDiff(t1,t2), ext ->
      let (t10', t11', t12') = check_term defined_refs_opt cur_array env prog t1 in
      let (t20', t21', t22') = check_term defined_refs_opt cur_array env prog t2 in
      if (t10'.t_type != t20'.t_type) && (t10'.t_type != Settings.t_any) && (t20'.t_type != Settings.t_any) then
	raise_error "<> expects expressions of the same type" ext;
      (Terms.make_diff_ext ext t10' t20',
       Terms.make_diff_ext ext t11' t21',
       Terms.make_diff_ext ext t12' t22')
  | PAnd(t1,t2), ext ->
      let (t10', t11', t12') = check_term defined_refs_opt cur_array env prog t1 in
      let (t20', t21', t22') = check_term defined_refs_opt cur_array env prog t2 in
      check_type (snd t1) t10' Settings.t_bool;
      check_type (snd t2) t20' Settings.t_bool;
      (Terms.make_and_ext ext t10' t20',
       Terms.make_and_ext ext t11' t21',
       Terms.make_and_ext ext t12' t22')
  | POr(t1,t2), ext ->
      let (t10', t11', t12') = check_term defined_refs_opt cur_array env prog t1 in
      let (t20', t21', t22') = check_term defined_refs_opt cur_array env prog t2 in
      check_type (snd t1) t10' Settings.t_bool;
      check_type (snd t2) t20' Settings.t_bool;
      (Terms.make_or_ext ext t10' t20',
       Terms.make_or_ext ext t11' t21',
       Terms.make_or_ext ext t12' t22')
  | PDiffIndist(t1,t2), ext ->
      if !current_location = InEquivalence then
	raise_error "diff not allowed in equivalence statements" ext;
      let (t10', t11', t12') = check_term defined_refs_opt cur_array env prog t1 in
      let (t20', t21', t22') = check_term defined_refs_opt cur_array env prog t2 in
      let t_common = merge_types t10'.t_type t20'.t_type ext in
      let b = get_diff_bit() in
      let cond = Terms.new_term b.btype ext (Var(b,[])) in
      if Terms.check_simple_term t10' && Terms.check_simple_term t20' then
	let if_fun = Settings.get_if_fun t_common in
	(Terms.new_term t_common ext (FunApp(if_fun, [cond; t10'; t20'])),
	 t11', t22')
      else
	(Terms.new_term t_common ext (TestE(cond, t10', t20')),
	 t11', t22')
      
  | PQEvent _,ext -> 
      raise_error "event(...) and inj-event(...) allowed only in queries" ext
  | PBefore _,ext ->
      raise_error "==> allowed only in queries" ext
  | PExists _, ext ->
      raise_error "exists allowed only in queries" ext
  | PForall _, ext ->
      raise_error "forall allowed only in queries" ext
  | PIndepOf _, ext ->
      raise_error "independent-of allowed only in side-conditions of collisions" ext

and check_br cur_array env prog ((_,ext) as id, tl) =
  try 
    let (tl',_,_) = list_map_3res (check_term None cur_array env prog) tl in
    List.iter2 (fun t t' -> check_no_iffindletnewevent "defined condition" (snd t) t') tl tl';
    let b = get_global_binder "in an array reference" id in
    let tl'' = check_array_type_list ext tl tl' cur_array b.args_at_creation in
    (b,tl'')
  with Undefined(i,ext) ->
    if !Settings.allow_undefined_var then
      begin
	input_warning (i ^ " not defined. Removing the find branch that requires its definition.") ext;
	raise RemoveFindBranch
      end
    else
      raise_error (i ^ " not defined") ext


(* Check pattern *)

and check_pattern defined_refs_opt cur_array env prog tyoptres = function
    PPatVar (id_underscore, tyopt), _ ->
      let (s1,ext1,is_underscore) =
	match id_underscore with
	| Ident(s1,ext1) -> (s1,ext1,false)
	| Underscore ext1 -> (Settings.underscore_var_name,ext1,true)
      in
      let ty = 
	match tyopt, tyoptres with
	  None, None ->
	    if is_underscore then
	      raise_error "type needed for _ pattern" ext1
	    else
	      raise_error "type needed for this variable" ext1
	| None, Some ty ->
	    ty
	| Some tyb, None -> 
	    let (ty',ext2) = get_ty env tyb in
	    begin
	      match ty'.tcat with
		Interv _ -> raise_error "Cannot input a term of interval type or extract one from a tuple" ext2
	        (* This condition simplifies greatly the theory:
	           otherwise, one needs to compute which channels the adversary
	           knows...
		   8/12/2017: I no longer understand this comment, and I am
		   wondering if I could relax this condition. *)
	      |	_ -> ()
	    end;
	    ty'
	| Some tyb, Some ty ->
	    let (ty',ext2) = get_ty env tyb in
	    if ty != ty' then
	      raise_error ("Pattern is declared of type " ^ ty'.tname ^ " and should be of type " ^ ty.tname) ext2;
	    ty'
      in
      let (env',b) =
	if is_underscore then
	  let b = Terms.create_binder s1 ty cur_array in
	  (StringMap.add s1 (EVar b) env, b)
	else
	  add_in_env env s1 ext1 ty cur_array
      in
      (env', PatVar b, PatVar b, PatVar b)
  | PPatTuple l, ext ->
      begin
	match tyoptres with
	  None -> ()
	| Some ty ->
	    if ty != Settings.t_bitstring then
	      raise_error ("A tuple pattern has type bitstring but is here used with type " ^ ty.tname) ext
      end;
      let tl = List.map (fun _ -> None) l in
      let (env', l0', l1', l2') = check_pattern_list defined_refs_opt cur_array env prog tl l in
      (env', build_pat_tuple l0', build_pat_tuple l1', build_pat_tuple l2')
  | PPatFunApp((s,ext),l), ext2 ->
      let f = get_function_no_letfun_no_if env s ext in
      (* "if_fun" is rejected because it is not [data] *)
      if (f.f_options land Settings.fopt_COMPOS) == 0 then
	raise_error "Only [data] and [bijective] functions are allowed in patterns" ext;
      begin
	match tyoptres with
	  None -> ()
	| Some ty ->
	    if ty != snd f.f_type then
	      raise_error ("Pattern returns type " ^ (snd f.f_type).tname ^ " and should be of type " ^ ty.tname) ext2
      end;
      if List.length (fst f.f_type) != List.length l then
	raise_error ("Function " ^ f.f_name ^ " expects " ^ 
		     (string_of_int (List.length (fst f.f_type))) ^ 
		     " arguments but is here applied to " ^  
		     (string_of_int (List.length l)) ^ " arguments") ext;
      let (env', l0', l1', l2') = check_pattern_list defined_refs_opt cur_array env prog (List.map (fun t -> Some t) (fst f.f_type)) l in
      (env', PatTuple(f, l0'), PatTuple(f, l1'), PatTuple(f, l2'))
  | PPatEqual t, ext ->
      let (t0', t1', t2') = check_term defined_refs_opt cur_array env prog t in
      begin
	match tyoptres with
	  None -> ()
	| Some ty ->
	    if (t0'.t_type != ty)  && (t0'.t_type != Settings.t_any) && (ty != Settings.t_any) then
	      raise_error ("Pattern has type " ^ (t0'.t_type).tname ^ " and should be of type " ^ ty.tname) ext
      end;
      (env, PatEqual t0', PatEqual t1', PatEqual t2')

and check_pattern_list defined_refs_opt cur_array env prog lty l = 
  match lty, l with
    [], [] -> (env,[],[],[])
  | (ty::lty),(a::l) ->
      let env', l0', l1', l2' = check_pattern_list defined_refs_opt cur_array env prog lty l in
      let env'', a0', a1', a2' = check_pattern defined_refs_opt cur_array env' prog ty a in
      (env'', a0'::l0', a1'::l1', a2'::l2')
  | _ -> Parsing_helper.internal_error "Lists have different length in check_pattern_list"

 
(* Check statement *)

let add_in_env_nobe env s ext t =
    let b = Terms.create_binder0 s t [] in
    if (StringMap.mem s env) then
      input_warning ("identifier " ^ s ^ " rebound") ext;
    (StringMap.add s (EVar b) env, b)

let rec check_binder_list env = function
    [] -> (env,[])
  | ((s1,ext1),(s2,ext2))::l ->
      let t = get_type env s2 ext2 in
      let (env',b) = add_in_env_nobe env s1 ext1 t in
      let (env'',l') = check_binder_list env' l in
      (env'', b::l')
	
let rec check_binder_list_ty env = function
    [] -> (env,[])
  | ((s1,ext1),ty)::l ->
      let t =
	match ty with
	| Tid(s2,ext2) -> get_type env s2 ext2
	| TBound(_,ext2) -> raise_error "interval type not allowed here" ext2
      in
      let (env',b) = add_in_env_nobe env s1 ext1 t in
      let (env'',l') = check_binder_list_ty env' l in
      (env'', b::l')

let rec link_ppat env ppat t =
  match ppat with
  | PPatVar(id_underscore,tyopt),ext2 -> 
      begin
	match id_underscore with
	| Underscore _ -> env
	| Ident (s1,ext1) -> 
	    begin
	      match tyopt with
	      | None -> ()
	      | Some ty ->
		  let (ty',_) = get_ty env ty in
		  if (ty' != t.t_type) && (t.t_type != Settings.t_any) then
		    raise_error ("Term of type "^(t.t_type.tname)^" stored in variable of type "^ty'.tname) ext1
	    end;
	    let env',b = add_in_env_nobe env s1 ext1 t.t_type in
	    b.link <- TLink t;
	    env'
      end
  | PPatFunApp((s,ext),l), ext2 ->
      let f = get_function_no_letfun_no_if env s ext in
      if f.f_options land Settings.fopt_BIJECTIVE == 0 then
	raise_error "pattern-matching is allowed only with bijective functions in queries, equation, and collision statements, and therefore also in letfun used in queries, equation, and collision statements" ext;
      let projs = Settings.get_proj f in
      List.fold_left2 (fun env pat proj -> link_ppat env pat (Terms.app proj [t])) env l projs
  | _, ext ->
      raise_error "let pat = t in is forbidden in queries, equation, and collision statements, and therefore also in letfun used in queries, equation, and collision statements, when pat does not match all values or there is an else branch" ext
      
let rec check_term_nobe env = function
    PIdent (s, ext), ext2 ->
      begin
      try 
	match StringMap.find s env with
	  EVar(b) ->
	    begin
	      match b.link with
	      | NoLink ->
		  Terms.new_term b.btype ext2
		    (Var(b,List.map Terms.term_from_repl_index b.args_at_creation))
	      | TLink t ->
		  Terms.copy_term DeleteFacts t
	    end
	| EFunc(f) ->
      	    if fst (f.f_type) = [] then
	      Terms.new_term (snd f.f_type) ext2 (FunApp(f, []))
	    else
	      raise_error (s ^ " has no arguments but expects some") ext
	| ELetFun(f, env', vardecl, t) ->
	    if fst (f.f_type) = [] then
	      begin
		assert (vardecl == []);
		check_term_nobe env' t
	      end
	    else
	      raise_error (s ^ " has no arguments but expects some") ext
	| d -> raise_error (s ^ " was previously declared as a "^(decl_name d)^". Expected a variable or a function (letfun allowed).") ext
      with Not_found -> raise_error (s ^ " not defined. Expected a variable or a function (letfun allowed).") ext
      end
  | PFunApp((s,ext), tl),ext2 ->
      let tl' = List.map (check_term_nobe env) tl in
      begin
	try
	  match StringMap.find s env with
	  | EFunc f ->
	      if f == dummy_if_fun then
		let f = get_if_fun_tl ext tl' in
		Terms.new_term (snd f.f_type) ext2 (FunApp(f, tl'))
	      else
		begin
		  check_type_list ext2 tl tl' (fst f.f_type);
		  Terms.new_term (snd f.f_type) ext2 (FunApp(f, tl'))
		end
	  | ELetFun(f, env', vardecl, t) ->
	      check_type_list ext2 tl tl' (fst f.f_type);
	      let (env'', bl) = check_binder_list_ty env' vardecl in
	      List.iter2 (fun b t -> b.link <- TLink t) bl tl';
	      check_term_nobe env'' t
	  | d -> raise_error (s ^ " was previously declared as a " ^ (decl_name d) ^". Expected a function (letfun allowed).") ext
	with Not_found -> raise_error (s ^ " not defined. Expected a function (letfun allowed).") ext
      end
  | PTuple(tl), ext2 ->
      let tl' = List.map (check_term_nobe env) tl in
      let f = Settings.get_tuple_fun (List.map (fun t -> t.t_type) tl') in
      check_type_list ext2 tl tl' (fst f.f_type);
      Terms.new_term (snd f.f_type) ext2 (FunApp(f, tl'))
  | PEqual(t1,t2), ext ->
      let t1' = check_term_nobe env t1 in
      let t2' = check_term_nobe env t2 in
      if (t1'.t_type != t2'.t_type) && (t1'.t_type != Settings.t_any) && (t2'.t_type != Settings.t_any) then
	raise_error "= expects expressions of the same type" ext;
      Terms.make_equal_ext ext t1' t2'
  | PDiff(t1,t2), ext ->
      let t1' = check_term_nobe env t1 in
      let t2' = check_term_nobe env t2 in
      if (t1'.t_type != t2'.t_type) && (t1'.t_type != Settings.t_any) && (t2'.t_type != Settings.t_any) then
	raise_error "<> expects expressions of the same type" ext;
      Terms.make_diff_ext ext t1' t2'
  | PAnd(t1,t2), ext ->
      let t1' = check_term_nobe env t1 in
      let t2' = check_term_nobe env t2 in
      check_type (snd t1) t1' Settings.t_bool;
      check_type (snd t2) t2' Settings.t_bool;
      Terms.make_and_ext ext t1' t2'
  | POr(t1,t2), ext ->
      let t1' = check_term_nobe env t1 in
      let t2' = check_term_nobe env t2 in
      check_type (snd t1) t1' Settings.t_bool;
      check_type (snd t2) t2' Settings.t_bool;
      Terms.make_or_ext ext t1' t2'
  | PLetE(pat,t1,t2,None), ext ->
      let t1' = check_term_nobe env t1 in
      let env' = link_ppat env pat t1' in
      check_term_nobe env' t2
  | PLetE _, ext ->
      raise_error "let pat = t in is forbidden in queries, equation, and collision statements, and therefore also in letfun used in queries, equation, and collision statements, when pat does not match all variables or there is an else branch" ext
  | (PArray _ | PTestE _ | PFindE _ | PResE _ | PEventAbortE _ | PEventE _ | PGetE _ | PInsertE _ | PDiffIndist _), ext ->
      raise_error "If, find, new, event, insert, get, diff, and array references forbidden in queries, equation, and collision statements, and therefore also in letfun used in queries, equation, and collision statements" ext
  | PQEvent _,ext -> 
      raise_error "event(...) and inj-event(...) allowed only in queries" ext
  | PBefore _,ext ->
      raise_error "==> allowed only in queries" ext
  | PExists _, ext ->
      raise_error "exists allowed only after ==> in queries" ext
  | PForall _, ext ->
      raise_error "forall allowed only in queries" ext
  | PIndepOf _, ext ->
      raise_error "independent-of allowed only in side-conditions of collisions, under && or ||" ext

let check_opt_eq options =
  let manual = ref false in
  let alias_gen = ref true in
  List.iter (fun (s,ext) ->
    match s with
    | "manual" -> manual := true 
    | "no_alias_generalization" -> alias_gen := false
    | "convergent" | "linear" ->
        (* Ignored for compatibility with ProVerif *)
	()
    | _ -> 
	raise_error ("Unrecognized option " ^ s ^ ". Only \"manual\", \"no_alias_generalization\", \"convergent\", and \"linear\" are allowed. (\"convergent\" and \"linear\" are ignored, for compatibility with ProVerif.)") ext
	) options;
  !manual, !alias_gen

let check_statement env (n,t0,side_cond,options) =
  let manual, alias_gen = check_opt_eq options in
  let (l,t) =
    match t0 with
    | PForall(l,t), ext -> (l,t)
    | _ -> ([],t0)
  in
  (* Note: This function uses check_binder_list, which calls
     Terms.create_binder0, so it does not rename the variables.
     That is why I do not save and restore the variable
     numbering state. *)
  let (env',l') = check_binder_list_ty env l in
  let t' = check_term_nobe env' t in
  begin
    match t'.t_desc with
    | FunApp(f, [t1;t2]) when f.f_cat == Equal ->
       if not (List.for_all (fun b -> Terms.refers_to b t1) l') then
	 raise_error "In equality statements, all bound variables should occur in the left-hand side" (snd t)
    | _ ->
       if not (List.for_all (fun b -> Terms.refers_to b t') l') then
	 raise_error "In statements, all bound variables should occur in the term" (snd t)
  end;
  check_type (snd t) t' Settings.t_bool;
  let side_cond' = check_term_nobe env' side_cond in
  check_type (snd side_cond) side_cond' Settings.t_bool;
  statements := (n, l',t',side_cond', ref (not manual), alias_gen) :: (!statements)

(* Check builtin equation statements *)

let check_builtin_eq env (eq_categ, ext) l_fun_symb =
  let get_fun env (s,ext) =
    let f = get_function_no_letfun_no_if env s ext in
    if f == dummy_if_fun then
      raise_error "Function if_fun is not allowed in built-in equation statements" ext;
    f
  in
  let l_fun = List.map (get_fun env) l_fun_symb in
  match eq_categ with
    "commut" -> 
      begin
	match l_fun with
	  [f] -> 
	    begin
	      match fst f.f_type with
		[t1;t2] when t1 == t2 -> ()
	      |	_ -> raise_error "A commutative function should have two arguments of the same type" ext
	    end;
	    if f.f_eq_theories = NoEq then
	      f.f_eq_theories <- Commut
	    else
	      raise_error ("Function " ^ f.f_name ^ " already has an equational theory") ext
	| _ -> raise_error "A commut declaration expects a single function symbol" ext
      end
  | "assoc" | "AC" ->
      begin
	match l_fun with
	  [f] -> 
	    begin
	      match f.f_type with
		([t1;t2], tres) when t1 == t2 && t1 == tres -> ()
	      |	_ -> raise_error ("An " ^ eq_categ ^ " function should have two arguments of the same type as the result") ext
	    end;
	    if f.f_eq_theories = NoEq then
	      f.f_eq_theories <- if eq_categ = "AC" then AssocCommut else Assoc
	    else
	      raise_error ("Function " ^ f.f_name ^ " already has an equational theory") ext
	| _ -> raise_error ("An " ^ eq_categ ^ " declaration expects a single function symbol") ext
      end
  | "assocU" | "ACU" ->
      begin
	match l_fun with
	  [f;n] -> 
	    begin
	      match f.f_type, n.f_type with
		([t1;t2], tres), ([], tn) when t1 == t2 && t1 == tres && tn == tres -> ()
	      |	_ -> raise_error ("An " ^ eq_categ ^ " function should have two arguments of the same type as the result, and a constant neutral element of the same type") ext
	    end;
	    if f.f_eq_theories = NoEq then
	      f.f_eq_theories <- if eq_categ = "ACU" then AssocCommutN(f,n) else AssocN(f,n)
	    else
	      raise_error ("Function " ^ f.f_name ^ " already has an equational theory") ext
	| _ -> raise_error ("An " ^ eq_categ ^ " declaration expects a single function symbol") ext
      end
  | "ACUN" ->
      begin
	match l_fun with
	  [f; n] -> 
	    begin
	      match f.f_type, n.f_type with
		([t1;t2], tres), ([], tneut) when t1 == t2 && t1 == tres && tneut == tres -> ()
	      |	_ -> raise_error "An ACUN function should have two arguments, the result, and a constant neutral element of the same type" ext
	    end;
	    if f.f_eq_theories = NoEq then
	      f.f_eq_theories <- ACUN(f,n)
	    else
	      raise_error ("Function " ^ f.f_name ^ " already has an equational theory") ext
	| _ -> raise_error "An ACUN declaration expects two function symbols" ext
      end  
  | "group" | "commut_group" ->
      begin
	match l_fun with
	  [f; inv; n] -> 
	    begin
	      match f.f_type, inv.f_type, n.f_type with
		([t1;t2], tres), ([invarg], invres), ([], tneut) when t1 == t2 && t1 == tres && invarg == tres && invres == tres && tneut == tres -> ()
	      |	_ -> raise_error "A group operation should be of type T,T -> T, with an inverse of type T -> T and a neutral element of type T" ext
	    end;
	    if f.f_eq_theories != NoEq then
	      raise_error ("Function " ^ f.f_name ^ " already has an equational theory") ext
	    else if inv.f_eq_theories != NoEq then
	      raise_error ("Function " ^ inv.f_name ^ " already has an equational theory") ext
	    else
	      begin
		let eq_th = 
		  if eq_categ = "group" 
		  then Group(f, inv, n) 
		  else CommutGroup(f, inv, n) 
		in
		f.f_eq_theories <- eq_th;
		inv.f_eq_theories <- eq_th
	      end
	| _ -> raise_error ("A " ^ eq_categ ^ " declaration expects 3 function symbols") ext
      end  
  | _ -> raise_error ("Equational theory " ^ eq_categ ^ " not implemented") ext	

(* Check equivalence statements *)

let rec check_term_proba env = function
    PIdent (s, ext), ext2 ->
      begin
      try 
	match StringMap.find s env with
	  EVar(b) ->
	    Terms.new_term b.btype ext2
	      (Var(b,List.map Terms.term_from_repl_index b.args_at_creation))
	| EFunc(f) ->
	    if fst (f.f_type) = [] then
	      Terms.new_term (snd f.f_type) ext2 (FunApp(f, []))
	    else
	      raise_error (s ^ " has no arguments but expects some") ext
	| d -> raise_error (s ^ " was previously declared as a "^(decl_name d)^". Expected a variable or a function (letfun forbidden).") ext
      with Not_found -> 
	let b = get_global_binder "outside its scope" (s,ext) in
	let tl'' = check_array_type_list ext2 [] [] b.args_at_creation b.args_at_creation in
	Terms.new_term b.btype ext2 (Var(b,tl''))
      end
  | PFunApp(id, tl),ext2 ->
      let tl' = List.map (check_term_proba env) tl in
      let f = get_function_no_letfun_if_allowed env id tl tl' ext2 in
      Terms.new_term (snd f.f_type) ext2 (FunApp(f, tl'))
  | PTuple(tl), ext2 ->
      let tl' = List.map (check_term_proba env) tl in
      let f = Settings.get_tuple_fun (List.map (fun t -> t.t_type) tl') in
      check_type_list ext2 tl tl' (fst f.f_type);
      Terms.new_term (snd f.f_type) ext2 (FunApp(f, tl'))
  | (PArray _ | PTestE _ | PLetE _ | PResE _ | PFindE _ | PEventAbortE _ | PEventE _ | PGetE _ | PInsertE _ | PDiffIndist _), ext ->
      raise_error "Array accesses/if/let/find/new/event/get/insert not allowed in terms in probability formulas" ext
  | PEqual(t1,t2), ext ->
      let t1' = check_term_proba env t1 in
      let t2' = check_term_proba env t2 in
      if (t1'.t_type != t2'.t_type) && (t1'.t_type != Settings.t_any) && (t2'.t_type != Settings.t_any) then
	raise_error "= expects expressions of the same type" ext;
      Terms.make_equal_ext ext t1' t2'
  | PDiff(t1,t2), ext ->
      let t1' = check_term_proba env t1 in
      let t2' = check_term_proba env t2 in
      if (t1'.t_type != t2'.t_type) && (t1'.t_type != Settings.t_any) && (t2'.t_type != Settings.t_any) then
	raise_error "<> expects expressions of the same type" ext;
      Terms.make_diff_ext ext t1' t2'
  | PAnd(t1,t2), ext ->
      let t1' = check_term_proba env t1 in
      let t2' = check_term_proba env t2 in
      check_type (snd t1) t1' Settings.t_bool;
      check_type (snd t2) t2' Settings.t_bool;
      Terms.make_and_ext ext t1' t2'
  | POr(t1,t2), ext ->
      let t1' = check_term_proba env t1 in
      let t2' = check_term_proba env t2 in
      check_type (snd t1) t1' Settings.t_bool;
      check_type (snd t2) t2' Settings.t_bool;
      Terms.make_or_ext ext t1' t2'
  | PQEvent _,ext -> 
      raise_error "event(...) and inj-event(...) allowed only in queries" ext
  | PBefore _,ext ->
      raise_error "==> allowed only in queries" ext
  | PExists _, ext ->
      raise_error "exists allowed only in queries" ext
  | PForall _, ext ->
      raise_error "forall allowed only in queries" ext
  | PIndepOf _, ext ->
      raise_error "independent-of allowed only in side-conditions of collisions" ext


(* TO DO we should output an error message when a term in a probability
  formula depends on variables occurring in several different expressions
  of the left-hand side of the equivalence. (In such a case, no expression
  can instantiate all variables, so it will always have result 0, which
  is not the desired behaviour!) *)
      
(* returns the checked formula and its dimension as a power of probability
   and a power of time *)
let get_compatible ext d1 d2 =
  match (d1,d2) with
    None, _ -> d2
  | _, None -> d1
  | Some(dp1,dt1,dl1),Some(dp2,dt2,dl2) -> 
      if (dt1 != dt2) || (dl1 != dl2) then
	raise_error "values of incompatible dimensions" ext
      else
	match dp1, dp2 with
	| None, _ -> d1
	| _, None -> d2
	| Some n1, Some n2 ->
	    if n1 = n2 then d1 else
	    begin
	      input_warning "values with different 'probability' dimension; that is strange, please check the formula" ext;
	      Some(None, dt1,dl1)
	    end

let compose_dim f d1 d2 =
  match (d1,d2) with
    None, _ -> None
  | _, None -> None
  | Some(dp1,dt1,dl1),Some(dp2,dt2,dl2) ->
      let dp = 
	match dp1, dp2 with
	| None, _  | _, None -> None
	| Some n1, Some n2 -> Some (f n1 n2)
      in
      Some (dp, f dt1 dt2, f dl1 dl2)

let mul_dim ext d n =
  match d with
  | None -> None
  | Some(dp1,dt1,dl1) ->
      let dp1' =
	match dp1 with
	| None -> None
	| Some dp -> Some (mul_check_overflow ovf_dim ext dp n)
      in
      Some(dp1',
	   mul_check_overflow ovf_dim ext dt1 n,
	   mul_check_overflow ovf_dim ext dl1 n)
	
let rec check_types ext pl0 pl tl = 
  match (pl0, pl, tl) with
    [],[],[] -> []
  | _::pl0', (TypeMaxlength(ty'))::pl', ty::tl' when ty.toptions land Settings.tyopt_BOUNDED != 0 && ty == ty' -> 
      (* print_string ("Type max length " ^ ty.tname ^ "\n"); *)
      check_types ext pl0' pl' tl'
  | _, _, ty::tl' when ty.toptions land Settings.tyopt_BOUNDED != 0 -> 
      (* print_string ("Bounded type " ^ ty.tname ^ "\n"); *)
      check_types ext pl0 pl tl'
  | (_, ext)::pl0', pt::pl', ty::tl' ->
      let rec check_pt ty = function
	  Maxlength(_,t) ->
	    if t.t_type != ty then
	      raise_error ("In a probability formula, time/length should be of form time/length(f, p1, ..., pn)\n" ^
			   "where p1...pn are probabilities pi ::= maxlength(ti) | length(fi, ...) | max(pi,pi)\n" ^
			   "for terms ti or result of fi of types the non-bounded arguments of f.\n" ^ 
			   "Type " ^ ty.tname ^ " expected, got " ^ t.t_type.tname ^ ".") ext
	| TypeMaxlength(t) ->
	    raise_error ("In a probability formula, time/length should be of form time/length(f, p1, ..., pn)\n" ^
			 "where p1...pn are probabilities pi ::= maxlength(ti) | length(fi, ...) | max(pi,pi)\n" ^
			 "for terms ti or result of fi of types the non-bounded arguments of f.\n" ^ 
			 "Unbounded type " ^ ty.tname ^ " expected, got bounded type " ^ t.tname ^ ".") ext
	| Max(l) ->
	    List.iter (check_pt ty) l
	| Length(f,l) ->
	    if snd f.f_type != ty then
	      raise_error ("In a probability formula, time/length should be of form time/length(f, p1, ..., pn)\n" ^
			   "where p1...pn are probabilities pi ::= maxlength(ti) | length(fi, ...) | max(pi,pi)\n" ^
			   "for terms ti or result of fi of types the non-bounded arguments of f.\n" ^ 
			   "Type " ^ ty.tname ^ " expected, got " ^ (snd f.f_type).tname ^ ".") ext
	    
	| _ ->
	    raise_error ("In a probability formula, time/length should be of form time/length(f, p1, ..., pn)\n" ^
			 "where p1...pn are probabilities pi ::= maxlength(ti) | length(fi, ...) | max(pi,pi)\n" ^
			 "for terms ti or result of fi of types the non-bounded arguments of f.\n" ^ 
			 "maxlength or max expected.") ext
      in
      check_pt ty pt;
      pt :: (check_types ext pl0' pl' tl')
  | _ -> 
      raise_error ("In a probability formula, time/length should be of form time/length(f, p1, ..., pn)\n" ^
		   "where p1...pn are probabilities pi ::= maxlength(ti) | length(fi, ...) | max(pi,pi)\n" ^
		   "for terms ti or result of fi of types the non-bounded arguments of f.\n" ^ 
		   "Unexpected number of arguments.") ext
    
let rec check_probability_formula seen_vals env = function
    PPIdent(s,ext), ext2 ->
      begin
	try 
	  match StringMap.find s env with
	    EParam p ->
	      let (seen_o, seen_repl, adv_time) = seen_vals in
	      if not (List.exists (fun b -> p == Terms.param_from_type b) seen_repl) then
		raise_error ("The parameter " ^s^ " should occur in each member of the equivalence") ext;
	      Count p, num_dim
	  | EProba p ->
	      begin
		match p.pargs with
		| None | Some [] -> ()
		| Some _ -> 
		    raise_error ("Probability function "^s^" has no arguments but expects some") ext
	      end;
	      Proba(p,[]), proba_dim
	  | EVarProba v ->
	      (v.vp_val, v.vp_dim)
	  | ELetProba(p,env',args,p') ->
	      if args != [] then
		raise_error ("Probability function "^s^" has no arguments but expects some") ext;
	      let p'' = p' env' in
	      (p'', proba_dim)
	  | d -> raise_error (s ^ " was previously declared as a "^(decl_name d)^". Expected a probability or a parameter.") ext
	with Not_found ->
	  raise_error (s ^ " is not defined. Expected a probability or a parameter.") ext
      end
  | PCount((s,ext), foreachopt), ext2 ->
      begin
	try
	  let (seen_o, seen_repl, adv_time) = seen_vals in
	  let (o, cur_array, env') = List.find (fun (o,_,_) -> o.oname = s) seen_o in
	  let n_foreach = 
	    match foreachopt with
	    | None -> 0
	    | Some idl ->
		let idl' =
		  List.map (fun (s,ext) ->
		    try 
		      match StringMap.find s env' with
		      | (EVar _ | EReplIndex _) as x -> x
		      | d -> raise_error (s ^ " was previously declared as a "^(decl_name d)^". Expected a variable or a replication index") ext
		    with Not_found ->
		      raise_error (s ^ " is not defined. Expected a variable or a replication index defined above oracle "^o.oname) ext
			) idl
		in
		match idl' with
		| [EVar b] ->
		    let n = List.length b.args_at_creation in
		    if n >= List.length cur_array then
		      raise_error "In #(O foreach a), the variable a should not be defined immediately above oracle O; there should be at least one replication in between" ext;
		    n
		| _ ->
		    let idl_rev = List.rev idl' in
		    let cur_array_rev = List.rev cur_array in
		    let rec check_prefix idl_rev cur_array_rev =
		      match idl_rev, cur_array_rev with
		      | _, [] ->
			  raise_error "In #(O foreach i1,...,in), the replication indices i1,...,in should be a strict suffix of the current replication indices at oracle O" ext
		      | [], _::_ -> ()
		      | id::id_rest, cur::cur_rest ->
			  begin
			    match id, cur with
			    | EReplIndex ri, ri' ->
				if ri != ri' then
				  raise_error "In #(O foreach i1,...,in), the replication indices i1,...,in should be a strict suffix of the current replication indices at oracle O" ext
			    | EVar _, _ ->
				raise_error "In #(O foreach i1,...,in), i1,...,in must be replication indices; #(O foreach a) is accepted when a is a variable only when there is a single identifier after foreach." ext
			    | _ -> assert false
			  end;
			  check_prefix id_rest cur_rest
		    in
		    check_prefix idl_rev cur_array_rev;
		    List.length idl'
	  in	    
	  OCount(o, n_foreach), num_dim
	with Not_found -> 
	  raise_error ("The oracle name " ^ s ^ " is not defined") ext
      end
  | PPFun((s,ext), l), ext2 ->
      let l'_full = List.map (check_probability_formula seen_vals env) l in
      Stringmap.apply_proba (s,ext) env l'_full, proba_dim 
  | PAdd(p1,p2), ext ->
      let (p1', d1) = check_probability_formula seen_vals env p1 in
      let (p2', d2) = check_probability_formula seen_vals env p2 in
      (Add(p1',p2'), get_compatible ext d1 d2)
  | PSub(p1,p2), ext ->
      let (p1', d1) = check_probability_formula seen_vals env p1 in
      let (p2', d2) = check_probability_formula seen_vals env p2 in
      (Sub(p1',p2'), get_compatible ext d1 d2)
  | (PMax(pl) | PMin(pl)) as proba, ext ->
      let rec check_comp = function
	  [] -> ([], None)
	| (p::l) -> 
	    let (p', d) = check_probability_formula seen_vals env p in
	    let (l', dl) = check_comp l in
	    if List.exists (Terms.equal_probaf p') l' then
	      (* remove duplicate elements for simplifying *)
	      (l',dl)
	    else
	      (p'::l', get_compatible ext d dl)
      in
      let (pl', d) = check_comp pl in
      begin
	(* The "max" is removed when the list contains a single element *)
	match proba, pl' with
	  _, [p] -> (p, d)
	| PMax _, _ -> (Max(pl'), d)
	| PMin _, _ -> (Min(pl'), d)
	| _ -> assert false
      end
  | PTime, ext ->
      let (seen_o, seen_repl, adv_time) = seen_vals in
      if not adv_time then
	raise_error "Cannot refer to the runtime of the adversary in letproba" ext;
      (AttTime, time_dim)
  | PActTime(action, pl), ext ->
      begin
	let pl' = List.map (fun p -> fst (check_probability_formula seen_vals env p)) pl in
	match action with
	  PAFunApp(s,ext') ->
	    let f = get_function_no_letfun_no_if env s ext' in
	    if f == dummy_if_fun then
	      raise_error "Use time(if) rather than time(if_fun, ...)" ext'; 
	    let pl' = check_types ext pl pl' (fst f.f_type) in
	    (ActTime(AFunApp f, pl'), time_dim)
	| PAPatFunApp(s,ext') ->
	    let f = get_function_no_letfun_no_if env s ext' in
	    (* Function "if_fun" is rejected because it is not [data] *)
	    if (f.f_options land Settings.fopt_COMPOS) == 0 then
	      raise_error "Only [data] and [bijective] functions are allowed in patterns" ext';
	    let pl' = check_types ext pl pl' (fst f.f_type) in
	    (ActTime(APatFunApp f, pl'), time_dim)
	| PACompare(s,ext') ->
	    let t = get_type_or_param env s ext' in
	    let pl' = check_types ext pl pl' [t] in
	    (ActTime(AFunApp(Settings.f_comp Equal t t), pl'), time_dim)
	| PANew(s,ext') ->
	    let t = get_type env s ext' in
	    if pl != [] then 
	      internal_error "No length arguments for time(new)";
	    (ActTime(ANew t, pl'), time_dim)
	| PAAppTuple(tl) ->
	    let tl' = List.map (fun (s,ext') -> get_type env s ext') tl in
	    let f = Settings.get_tuple_fun tl' in
	    let pl' = check_types ext pl pl' (fst f.f_type) in
	    (ActTime(AFunApp f, pl'), time_dim)
	| PAPatTuple(tl) ->
	    let tl' = List.map (fun (s,ext') -> get_type env s ext') tl in
	    let f = Settings.get_tuple_fun tl' in
	    let pl' = check_types ext pl pl' (fst f.f_type) in
	    (ActTime(APatFunApp f, pl'), time_dim)
	| _ ->
	    begin
	      if pl != [] then 
		internal_error "No length arguments for this action";
	      let action' = match action with
		PAReplIndex -> AReplIndex
	      |	PAArrayAccess n -> AArrayAccess n
	      |	PAAnd -> AFunApp(Settings.f_and)
	      |	PAOr -> AFunApp(Settings.f_or)
	      |	PAIf -> AIf
	      |	PANewOracle -> ANewOracle
	      |	PAFind n -> AFind n
	      |	_ -> internal_error "Unexpected action (syntax.ml)"
	      in
	      (ActTime(action', pl'), time_dim)
	    end
      end
  | PMaxlength(t), ext ->
      begin
	try
	  (* Allow [t] to be a type. If that possibility does not work, 
	     we raise Not_found and we consider [t] as a term. *)
	  match t with 
	  | PIdent (s, ext), ext2 ->
	      begin
		match StringMap.find s env with
		| EType ty -> 
		    (TypeMaxlength(ty), length_dim)
		| _ -> raise Not_found
	      end
	  | _ -> raise Not_found
	with Not_found -> 
	  let t' = check_term_proba env t in
	  if t'.t_type.toptions land Settings.tyopt_BOUNDED != 0 then
	    (TypeMaxlength(t'.t_type), length_dim)
	  else
	    (Maxlength(Terms.lhs_game, t'), length_dim)
      end
  | PLength((s,ext'), pl), ext ->
      begin
	let pl' = List.map (fun p -> fst (check_probability_formula seen_vals env p)) pl in
	try 
	  match StringMap.find s env with
	  | EFunc f ->
	      if f == dummy_if_fun then
		raise_error "avoid using if functions when computing lengths in probability formulas; rather use the length of the then and else parts" ext'; 
	      let pl' = check_types ext pl pl' (fst f.f_type) in
	      if (snd f.f_type).toptions land Settings.tyopt_BOUNDED != 0 then
		(TypeMaxlength (snd f.f_type), length_dim)
	      else
		(Length(f, pl'), length_dim)
	  | EType t ->
	      if pl != [] then
		raise_error "the length of a type should have no additional argument" ext';
	      if t.toptions land Settings.tyopt_BOUNDED != 0 then
		(TypeMaxlength t, length_dim)
	      else
		raise_error "the length of a type is allowed only when the type is bounded" ext'
	  | d -> raise_error (s ^ " was previously declared as a "^(decl_name d)^". Expected a function symbol (letfun forbidden) or a type.") ext'
	with Not_found ->
	  raise_error (s ^ " is not defined. Expected a function symbol (letfun forbidden) or a type.") ext'
      end
  | PLengthTuple(tl,pl), ext ->
      let pl' = List.map (fun p -> fst (check_probability_formula seen_vals env p)) pl in
      let tl' = List.map (fun (s,ext') -> get_type env s ext') tl in
      let f = Settings.get_tuple_fun tl' in
      let pl' = check_types ext pl pl' (fst f.f_type) in
      (Length(f, pl'), length_dim)
  | PProd(p1,p2), ext ->
      let (p1', d1) = check_probability_formula seen_vals env p1 in
      let (p2', d2) = check_probability_formula seen_vals env p2 in
      (Mul(p1',p2'), compose_dim (add_check_overflow ovf_dim ext) d1 d2)
  | PDiv(p1,p2), ext ->
      let (p1', d1) = check_probability_formula seen_vals env p1 in
      let (p2', d2) = check_probability_formula seen_vals env p2 in
      (Div(p1',p2'), compose_dim (sub_check_overflow ovf_dim ext) d1 d2)
  | PPower(p1,n), ext ->
      let (p1', d1) = check_probability_formula seen_vals env p1 in
      if n = 0 then
	begin
	  Parsing_helper.input_warning "probability^0 simplified into 1" ext;
	  (Cst 1.0, num_dim)
	end
      else if n = 1 then
	begin
	  Parsing_helper.input_warning "probability^1 simplified into probability" ext;
	  (p1', d1)
	end
      else
	(Power(p1',n), mul_dim ext d1 n)
  | PPZero, ext -> Zero, None
  | PCard (s,ext'), ext ->
      let t = get_type env s ext' in
      if t.toptions land Settings.tyopt_BOUNDED != 0 then
	Card t, Some(Some (-1), 0, 0)
      else
	raise_error (s ^ " should be bounded") ext'
  | PCst i, ext ->
      Cst (float_of_int i), num_dim
  | PFloatCst f, ext ->
      Cst f, num_dim
  | PEpsFind, ext -> (if (!Settings.ignore_small_times) > 0 then Zero else EpsFind), proba_dim
  | PEpsRand(s,ext'), ext ->
      let t = get_type env s ext' in
      if t.toptions land Settings.tyopt_NONUNIFORM != 0 then
	raise_error (s ^ " should be bounded or fixed, it should not be nonuniform") ext'
      else if t.toptions land Settings.tyopt_FIXED != 0 then
	Zero, proba_dim
      else if t.toptions land Settings.tyopt_BOUNDED != 0 then
	(if (!Settings.ignore_small_times) > 0 then Zero else EpsRand t), proba_dim
      else
	raise_error (s ^ " should be bounded or fixed") ext'
  | PPColl1Rand(s,ext'), ext ->
      let t = get_type env s ext' in
      if t.toptions land Settings.tyopt_CHOOSABLE != 0 then
	Proba.pcoll1rand t, proba_dim
      else 
	raise_error (s ^ " should be fixed, bounded, or nonuniform") ext'
  | PPColl2Rand(s,ext'), ext ->
      let t = get_type env s ext' in
      if t.toptions land Settings.tyopt_CHOOSABLE != 0 then
	Proba.pcoll2rand t, proba_dim
      else 
	raise_error (s ^ " should be fixed, bounded, or nonuniform") ext'
  | POptimIf(cond, p1,p2), ext ->
      let cond' = check_optim_cond seen_vals env cond in
      let (p1', d1) = check_probability_formula seen_vals env p1 in
      let (p2', d2) = check_probability_formula seen_vals env p2 in
      (OptimIf(cond', p1', p2'), get_compatible ext d1 d2)

and check_optim_cond seen_vals env = function
  | POCProbaFun((s,ext_s),l), ext ->
      (* possible functions are is-cst (one argument), 
	 =, <=, < (two arguments). In all cases, the 2 arguments must have the
	 same dimension. *)
      let (l', ldim) = List.split (List.map (check_probability_formula seen_vals env) l) in
      begin
	match ldim with
	| [_] -> ()
	| [d1;d2] -> ignore (get_compatible ext d1 d2)
	| _ -> Parsing_helper.internal_error "POCProbaFun fcts should have 1 or 2 arguments"
      end;
      OCProbaFun(s,l')
  | POCBoolFun((s,ext_s),l), ext ->
      (* possible functions are ||, && (two arguments) *)
      let l' = List.map (check_optim_cond seen_vals env) l in
      OCBoolFun(s,l')
      
and check_probability_formula2 seen_vals env p =
  let (p', d) = check_probability_formula seen_vals env p in
  begin
    match d with
      None -> ()
    | Some(dp,dt,dl) ->
	if (dt != 0) || (dl != 0) then 
	  raise_error "The result of this formula is not a probability" (snd p);
	match dp with
	| None | Some 1 -> ()
	| Some n ->
	    input_warning "This formula may not be a probability; please check" (snd p)
  end;
  p'

let rec check_binder_list2 cur_array env = function
    [] -> (env,[])
  | ((s1,ext1),ty)::l ->
      let t = 
	match ty with
	  Tid (s2,ext2) -> get_type env s2 ext2 
	| TBound (s2,ext2) -> 
	    let p = get_param env s2 ext2 in
	    type_for_param p
      in
      (* Interval types now allowed in arguments of oracles
	 check_bit_string_type ext2 t; *)
      let (env',b) = add_in_env env s1 ext1 t cur_array in
      let (env'',l') = check_binder_list2 cur_array env' l in
      (env'', b::l')

(* On the right-hand side, reuse the same names of variables as in 
   the left-hand side for the arguments of oracles *)
let rec check_binder_list_rhs2 ext0 cur_array env arglist0 arglist =
  match arglist0, arglist with
  | [], [] -> (env,[])
  | b0::l0, ((s1,ext1),ty)::l ->
      let t = 
	match ty with
	  Tid (s2,ext2) -> get_type env s2 ext2 
	| TBound (s2,ext2) -> 
	    let p = get_param env s2 ext2 in
	    type_for_param p
      in
      if b0.btype != t then
	raise_error "Incompatible types of arguments between left and right members of equivalence" ext1;	
      let (env',b) =
	begin
	  if (StringMap.mem s1 env) then
	    input_warning ("identifier " ^ s1 ^ " rebound") ext1;
	  (* I reuse the variable name of the LHS *)
	  match get_global_binder_if_possible s1 with
	    Some b ->
	      assert (b.sname = b0.sname && b.vname = b0.vname);
	      (StringMap.add s1 (EVar b) env, b)
	  | None ->
	      let b = Terms.create_binder_internal b0.sname b0.vname t cur_array in
	      (StringMap.add s1 (EVar b) env, b)
	end
      in
      let (env'',l') = check_binder_list_rhs2 ext0 cur_array env' l0 l in
      (env'', b::l')
  | _ -> 
      raise_error "Argument lists have different lengths in left and right members of equivalence" ext0
      
let rec check_lm_restrlist cur_array env = function
    [] -> (env, [])
  | ((s1,ext1),(s2,ext2),opt)::l ->
      let t = get_type env s2 ext2 in
      if t.toptions land Settings.tyopt_CHOOSABLE == 0 then
	raise_error ("Cannot choose randomly a bitstring from " ^ t.tname) ext2;
      begin
	match opt with
	  [] -> ()
	| (_,ext3)::_ ->
	    raise_error ("Restrictions should have no options in the left-hand side of an equivalence") ext3
      end;
      let (env',b) = add_in_env env s1 ext1 t cur_array in
      let (env'',bl) = check_lm_restrlist cur_array env' l in
      (env'', (b, ext1, NoOpt)::bl)

let rec get_fungroup_ext = function
  | PReplRestr(Some(_, _, (rep,ext)), _, _) -> ext
  | PReplRestr(None, ((_,ext),_,_)::_, _) -> ext
  | PReplRestr(None, [], f1::_) -> get_fungroup_ext f1
  | PReplRestr(None, [], []) -> Parsing_helper.internal_error "empty fungroup"
  | PFun((_,ext),_,_,_) -> ext
	
(* Check and simplify the left member of equivalence statements *)

let rec link_pat pat t =
  match pat with
  | PatVar b ->
      if Terms.refers_to b t then
	raise_error "Cyclic assignment in left member of equivalence" t.t_loc;
      Terms.link b (TLink t)
  | PatTuple(f,l) when f.f_options land Settings.fopt_BIJECTIVE != 0 ->
      let projs = Settings.get_proj f in
      List.iter2 (fun pat proj -> link_pat pat (Terms.app proj [t])) l projs
  | _ -> 
      raise_error "let with patterns that do not match all values are forbidden in left member of equivalences" t.t_loc      
	
let rec check_lm_term t = 
  match t.t_desc with
    Var(b, l) -> 
      (* Now, array references are allowed, with indices given as argument to the oracle
      if not (Terms.is_args_at_creation b l) then
	raise_error "Array references forbidden in left member of equivalences" t.t_loc;
      *)
      begin
      match b.link with
	TLink t ->
	  (* Adding the following test for safety. Should never be
	     triggered because only restrictions are allowed to take 
	     arguments as indices *)
	  if not (Terms.is_args_at_creation b l) then
	    raise_error "Array references to variables defined by let or <- forbidden in left member of equivalences" t.t_loc;
	  check_lm_term t
      |	NoLink ->
	  (* We make a copy of the term, to make sure that terms are not
	     physically equal, even if there are several references to the
	     variable via "let".
	     Physically equal terms cause a bug in [Def.build_def_member] *)
	  ([],Terms.copy_term DeleteFacts t)
      end 
  | ReplIndex _ ->
      raise_error "One cannot refer to replication indices in the left-hand side of equivalences" t.t_loc
  | FunApp(f,l) ->
      let (lres, lt) = List.split (List.map check_lm_term l) in
      (List.concat lres, Terms.build_term t (FunApp(f, lt)))
  | LetE(pat,t,t1,_) ->
      link_pat pat t;
      check_lm_term t1
  | ResE(b,t1) ->
      let (lres, t1') = check_lm_term t1 in
      (* Remove useless new; move it outside the oracle when it is useful *)
      if Terms.refers_to b t1' then
	((b, t.t_loc)::lres, t1')
      else
	(lres, t1')
  | (TestE _ | FindE _ | EventAbortE _ | GetE _ | InsertE _ | EventE _) ->
      raise_error "if, find, get, insert, event, and event_abort forbidden in left member of equivalences" t.t_loc

let rec reduce_rec t =
  let reduced = ref false in
  let t' = Terms.apply_eq_reds Terms.simp_facts_id reduced t in
  if !reduced then 
    reduce_rec t'
  else t
      
let check_oracle_id env seen_o (s,ext) cur_array arg_ty ret_ty =
  if List.exists (fun (o0,_,_) -> o0.oname == s) (!seen_o) then
    raise_error ("Oracle name " ^ s ^ " already used in this equivalence") ext;
  let o =
    { oname = s;
      oindices_type = List.map (fun ri -> ri.ri_type) cur_array;
      oarg_type = arg_ty;
      oret_type = Some [ret_ty];
      oret_ch = NoChan }
  in
  seen_o := (o, cur_array, env) :: (!seen_o);
  o
  
let rec check_lm_fungroup2 cur_array cur_restr env seen_o seen_repl = function
    PReplRestr(repl_opt, restrlist, funlist) as fg ->
      let (cur_array', repl_opt', env) =
	match repl_opt with
	| Some(repl_index_ref, idopt, (rep,ext)) ->
	    let repl_count' = 
	      match !repl_index_ref with
		Some b -> b
	      | None -> Parsing_helper.internal_error "Repl index should have been initialized in check_lm_fungroup2"
	    in
	    let cur_array' = repl_count' :: cur_array in
	    if List.memq repl_count'.ri_type (!seen_repl) then
	      raise_error "In an equivalence, different functions must have a different number of repetitions" ext;
	    seen_repl := repl_count'.ri_type :: (!seen_repl);
	    let env' =
	      match idopt with
	      | None -> env
	      | Some (id,ext) -> StringMap.add id (EReplIndex repl_count') env
	    in
	    (cur_array', Some repl_count', env')
	| None ->
	    (cur_array, None, env)
      in
      let (env',restrlist') = check_lm_restrlist cur_array' env restrlist in
      let (lrestr, funlist') = List.split (List.map (check_lm_fungroup2 cur_array' (restrlist'::cur_restr) env' seen_o seen_repl) funlist) in
      (* Check that all new from [restrlist'] are used *)
      List.iter2 (fun ((bname, ext),_,_) (b,_,_) ->
	if not (List.exists (Terms.refers_to_fungroup b) funlist') then
	  raise_error ("Random variable "^bname^" is not used") ext
	    ) restrlist restrlist';
      (* It is important that the "new" originally present in PReplRestr [restrlist']
	 are put first, so that they are used by combine_first later in the code *)
      let restrlist'' = restrlist' @ (List.map (fun (b,ext) -> (b, ext, NoOpt)) (List.concat lrestr)) in
      if restrlist'' == [] then
	begin
	  match funlist' with
	  | [Fun _] -> ()
	  | _ -> raise_error "In equivalences, under a replication without new, there should be a single function" (get_fungroup_ext fg)
	end;
      ([], ReplRestr(repl_opt', restrlist'', funlist'))
  | PFun(((s, ext) as o), arglist, tres, (priority, options)) ->
      let (env', arglist') = check_binder_list2 cur_array env arglist in
      let (tres',_,_) = check_term (Some []) cur_array env' None tres in
      let o' = check_oracle_id env' seen_o o cur_array (List.map (fun b -> b.btype) arglist') tres'.t_type in
      (* Note: restriction. Could be lifted, but simplifies cryptotransf.ml greatly 
	 Restriction partly lifted, by completing sequences of names with names already in the map.
      if not (List.for_all (List.for_all (fun b -> Terms.refers_to b tres')) cur_restr) then
	raise_error ("In equivalences, each expression should use all names defined by\n" ^
				    "random choices above it. This is a simplifying restriction.") (snd tres);
      *)
      check_bit_string_type (snd tres) tres'.t_type;
      List.iter2 (fun ((argname,ext),_) arg' ->
	if not (Terms.refers_to arg' tres') then
	  if (!Settings.front_end) == Settings.Channels then
            raise_error ("Variable " ^ argname ^ " is not used in the result of the function") ext
	  else
	    raise_error ("Variable " ^ argname ^ " is not used in the result of the oracle") ext
	      ) arglist arglist';
      let (restr, tres2) = check_lm_term tres' in
      Terms.cleanup();
      let tres3 = reduce_rec tres2 in
      List.iter2 (fun ((argname,ext),_) arg' ->
	if not (Terms.refers_to arg' tres3) then
	  raise_error ("After simplification, variable " ^ argname ^ " is not used in this term") tres'.t_loc
	    ) arglist arglist';
      (* Remove new that are unused after simplification *)
      let restr = List.filter (fun (b,_) -> Terms.refers_to b tres3) restr in
      let options' = ref StdOpt in
      List.iter (fun (s,ext) ->
	if s = "useful_change" then options' := UsefulChange else
	raise_error ("Unrecognized option " ^ s ^ ". Only \"useful_change\" is allowed.") ext) options;
      (restr, Fun(o', arglist', tres3, (priority, !options')))


let rec check_rm_restrlist options2 cur_array env restrlist0 = function
    [] -> (env, [])
  | ((s1,ext1),(s2,ext2),opt)::l ->
      let t = get_type env s2 ext2 in
      if t.toptions land Settings.tyopt_CHOOSABLE == 0 then
	raise_error ("Cannot choose randomly a bitstring from " ^ t.tname) ext2;
      let opt' = ref NoOpt in
      List.iter (fun (s,ext) ->
	if s = "unchanged" then 
	  if options2 = Computational then
	    opt' := Unchanged
	  else
	    raise_error "The option [unchanged] is allowed only for computational equivalences" ext
	else
	  raise_error "The only allowed option for random choices is [unchanged]" ext
	    ) opt;
      let opt'' = !opt' in
      let (env',b) =
	try
	  let b =
	    match get_global_binder_if_possible s1 with
	    | Some b ->
		if opt'' = Unchanged && not (List.exists (fun (_,(b',_,_)) -> Terms.equiv_same_vars b b') restrlist0) then
		  raise Not_found;
		b
	    | None ->
                  (* Look for a variable in the left-hand side with the same name and type *)
		let (_,(b0,_,_)) =
		  List.find (fun (((s1',_),_,_), (b0,_,_)) ->
		    s1' = s1 && b0.btype == t) restrlist0
		in
		Terms.create_binder_internal b0.sname b0.vname t cur_array 
	  in
	  (StringMap.add s1 (EVar b) env, b)
	with Not_found ->
	  (* A non-matching variable has been returned by [get_global_binder_if_possible s1]
	     or [List.find] failed. *)
	  if opt'' = Unchanged then
	    raise_error "When a random choice is marked [unchanged] in the right-hand side,\nthere should exist a corresponding random choice of the same name in the\nleft-hand side" ext1;
	  add_in_env env s1 ext1 t cur_array
      in
      let (env'',bl) = check_rm_restrlist options2 cur_array env' restrlist0 l in
      (env'', (b, ext1, opt'')::bl)

let rec check_rm_fungroup2 options2 cur_array env pfg0 fg0 fg = 
  match (pfg0, fg0, fg) with
    PReplRestr(_, prestrlist0, pfunlist0),
    ReplRestr(repl_opt0, restrlist0, funlist0),
    PReplRestr(repl_opt, restrlist, funlist) ->
      let (cur_array', repl_opt', env) =
	match repl_opt0, repl_opt with
	| Some repl_count0, Some (repl_index_ref, idopt, (rep,ext)) ->
	    let repl_count' = 
	      match !repl_index_ref with
		Some b -> b
	      | None -> Parsing_helper.internal_error "Repl index should have been initialized in check_rm_fungroup2"
	    in
	    if repl_count'.ri_type != repl_count0.ri_type then
	      raise_error "Different number of repetitions in left and right members of equivalence" ext;
	    let cur_array' = repl_count' :: cur_array in
	    let env' =
	      match idopt with
	      | None -> env
	      | Some (id,ext) -> StringMap.add id (EReplIndex repl_count') env
	    in
	    (cur_array', Some repl_count', env')
	| None, None ->
	    (cur_array, None, env)
	| _ ->
	    Parsing_helper.internal_error "Replication present on one side only, should have been detected earlier"
      in
      let (env',restrlist') = check_rm_restrlist options2 cur_array' env (combine_first prestrlist0 restrlist0) restrlist in
      if List.length funlist != List.length funlist0 then
	raise_error "Different number of functions in left and right sides of equivalence" (get_fungroup_ext fg);
      ReplRestr(repl_opt', restrlist', check_rm_fungroup_list2 options2 cur_array' env' pfunlist0 funlist0 funlist)
  | _, Fun(o0, arglist0, tres0, priority0), PFun((oname, ext), arglist, tres, _) ->
      let (env', arglist') = check_binder_list_rhs2 (snd tres) cur_array env arglist0 arglist in
      if List.length arglist' != List.length arglist0 then
	raise_error "Argument lists have different lengths in left and right members of equivalence" (snd tres);
      List.iter2 (fun b b' ->
	if b.btype != b'.btype then
	  raise_error "Incompatible types of arguments between left and right members of equivalence" (snd tres)
	    ) arglist' arglist0;
      let (tres',_,_) = check_term (Some []) cur_array env' None tres in
      (* Check that the type of the right member is the same as
	 the type of the corresponding left member. This is required
	 so that after transformation, the process remains well-typed. *)
      check_type (snd tres) tres' tres0.t_type;
      if oname <> o0.oname then
	raise_error "Oracle names should be the same in the left and right members of the equivalence" ext;
      (* The priority is ignored in the right-hand side; one takes
         the priority of the left-hand side *)
      Fun(o0, arglist', tres', priority0)
  | _, _, PReplRestr(Some(_, _, (_,ext)), _,_) ->
      raise_error "Left member is a function, right member is a replication" ext
  | _, _, PReplRestr(None, (((s1,ext1),_,_)::_),_) ->
      raise_error "Left member is a function, right member is a random number generation" ext1
  | _, _, PReplRestr(None, [],_) ->
      Parsing_helper.internal_error "Left member is a function, right member is PReplRestr with no replication and no new"
  | _, _, PFun(ch, arglist, tres, _) ->
      raise_error "Left member is a replication, right member is a function" (snd tres)

and check_rm_fungroup_list2 options2 cur_array env lpfg0 lfg0 lpfg =
  match lpfg0, lfg0, lpfg with
  | [], [], [] -> []
  | pfg0::rpfg0, fg0::rfg0, pfg::rpfg ->
      (check_rm_fungroup2 options2 cur_array env pfg0 fg0 pfg) ::
      (check_rm_fungroup_list2 options2 cur_array env rpfg0 rfg0 rpfg)
  | _ -> Parsing_helper.internal_error "Lists should have same length in check_rm_fungroup_list2"
	
let check_mode right = function
    Some (modes, ext) -> 
      if right then
	raise_error "Modes can be specified only in the left-hand side of an equivalence" ext;
      if modes = "all" then AllEquiv else 
      if modes = "exist" then ExistEquiv else
      raise_error "Only modes all and exist can be specified" ext
  | None -> ExistEquiv

let rec check_rm_funmode_list2 options2 env plm lm prm =
  match plm, lm, prm with
  | [], [], [] -> []
  | (pfg0, _, _)::rplm, (fg0, _)::rlm, (pfg, mode, _)::rprm ->
      (check_rm_fungroup2 options2 [] env pfg0 fg0 pfg, check_mode true mode) ::
      (check_rm_funmode_list2 options2 env rplm rlm rprm)
  | _ -> Parsing_helper.internal_error "check_rm_funmode_list2: lists of different lengths"
	
let check_opt_manual options =
  let options' = ref false in
  List.iter (fun (s,ext) ->
    if s = "manual" then
      options' := true 
    else
      raise_error ("Unrecognized option " ^ s ^ ". Only \"manual\" is allowed.") ext
	) options;
  !options'

let check_eqstatement normalize (name, equiv, (priority, options)) =
  match equiv with
  | EquivSpecial(special_name,args) ->
      let manual = check_opt_manual options in
      { eq_name = name;
	eq_fixed_equiv = None;
	eq_name_mapping = None;
	eq_special = Some(special_name,args);
	eq_prio = priority;
	eq_auto = ref (not manual)
      }
  | EquivNormal((mem1, ext1), (mem2, ext2), proba) ->
      current_location := InEquivalence;
      let mem1 =
	match mem1 with
	| [(PFun _) as f, mode, ext] ->
	    (* When there is no replication at all in the LHS,
               add an implicit replication with no new.
	       This is useful because the parser never considers implicit replications without "new". *)
	    [PReplRestr(None, [], [f]), mode, ext]
	| _ -> mem1
      in
      let mem2 =
	match mem1, mem2 with
	| [PReplRestr(None, _,_),_,_], _ ->
	    if List.for_all (fun (fg, mode, ext) ->
	      mode == None (* The mode can only be specified in the LHS; I check that for safety *) &&
	      match fg with
	      | PReplRestr(None,_,_) -> false
	      | _ -> true) mem2
	    then
	      (* We have an implicit replication in the LHS but not in the RHS.
		 This cannot be correct, let's add an implicit replication with no "new" in the RHS.
		 This is useful because the parser never considers implicit replications without "new". *)
	      [PReplRestr(None, [], List.map (fun (fg, mode, ext) -> fg) mem2), None(*no mode specified*), ext2]
	    else
	      (* In all other cases, leave the equivalence unchanged *)
	      mem2
	| _ -> mem2
      in
      let var_num_state = Terms.get_var_num_state() in
      let options' = ref true in
      let options2' = ref Decisional in
      List.iter (fun (s,ext) ->
	if s = "manual" then
	  options' := false 
	else if s = "computational" then 
	  options2' := Computational 
	else
	  raise_error ("Unrecognized option " ^ s ^ ". Only \"manual\" and \"computational\" are allowed.") ext
	    ) options;
      let seen_repl = ref [] in
      let seen_o = ref [] in
      set_binder_env 
	(List.fold_left (fun binder_env (fg, _, _) -> check_lm_fungroup1 [] (!env) binder_env fg) empty_binder_env mem1); (* Builds binder_env *)
      let mem1' = List.map (fun (fg, mode, ext) ->
	let (lrestr, fg') = check_lm_fungroup2 [] [] (!env) seen_o seen_repl fg in
	begin
	  match lrestr with
	  | [] -> ()
	  | (r1,ext)::_ ->
	      raise_error "when a new or <-R is inside the result of an oracle in the left-hand side of equiv, the oracle should occur under a replication or a new or <-R" ext
	end;	
	let res = (fg', check_mode false mode) in
	match res with
	| (ReplRestr(_,[],_), ExistEquiv) ->
	    raise_error "In equivalences, a function without any random variable should always be in mode [all]" ext
	| (ReplRestr(None,_,_),_) when List.length mem1 > 1 ->
	    raise_error "One cannot write an equivalence with omitted replication at the root when it has several function groups" ext
	| _ -> res
	      ) mem1
      in
      (* The probability formula must be checked in the binder_env for the
	 left-hand side of the equivalence. Arguments of Maxlength may use
	 variables of the left-hand side of the equivalence. *)
      let proba' = check_probability_formula2 (!seen_o, !seen_repl, true) (!env) proba in
      if List.length mem1 <> List.length mem2 then
	raise_error "Both sides of this equivalence should have the same number of function groups" ext2;
      set_binder_env
	(check_rm_funmode_list empty_binder_env mem1 mem1' mem2); (* Builds binder_env *)
      let mem2' = check_rm_funmode_list2 (!options2') (!env) mem1 mem1' mem2 in
      let equiv =
	{ eq_name = name;
	  eq_fixed_equiv = Some(mem1',mem2', (if proba' = Zero then [] else [SetProba proba' ]), !options2');
	  eq_name_mapping = None;
	  eq_special = None;
	  eq_prio = priority;
	  eq_auto = options' }
      in
      (* Check.check_equiv creates new variables (in make_let).
	 They should not collide with variables in the equivalence,
	 so we do Check.check_equiv before resetting var_num_state *)
      let equiv' = Check.check_equiv normalize equiv in
      (* The variables defined in the equivalence are local,
         we can reuse the same numbers in other equivalences or
         in the process. *)
      Terms.set_var_num_state var_num_state;
      equiv' 

(* Check collision statement *)

let check_collision_var env (s, ext) =
  try 
    match StringMap.find s env with
      EVar(v) -> v
    | d -> raise_error (s ^ " was previously declared as a " ^ (decl_name d) ^". Expected a variable.") ext
  with Not_found ->
    raise_error (s ^ " not defined. Expected a variable.") ext

let make_and_indep_cond c1 c2 =
  match c1, c2 with
    IC_True, _ -> c2
  | _, IC_True -> c1
  | _ -> IC_And(c1, c2)
	
let make_or_indep_cond c1 c2 =
  match c1, c2 with
    IC_True, _ | _, IC_True -> IC_True
  | _ -> IC_Or(c1, c2)
	
let rec check_side_cond restr_may_be_equal forall restr env = function
  | PAnd(t1,t2), ext ->
      let (indep_cond1, t1') = check_side_cond restr_may_be_equal forall restr env t1 in
      let (indep_cond2, t2') = check_side_cond restr_may_be_equal forall restr env t2 in
      (make_and_indep_cond indep_cond1 indep_cond2,
       Terms.make_and_ext ext t1' t2')
  | POr(t1,t2), ext ->
      let (indep_cond1, t1') = check_side_cond restr_may_be_equal forall restr env t1 in
      let (indep_cond2, t2') = check_side_cond restr_may_be_equal forall restr env t2 in
      if indep_cond1 = IC_True && indep_cond2 = IC_True then
	(IC_True, Terms.make_or_ext ext t1' t2')
      else if (Terms.is_true t1') && (Terms.is_true t2') then
	(make_or_indep_cond indep_cond1 indep_cond2, Terms.make_true())
      else
	raise_error "Cannot mix terms and independence conditions in a disjunction" ext
  | PIndepOf(((_, ext1) as v1), ((_, ext2) as v2)), ext ->
      (* v1 independent of v2 *)
      let v1' = check_collision_var env v1 in
      let v2' = check_collision_var env v2 in
      (* With the option "random_choices_may_be_equal", independence conditions are allowed between random choices
	 so [v1'] may be bound by forall or restr, which is always the case: nothing to check in this case. *)
      if (not restr_may_be_equal) && (not (List.memq v1' forall)) then
	raise_error "independent variables should be bound by \"forall\"" ext1;
      if not (List.memq v2' restr) then
	raise_error "variables of which other variables are independent should be bound by \"new\" or \"<-R\"" ext2;
      (IC_Indep(v1', v2'), Terms.make_true())
  | PLetE(pat,t1,t2,None), ext ->
      let t1' = check_term_nobe env t1 in
      let env' = link_ppat env pat t1' in
      check_side_cond restr_may_be_equal forall restr env' t2
  | t ->
      let t' = check_term_nobe env t in
      check_type (snd t) t' Settings.t_bool;
      (IC_True, t')

let check_opt_coll options =
  let manual = ref false in
  let alias_gen = ref true in
  List.iter (fun (s,ext) ->
    match s with
    | "manual" -> manual := true 
    | "no_alias_generalization" -> alias_gen := false
    | _ -> 
	raise_error ("Unrecognized option " ^ s ^ ". Only \"manual\" and \"no_alias_generalization\" are allowed.") ext
	) options;
  !manual, !alias_gen
	
let check_collision env (name, restr, forall, t1, proba, t2, side_cond, options, col_options) =
  let manual, alias_gen = check_opt_coll col_options in
  (* Note: This function uses check_binder_list, which calls
     Terms.create_binder0, so it does not rename the variables.
     That is why I do not save and restore the variable
     numbering state. *)
  let restr_may_be_equal = ref false in
  List.iter (fun (s,ext) ->
    if s = "random_choices_may_be_equal" then
      restr_may_be_equal := true
    else
      raise_error "The only allowed option for collisions is random_choices_may_be_equal" ext
    ) options;
  set_binder_env empty_binder_env;
  let (env',restr') = check_binder_list env restr in
  List.iter2 (fun b (_,(_,ext)) ->
    if b.btype.toptions land Settings.tyopt_CHOOSABLE == 0 then
      raise_error ("Cannot choose randomly a bitstring from " ^ b.btype.tname) ext
      ) restr' restr;
  let (env'',forall') = check_binder_list_ty env' forall in
  let proba' = check_probability_formula2 ([], [], true) env'' proba in
  let t1' = check_term_nobe env'' t1 in
  if not (List.for_all (fun b -> Terms.refers_to b t1') (restr' @ forall')) then
    raise_error "In collision statements, all bound variables should occur in the left-hand side" (snd t1);
  let t2' = check_term_nobe env'' t2 in
  check_bit_string_type (snd t1) t1'.t_type;
  if t1'.t_type != t2'.t_type then 
    raise_error "Both sides of a collision statement should have the same type" (snd t2);
  let (indep_cond', side_cond') = check_side_cond (!restr_may_be_equal) forall' restr' env'' side_cond in
  collisions := (Terms.create_collision (Normal name) restr' forall' t1' proba' t2'
		   indep_cond' side_cond' (!restr_may_be_equal) (ref (not manual)) alias_gen) :: (!collisions)


(* For collision information in "move array" 
   Be careful to always create fresh identifiers, so that the
   identifiers in the printed equivalence are distinct. *)
												       
let check_binder_expect env expect ((s,ext),(s2,ext2)) =
  let t = get_type env s2 ext2 in
  let b = 
    match expect with
    | ExpectType ty ->
	if ty != t then raise (Error("Random values in collisions should have the same type as the result of the random function", ext2));
	Terms.create_binder s t []
    | ExpectVar b -> 
	if b.btype != t then raise (Error("Random values in collisions in \"move array\" should have the same type as the variable(s) we move", ext2));
	b
  in
  if t.toptions land Settings.tyopt_CHOOSABLE == 0 then
    raise_error ("Cannot choose randomly a bitstring from " ^ t.tname) ext2;
  if (StringMap.mem s env) then
    input_warning ("identifier " ^ s ^ " rebound") ext;
  (StringMap.add s (EVar b) env, b)

let rec check_binder_expect_list env expect = function
  | [] -> (env,[])
  | id_ty::l ->
      let (env',b) = check_binder_expect env expect id_ty in
      let (env'',l') = check_binder_expect_list env' expect l in
      (env'', b::l')
    
let check_binder_moc env ((s,ext),ty) =
  let t =
    match ty with
    | Tid (s2,ext2) -> get_type env s2 ext2
    | TBound (_,ext2) -> raise_error "interval type not allowed here" ext2
  in
  let b = Terms.create_binder s t [] in
  if (StringMap.mem s env) then
    input_warning ("identifier " ^ s ^ " rebound") ext;
  (StringMap.add s (EVar b) env, b)

let rec check_binder_list_moc env = function
    [] -> (env,[])
  | id_ty::l ->
      let (env',b) = check_binder_moc env id_ty in
      let (env'',l') = check_binder_list_moc env' l in
      (env'', b::l')

let check_special_equiv_coll env expect (forall, restr, t) =
  set_binder_env empty_binder_env;
  let (env',forall') = check_binder_list_moc env forall in
  let (env'',restr') = check_binder_expect_list env' expect restr in
  let t' = check_term_nobe env'' t in
  check_bit_string_type (snd t) t'.t_type;
  if not (List.for_all (fun b -> Terms.refers_to b t') (restr' @ forall')) then
    raise_error "In collision statements, all bound variables should occur in the left-hand side" (snd t);
  (forall', restr', t')
  
(* Check process
   check_process returns the process, as well as a list of oracles with their
   types (oracle name, list of index types, list of args types, list of result types)
   check_oprocess returns the process, as well as a list of oracles with their
   types and the list of result types returned by this process
 *)

let mergeres ext topt1 topt2 =
  try
    Terms.mergetypesopt topt1 topt2
  with IncompatibleTypes ->
    raise_error ("Several branches of a process have incompatible return types, type "
                 ^(Display.opt_type_tuple_to_string topt1)
                 ^" incompatible with "
                 ^(Display.opt_type_tuple_to_string topt2)
                ) ext

let ostructure_elem() =
  if !Settings.front_end == Settings.Channels then
    "inputs on channel"
  else
    "definitions of oracle"
      
let check_distinct ext l1 l2 =
  List.iter (fun o ->
    if List.memq o l1 then
      raise_error ("Duplicate "^(ostructure_elem())^" " ^ o.oname ^" in parallel") ext
	) l2

let rec merge_list l1 l2 =
  match l1,l2 with
  | [], l2 -> l2
  | l1, [] -> l1
  | a1::l1r,a2::l2r ->
      if a1.oname < a2.oname then
	a1 :: (merge_list l1r l2)
      else
	a2 :: (merge_list l1 l2r)

let merge_oracle_chans ext c1 c2 =
  match c1,c2 with
  | ChUnset, _ | _, ChUnset -> assert false
  | NoChan, x -> x
  | x, NoChan -> x
  | Channel c1, Channel c2 when c1 = c2 -> Channel c1
  | Channel c1, Channel c2 -> raise_error ("all outputs below inputs on a given channel must be made on the same channel; here both channels "^c1^" and "^c2^" are used") ext
      

let merge_oracle_types ext o (tindex',targs', tres', tcs') =
  (try
    o.oindices_type <- Terms.merge_type_lists o.oindices_type tindex'
  with IncompatibleTypes -> 
    raise_error ((String.capitalize_ascii (ostructure_elem()))^" " ^ o.oname ^ " with different replication indexes types") ext);
  (try 
    o.oarg_type <- Terms.merge_type_lists o.oarg_type targs'
  with IncompatibleTypes -> 
    raise_error ((String.capitalize_ascii (ostructure_elem()))^" " ^ o.oname ^ " with different argument types") ext);
  (try
    o.oret_type <- Terms.mergetypesopt o.oret_type tres'
  with IncompatibleTypes ->
    raise_error ((String.capitalize_ascii (ostructure_elem()))^" " ^ o.oname ^ " with different result types") ext);
  o.oret_ch <- merge_oracle_chans ext o.oret_ch tcs'
	
let rec merge_struct ext l1 l2 =
  match l1,l2 with
  | [], l2 -> l2
  | l1, [] -> l1
  | a1::l1r, a2::l2r ->
      if a1 == a2 then
	a1 :: (merge_struct ext l1r l2r) 
      else if a1.oname < a2.oname then
	a1 :: (merge_struct ext l1r l2)
      else
	a2 :: (merge_struct ext l1 l2r)

let check_no_communications ext l1 l2 =
  (* check if any oracle in l2 has any return channel with the same name and index replication arity as a possible input oracle  name in l1 *)
  let can_communicate x1 x2 =
    let in_chans l =
      List.map (fun o -> (o.oname,List.length o.oindices_type)) l
    in
    List.iter (fun ret_oracle ->
      match ret_oracle.oret_ch with
        | ChUnset -> assert false
        | NoChan -> ()
        | Channel cname -> 
            let out_channel = (cname, List.length ret_oracle.oindices_type) in
            if List.mem out_channel (in_chans x2) then
              raise_error ("The output on channel " ^ cname ^" may be received by another channel in parallel.") ext
      ) x1 in
  can_communicate l1 l2;
  can_communicate l2 l1

let check_compatible = merge_struct  

let same_oracles ext l1 l2 =
  if Terms.equal_lists (==) l1 l2 then l1 else
  raise_error ("Both sides of diff should define the same "^
	       (if !Settings.front_end == Settings.Channels then "channel inputs" else "oracle definitions")) ext
    
let dummy_channel = "dummy_channel"

    
(* Check channels / oracles *)

let oracle_id_env = (Hashtbl.create 7: (string, oracle) Hashtbl.t)

let check_channel cur_array env ((s, ext), idx_opt) =
  begin
    match idx_opt with
      None -> ()
    | Some l ->
	if List.length l <> List.length cur_array then
	  raise_error "The indices of a channel should be the current replication indices" ext;
	List.iter2 (fun (id,ext) ri ->
	  try
	    match StringMap.find id env with
	      EReplIndex ri' ->
		if ri != ri' then
		  raise_error "The indices of a channel should be the current replication indices in the right order" ext
	    | d ->
		raise_error (id ^ " was previously declared as a "^(decl_name d)^". Expected a current replication index") ext
	  with Not_found ->
	    raise_error (id ^ " not defined. Expected a current replication index.") ext
	      ) l cur_array
  end;
  try 
    match StringMap.find s env with
      EChannel(c) -> c
    | d -> raise_error (s ^ " was previously declared as a "^(decl_name d)^". Expected a channel.") ext
  with Not_found -> 
    raise_error (s ^ " not defined. Expected a channel.") ext

      
let check_process_oracle_ids env (((s, ext) as id), idx_opt) cur_array input_type tres cres =
  let ind_type = List.map (fun ri -> ri.ri_type) cur_array in
  if (!Settings.front_end) == Settings.Channels then
    begin
      let c = check_channel cur_array env (id, idx_opt) in
      if c.oret_ch = ChUnset then
	begin
	  c.oindices_type <- ind_type;
	  c.oarg_type <- input_type;
	  c.oret_type <- tres;
	  c.oret_ch <- cres
	end
      else
	merge_oracle_types ext c (ind_type, input_type, tres, cres);
      c
    end
  else
    try
      let o = Hashtbl.find oracle_id_env s in
      merge_oracle_types ext o (ind_type, input_type, tres, cres);
      o
    with Not_found -> 
      let o = { oname = s;
		oindices_type = ind_type;
		oarg_type = input_type;
		oret_type = tres;
		oret_ch = cres }
      in
      Hashtbl.add oracle_id_env s o;
      o


(* Add a role *)

let check_opt opt =
  List.map (function
      PRead (id,(f,ext')) ->
	let b = get_global_binder "in read variables" id in
	Read (b,f)
    | PWrite (id,(f,ext')) ->
	let b = get_global_binder "in written variables" id in
	Write (b,f)
          ) opt
    
let add_role ((id,ext),opt) ip1 ip2 =
  let opt' =  check_opt opt in
  if !set_role1 then
    begin
      try 
	let _=StringMap.find id !impl_roles1 in
	raise_error ("Role " ^ id ^ " has already been defined") ext
      with Not_found ->
	impl_roles1 := StringMap.add id (ip1,opt') !impl_roles1
    end;
  if !set_role2 then
    begin
      try 
	let _=StringMap.find id !impl_roles2 in
	raise_error ("Role " ^ id ^ " has already been defined") ext
      with Not_found ->
	impl_roles2 := StringMap.add id (ip2,opt') !impl_roles2
    end
  
let rec check_process defined_refs cur_array env prog = function
    PBeginModule (a,p), ext ->
      if (prog <> None) then
         raise_error "Roles cannot be nested" ext
      else
        let (p,oracles,ip1,ip2) = check_process defined_refs cur_array env (Some a) p in
          add_role a ip1 ip2;
          (p,oracles,ip1,ip2)
  | PNil, ext -> (new_iproc Nil ext, [], new_iproc Nil ext, new_iproc Nil ext)
  | PPar(p1,p2), ext -> 
      let (p1',oracles1,ip11,ip12) = check_process defined_refs cur_array env prog p1 in
      let (p2',oracles2,ip21,ip22) = check_process defined_refs cur_array env prog p2 in
      (* Check Invariant: no two same oracle (or inputs channel name) in different branches. *) 
      check_distinct ext oracles1 oracles2;
      (* Check Invariant: no input channel on one side maps to some output on the other side. *)
      check_no_communications ext oracles1 oracles2;
      (new_iproc (Par(p1',p2')) ext, merge_list oracles1 oracles2,
       new_iproc (Par(ip11,ip21)) ext,
       new_iproc (Par(ip12,ip22)) ext)
  | PRepl(repl_index_ref,idopt,(s2,ext2),p), ext ->
      let b' = 
	match !repl_index_ref with
	  Some b -> b
	| None -> Parsing_helper.internal_error "Repl index should have been initialized in check_process"
      in
      let env' =
	match idopt with
	  None -> env
	| Some(id,ext) -> StringMap.add id (EReplIndex b') env
      in
      let (p',oracles,ip1, ip2) = check_process defined_refs (b'::cur_array) env' prog p in
      (new_iproc (Repl(b', p')) ext, oracles,
       new_iproc (Repl(b',ip1)) ext,
       new_iproc (Repl(b',ip2)) ext)
  | PInput(t, pat, p), ext ->
      let (env', pat0', pat1', pat2') = check_pattern (Some defined_refs) cur_array env prog None pat in
      let (p', tres, cres, oracles, ip1, ip2) = check_oprocess defined_refs cur_array env' prog p in
      let input_type, patl0'', patl1'', patl2''  =
	if (!Settings.front_end) == Settings.Channels then
          match pat0', pat1', pat2' with
	  | PatTuple(f0,patl0), PatTuple(f1,patl1), PatTuple(f2,patl2) when f0.f_name = "" && f0 == f1 && f0 == f2 ->
	      (* Open basic tuples, to avoid useless tuples of tuples *)
	      List.map get_type_for_pattern patl0, patl0, patl1, patl2
          | _ ->
	      let ty = get_type_for_pattern pat0' in
              [ty], [pat0'], [pat1'], [pat2']
	else
	  match pat0', pat1', pat2' with
	  | PatTuple(_,patl0), PatTuple(_,patl1), PatTuple(_,patl2) ->
	      List.map get_type_for_pattern patl0, patl0, patl1, patl2
          | _ -> internal_error "One can only have a tuple as argument"
      in
      let oracle = check_process_oracle_ids env t cur_array input_type tres cres in
      if List.memq oracle oracles then
	raise_error ("Duplicate "^(ostructure_elem())^" " ^ oracle.oname ^ 
			  "\n(The second one is located under the return of the first one.)") ext;
      let new_oracle_decl =
        { ooracle = oracle;
          oindices = cur_array }
      in      
      (new_iproc (OracleDecl(new_oracle_decl, patl0'', p')) ext,
       merge_list [oracle] oracles (* Maintain the invariant that the list of oracles is sorted *),
       new_iproc (OracleDecl(new_oracle_decl,patl1'',ip1)) ext,
       new_iproc (OracleDecl(new_oracle_decl,patl2'',ip2)) ext)
  | PLetDef((s,ext),args), _ ->
      let (env', vardecl, p) = get_process env s ext in
      if List.length vardecl != List.length args then
	raise_error ("Process "^s^" expects "^(string_of_int (List.length vardecl))^" argument(s), but is here given "^(string_of_int (List.length args))^" argument(s)") ext;
      let (args0', args1', args2') = list_map_3res (check_term (Some defined_refs) cur_array env prog) args in
      (* Only simple terms (variables, replication indices, and function 
	 applications) are allowed in arguments. *)
      List.iter (fun t ->
	check_no_iffindletnewevent "argument of a parametric input process" t.t_loc t) (args0' @ args1' @ args2');
      let (env'', lets0, lets1, lets2) = check_args cur_array env' vardecl (args0', args1', args2') in
      let (p', oracles, ip1, ip2) = check_process [] cur_array env'' prog p in
      (* Array accesses are forbidden for the arguments of the process.
	 (This is guaranteed in check_process1.)
	 So we just replace the variables with their values. *)
      let inst lets p =
	Terms.auto_cleanup (fun () ->
	  List.iter (function
	      PatVar b, t -> Terms.link b (TLink t)
	    | _ -> Parsing_helper.internal_error "PLetDef: lets should bind variables"
		  ) lets;
	  copy_process Links_Vars p
	    )
      in
      let p_inst' = inst lets0 p' in
      let ip1_inst = inst lets1 ip1 in
      let ip2_inst = inst lets2 ip2 in
      (p_inst', oracles, ip1_inst, ip2_inst)
  | _, ext ->
      raise_error "input process expected" ext

and check_oprocess defined_refs cur_array env prog = function
    PYield, ext ->
      (new_oproc Yield ext, None, NoChan, [],
       new_oproc Yield ext, new_oproc Yield ext)
  | PEventAbort(s,ext), ext' ->
      let f = get_event env s ext in
      check_type_list ext [] [] (List.tl (fst f.f_type));
      let p_desc = EventAbort(f) in
      (new_oproc p_desc ext', None, NoChan, [],
       new_oproc p_desc ext',
       new_oproc p_desc ext')
  | PRestr((s1,ext1),(s2,ext2),p), ext ->
      let t = get_type env s2 ext2 in
        if t.toptions land Settings.tyopt_CHOOSABLE == 0 then
	  raise_error ("Cannot choose randomly a bitstring from " ^ t.tname) ext2;
        let (env',b) = add_in_env env s1 ext1 t cur_array in
        let (p', tres, cres, oracle,ip1',ip2') = check_oprocess defined_refs cur_array env' prog p in
          (new_oproc (Restr(b, p')) ext, tres, cres, oracle,
           new_oproc (Restr(b, ip1')) ext,
	   new_oproc (Restr(b, ip2')) ext)
  | PLetDef((s,ext), args), _ ->
      let (env', vardecl, p) = get_process env s ext in
      if List.length vardecl != List.length args then
	raise_error ("Process "^s^" expects "^(string_of_int (List.length vardecl))^" argument(s), but is here given "^(string_of_int (List.length args))^" argument(s)") ext;
      let (args0', args1', args2') = list_map_3res (check_term (Some defined_refs) cur_array env prog) args in
      let (env'', lets0, lets1, lets2) = check_args cur_array env' vardecl (args0', args1', args2') in
      let (p', tres, cres, oracle, ip1', ip2')  = check_oprocess [] cur_array env'' prog p in
      (Terms.put_lets lets0 p' (Terms.oproc_from_desc Yield), tres, cres, oracle,
       Terms.put_lets lets1 ip1' (Terms.oproc_from_desc Yield),
       Terms.put_lets lets2 ip2' (Terms.oproc_from_desc Yield))
  | PTest(t,p1,p2), ext ->
      let (t0',t1',t2') = check_term (Some defined_refs) cur_array env prog t in
      check_type (snd t) t0' Settings.t_bool;
      let (p1',tres1,cres1,oracles1,ip11',ip12') = check_oprocess defined_refs cur_array env prog p1 in
      let (p2',tres2,cres2,oracles2,ip21',ip22') = check_oprocess defined_refs cur_array env prog p2 in
      (new_oproc (Test(t0', p1', p2')) ext,
       mergeres ext tres1 tres2,
       merge_oracle_chans ext cres1 cres2,
       check_compatible ext oracles1 oracles2,
       new_oproc (Test(t1', ip11', ip21')) ext,
       new_oproc (Test(t1', ip12', ip22')) ext)
  | PFind(l0,p2,opt), ext ->
      if in_impl_process() && prog <> None then
	raise_error "Implementation does not support find" ext;
        (* So we do not need to bother generating the second and third processes, useful
           for implementation only *)
      let find_info,_ = parse_unique "find" opt in
      let (p2', tres2, cres2, oracles2,_,_) = check_oprocess defined_refs cur_array env prog p2 in
      let trescur = ref tres2 in
      let crescur = ref cres2 in
      let oraclescur = ref oracles2 in
      let rec add env = function
	  [] -> (env,[])
	| ((s0,ext0),(s1,ext1),(s2,ext2))::bl ->
	    let p = get_param env s2 ext2 in
	    let (env',b) = add_in_env env s0 ext0 (type_for_param p) cur_array in
	    let (env'',bl') = add env' bl in
	      (env'',b::bl')
      in
      let l0' = List.fold_left (fun accu (bl_ref,bl,def_list,t,p1) ->
	try 
	  let (env', bl') = add env bl in
	  let bl'' = !bl_ref in (* recover replication indices *)
	  let env'' = List.fold_left2 (fun env (_,(s1, ext1),_) b -> StringMap.add s1 (EReplIndex b) env) env bl bl'' in
	  let cur_array' = bl'' @ cur_array in
	  let bl_bin = List.combine bl' bl'' in
	  let def_list' = List.map (check_br cur_array' env'' prog) def_list in
	  let (defined_refs_t, defined_refs_p1) = Terms.defined_refs_find bl_bin def_list' defined_refs in
	  let (t',_,_) = check_term (Some defined_refs_t) cur_array' env'' prog t in
	  check_no_event_insert (snd t) false t';
	  check_type (snd t) t' Settings.t_bool;
	  let (p1', tres1, cres1, oracles1,_,_) = check_oprocess defined_refs_p1 cur_array env' prog p1 in
          trescur := mergeres ext tres1 (!trescur);
          crescur := merge_oracle_chans ext cres1 (!crescur);
	  oraclescur := check_compatible ext oracles1 (!oraclescur);
	  (bl_bin, def_list', t', p1')::accu
	with RemoveFindBranch ->
	  accu
	    ) [] l0
      in
      let p' = new_oproc (Find(List.rev l0', p2', find_info)) ext in
      (p', (!trescur), (!crescur), (!oraclescur), p', p')
  | POutput(rt,t1,t2,p), ext ->
      let (t20', t21', t22') = check_term (Some defined_refs) cur_array env prog t2 in
      begin
        match t20'.t_type.tcat with
	  Interv _ -> raise_error "Cannot output a term of interval type" (snd t2)
        |	_ -> ()
      end;
      if rt && prog = None then
	raise_error "Cannot close inexistent role" ext;
      let (p', oracles,ip1'',ip2'') = check_process defined_refs cur_array env (if rt then None else prog) p in
      let ip1',ip2' = if rt then (iproc_from_desc Nil),(iproc_from_desc Nil) else ip1'',ip2'' in
      if (!Settings.front_end) == Settings.Channels then
	let c = check_channel cur_array env t1 in
	let tl20', tl21', tl22', tl0 =
	  match t20'.t_desc, t21'.t_desc, t22'.t_desc with
	  | FunApp(f0,tl0), FunApp(f1,tl1), FunApp(f2,tl2) when f0.f_name = "" && f0 == f1 && f0 == f2 ->
	      tl0, tl1, tl2, tl0
	  | _ -> [t20'], [t21'], [t22'], [t20']
	in
        (new_oproc (Return(tl20', p')) ext,
	 Some (List.map (fun t -> t.t_type) tl0), Channel c.oname, oracles,
	 new_oproc (Return(tl21', ip1')) ext,
	 new_oproc (Return(tl22', ip2')) ext)
      else
	begin
	  match t20'.t_desc, t21'.t_desc, t22'.t_desc with
	    FunApp(_,tl0), FunApp(f1,tl1), FunApp(f2,tl2) ->
	      (new_oproc (Return(tl0, p')) ext, 
	       Some (List.map (fun t -> t.t_type) tl0), NoChan, oracles,
               new_oproc (Return(tl1, ip1')) ext,
	       new_oproc (Return(tl2, ip2')) ext)
	  | _ -> 
	      internal_error "One can only return a tuple"
	end
  | PLet(pat, t, p1, p2), ext ->
      let (t0',t1',t2') = check_term (Some defined_refs) cur_array env prog t in
      let (env', pat0', pat1', pat2') = check_pattern (Some defined_refs) cur_array env prog (Some t0'.t_type) pat in
      let (p1',tres1,cres1,oracles1,ip11',ip12') = check_oprocess defined_refs cur_array env' prog p1 in
      let (p2',tres2,cres2,oracles2,ip21',ip22') = check_oprocess defined_refs cur_array env prog p2 in
      if Terms.is_full_pattern pat0' && p2'.p_desc != Yield then
	raise_error "When a let has a non-empty else part, the pattern must not match all values" ext;
      (new_oproc (Let(pat0', t0', p1', p2')) ext, 
       mergeres ext tres1 tres2,
       merge_oracle_chans ext cres1 cres2,
       check_compatible ext oracles1 oracles2,
       new_oproc (Let(pat1', t1', ip11', ip21')) ext,
       new_oproc (Let(pat2', t2', ip12', ip22')) ext)
  | PEvent((PFunApp((s,ext0),tl), ext), p), ext2 ->
      let f = get_event env s ext0 in
      let tl0', tl1', tl2' = list_map_3res (check_term (Some defined_refs) cur_array env prog) tl in
      check_type_list ext tl tl0' (List.tl (fst f.f_type));
      let tupf = Settings.get_tuple_fun (List.map (fun ri -> ri.ri_type) cur_array) in
      let tcur_array =
	Terms.new_term Settings.t_bitstring ext2
	  (FunApp(tupf, List.map Terms.term_from_repl_index cur_array))
      in
      let (p', tres, cres, oracles,ip1',ip2') = check_oprocess defined_refs cur_array env prog p in
      let event0 = Terms.new_term Settings.t_bool ext2 (FunApp(f, tcur_array::tl0')) in
      let event1 = Terms.new_term Settings.t_bool ext2 (FunApp(f, tcur_array::tl1')) in
      let event2 = Terms.new_term Settings.t_bool ext2 (FunApp(f, tcur_array::tl2')) in
      (new_oproc (EventP(event0, p')) ext2, tres, cres, oracles,
       new_oproc (EventP(event1, ip1')) ext2,
       new_oproc (EventP(event2, ip2')) ext2)
  | PGet((id,ext),patl,topt,p1,p2,opt), ext' ->
      let find_info0, find_infoi = parse_unique "get" opt in
      let tbl = get_table env id ext in
      if List.length patl != List.length tbl.tbltype then
	raise_error ("Table "^id^" expects "^
		     (string_of_int (List.length tbl.tbltype))^
		     " argument(s), but is here given "^
		     (string_of_int (List.length patl))^" argument(s)") ext;
      let (p2',tres2,cres2,oracles2,ip21',ip22') = check_oprocess defined_refs cur_array env prog p2 in
      let (env', patl0', patl1', patl2') = check_pattern_list (Some defined_refs) cur_array env prog (List.map (fun x->Some x) tbl.tbltype) patl in
      let topt0', topt1', topt2' = 
	match topt with 
	  None -> None, None, None 
	| Some t -> 
	    let t0',t1',t2' = check_term (Some defined_refs) cur_array env' prog t in
	    check_no_event_insert (snd t) true t0';
	    check_type (snd t) t0' Settings.t_bool;
	    Some t0', Some t1', Some t2'
      in
      let (p1',tres1,cres1,oracles1,ip11',ip12') = check_oprocess defined_refs cur_array env' prog p1 in
        (new_oproc (Get(tbl, patl0',topt0',p1', p2', find_info0)) ext',
         mergeres ext tres1 tres2,
         merge_oracle_chans ext cres1 cres2,
         check_compatible ext oracles1 oracles2,
         new_oproc (Get(tbl, patl1',topt1',ip11', ip21', find_infoi)) ext',
	 new_oproc (Get(tbl, patl2',topt2',ip12', ip22', find_infoi)) ext')
          
  | PInsert((id,ext),tl,p),ext2 ->
      let tbl = get_table env id ext in
      let (t0',t1',t2') = list_map_3res (check_term (Some defined_refs) cur_array env prog) tl in
      check_type_list ext2 tl t0' tbl.tbltype;
      let (p',tres,cres,oracles,ip1',ip2') = check_oprocess defined_refs cur_array env prog p in
      (new_oproc (Insert(tbl, t0', p')) ext2, tres, cres, oracles,
       new_oproc (Insert(tbl, t1', ip1')) ext2,
       new_oproc (Insert(tbl, t2', ip2')) ext2)

  | PDiffIndistProc(p1,p2), ext ->
      let (p1',tres1,cres1,oracles1,ip11',ip12') = check_oprocess defined_refs cur_array env prog p1 in
      let (p2',tres2,cres2,oracles2,ip21',ip22') = check_oprocess defined_refs cur_array env prog p2 in
      let b = get_diff_bit() in
      let cond = Terms.new_term b.btype ext (Var(b,[])) in
      (new_oproc (Test(cond, p1', p2')) ext,
       mergeres ext tres1 tres2,
       merge_oracle_chans ext cres1 cres2,
       same_oracles ext oracles1 oracles2,
       ip11',
       ip22')
	  
  | _, ext -> 
      raise_error "non-input process expected" ext
        
(* Macro expansion *)

let rename_table = ref StringMap.empty

let get_rename_state() =
  (!rename_table, Terms.get_var_num_state())

let set_rename_state (rename_tbl, var_num_state) =
  rename_table := rename_tbl;
  Terms.set_var_num_state var_num_state
    
let rename_ident i = 
  try
    StringMap.find i (!rename_table)
  with Not_found ->
    let r = Terms.fresh_id i in
    rename_table := StringMap.add i r (!rename_table);
    r

let rename_ie (i,ext) = (rename_ident i, ext)

(* Given the code of the parser, projections can appear only
   as arguments of PFunApp and PAFunApp, so it is enough to call
   [rename_ident_proj] in these two cases. *)
let rename_ident_proj i =
  match Terms.get_name_proj i with
  | None -> rename_ident i
  | Some (s1,s2) -> s1 ^ (rename_ident s2)
      
let rename_ie_proj (i,ext) = (rename_ident_proj i, ext)

let rename_id_und = function
  | Ident i -> Ident (rename_ie i)
  | (Underscore _) as x -> x
    
let rename_channel (i, idx_opt) =
  (rename_ie i,
   match idx_opt with
     None -> None
   | Some l -> Some (List.map rename_ie l))
    
let rename_ty = function
    Tid i -> Tid (rename_ie i)
  | TBound n -> TBound (rename_ie n)
    
let rec rename_term (t,ext) = 
  let t' = match t with
    PIdent i -> PIdent (rename_ie i)
  | PArray(i,l) -> PArray(rename_ie i, List.map rename_term l)
  | PFunApp(f,l) -> PFunApp(rename_ie_proj f, List.map rename_term l)
  | PQEvent(inj,t) -> PQEvent(inj, rename_term t)
  | PTuple(l) -> PTuple(List.map rename_term l)
  | PTestE(t1,t2,t3) -> PTestE(rename_term t1, rename_term t2, rename_term t3)
  | PFindE(l,t,opt) -> PFindE(List.map (fun (_,bl, def_list, c,t) ->
      (ref [], 
       List.map (fun (x,y,t) -> (rename_ie x, rename_ie y, rename_ie t)) bl,
       List.map rename_br def_list,
       rename_term c,
       rename_term t)) l, rename_term t, opt)
  | PLetE(pat, t1, t2, topt) ->
      PLetE(rename_pat pat, rename_term t1, rename_term t2, match topt with
	None -> None
      |	Some t -> Some (rename_term t))
  | PResE(i,ty,t) -> PResE(rename_ie i, rename_ie ty, rename_term t)
  | PEventAbortE(i) -> PEventAbortE(rename_ie i)
  | PEventE(t,p) -> PEventE(rename_term t, rename_term p)
  | PGetE(id, patlist, topt, p1, p2, opt) ->
      PGetE(rename_ie id, List.map rename_pat patlist, (match topt with None -> None|Some t -> Some (rename_term t)), rename_term p1, rename_term p2, opt)
  | PInsertE(id, tlist, p) ->
      PInsertE(rename_ie id, List.map rename_term tlist, rename_term p)
  | PEqual(t1,t2) -> PEqual(rename_term t1, rename_term t2)
  | PDiff(t1,t2) -> PDiff(rename_term t1, rename_term t2)
  | POr(t1,t2) -> POr(rename_term t1, rename_term t2)
  | PAnd(t1,t2) -> PAnd(rename_term t1, rename_term t2)
  | PBefore(t1,t2) -> PBefore(rename_term t1, rename_term t2)
  | PExists(exists,t) -> PExists(List.map (fun (x,ty) -> (rename_ie x, rename_ty ty)) exists,
				 rename_term t)
  | PForall(exists,t) -> PForall(List.map (fun (x,ty) -> (rename_ie x, rename_ty ty)) exists,
				 rename_term t)
  | PIndepOf(i1,i2) -> PIndepOf(rename_ie i1, rename_ie i2)
  | PDiffIndist(t1,t2) -> PDiffIndist(rename_term t1, rename_term t2)
  in
  (t',ext)

and rename_pat = function
    PPatVar(i,topt), ext -> PPatVar(rename_id_und i, match topt with
      None -> None
    | Some t -> Some (rename_ty t)), ext
  | PPatTuple(l), ext -> PPatTuple(List.map rename_pat l), ext
  | PPatFunApp(f,l), ext -> PPatFunApp(rename_ie f, List.map rename_pat l), ext
  | PPatEqual t, ext -> PPatEqual(rename_term t), ext

and rename_br (b, tl) = (rename_ie b, List.map rename_term tl)

let rename_beginmodule_opt = function
    PWrite(b,file) -> PWrite(rename_ie b, rename_ie file)
  | PRead(b,file) -> PRead(rename_ie b, rename_ie file)
    
let rec rename_proc (p, ext) = 
  let p' = match p with
    PNil -> PNil
  | PYield -> PYield
  | PEventAbort id -> PEventAbort(rename_ie id)
  | PPar(p1,p2) -> PPar(rename_proc p1, rename_proc p2)
  | PRepl(i, idopt, id, p) -> PRepl(ref None, (match idopt with
      None -> None
    | Some id -> Some (rename_ie id)), rename_ie id, rename_proc p)
  | PRestr(id,t,p) -> PRestr(rename_ie id, rename_ie t, rename_proc p)
  | PLetDef(x, tl) -> PLetDef(rename_ie x, List.map rename_term tl)
  | PTest(t,p1,p2) -> PTest(rename_term t, rename_proc p1, rename_proc p2)
  | PFind(l,p,opt) -> PFind(List.map (fun (_, bl, def_list, c,p1) ->
      (ref [],
       List.map (fun (x,y,t) -> (rename_ie x, rename_ie y, rename_ie t)) bl,
       List.map rename_br def_list,
       rename_term c,
       rename_proc p1)) l, rename_proc p, opt)
  | PEvent(t,p) -> PEvent(rename_term t, rename_proc p)
  | PInput(c,tpat,p) -> PInput(rename_channel c, rename_pat tpat, rename_proc p)
  | POutput(b,c,t2,p) -> POutput(b,rename_channel c, rename_term t2, rename_proc p)
  | PLet(pat, t, p1, p2) -> PLet(rename_pat pat, rename_term t, rename_proc p1, rename_proc p2)
  | PGet(id, patlist, sto, p1,p2, opt) -> PGet(rename_ie id, List.map rename_pat patlist, (match sto with None -> None|Some t -> Some (rename_term t)), rename_proc p1, rename_proc p2, opt)
  | PInsert(id, tlist, p) -> PInsert(rename_ie id, List.map rename_term tlist, rename_proc p)
  | PBeginModule ((id,opt),p) -> PBeginModule ((rename_ie id,List.map rename_beginmodule_opt opt),rename_proc p)
  | PDiffIndistProc(p1,p2) -> PDiffIndistProc(rename_proc p1,rename_proc p2)
  in
    (p', ext)

let rename_act = function
    PAFunApp i -> PAFunApp (rename_ie_proj i)
  | PAPatFunApp i -> PAPatFunApp (rename_ie i)
  | PACompare i -> PACompare (rename_ie i)
  | (PAReplIndex | PAArrayAccess _ |
     PAAnd | PAOr | PANewOracle | PAIf | PAFind _) as x -> x
  | PAAppTuple l -> PAAppTuple (List.map rename_ie l)
  | PAPatTuple l -> PAPatTuple (List.map rename_ie l)
  | PANew i -> PANew  (rename_ie i)

let rec rename_probaf (p,ext) =
  let p' = match p with
    PAdd(p1,p2) -> PAdd(rename_probaf p1, rename_probaf p2)
  | PSub(p1,p2) -> PSub(rename_probaf p1, rename_probaf p2)
  | PProd(p1,p2) -> PProd(rename_probaf p1, rename_probaf p2)
  | PDiv(p1,p2) -> PDiv(rename_probaf p1, rename_probaf p2)
  | PPower(p,n) -> PPower(rename_probaf p,n)
  | PMax l -> PMax (List.map rename_probaf l)
  | PMin l -> PMin (List.map rename_probaf l)
  | PPIdent i -> PPIdent (rename_ie i)
  | PCount (i,lopt) ->
      PCount (rename_ie i, match lopt with
      | None -> None
      | Some l -> Some (List.map rename_ie l))
  | PPFun(i,l) -> PPFun(rename_ie i, List.map rename_probaf l)
  | (PPZero | PCst _ | PFloatCst _ | PTime) as x -> x
  | PCard i -> PCard (rename_ie i)
  | PActTime(act, l) -> PActTime(rename_act act, List.map rename_probaf l)
  | PMaxlength t -> PMaxlength (rename_term t)
  | PLength(i,l) -> PLength(rename_ie i, List.map rename_probaf l)
  | PLengthTuple(il,l) -> PLengthTuple(List.map rename_ie il, 
				       List.map rename_probaf l)
  | PEpsFind -> PEpsFind
  | PEpsRand i -> PEpsRand (rename_ie i)
  | PPColl1Rand i -> PPColl1Rand (rename_ie i)
  | PPColl2Rand i -> PPColl2Rand (rename_ie i)
  | POptimIf(cond, p1,p2) -> POptimIf(rename_optim_cond cond,
				      rename_probaf p1, rename_probaf p2)
  in
  (p', ext)

and rename_optim_cond (cond, ext) =
  let cond' = 
    match cond with
    | POCProbaFun(f, l) -> POCProbaFun(f, List.map rename_probaf l)
    | POCBoolFun(f, l) -> POCBoolFun(f, List.map rename_optim_cond l)
  in
  (cond', ext)
    
let rec rename_fungroup = function
    PReplRestr(repl_opt, lres, lfg) ->
      let repl_opt' =
	match repl_opt with
	| Some (n, iopt, i) ->
	    Some (ref None, 
		  (match iopt with
		    None -> None
		  | Some i1 -> Some (rename_ie i1)), rename_ie i)
	| None -> None
      in
      PReplRestr(repl_opt',
		 List.map (fun (x,t,opt) -> (rename_ie x, rename_ie t,opt)) lres,
		 List.map rename_fungroup lfg)
  | PFun(i, larg, r, n) ->
      PFun(rename_ie i,
	   List.map (fun (x,t) -> (rename_ie x, rename_ty t)) larg,
	   rename_term r, n)

let rename_eqname = function
    CstName i -> CstName i
  | ParName(i,p) -> ParName(i, rename_ie p)
  | NoName -> NoName

let rename_eqmember (l,ext1) =
  (List.map (fun (fg, mode, ext) -> (rename_fungroup fg, mode, ext)) l, ext1) (* Do not rename the mode! *)

let rename_query = function
    (PQSecret (i, pub_vars, options),ext) -> (PQSecret (rename_ie i, List.map rename_ie pub_vars, options),ext)
  | (PQEventQ(syntax,decl,t, pub_vars),ext) -> 
      (PQEventQ(syntax,List.map (fun (x,t) -> (rename_ie x, rename_ty t)) decl, 
	      rename_term t, List.map rename_ie pub_vars),ext)



let rename_impl = function
  | Type(t,s,o) -> Type(rename_ie t, s, o)
  | Function(name,f,fo) -> 
      Function(rename_ie name,f,fo)
  | Constant(name,c) ->
      Constant(rename_ie name,c)
  | ImplTable(tbl,file) ->
      ImplTable(rename_ie tbl,file (* TO DO I might want to rename the file to have a different file for each expansion of the macro. Then the file should probably not be between quotes in the input file *))

let rec rename_spec_args = function
  | SpecialArgId i, ext -> SpecialArgId(rename_ie i), ext
  | (SpecialArgString _) as x,ext -> x, ext
  | SpecialArgTuple l, ext -> SpecialArgTuple(List.map rename_spec_args l), ext
	
let rename_equiv = function
  | EquivNormal(l,r,p) ->
      EquivNormal(rename_eqmember l, rename_eqmember r, rename_probaf p)
  | EquivSpecial(n,args) ->
      EquivSpecial(n,List.map rename_spec_args args)
	
let rename_decl = function
    ParamDecl(s, options) -> ParamDecl(rename_ie s, options)
  | ProbabilityDecl (s, args, options) -> ProbabilityDecl(rename_ie s, args, options)
  | TypeDecl(s,options) -> TypeDecl(rename_ie s, options)
  | ConstDecl(s1,s2) -> ConstDecl(rename_ie s1, rename_ie s2)
  | ChannelDecl(s) -> ChannelDecl(rename_ie s)
  | Setting((p,ext),v) -> Setting((p,ext),v)
  | FunDecl(s1,l,sr,f_options) ->
      FunDecl(rename_ie s1,
	      List.map rename_ie l,
	      rename_ie sr,f_options)
  | AliasDecl(s1,s2) -> AliasDecl(rename_ie s1, rename_ie s2)
  | EventDecl(s1, l) ->
      EventDecl(rename_ie s1, List.map rename_ie l)
  | Statement(n, t, side_cond, options) ->
      let n' = rename_eqname n in
      (* Variables created in the statement are local, 
         I can reuse their names later *)
      let rename_state = get_rename_state() in
      let renamed_statement =
	Statement(n', rename_term t, rename_term side_cond, options)
      in
      set_rename_state rename_state;
      renamed_statement
  | BuiltinEquation(eq_categ, l_fun_symb) ->
      BuiltinEquation(eq_categ, List.map rename_ie l_fun_symb)
  | EqStatement(n,equiv,options) ->
      let n' = rename_eqname n in
      (* Variables created in the statement are local, 
         I can reuse their names later *)
      let rename_state = get_rename_state() in
      let renamed_eq_statement =
	EqStatement(n', rename_equiv equiv, options)
      in
      set_rename_state rename_state;
      renamed_eq_statement
  | Collision(n, restr, forall,  t1, p, t2, side_cond, options, col_options) ->
      let n' = rename_eqname n in
      (* Variables created in the statement are local, 
         I can reuse their names later *)
      let rename_state = get_rename_state() in
      let renamed_coll_statement =
	Collision(n', List.map (fun (x,t) -> (rename_ie x, rename_ie t)) restr,
		  List.map (fun (x,t) -> (rename_ie x, rename_ty t)) forall,
		  rename_term t1,
		  rename_probaf p,
		  rename_term t2,
		  rename_term side_cond, options, col_options)
      in
      set_rename_state rename_state;
      renamed_coll_statement      
  | Query (vars, l) ->
      (* For queries, some variables are local (those in correspondence
         queries), but some are global (those in secrecy queries and
         public variables). For simplicity, I do not allow reusing
         the same names for any of these variables *)
      Query(List.map (fun (x,t) -> (rename_ie x, rename_ty t)) vars,
	    List.map rename_query l)
  | PDef(s,vardecl,p) ->
      PDef(rename_ie s,
	   List.map (fun (x,t) -> (rename_ie x, rename_ty t)) vardecl,
	   rename_proc p)
  | Proofinfo(pr, ext) ->
      raise_error "Proof indications not allowed in macros" ext
  | Define((_,ext1),_,_) ->
      raise_error "macro definitions are not allowed inside macro definitions" ext1
  | Expand(s1,argl) ->
      Expand(s1, List.map rename_ie argl)
  | Implementation(ilist) ->
      Implementation(List.map rename_impl ilist)
  | TableDecl (id, tlist) ->
      TableDecl(rename_ie id, List.map rename_ie tlist)
  | LetFun(name,l,t) ->
      (* The defined function is global, it must not be reused *)
      let name' = rename_ie name in
      (* Variables created in the statement are also global, 
	 because I can make array references to them,
	 as well as references in queries.
         I cannot reuse their names later *)
      LetFun(name',
	     List.map (fun (b,t) -> (rename_ie b,rename_ty t)) l,
	     rename_term t)
  | LetProba(name, l, p) ->
      LetProba(rename_ie name,
	       List.map (fun (b,t) -> (rename_ie b,t)) l,
	       rename_probaf p)
	
(* Check declarations *)

let add_not_found s ext v =
  if StringMap.mem s (!env) then
    raise_error (s ^ " already defined.") ext
  else
    env := StringMap.add s v (!env)

let rec write_type_options typ = function
  | ((i, ext), l) :: l' ->
      if i = "pred" then
        match l with
        | [ (pr, ext) ] ->
            if typ.timplsize != None then
              raise_error
                ( "Cannot set predicate for type " ^ typ.tname
                ^ ".\n(Predicate already determined by the size.)" )
                ext
            else if !Settings.get_implementation = FStar then
              Parsing_helper.input_warning
		"Type option pred is not used for FStar implementations \
                 and will be ignored."
                ext
            else 
	      typ.tpredicate <- Some pr
        | _ -> raise_error "Wrong format for the pred option" ext
      else if i = "serial" then
        match l with
        | [ (s, ext); (d, ext') ] -> typ.tserial <- Some (s, d)
        | _ -> raise_error "Wrong format for the serial option" ext
      else if i = "random" then
        match l with
        | [ (r, ext) ] ->
            if
              typ.timplsize <> None
              && typ.toptions land Settings.tyopt_NONUNIFORM == 0
            then
              raise_error
                ( "Cannot set random generator function for type " ^ typ.tname
                ^ ".\n\
                   (Function already determined by the size and the generator \
                   is uniform.)" )
                ext
            else if typ.toptions land Settings.tyopt_CHOOSABLE == 0 then
              raise_error
                ( "One cannot generate random values from type  " ^ typ.tname
                ^ ".\nYou should not specify the random generator." )
                ext
            else typ.trandom <- Some r
        | _ -> raise_error "Wrong format for the random option" ext
      else if i = "equal" then
        if !Settings.get_implementation = OCaml then
          Parsing_helper.input_warning
            "Type option equal is not supported for OCaml implementations \
             and will be ignored."
            ext
        else 
          match l with
          | [ (eq, ext) ] -> typ.tequal <- Some eq
          | _ -> raise_error "Wrong format for the equal option" ext
      else raise_error ("Type option " ^ i ^ " not recognized") ext;
      write_type_options typ l'
  | [] -> ()

let rec write_fun_options fu = function
  | ((i, ext), (j, ext')) :: l' ->
      if i = "inverse" then
        if fu.f_options land Settings.fopt_COMPOS <> 0 then
          fu.f_impl_inv <- Some j
        else
          raise_error
            (fu.f_name ^ " is not composable and an inverse is given.")
            ext'
      else if i = "use_entropy" then
        if !Settings.get_implementation = OCaml then
          Parsing_helper.input_warning
            "Calling with entropy is not supported for OCaml implementations \
             and will be ignored."
            ext
        else
          match j with
          | "true" -> fu.f_impl_ent <- true
          | "false" -> fu.f_impl_ent <- false
          |_ -> raise_error ("Value for use_entropy must be true or false.") ext'
      else raise_error ("Fun option " ^ i ^ " not recognized") ext;
      write_fun_options fu l'
  | [] -> ()

let queries_map_vars vars l =
  match vars with
  | [] -> (* Explicit quantifiers is the default *)
      l
  | ((s1,ext1),_)::_ ->
      (* The bound variables are defined globally for the group of queries [l].
	 Put them for each queries that uses bound variables, that is, [PQEventQ] queries *)
      List.map (function
	  (PQEventQ(_, vars', t, pub_vars),ext) ->
	    assert(vars' = []);
	    (PQEventQ(ImplicitQuantifiers, vars, t, pub_vars),ext)
	| q -> q
	      ) l

let rec add_aliases f_alias f =
  match f.f_cat with
  | Alias f' ->
      f'.f_aliases <- f_alias :: f'.f_aliases;
      add_aliases f_alias f'
  | _ -> ()
	
let rec check_one = function
  | ParamDecl ((s, ext), options) ->
      let size =
        match options with
        | [] -> Settings.psize_DEFAULT
        | [ opt_ext ] -> Settings.parse_psize opt_ext
        | _ :: _ :: _ ->
            raise_error "Parameters accept a single size option" ext
      in
      add_not_found s ext (EParam{ pname = s; psize = size })
  | ProbabilityDecl((s,ext), args, options) ->
      let est =
	match options with
	  [] -> Settings.tysize_LARGE
	| [opt_ext] -> Settings.parse_pest opt_ext
	| _ ::_::_ -> raise_error "Probabilities accept a single estimate option" ext
      in
      let args' =
	match args with
	| None -> None
	| Some l -> Some (List.map (fun (d,ext) -> Some d) l)
      in
      add_not_found s ext (EProba{ prname = s; pargs = args'; pestimate = est })
  | TableDecl((s,ext),st) ->
      add_not_found s ext (ETable ({ tblname = s;
				     tbltype = List.map (fun (s,ext) -> get_type_or_param (!env) s ext) st;
				     tblfile = None }))
  | TypeDecl((s1,ext1),options) ->
	let opt = ref 0 in
	let size = ref None in
	let pcoll = ref None in
	List.iter (fun (sopt, extopt) ->
	  match sopt with
	    "fixed" -> opt := (!opt) lor Settings.tyopt_FIXED lor Settings.tyopt_BOUNDED
	  | "bounded" -> opt := (!opt) lor Settings.tyopt_BOUNDED
	  | "nonuniform" -> opt := (!opt) lor Settings.tyopt_NONUNIFORM
	  | "ghost" -> opt := (!opt) lor Settings.tyopt_GHOST
	  | _ -> (* options large, password, size<n> *)
	      let rsize, rcoll = Settings.parse_type_size_pcoll (sopt, extopt) in
	      begin
		match !size, rsize with
		| None, rsize -> size := rsize
		| Some _, None -> ()
		| Some _, Some _ -> 
		    raise_error ("Types options large, password, size<n>, and size<min>_<max> are incompatible") ext1
	      end;
	      begin
		match !pcoll, rcoll with
		| None, rcoll -> pcoll := rcoll
		| Some _, None -> ()
		| Some _, Some _ ->
		    raise_error ("Types options large, password, and pcoll<n> are incompatible") ext1
	      end
		) options;
	if !opt land Settings.tyopt_NONUNIFORM == 0 then
	  begin
	    match !size, !pcoll with
	    | Some _, None -> pcoll := !size
	    | None, Some _ -> size := !pcoll
	    | None, None -> ()
	    | Some (nsize_min, nsize_max), Some (ncoll_min, ncoll_max) ->
		if nsize_min <> ncoll_min then
		  raise_error "For uniform distributions, the estimate for size of the type and probability of collision should be equal" ext1;
		if nsize_max <> ncoll_max && ncoll_max <> Settings.max_exp (* pcoll<n> sets ncoll_max to max_exp *) then
		  raise_error "For uniform distributions, the estimate for size of the type and probability of collision should be equal" ext1;
		pcoll := !size
	  end;
	begin
	  match !size, !pcoll with
	  | Some (nsize_min, nsize_max), Some (ncoll_min, ncoll_max) ->
	      (* The maximum probability of collision with a random element, 1/2^{pcoll estimate} is at least 1/|T| = 1/2^{size estimate}, so pcoll estimate <= size estimate 
	         2^nsize_min <= |T| <= 2^nsize_max
		 2^-ncoll_min >= Pcoll1rand(T) >= 2^-ncoll_max
		 Pcoll1rand(T) >= 1/|T| 
		 In case |T| = 2^nsize_min, we have 
		 2^-ncoll_min >= Pcoll1rand(T) >= 1/|T| >= 2^-nsize_min
		 so we must have ncoll_min <= nsize_min. *)
	      if ncoll_min > nsize_min then
		raise_error "The estimate for collision probability should be at most the estimate for the size of the type" ext1;
	      if nsize_max <= ncoll_max then
		pcoll := Some (ncoll_min, nsize_max) (* The previous error guarantees that ncoll_min <= nsize_min <= nsize_max *)
	  | _ -> ()
	end;
	if !opt land Settings.tyopt_BOUNDED == 0 then
	  begin
	    match !size with
	    | Some (_, nsize_max) when nsize_max < Settings.max_exp ->
		(* The size estimate shows that the type is bounded *)
		input_warning ("The size estimate shows that type "^ s1 ^" is bounded; adding the [bounded] option") ext1;
		opt := Settings.tyopt_BOUNDED lor (!opt)
	    | _ -> ()
	  end;
	let ty = { tname = s1;
		   tcat = BitString;
		   toptions = !opt;
		   tsize = !size;
		   tpcoll = !pcoll;
                   timplsize = None;
                   tpredicate = None;
                   timplname =
		     if !opt land Settings.tyopt_GHOST != 0
		     then Some "unit"
		     else None;
                   tserial = None;
                   tserial_lemmas = None;
                   trandom = None;
		   tequal = None }
	in
	add_not_found s1 ext1 (EType ty);
  | ConstDecl((s1,ext1),(s2,ext2)) ->
      let s2' = get_type (!env) s2 ext2 in
      add_not_found s1 ext1 (EFunc (Settings.create_fun s1 ([], s2') Std))
  | ChannelDecl(s1,ext1) ->
      if (!Settings.front_end) == Settings.Channels then
        add_not_found s1 ext1 (EChannel { oname = s1;
					  (* The following fields will be set later *)
					  oindices_type = [];
					  oarg_type = [];
					  oret_type = None;
					  oret_ch = ChUnset })
      else
	internal_error "ChannelDecl not allowed in oracles front-end"
  | Setting((p,ext),v) ->
      begin
	try 
	  Settings.do_set p v 
	with Not_found -> 
	  raise_error  ("Bad setting " ^ p ^ "=" ^
                        (match v with S (s,_) -> s | I n -> string_of_int n)) ext
      end
  | FunDecl((s1,ext1),l,(sr,extr),f_options) ->
      let sr' = get_type (!env) sr extr in
      let l' = List.map (fun (s,ext) ->
	get_type (!env) s ext) l 
      in
      let opt = ref 0 in
      List.iter (fun (sopt, extopt) ->
	  if sopt = "projection" then 
	    begin
	      opt := (!opt) lor Settings.fopt_DECOMPOS;
	      if List.length l' != 1 then
		raise_error "A [projection] function should be unary" extopt
	    end
	  else if sopt = "uniform" then
	    begin
	      opt := (!opt) lor Settings.fopt_UNIFORM;
	      if List.length l' != 1 then
		raise_error "A uniform function should be unary" extopt;
	      if sr'.toptions land Settings.tyopt_CHOOSABLE == 0 then
		raise_error "A uniform function should have a result that can be randomly chosen" extopt;
	      if (List.hd l').toptions land Settings.tyopt_CHOOSABLE == 0 then
		raise_error "A uniform function should have an argument that can be randomly chosen" extopt
	    end
	  else if sopt = "data" then
	    begin
	      if sr'.toptions land Settings.tyopt_BOUNDED != 0 then
		begin
		  List.iter (fun ty ->
		    if ty.toptions land Settings.tyopt_BOUNDED == 0 then
		      print_string ("Warning: due to the injective function " ^ s1 ^ ", the type " ^ ty.tname ^ " must be bounded. You should declare it as such (or revise the other declarations if it is not bounded).\n")
		    ) l'
		end;
	      if Terms.sum_list Terms.get_size_low l' > Terms.get_size_high sr' then
		input_warning ("The size estimates for the types of the arguments and result of function " ^ s1 ^ " are not coherent with this function being injective: the size estimate for the result should be at least the sum of the size estimates for the arguments. Fixing that would help CryptoVerif take better decisions on when to eliminate collisions.") ext1;
	      opt := (!opt) lor Settings.fopt_COMPOS
	    end
	  else if sopt = "bijective" then
	    begin
	      if sr'.toptions land Settings.tyopt_BOUNDED != 0 then
		begin
		  List.iter (fun ty ->
		    if ty.toptions land Settings.tyopt_BOUNDED == 0 then
		      print_string ("Warning: due to the bijective function " ^ s1 ^ ", the type " ^ ty.tname ^ " must be bounded. You should declare it as such (or revise the other declarations if it is not bounded).\n")
		    ) l'
		end;
	      if List.for_all (fun ty -> ty.toptions land Settings.tyopt_BOUNDED != 0) l' &&
		sr'.toptions land Settings.tyopt_BOUNDED == 0 then
		print_string ("Warning: due to the bijective function " ^ s1 ^ ", the type " ^ sr'.tname ^ " must be bounded. You should declare it as such (or revise the other declarations if it is not bounded).\n");
	      if Terms.sum_list Terms.get_size_low l' > Terms.get_size_high sr' then
		input_warning ("The size estimates for the types of the arguments and result of function " ^ s1 ^ " are not coherent with this function being bijective: the size estimate for the result should be at least the sum of the size estimates for the arguments. Fixing that would help CryptoVerif take better decisions on when to eliminate collisions.") ext1;
	      if Terms.sum_list Terms.get_size_high l' < Terms.get_size_low sr' then
		input_warning ("The size estimates for the types of the arguments and result of function " ^ s1 ^ " are not coherent with this function being bijective: the size estimate for the result should be at most the sum of the size estimates for the arguments. Fixing that would help CryptoVerif take better decisions on when to eliminate collisions.") ext1;
	      opt := (!opt) lor Settings.fopt_COMPOS lor Settings.fopt_BIJECTIVE
	    end
	  else if sopt = "typeConverter" then
            begin
              (* for compatibility with ProVerif *)
	      if List.length l' != 1 then
	        raise_error "only unary functions can be declared \"typeConverter\"" extopt;
	      opt := (!opt) lor Settings.fopt_COMPOS
            end
	  else if sopt = "autoSwapIf" then
	    opt := (!opt) lor Settings.fopt_AUTO_SWAP_IF
          else
	    raise_error ("Unknown function option " ^ sopt) extopt
		) f_options;
      let f = Settings.create_fun s1 (l',sr') ~options:(!opt) Std in
      add_not_found s1 ext1 (EFunc f);
      if (!opt) land Settings.fopt_BIJECTIVE != 0 then
	begin
	  (* For bijective functions, define the projections *)
	  let projs = Settings.get_proj f in
	  List.iter (fun proj -> add_not_found proj.f_name ext1 (EFunc proj)) projs;
	  (* Add equations *)
	  (* i-proj-f(f(x1...xn)) = xi *)
	  let vars = List.map (fun ty -> create_binder "v" ty []) l' in
	  let vars_terms = List.map Terms.term_from_binder vars in
	  List.iter2 (fun proj var ->
	    let statement = Terms.create_statement (Normal NoName) vars
		(Terms.app proj [Terms.app f vars_terms])
		var (Terms.make_true()) (ref true) true
	    in
	    Settings.statements := statement :: (!Settings.statements);
	    proj.f_statements <- statement :: proj.f_statements
		     ) projs vars_terms;
	  (* f(1-proj-f(x), ..., n-proj-f(x)) = x *)
	  let var = create_binder "v" sr' [] in
	  let var_term = Terms.term_from_binder var in
	  let statement = Terms.create_statement (Normal NoName) [var]
	      (Terms.app f (List.map (fun proj -> Terms.app proj [var_term]) projs))
	      var_term (Terms.make_true()) (ref true) true
	  in
	  Settings.statements := statement :: (!Settings.statements);
	  f.f_statements <- statement :: f.f_statements
	end
  | AliasDecl((s1,ext1),(s2,ext2)) ->
      let f = get_function_no_letfun_no_if (!env) s2 ext2 in
      begin
	match f.f_cat with
	| Std | Alias _ -> ()
	| _ -> raise_error "alias allowed only for standard functions (not =, <>, ||, &&)" ext2
      end;
      (* When [s1 = s2], just do nothing *)
      if s1 <> s2 then
	let f' = Settings.create_fun s1 f.f_type ~options:(f.f_options) (Alias f) in
	add_aliases f' f';
	add_not_found s1 ext1 (EFunc f')
  | LetFun((s1,ext1), l, s2) ->
      let (tl,bl,env')=
        List.fold_right (fun ((s1, ext1), tyb) ((_,_,env') as accu) ->
          if (StringMap.mem s1 env') then
            raise_error ("The name "^s1^" already defined before cannot be used here") ext1
          else
            let (t,_) = get_ty env' tyb in
            add_in_env_letfun accu s1 ext1 t
	      ) l ([],[],!env)
      in
      let f = 
	if (!Settings.get_implementation <> Prove) then
	  begin
	    (* Implementation does not support array accesses and find;
	       it is not a problem if I do not have all variables of processes in binder_env.
	       In case the letfun makes an array access or out of scope access or uses find,
	       the exception [CannotSeparateLetFun] will be raised. In this case, the letfun
	       will be inlined (by setting the category [Std] instead of [SepLetFun]).
	       An error will occur in the generation of the implementation in case 
	       this letfun is used in the part of the code that is translated into an 
	       implementation. *)
            current_location := InLetFun;
            try
	      set_binder_env (check_term1 empty_binder_env false [] env' s2); (* Builds binder_env *)
	      let (_, tres1, tres2) = check_term (Some []) [] env' None s2 in
	      let f = Settings.create_fun s1 (tl, tres1.t_type) SepLetFun in
	      impl_letfuns1 := (f, bl, tres1) :: (!impl_letfuns1);
	      impl_letfuns2 := (f, bl, tres2) :: (!impl_letfuns2);
	      f
	    with CannotSeparateLetFun ->
	      Settings.create_fun s1 (tl, unused_type) Std
	  end
	else
	  Settings.create_fun s1 (tl, unused_type) Std
      in
      add_not_found s1 ext1 (ELetFun(f, !env, l, s2))

  | LetProba((s,ext), args, p) ->
      let rec check_distinct = function
	| [] | [_] -> ()
	| ((s1,ext1),_)::l ->
	    if List.exists (fun ((s',_),_) -> s' = s1) l then
	      raise_error ("The name "^s1^" has already been used as argument of the same letproba function") ext1;
	    check_distinct l
      in
      check_distinct (List.rev args);
      let args' =
	Some (List.map (fun (_,(d,ext)) -> Some d) args)
      in
      let proba = { prname = s; pargs = args'; pestimate = 0 } in
      add_not_found s ext
	(ELetProba(proba, !env, args,
		   (fun env' ->
		     let old_env = set_and_get_old_binder_env empty_binder_env in
		     let p' = check_probability_formula2 ([], [], false) env' p in
		     set_binder_env old_env;
		     p')))
	
  | EventDecl ((s1, ext1), l) ->
      let l' = List.map (fun (s, ext) -> get_type_or_param !env s ext) l in
      add_not_found s1 ext1 (EEvent (Terms.create_event s1 l'))
  | Statement s -> check_statement !env s
  | BuiltinEquation (eq_categ, l_fun_symb) ->
      check_builtin_eq !env eq_categ l_fun_symb
  | EqStatement s -> equivalences := check_eqstatement (!Settings.easy_crypt_convert = None) s :: !equivalences
  | Collision s -> check_collision !env s
  | Query (vars, l) ->
      queries_parse := (queries_map_vars vars l) @ (!queries_parse)
  | PDef((s1,ext1),vardecl,p) ->
      add_not_found s1 ext1 (EProcess (!env, vardecl, p))
  | Proofinfo (pr, ext) ->
      if !proof != None then raise_error "Several proof indications" ext
      else proof := Some pr
  | Implementation impl ->
      (* adding implementation informations *)
      List.iter 
        (function 
           | Type ((t,ext), tid, opts) ->
               let typ = get_type !env t ext in
	       if typ.timplname != None then
		 raise_error ("Type " ^ t ^ " already has implementation informations") ext;
               begin
                 match tid with
                   TypeSize (size) -> 
                     begin
                       typ.timplsize <- Some(size);
		       if size = 1 then
			 begin
                           typ.tpredicate <- Some "always_true";
                           typ.timplname <- Some "bool";
                           (* writing default serializers *)
			   typ.tserial <-
			      (match !Settings.get_implementation with
			      | OCaml -> Some ("bool_to", "bool_from")
			      | FStar -> Some ("serialize_bool", "deserialize_bool")
			      | Prove -> None);
			   typ.tserial_lemmas <-
			      (match !Settings.get_implementation with
			      | OCaml | Prove -> None
			      | FStar -> Some ("lemma_deser_ok_bool", "lemma_deser_too_short_bool", "lemma_deser_rev_bool"));
                           typ.trandom   <-
			     (match !Settings.get_implementation with
			     | OCaml -> Some ("rand_bool")
			     | FStar -> Some ("gen_bool")
			     | Prove -> None);
			   typ.tequal <- Some "(=)"
			 end
                       else if size mod 8 = 0 then 
			 begin
			   let size_str = string_of_int (size/8) in
                           typ.tpredicate <- Some ("(sizep "^size_str^")");
                           typ.timplname <-
			      (match !Settings.get_implementation with
				(* Always set [timplname] because it is used to detect duplicate implementation declarations *)
			      | OCaml | Prove -> Some "string"
			      | FStar -> Some ("lbytes " ^ size_str));
                          (* writing default serializers *)
			   typ.tserial <-
			      (match !Settings.get_implementation with
			      | OCaml -> Some ("id", "(size_from " ^ size_str ^ ")")
			      | FStar -> Some ("serialize_lbytes", "(deserialize_lbytes " ^ size_str ^ ")")
			      | Prove -> None);
			   typ.tserial_lemmas <-
			      (match !Settings.get_implementation with
			      | OCaml | Prove -> None
			      | FStar -> Some ("lemma_deser_ok_lbytes #"^size_str, "lemma_deser_too_short_lbytes #"^size_str, "lemma_deser_rev_lbytes #"^size_str));
			   typ.trandom <-
			      (match !Settings.get_implementation with
			      | OCaml -> Some ("(rand_string " ^ string_of_int (size / 8) ^ ")")
			      | FStar -> Some ("(gen_lbytes " ^ string_of_int (size / 8) ^ ")")
			      | Prove -> None);
			   typ.tequal <- Some "eq_lbytes"; 			   
			 end
                       else 
			 raise_error "Fixed-length types of size different from 1 and non-multiple of 8 not supported" ext
                     end
                 | TypeName (n,ext) ->
                     begin
		       if typ.toptions land Settings.tyopt_FIXED != 0 then
			 raise_error "The implementation of fixed types should be given by specifying their size" ext;
                       typ.timplname <- Some(n);
                       typ.tpredicate <- Some ("always_true")
                     end
               end;
               (* Parse options *)
               write_type_options typ opts
           | Function((f,ext),(i,ext1),fopts) ->
               let fu=get_function_or_letfun_no_if !env f ext in
	       (* "if_fun" is rejected because it already has implementation informations *)
	       if fu.f_impl != No_impl then
		 raise_error ("Function " ^ f ^ " already has implementation informations") ext;
                fu.f_impl <- Func i;
               write_fun_options fu fopts
           | Constant((f,ext),(i,ext')) ->
               let fu=get_function_or_letfun_no_if !env f ext in
	       (* "if_fun" is rejected because it already has implementation informations *)
 	       if fu.f_impl != No_impl then
		 raise_error ("Function " ^ f ^ " already has implementation informations") ext;
               if (fst fu.f_type <> []) then
                 raise_error (fu.f_name^" is not a function without arguments.") ext
               else
                 fu.f_impl <- Const i
           | ImplTable((tbl,ext),(file,ext')) ->
               let t=get_table !env tbl ext in
 	       if t.tblfile != None then
		 raise_error ("Table " ^ tbl ^ " already has implementation informations") ext;
               t.tblfile <- Some file;
        ) impl;
      implementation := impl @ (!implementation)
  | Define _ ->
      internal_error "macros should have been expanded"
  | Expand((s,ext),_) ->
      raise_error "macros should have been expanded" ext


let check_process_full p =
  set_binder_env (check_process1 empty_binder_env [] (!env) p); (* Builds binder_env *)
  let (result,_,_,_) = check_process [] [] (!env) None p in
  check_process2 p; (* Checks oracles that finish roles contain only
                       one return *)
  warn_process_form p; (* Warns user if form of process is not optimal *)
  (* For F* implementations, check that the top oracles, before a replication, 
     are all ghost *)
  if !Settings.get_implementation = FStar then
    Delete_ghost.check_top_ghost result;
  match !diff_bit with
  | None -> result
  | Some b ->
      let out_proc = Terms.oproc_from_desc (Return([], result)) in
      let new_proc = Terms.oproc_from_desc (Restr(b,out_proc)) in
      let oracle = {
        oname = Terms.fresh_id "diff_o";
        oindices_type = [];
        oarg_type = [];
        oret_type = Some [];
	oret_ch = NoChan
      }
      in
      let oracle_decl = {
        ooracle = oracle;
        oindices = [] }
      in
      Terms.iproc_from_desc (OracleDecl(oracle_decl, [], new_proc))
	
let new_bitstring_binder() = 
  let b = Terms.create_binder "!l" Settings.t_bitstring [] in
  Terms.term_from_binder b

let rec check_term_query_premises env = function
  | PQEvent (inj, (PFunApp ((s, ext), tl), ext2)), ext3 ->
      let tl' = List.map (check_term_nobe env) tl in
      let f = get_event env s ext in
      check_type_list ext2 tl tl' (List.tl (fst f.f_type));
      [inj, Terms.new_term (snd f.f_type) ext2 (FunApp(f, (new_bitstring_binder()) :: tl'))]
  | PQEvent _, ext ->
      raise_error "Events should be function applications" ext
  | PAnd(t1,t2), ext ->
      (check_term_query_premises env t1) @ (check_term_query_premises env t2)
  | PLetE(pat,t1,t2,None), ext ->
      let t1' = check_term_nobe env t1 in
      let env' = link_ppat env pat t1' in
      check_term_query_premises env' t2
  | _,ext2 -> raise_error "the left-hand side of a correspondence query should be an event or a conjunction of events" ext2

let add_in_env_query env s ext t =
    let b = Terms.create_binder s t [] in
    if (StringMap.mem s env) then
      input_warning ("identifier " ^ s ^ " rebound") ext;
    (StringMap.add s (EVar b) env, b)

let rec check_binder_list_typaram env = function
    [] -> (env,[])
  | ((s1,ext1),ty)::l ->
      let t =
	match ty with
	  Tid (s2,ext2) -> get_type env s2 ext2 
	| TBound (s2,ext2) -> 
	    let p = get_param env s2 ext2 in
	    type_for_param p
      in
      let (env',b) = add_in_env_query env s1 ext1 t in
      let (env'',l') = check_binder_list_typaram env' l in
      (env'', b::l')

let rec check_term_query_concs syntax env = function
    (PIdent (s, ext), ext2) as x ->
      begin
      try 
	match StringMap.find s env with
	  EVar(b) -> 
	    let x' =
	      Terms.new_term b.btype ext2
		(Var(b,List.map Terms.term_from_repl_index b.args_at_creation))
	    in
	    check_type (snd x) x' Settings.t_bool;	    
            QTerm x'
	| EFunc(f) ->
	    if fst (f.f_type) = [] then
	      let x' = Terms.new_term (snd f.f_type) ext2 (FunApp(f, [])) in
	      check_type (snd x) x' Settings.t_bool;
	      QTerm x'
	    else
	      raise_error (s ^ " has no arguments but expects some") ext
	| d -> raise_error (s ^ " was previously declared as a " ^ (decl_name d) ^". Expected a variable or a function (letfun forbidden).") ext
      with Not_found -> 
	raise_error (s ^ " not defined. Expected a variable or a function (letfun forbidden).") ext
      end
  | PQEvent(inj, (PFunApp((s,ext), tl), ext2)),ext3 ->
      let tl' = List.map (check_term_nobe env) tl in
      let f = get_event env s ext in
      check_type_list ext2 tl tl' (List.tl (fst f.f_type));
      QEvent (inj, Terms.new_term (snd f.f_type) ext2 (FunApp(f, (new_bitstring_binder()) :: tl')))
  | PQEvent _, ext ->
      raise_error "Events should be function applications" ext
  | PAnd(t1,t2), ext ->
      QAnd(check_term_query_concs syntax env t1, check_term_query_concs syntax env t2)
  | POr(t1,t2), ext ->
      QOr(check_term_query_concs syntax env t1, check_term_query_concs syntax env t2)
  | PLetE(pat,t1,t2,None), ext ->
      let t1' = check_term_nobe env t1 in
      let env' = link_ppat env pat t1' in
      check_term_query_concs syntax env' t2
  | PExists(vl,t),ext ->
      if syntax = ImplicitQuantifiers then
	raise_error "cannot mix implicit and explicit quantifiers in queries" ext;
      let (env', l') = check_binder_list_typaram env vl in
      let t' = check_term_query_concs syntax env' t in
      let l'' = 
	List.fold_right2 (fun b ((_,ext_b),_) acc ->
	  if not (Terms.refers_to_qterm b t') then
	    begin
	      Parsing_helper.input_warning ("unused variable "^(Display.binder_to_string b)) ext_b;
	      acc
	    end
	  else
	    (b,ext_b)::acc
	      ) l' vl []
      in
      QExists(l'',t')
  | PForall(vl,t),ext ->
      if syntax = ImplicitQuantifiers then
	raise_error "cannot mix implicit and explicit quantifiers in queries" ext;
      let (env', l') = check_binder_list_typaram env vl in
      let t' = check_term_query_concs syntax env' t in
      let l'' = 
	List.fold_right2 (fun b ((_,ext_b),_) acc ->
	  if not (Terms.refers_to_qterm b t') then
	    begin
	      Parsing_helper.input_warning ("unused variable "^(Display.binder_to_string b)) ext_b;
	      acc
	    end
	  else
	    (b,ext_b)::acc
	      ) l' vl []
      in
      QForall(l'',t')
	
  | PFunApp( (s, ext),[t]), ext2 when s=Settings.sym_not ->
      QNot (check_term_query_concs syntax env t)
  | PBefore(t1, t2), ext ->
      (* Encode t1 ==> t2 as (not t1) || t2 *)
      QOr (QNot (check_term_query_concs syntax env t1),
	   check_term_query_concs syntax env t2)
  | x -> 
      let x' = check_term_nobe env x in
      check_type (snd x) x' Settings.t_bool;
      QTerm x'

(* Push QNot down in queries
   [normalize_not q] returns a query equivalent to [q]
   [normalize_not_not q] returns a query equivalent to [not q] *)
	
let rec normalize_not = function
  | QAnd(t1,t2) -> QAnd(normalize_not t1, normalize_not t2)
  | QOr(t1,t2) -> QOr(normalize_not t1, normalize_not t2)
  | QNot(t) -> normalize_not_not t
  | QExists(l,t) -> QExists(l,normalize_not t)
  | QForall(l,t) -> QForall(l,normalize_not t)
  | QEvent(b,t) -> QEvent(b,t)
  | QTerm t -> QTerm t

and normalize_not_not = function
  (* not( t1 && t2 ) = not(t1) || not(t2) *)
  | QAnd(t1,t2) -> QOr(normalize_not_not t1, normalize_not_not t2)
  (* not( t1 || t2 ) = not(t1) && not(t2) *)                     
  | QOr(t1,t2) -> QAnd(normalize_not_not t1, normalize_not_not t2)
  | QNot(t) -> normalize_not t
  | QExists(l,t) -> QForall(l,normalize_not_not t)
  | QForall(l,t) -> QExists(l,normalize_not_not t)
  | QEvent(is_inj,t) ->
      if is_inj then
	raise_error "Injective events are not allowed under negation" t.t_loc;
      QNot(QEvent(is_inj,t))
  | QTerm t -> QTerm (Terms.make_not t)

(* Remove explicit quantifiers; all variables must be existentially
   quantified, except variables that occur only in not(event(...))
   which can be universally quantified. *)

let rec only_in_not_event b = function
  | QAnd(t1,t2) | QOr(t1,t2) ->
      only_in_not_event b t1 && only_in_not_event b t2
  | QNot(QEvent(false,_)) -> true
  | QNot _ -> assert false
  | QExists(_,t) | QForall(_,t) ->
      only_in_not_event b t
  | QEvent(_,t) | QTerm t ->
      not (Terms.refers_to b t)
	
let rec normalize_quantifiers univ = function
  | QAnd(t1,t2) -> QAnd(normalize_quantifiers univ t1, normalize_quantifiers univ t2)
  | QOr(t1,t2) ->
      let univ1 = List.filter (fun (b,_) -> Terms.refers_to_qterm b t1) univ in
      let univ2 = List.filter (fun (b,_) -> Terms.refers_to_qterm b t2) univ in
      List.iter (fun (b, ext_b) ->
	if List.exists (fun (b', _) -> b' == b) univ2 then
	  raise_error ("Variable "^Display.binder_to_string b^" is universally quantified above a disjunction.") ext_b
	    ) univ1;
      QOr(normalize_quantifiers univ1 t1, normalize_quantifiers univ2 t2)
  | (QNot(QEvent(false,_))) as x -> x
  | QNot _ -> assert false
  | QExists(l,t) ->
      begin
	match univ with
	| [] -> ()
	| (b,ext_b)::_ -> 
	    raise_error ("Variable "^Display.binder_to_string b^" is universally quantified above an existential quantification.") ext_b
      end;
      List.iter (fun (b,ext_b) ->
	if only_in_not_event b t then
	  raise_error ("Variable "^Display.binder_to_string b^" occurs only in not(event(...)) and is existentially quantified.") ext_b
	) l;
      normalize_quantifiers [] t
  | QForall(l,t) ->
      normalize_quantifiers (l @ univ) t
  | (QEvent(_,t) | QTerm t) as x ->
      List.iter (fun (b,ext_b) ->
	if Terms.refers_to b t then
	  raise_error ("Variable "^Display.binder_to_string b^" is universally quantified and does not occur only in not(event(...)).") ext_b
	) univ;
      x
    
let rec check_term_query_corresp syntax env = function
  | PLetE(pat,t1,t2,None), ext ->
      let t1' = check_term_nobe env t1 in
      let env' = link_ppat env pat t1' in
      check_term_query_corresp syntax env' t2
  | PBefore(t1,t2),ext ->
      let t1' = check_term_query_premises env t1 in
      let t2' = normalize_not (check_term_query_concs syntax env t2) in
      let t2' =
	if syntax = ImplicitQuantifiers then
	  t2'
	else
	  normalize_quantifiers [] t2'
      in
      (t1',t2')
  | PForall(vl,t), ext ->
      if syntax = ImplicitQuantifiers then
	raise_error "cannot mix implicit and explicit quantifiers in queries" ext;
      let (env', l') = check_binder_list_typaram env vl in
      let (t1',t2') = check_term_query_corresp syntax env' t in
      List.iter2 (fun b ((_,ext_b),_) ->
	if not (List.exists (fun (_,t) -> Terms.refers_to b t) t1') then
	  if Terms.refers_to_qterm b t2' then
	    raise_error ("In this query, the universally quantified variable "^(Display.binder_to_string b)^" occurs after ==> and not before ==>. Universally quantified variables should occur before ==>.") (snd t)
	  else
	    Parsing_helper.input_warning ("unused variable "^(Display.binder_to_string b)) ext_b
	      ) l' vl;
      t1', t2'
  | x -> 
      let x' = check_term_query_premises env x in
      (x', QTerm (Terms.make_false()))


	
let rec find_inj = function
  | QAnd (t1, t2) | QOr (t1, t2) -> find_inj t1 || find_inj t2
  | QNot(QEvent(false, t)) -> false (* Invariant: no inj-event below a negation. *)
  | QNot _ | QExists _ | QForall _ -> assert false
  | QEvent (b, t) -> b
  | QTerm t -> false

let get_qpubvars l =
  List.map (get_global_binder "in public variables of a query") l

let check_query = function
    (PQSecret (i, pub_vars, options), ext_full) ->
      let v = get_global_binder "in a secrecy query" i in
      let secr_type = ref None in
      let onesession = ref false in
      List.iter (fun (s,ext) ->
	if StringPlus.starts_with s "pv_" then
	  () (* Ignore ProVerif options *)
	else if s = "real_or_random" || s = "cv_real_or_random" then
	  match !secr_type with
	  | None -> secr_type := Some (RealOrRandom false)
	  | Some _ -> raise_error "You should have a single option to set the type of a secrecy query (among real_or_random, cv_real_or_random, reachability, cv_reachability, cv_bit)" ext
	else if s = "reachability" || s = "cv_reachability" then
	  match !secr_type with
	  | None ->
	      if not (Proba.is_large v.btype) then
		raise_error "Reachability secrecy is allowed only for large types (otherwise, the adversary may have a non-negligible probability of success just by guessing)" ext;
	      secr_type := Some (Reachability false)
	  | Some _ -> raise_error "You should have a single option to set the type of a secrecy query (among real_or_random, cv_real_or_random, reachability, cv_reachability, cv_bit)" ext	  
	else if s = "cv_bit" then
	  match !secr_type with
	  | None ->
	      if not (v.args_at_creation = [] && v.btype == Settings.t_bool) then
		raise_error "The option cv_bit is allowed only for booleans defined under no replication" ext;
	      secr_type := Some (Bit)
	  | Some _ -> raise_error "You should have a single option to set the type of a secrecy query (among real_or_random, cv_real_or_random, reachability, cv_reachability, cv_bit)" ext
	else if s = "onesession" || s = "cv_onesession" then
	  onesession := true
	else
	  raise_error "The allowed options for secret are real_or_random, cv_real_or_random, reachability, cv_reachability, cv_bit, onesession, cv_onesession, and options starting with pv_ which are ignored" ext) options;
      let combined_option =
	match !secr_type with
	| None | Some (RealOrRandom _) ->
	    if v.args_at_creation = [] && v.btype == Settings.t_bool then
	      input_warning "For booleans defined under no replication, the option cv_bit may be more appropriate than real_or_random (the default)" ext_full;
	    RealOrRandom(!onesession)
	| Some (Reachability _) ->
	    Reachability(!onesession)
	| Some (Bit) -> Bit
      in
      QSecret (v, get_qpubvars pub_vars, combined_option)
  | (PQEventQ (syntax,vl,t, pub_vars),ext_full) ->
      let var_num_state = Terms.get_reset_var_num_state() in
      let (env',l') = check_binder_list_typaram (!env) vl in
      let t1',t2' = check_term_query_corresp syntax env' t in
      Terms.set_var_num_state var_num_state;
      let has_inj_before_impl = List.exists (fun (b,_) -> b) t1' in
      let has_inj_after_impl = find_inj t2' in
      if has_inj_before_impl && not has_inj_after_impl then
	raise_error "In this query, inj-event is present before ==> but not after ==>.\ninj-event should be present either both before and after ==> or not at all." (snd t);
      if (not has_inj_before_impl) && has_inj_after_impl then
	raise_error "In this query, inj-event is present after ==> but not before ==>.\ninj-event should be present either both before and after ==> or not at all." (snd t);
      if syntax = ExplicitQuantifiers then
	List.iter2 (fun b ((_,ext_b),_) ->
	  if not (List.exists (fun (_,t) -> Terms.refers_to b t) t1') then
	    if Terms.refers_to_qterm b t2' then
	      raise_error ("In this query, the universally quantified variable "^(Display.binder_to_string b)^" occurs after ==> and not before ==>. Universally quantified variables should occur before ==>.") (snd t)
	    else
	      Parsing_helper.input_warning ("unused variable "^(Display.binder_to_string b)) ext_b
	  ) l' vl;
      QEventQ(t1',t2', get_qpubvars pub_vars)

	
let get_impl ()=
  (* Return the used letfuns, in the order in which they have been declared *)
  let letfuns1 =
    List.rev
      (List.filter (fun (f, vardecl, res) -> f.f_impl = SepFun) !impl_letfuns1)
  in
  let letfuns2 =
    List.rev
      (List.filter (fun (f, vardecl, res) -> f.f_impl = SepFun) !impl_letfuns2)
  in
  (* Return the roles *)
  let roles1 =
    StringMap.fold (fun s (p, opt) l -> (s, opt, p) :: l) !impl_roles1 []
  in
  let roles2 =
    StringMap.fold (fun s (p, opt) l -> (s, opt, p) :: l) !impl_roles2 []
  in
  ((letfuns1, roles1), (letfuns2, roles2))

let rec check_all (l, p) =
  List.iter check_one l;
  current_location := InProcess;
  unique_to_prove := true;
  let result =
    match p with
    | PSingleProcess p1 ->
	set_role1 := true;
	set_role2 := true;
	let p = check_process_full p1 in
	let ql = List.map check_query (!queries_parse) in
      (* Remove duplicate queries. They are useless, take time,
	 and might cause an internal error with the current
	 implementation of the command "focus". *)
	let rec remove_dup = function
	    q::ql ->
	      let ql' = remove_dup ql in 
	      if List.exists (Terms.equal_query q) ql' then
		ql'
	      else
		q::ql'
	  | [] -> []
	in
	let queries = queries_for_unique (remove_dup ql) in
	let (queries, p) = Encode_queries.encode_queries queries p in
	let queries =
	  match !diff_bit with
	  | None -> queries
	  | Some b ->
	      let pub_vars = Settings.get_public_vars0 (List.map fst queries) in
	      (QSecret(b, pub_vars, Bit), Some (QDiffEquivalence(pub_vars))) ::
	      (List.map (fun (q, init_q) ->
		let q' = 
		  match q with 
		  | QSecret(v, pub_vars, opt) -> QSecret(v, b::pub_vars, opt)
		  | QEventQ(t1,t2,pub_vars) -> QEventQ(t1,t2,b::pub_vars)
		  | QEquivalence(s,pub_vars,lhs) -> QEquivalence(s,b::pub_vars,lhs)
		  | QEquivalenceFinal(g,pub_vars) -> QEquivalenceFinal(g,b::pub_vars)
		  | QDiffEquivalence _ | AbsentQuery -> assert false
		in
		let init_q' =
		  match init_q with
		  | Some _ -> init_q
		  | None -> Some q
		in
		(q', init_q')
		  ) queries)
	in
	let impl =
	  if !Settings.get_implementation <> Prove then
	    if !diff_bit <> None then
	      Impl2 (get_impl())
	    else
	      Impl1 (fst (get_impl()))
	  else
	    Impl0
	in
	(!statements, !collisions, !equivalences,
	 queries, !proof, impl, SingleProcess p)
    | PEquivalence(p1,p2,pub_vars) ->
	if (!queries_parse) != [] then
	  raise_user_error "Queries are incompatible with equivalence";
	set_role1 := true;
	set_role2 := false;
	let p1' = check_process_full p1 in
	let nu1 = !non_unique_events in
	non_unique_events := [];
	set_role1 := false;
	set_role2 := true;
	let p2' = check_process_full p2 in
	let nu2 = !non_unique_events in
	if !diff_bit <> None then
	  raise_user_error "diff is incompatible with equivalence";
	let pub_vars' =  get_qpubvars pub_vars in
	let q1 = List.map (fun e -> Terms.build_event_query e pub_vars') nu1 in
	let q2 = List.map (fun e -> Terms.build_event_query e pub_vars') nu2 in
	let final_p = Equivalence(p1', p2', q1, q2, pub_vars') in
	let impl =
	  if !Settings.get_implementation <> Prove then Impl2 (get_impl()) else Impl0
	in
	(!statements, !collisions, !equivalences,
	 [], !proof, impl, final_p)
    | PQueryEquiv equiv_statement ->
        if !queries_parse != [] then
          raise_user_error "Queries are incompatible with query_equiv";
        if !Settings.get_implementation <> Prove then
          raise_user_error "Implementation is incompatible with query_equiv";
        let equiv_statement' = check_eqstatement false equiv_statement in
        let queries, final_p = Query_equiv.equiv_to_process equiv_statement' in
        ( !statements,
          !collisions,
          !equivalences,
          queries,
          !proof,
          Impl0,
          final_p )
  in
  (* Reset unique_to_prove so that it is false when we parse special equivalences
     generated later *)
  unique_to_prove := false;
  result

let declares = function
  | ParamDecl(id, _)
  | ProbabilityDecl(id, _, _)
  | TableDecl(id,_)
  | TypeDecl(id,_)
  | ConstDecl(id,_)
  | ChannelDecl id
  | FunDecl(id,_,_,_)
  | LetFun(id,_,_)
  | LetProba(id,_,_)
  | EventDecl(id, _)
  | PDef(id,_,_) ->
      Some id
  | _ -> None

let rec record_ids l = 
  List.iter (fun decl ->
	match declares decl with
	  Some (s,ext) -> Terms.record_id s ext
	| None -> ()
	  ) l

(* [add_already_def argl expanded_macro already_def] adds to [already_def]
   the identifiers defined in [expanded_macro] that also occur in [argl].
   [argl] is supposed to represent the arguments of the macro before expansion.
   The identifiers in [argl] defined by the macro can be used after the
   macro. This function is useful to compute the value of [already_def]
   after the macro. *)

let rec add_already_def argl expanded_macro already_def =
  match expanded_macro with
    [] -> already_def
  | a::l ->
      let already_def' = 
	match declares a with
	| Some (s,_) ->
	    if List.exists (fun (s',_) -> s' = s) argl then
	      s::already_def
	    else
	      already_def
	| None ->
	    already_def
      in
      add_already_def argl l already_def'

let rec check_no_dup = function
    [] -> ()
  | (arg,ext)::l ->
      List.iter (fun (arg',ext') ->
	if arg = arg' then
	  raise_error ("Macro contains twice the argument " ^ arg ^
		       ". It already appears at " ^
		       (in_file_position ext' ext)) ext'
	    ) l;
      check_no_dup l

let rec apply_and_expand argl paraml macro_table already_def def =
  let old_rename_table = !rename_table in
  rename_table := StringMap.empty;
  List.iter (fun s -> 
    rename_table := StringMap.add s s (!rename_table)) already_def;
  List.iter2 (fun (a,_) (p,_) -> 
    rename_table := StringMap.add p a (!rename_table)) argl paraml;
  let macro_state = (macro_table, already_def, []) in
  let (_,_,def') = List.fold_left (fun macro_state decl ->
    let decl' = rename_decl decl in
    (* Expand the nested macro inside the current declaration [decl],
       if any, before renaming the next declarations. 
       That avoids a clash between global identifiers declared
       in the nested macro (e.g. functions declared in the nested macro)
       and local identifiers of the next declaration
       (e.g. undeclared functions inside equation or equiv).*)
    expand_macro_one_decl macro_state decl'
      ) macro_state def
  in
  rename_table := old_rename_table;
  def'

and expand_macro_one_decl (macro_table, already_def, accu_decl) = function
  | Define((s1,ext1),argl,def) ->
      if StringMap.mem s1 macro_table then
	raise_error ("Macro " ^ s1 ^ " already defined.") ext1
      else
	begin
	  check_no_dup argl;
	  let macro_table' = StringMap.add s1 (Macro(argl, def, already_def, macro_table)) macro_table in
	  (macro_table', already_def, accu_decl)
	end
  | Expand((s1,ext1),argl) ->
      begin
	try 
	  let Macro(paraml, def, old_already_def, old_macro_table) = StringMap.find s1 macro_table in
	  if List.length argl != List.length paraml then
	    raise_error ("Macro " ^ s1 ^ " expects " ^ (string_of_int (List.length paraml)) ^
			 " arguments, but is here given " ^ (string_of_int (List.length argl)) ^ " arguments.") ext1;
	  let expanded_macro = apply_and_expand argl paraml old_macro_table old_already_def def in
	  let already_def_after_macro = add_already_def argl expanded_macro already_def in
	  (macro_table, already_def_after_macro, expanded_macro @ accu_decl)
	with Not_found ->
	  raise_error ("Macro " ^ s1 ^ " not defined.") ext1
      end
  | a ->
      let already_def' = 
	match declares a with
	  Some(s,_) -> s::already_def
	| None -> already_def
      in
      (macro_table, already_def', a :: accu_decl)
			
(* Collect all identifiers 
   This is to avoid clashes during macro expansion *)
		
let add_id accu (s,ext) =
  if not (List.mem s (!accu)) then
    accu := s :: (!accu)

let collect_id_ty accu = function
    Tid id | TBound id -> add_id accu id
		   
let rec collect_id_term accu (t,ext) =
  match t with
  | PIdent id -> add_id accu id
  | PArray(id, tl) | PFunApp(id, tl) ->
      add_id accu id;
      List.iter (collect_id_term accu) tl
  | PQEvent(_,t) -> collect_id_term accu t
  | PTuple(tl) -> List.iter (collect_id_term accu) tl
  | PTestE(t1,t2,t3) ->
      collect_id_term accu t1;
      collect_id_term accu t2;
      collect_id_term accu t3
  | PFindE(l0,t,_) ->
      List.iter (fun (_,bl,def_list,t1,t2) ->
	List.iter (fun (u,i,n) ->
	  add_id accu u;
	  add_id accu i;
	  add_id accu n
	    ) bl;
	List.iter (fun (b,l) ->
	  add_id accu b;
	  List.iter (collect_id_term accu) l
	    ) def_list;
	collect_id_term accu t1;
	collect_id_term accu t2
	  ) l0;
      collect_id_term accu t
  | PLetE(pat, t1, t2, topt) ->
      collect_id_pat accu pat;
      collect_id_term accu t1;
      collect_id_term accu t2;
      begin
	match topt with
	  None -> ()
	| Some t -> collect_id_term accu t
      end
  | PResE(b,ty,t) ->
      add_id accu b;
      add_id accu ty;
      collect_id_term accu t
  | PEventAbortE e ->
      add_id accu e
  | PEventE(t,p) ->
      collect_id_term accu t;
      collect_id_term accu p
  | PGetE(tbl, pat_list, topt, t1, t2, _) ->
      add_id accu tbl;
      List.iter (collect_id_pat accu) pat_list;
      begin
	match topt with
	  None -> ()
	| Some t -> collect_id_term accu t
      end;
      collect_id_term accu t1;
      collect_id_term accu t2
  | PInsertE(tbl, tl, t) ->
      add_id accu tbl;
      List.iter (collect_id_term accu) tl;
      collect_id_term accu t
  | PEqual(t1,t2) | PDiff(t1,t2) | POr(t1,t2) | PAnd(t1,t2) | PBefore(t1,t2)
  | PDiffIndist(t1,t2) ->
      collect_id_term accu t1;
      collect_id_term accu t2
  | PExists(exists,t) ->
      List.iter (fun (x,ty) ->  add_id accu x; collect_id_ty accu ty) exists;
      collect_id_term accu t
  | PForall(forall,t) ->
      List.iter (fun (x,ty) ->  add_id accu x; collect_id_ty accu ty) forall;
      collect_id_term accu t
  | PIndepOf(i1,i2) ->
      add_id accu i1;
      add_id accu i2
	
and collect_id_pat accu (pat,ext) =
  match pat with
    PPatVar(id_und,tyopt) ->
      begin
	match id_und with
	| Ident b -> add_id accu b
	| Underscore _ -> ()
      end;
      begin
	match tyopt with
	  None -> ()
	| Some ty -> collect_id_ty accu ty
      end
  | PPatTuple(patl) ->
      List.iter (collect_id_pat accu) patl
  | PPatFunApp(f,patl) ->
      add_id accu f;
      List.iter (collect_id_pat accu) patl
  | PPatEqual t ->
      collect_id_term accu t

let collect_id_ch accu (id, idlopt) =
  add_id accu id;
  match idlopt with
    None -> ()
  | Some idl -> List.iter (add_id accu) idl

let collect_id_beginmodule_opt accu = function
    PWrite(b,file) | PRead(b,file) ->
      add_id accu b;
      add_id accu file
	
let rec collect_id_proc accu (proc,ext) =
  match proc with
    PNil | PYield -> ()
  | PEventAbort e ->
      add_id accu e
  | PPar(p1,p2) | PDiffIndistProc(p1,p2) ->
      collect_id_proc accu p1;
      collect_id_proc accu p2
  | PRepl(_,iopt,n,p) ->
      begin
	match iopt with
	  None -> ()
	| Some i -> add_id accu i
      end;
      add_id accu n;
      collect_id_proc accu p
  | PRestr(b,ty,p) ->
      add_id accu b;
      add_id accu ty;
      collect_id_proc accu p
  | PLetDef(id, tl) ->
      add_id accu id;
      List.iter (collect_id_term accu) tl
  | PTest(t,p1,p2) ->
      collect_id_term accu t;
      collect_id_proc accu p1;
      collect_id_proc accu p2
  | PFind(l0,p,_) ->
      List.iter (fun (_,bl,def_list,t,p1) ->
	List.iter (fun (u,i,n) ->
	  add_id accu u;
	  add_id accu i;
	  add_id accu n
	    ) bl;
	List.iter (fun (b,l) ->
	  add_id accu b;
	  List.iter (collect_id_term accu) l
	    ) def_list;
	collect_id_term accu t;
	collect_id_proc accu p1
	  ) l0;
      collect_id_proc accu p
  | PEvent(t,p) ->
      collect_id_term accu t;
      collect_id_proc accu p
  | PInput(ch,pat,p) ->
      collect_id_ch accu ch;
      collect_id_pat accu pat;
      collect_id_proc accu p
  | POutput(_,ch,t,p) ->
      collect_id_ch accu ch;
      collect_id_term accu t;
      collect_id_proc accu p
  | PLet(pat, t, p1, p2) ->
      collect_id_pat accu pat;
      collect_id_term accu t;
      collect_id_proc accu p1;
      collect_id_proc accu p2
  | PGet(tbl, pat_list, topt, p1, p2, _) ->
      add_id accu tbl;
      List.iter (collect_id_pat accu) pat_list;
      begin
	match topt with
	  None -> ()
	| Some t -> collect_id_term accu t
      end;
      collect_id_proc accu p1;
      collect_id_proc accu p2
  | PInsert(tbl, tl, p) ->
      add_id accu tbl;
      List.iter (collect_id_term accu) tl;
      collect_id_proc accu p
  | PBeginModule((id, opt), p) ->
      add_id accu id;
      List.iter (collect_id_beginmodule_opt accu) opt;
      collect_id_proc accu p

let collect_id_act accu = function
  | PAFunApp i | PAPatFunApp i | PACompare i | PANew i -> add_id accu i
  | PAReplIndex | PAArrayAccess _ 
  | PAAnd | PAOr | PANewOracle | PAIf | PAFind _ -> ()
  | PAAppTuple l | PAPatTuple l -> List.iter (add_id accu) l

let rec collect_id_probaf accu (p,ext) =
  match p with
  | PAdd(p1,p2) | PSub(p1,p2) | PProd(p1,p2) | PDiv(p1,p2) ->
      collect_id_probaf accu p1;
      collect_id_probaf accu p2
  | PPower(p,n) ->
      collect_id_probaf accu p
  | PMax l | PMin l ->
      List.iter (collect_id_probaf accu) l
  | PPIdent i | PCard i | PEpsRand i
  | PPColl1Rand i | PPColl2Rand i ->
      add_id accu i
  | PCount (i,lopt) -> 	
      add_id accu i;
      begin
	match lopt with
	| None -> ()
	| Some l -> List.iter (add_id accu) l
      end
  | PPFun(i,l) | PLength(i,l) ->
      add_id accu i;
      List.iter (collect_id_probaf accu) l
  | PPZero | PCst _ | PFloatCst _ | PTime | PEpsFind -> ()
  | PActTime(act, l) ->
      collect_id_act accu act;
      List.iter (collect_id_probaf accu) l
  | PMaxlength t ->
      collect_id_term accu t
  | PLengthTuple(il,l) ->
      List.iter (add_id accu) il;
      List.iter (collect_id_probaf accu) l
  | POptimIf(cond, p1,p2) ->
      collect_id_optim_cond accu cond;
      collect_id_probaf accu p1;
      collect_id_probaf accu p2

and collect_id_optim_cond accu (cond, ext) =
  match cond with
  | POCProbaFun(_,l) -> List.iter (collect_id_probaf accu) l
  | POCBoolFun(_,l) -> List.iter (collect_id_optim_cond accu) l
	
let rec collect_id_fungroup accu = function
  | PReplRestr(repl_opt, restr, funs) ->
      begin
	match repl_opt with
	| Some(_, iopt, n) ->
	    begin
	      match iopt with
	      |	None -> ()
	      | Some i -> add_id accu i
	    end;
	    add_id accu n
	| None -> ()
      end;
      List.iter (fun (x,t,opt) -> add_id accu x; add_id accu t) restr;
      List.iter (collect_id_fungroup accu) funs
  | PFun(i, larg, r, n) ->
      add_id accu i;
      List.iter (fun (x,t) ->  add_id accu x; collect_id_ty accu t) larg;
      collect_id_term accu r

let collect_id_eqname accu = function
    CstName _ | NoName -> ()
  | ParName(_,p) -> add_id accu p

let collect_id_eqmember accu (l,ext) =
  List.iter (fun (fg, mode, ext) -> collect_id_fungroup accu fg) l

let collect_id_query accu = function
    (PQSecret(i,pub_vars,options),_) ->
      add_id accu i;
      List.iter (add_id accu) pub_vars
  | (PQEventQ(syntax,decl,t,pub_vars),_) ->
      List.iter (fun (x,t) ->  add_id accu x; collect_id_ty accu t) decl;
      collect_id_term accu t;
      List.iter (add_id accu) pub_vars

let collect_id_impl accu = function
  | Type(i,_,_) | Function(i,_,_) | Constant(i,_) ->
      add_id accu i
  | ImplTable(i,file) ->
      add_id accu i; add_id accu file

let rec collect_id_spec_arg accu = function
  | SpecialArgId i,_ -> add_id accu i
  | SpecialArgString _,_ -> ()
  | SpecialArgTuple l,_ -> List.iter (collect_id_spec_arg accu) l
	
let collect_id_decl accu = function
  | ParamDecl(i,_) | ProbabilityDecl (i,_,_) | TypeDecl(i,_) | ChannelDecl i ->
      add_id accu i
  | ConstDecl(i1,i2) ->
      add_id accu i1;
      add_id accu i2
  | Setting _ -> ()
  | FunDecl(s1,l,sr,f_options) ->
      add_id accu s1;
      List.iter (add_id accu) l;
      add_id accu sr
  | AliasDecl(s1,s2) ->
      add_id accu s1;
      add_id accu s2
  | EventDecl(s1,l) | TableDecl(s1,l) ->
      add_id accu s1;
      List.iter (add_id accu) l
  | Statement(n, t,side_cond, opt) ->
      collect_id_eqname accu n;
      collect_id_term accu t;
      collect_id_term accu side_cond
  | BuiltinEquation(eq_categ, l_fun_symb) ->
      List.iter (add_id accu) l_fun_symb
  | EqStatement(n, equiv,options) ->
      collect_id_eqname accu n;
      begin
	match equiv with
	| EquivNormal(l,r,p) ->
	    collect_id_eqmember accu l;
	    collect_id_eqmember accu r;
	    collect_id_probaf accu p
	| EquivSpecial(_,spec_args) ->
	    List.iter (collect_id_spec_arg accu) spec_args
      end
  | Collision(name, restr, forall,  t1, p, t2, side_cond, options, col_options) ->
      collect_id_eqname accu name;
      List.iter (fun (x,t) ->  add_id accu x; add_id accu t) restr;
      List.iter (fun (x,t) ->  add_id accu x; collect_id_ty accu t) forall;
      collect_id_term accu t1;
      collect_id_probaf accu p;
      collect_id_term accu t2;
      collect_id_term accu side_cond
  | Query (vars, l) ->
      List.iter (fun (x,t) ->  add_id accu x; collect_id_ty accu t) vars;
      List.iter (collect_id_query accu) l
  | PDef(s,vardecl,p) ->
      add_id accu s;
      List.iter (fun (x,t) ->  add_id accu x; collect_id_ty accu t) vardecl;
      collect_id_proc accu p
  | Proofinfo _ | Define _ -> ()
  | Expand(_, argl) ->
      List.iter (add_id accu) argl
  | Implementation ilist ->
      List.iter (collect_id_impl accu) ilist
  | LetFun(name,l,t) ->
      add_id accu name;
      List.iter (fun (x,t) ->  add_id accu x; collect_id_ty accu t) l;
      collect_id_term accu t
  | LetProba(name,l,p) ->
      add_id accu name;
      List.iter (fun (x,dim) ->  add_id accu x) l;
      collect_id_probaf accu p
      

let record_all_ids (l,p) =
  let accu = ref [] in
  List.iter (collect_id_decl accu) l;
  begin
    match p with
      PSingleProcess p1 -> 
	collect_id_proc accu p1
    | PEquivalence (p1, p2, pub_vars) ->
	collect_id_proc accu p1;
	collect_id_proc accu p2;
	List.iter (add_id accu) pub_vars
    | PQueryEquiv(n, equiv,options) ->
	collect_id_eqname accu n;
	match equiv with
	| EquivNormal(l,r,p) ->
	    collect_id_eqmember accu l;
	    collect_id_eqmember accu r;
	    collect_id_probaf accu p
	| EquivSpecial _ ->
	    Parsing_helper.internal_error "query_equiv ... special ... should not occur"
  end;
  List.iter (fun i -> Terms.record_id i dummy_ext) (!accu)
	
let read_file f =
  try
    unique_to_prove := false;
    let (l,p) = parse_with_lib f in
    env := init_env();
    let rename_state = Terms.get_var_num_state() in
    (* Record all identifiers, to avoid any clash during macro expansion *)
    record_all_ids (l,p);
    let already_def = StringMap.fold (fun s _ already_def -> s :: already_def) (!env) [] in
    let macro_state = (StringMap.empty, already_def, []) in
    let (_,_,expanded_l) = List.fold_left expand_macro_one_decl macro_state l in
    let l' = List.rev expanded_l in
    Terms.set_var_num_state rename_state;
    (* Record top-level identifiers, to make sure that we will not need to 
       rename them. *)
    record_ids l';
    check_all (l',p)    
  with
  | Undefined(i,ext) ->
      Parsing_helper.input_error (i ^ " not defined") ext
  | Error(s, ext) ->
      Parsing_helper.input_error s ext

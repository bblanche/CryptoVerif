open Types

(* Encodes query secret .. [reachability] as a correspondence query *)
val encode_queries : query list -> inputprocess ->
  (query(*encoded query*) * query option(*initial query, if it differs from the encoded query*)) list * inputprocess

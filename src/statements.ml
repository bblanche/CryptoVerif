open Types

(* Prepare the equation statements given by the user *)

(* [collect_f_with_alias accu t] collects in [accu] the
   functions of [t] that have aliases *)
				  
let rec collect_f_with_alias accu t =
  match t.t_desc with
  | FunApp(f,l) ->
      let accu' = 
	if (f.f_aliases != []) && not (List.memq f accu) then f::accu else accu
      in
      List.fold_left collect_f_with_alias accu' l
  | ReplIndex _ -> accu
  | Var(_,l) ->
      List.fold_left collect_f_with_alias accu l
  | _ -> assert false

(* [has_alias_of t] returns true when [t] contains a function
   that is an alias of another function *)
	
let rec has_alias_of t =
  match t.t_desc with
  | FunApp(f,l) ->
      begin
	match f.f_cat with
	| Alias _ -> true
	| _ -> List.exists has_alias_of l
      end
  | _ ->
      Terms.exists_subterm has_alias_of has_alias_of_def_list has_alias_of_pat t

and has_alias_of_def_list l =
  List.exists (fun (b,l) -> List.exists has_alias_of l) l

and has_alias_of_pat pat = Terms.exists_subpat has_alias_of has_alias_of_pat pat

(* [replace_f f1 f1' t] replaces the function [f1] with [f1'] in term [t] *)
    
let rec replace_f f1 f1' t =
  match t.t_desc with
  | FunApp(f,l) ->
      let f' = if f == f1 then f1' else f in
      Terms.build_term t (FunApp(f', List.map (replace_f f1 f1') l))
  | Var(b,l) ->
      Terms.build_term t (Var(b, List.map (replace_f f1 f1') l))
  | (ReplIndex i) as d ->
      Terms.build_term t d
  | _ -> assert false

let replace_f_coll f1 f1' coll =
   { c_name = coll.c_name; c_restr = coll.c_restr; c_forall = coll.c_forall;
     c_redl = replace_f f1 f1' coll.c_redl; c_proba = coll.c_proba;
     c_redr = replace_f f1 f1' coll.c_redr;
     c_indep_cond = coll.c_indep_cond;
     c_side_cond = replace_f f1 f1' coll.c_side_cond;
     c_restr_may_be_equal = coll.c_restr_may_be_equal;
     c_active = coll.c_active;
     c_aliased = false }

(* Record a collision/equation statement [coll], by storing it in [coll_accu]
   and adding it in the root symbol of its LHS by calling [record_coll].
   This function generalizes statements using aliases. *)
    
let record_with_aliases coll_accu record_coll coll =
  match coll.c_redl.t_desc with
  | FunApp(f,_) ->
      if coll.c_aliased then
	let no_alias_of = not (
	  has_alias_of coll.c_redl ||
	  has_alias_of coll.c_redr ||
	  has_alias_of coll.c_side_cond)
	in
	let f_with_alias = collect_f_with_alias [] coll.c_redr in
	(* set [c_aliased]. When true, [coll] is considered modulo aliasing *)
	coll.c_aliased <- no_alias_of && (f_with_alias == []);
	(* build statements via alias renaming, when the RHS contains
	   a single symbol with aliases *)
	let aliased_statements =
	  match f_with_alias with
	  | [f] when Terms.uses_f f coll.c_redl ->
	      List.map (fun f' -> replace_f_coll f f' coll) f.f_aliases
	  | _ -> []
	in
	(* store [coll] in the root symbol of its LHS, taking into account
	   aliases *)
	if coll.c_aliased then
	  List.iter (fun f' -> record_coll f' coll) f.f_aliases
	else
	  List.iter (fun coll' ->
	    (* print_string "Added "; Display.display_collision coll'; print_newline(); *)
	    match coll'.c_redl.t_desc with
	    | FunApp(f',_) -> record_coll f' coll'
	    | _ -> assert false
		    ) aliased_statements;
	record_coll f coll;
	(* store the aliased statements in [coll_accu] *)
	coll_accu := aliased_statements @ (!coll_accu)
      else
	record_coll f coll
  | _ ->
      Display.display_collision coll;
      print_string " ignored: the left-hand side should start with a function symbol.\n"
				  
(* Specialized versions of [record_with_aliases] for equation
   and collision statements respectively *)

let f_record_statement f statement =
  f.f_statements <- statement :: f.f_statements

let f_record_collision f collision =
  f.f_collisions <- collision :: f.f_collisions

let record_collision collision =
  record_with_aliases Settings.collisions f_record_collision collision

let record_statement statement =
  record_with_aliases Settings.statements f_record_statement statement

(* Simplify an equation statements given by the user *)    

let create_record_statement (n, vl, t1, t2, side_cond, active, alias_gen) =
  let statement = Terms.create_statement (Normal n) vl t1 t2 side_cond active alias_gen in
  Settings.initial_statements := statement :: (!Settings.initial_statements);
  Settings.statements := statement :: (!Settings.statements);
  record_statement statement

let display_statement name t side_cond =
  if name = NoName then
    begin
      Display.display_term t;
      if not (Terms.is_true side_cond) then
	begin
	  print_string " if ";
	  Display.display_term side_cond
	end
    end
  else
    Display.display_eqname name
	
let simplify_statement (n, vl, t, side_cond, active, alias_gen) =
  let glob_reduced = ref false in
  let rec reduce_rec t =
    let reduced = ref false in
    let t' = Terms.apply_eq_reds Terms.simp_facts_id reduced t in
    if !reduced then 
      begin
	glob_reduced := true;
	reduce_rec t'
      end
    else t
  in
  let t' = reduce_rec t in
  let side_cond' = reduce_rec side_cond in
  if Terms.is_true t' then 
    begin
      print_string "Warning: statement ";
      display_statement n t side_cond;
      print_string " removed using the equational theory.\n"
    end
  else if Terms.is_false t' then
    begin
      print_string "Error: statement ";
      display_statement n t side_cond;
      Parsing_helper.user_error " contradictory.\n"
    end
  else if Terms.is_false side_cond' then
    begin
      print_string "Warning: statement ";
      display_statement n t side_cond;
      print_string " removed using the equational theory: side condition always false.\n"
    end
  else
    begin
      if !glob_reduced then 
	begin
	  print_string "Statement ";
	  display_statement n t side_cond;
	  print_string " simplified into ";
	  display_statement NoName t' side_cond';
	  print_string " using the equational theory.\n"
	end;
      match t'.t_desc with
	FunApp(f, [t1;t2]) when f.f_cat == Equal ->
	  let vars = ref [] in
	  Terms.collect_vars vars t2;
	  Terms.collect_vars vars side_cond';
	  if not (List.for_all (fun b ->
	    Terms.refers_to b t1
	      ) (!vars)) then
	    begin
	      print_string "Error in simplified statement ";
	      display_statement NoName t' side_cond';
	      Parsing_helper.user_error ": all variables of the right-hand side and of the side condition should occur in the left-hand side.\n"
	    end;	  
	  create_record_statement (n, vl, t1, t2, side_cond', active, alias_gen) 
      | _ ->
	  let vars = ref [] in
	  Terms.collect_vars vars side_cond';
	  if not (List.for_all (fun b ->
	    Terms.refers_to b t'
	      ) (!vars)) then
	    begin
	      print_string "Error in simplified statement ";
	      display_statement NoName t' side_cond';
	      Parsing_helper.user_error ": all variables of the side condition should occur in the term.\n"
	    end;	  
	  create_record_statement (n, vl, t', Terms.make_true(), side_cond', active, alias_gen);
          match t'.t_desc with
          | FunApp(f, [t1;t2]) when f.f_cat == Diff ->
	     create_record_statement (n, vl, Terms.make_equal t1 t2, Terms.make_false(), side_cond', active, alias_gen)
          | _ -> 
	     ()
    end
	  

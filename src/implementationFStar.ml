open Types
module StringMap = Map.Make (String)

let alphabetize_string = StringPlus.alphabetize_string

let get_oracle_function_name name = "oracle_" ^ alphabetize_string name

let get_binder_name b =
  "var_" ^ alphabetize_string b.sname ^ "_" ^ string_of_int b.vname

let get_letfun_name f = "fun_" ^ alphabetize_string f.f_name

let get_proj_name f i = "proj_" ^ (string_of_int i) ^ "_" ^ alphabetize_string f.f_name
				   
let get_oracle_name o = "Oracle_" ^ alphabetize_string o

let get_types_name tl =
  String.concat "_" (List.map (fun t -> alphabetize_string (
    match t.tcat with
    | BitString -> t.tname
    | Interv p -> p.pname)) tl)

let get_tuple_dec_fun_name tl = "decompose_" ^ get_types_name tl
let get_tuple_tag_name tl = "tag_" ^ get_types_name tl

module Binder =
  struct
    type t = binder
    let compare b1 b2 = compare (get_binder_name b1) (get_binder_name b2)
  end

module BinderSet = Set.Make (Binder)

(*informations to the user*)
let info mess = print_string ("Information: " ^ mess ^ " (implementation)\n")

let error mess =
  Parsing_helper.user_error ("Error: " ^ mess ^ " (implementation)\n")

let activate_debug_comments = false

let debug_comment str =
  if activate_debug_comments then " (* " ^ str ^ " *) " else ""

let protocol_name = ref "nsl"
let protocol_name_allcaps = ref "NSL"

let set_protocol_name filename =
  let suffix =
    try
      List.find (fun suffix -> StringPlus.case_insensitive_ends_with filename suffix) [".pcv"; ".cv"; ".ocv" ]
    with Not_found ->
      ""
  in
  let basename = String.sub filename 0 (String.length filename - String.length suffix) in
  let alph_basename = alphabetize_string basename in
  protocol_name := String.lowercase_ascii alph_basename;
  protocol_name_allcaps := String.uppercase_ascii alph_basename

let letfun_module = "Letfun"

let letfun_prefix = ref ""

let side1 = "Real"
let side2 = "Ideal"

let current_side = ref ""
(* This variable can contain:
   - "" when we generate a single implementation
   - side1^"." or side2^"." when we generate 2 implementations *)

let string_list_sep = String.concat

let list_args l =
  if l = [] then
    "()"
  else
    String.concat " " l

let state_type () = !protocol_name ^ "_state"

let state_variable = "state"

let state_declaration () = state_variable ^ ": " ^ (state_type ())

let table_entry_exists = "entry_exists"
let table_entry_exists_full = "entry_exists_full"

let table_get = "get"

let table_get_unique = "get_unique"
let table_get_unique_full = "get_unique_full"

let table_insert = "insert"

let call_with_entropy = "call_with_entropy"

let session_id_variable = "sid"

let get_impl f =
  match f.f_cat with
  | Proj(finv,n) ->
      begin
	match finv.f_impl_inv with
	| Some _ -> Func(get_proj_name finv n)
	| None -> No_impl
      end
  | _ -> f.f_impl

let missing_impl f =
  match f.f_cat with
  | Proj(finv,_) ->  error ("Inverse function of " ^ finv.f_name ^ " not registered. Add a line 'implementation fun " ^ finv.f_name ^ "=\"f1\" [inverse = \"f2\"].' in the source.")
  | _ -> error ("Function not registered:" ^ f.f_name)

let in_parens str = "(" ^ str ^ ")"

let in_parens_if_needed code_str =
  let trimmed = String.trim code_str in
  (* We do not produce \r, nor form feed characters, nor other whitespace, so these should suffice: *)
  if String.contains trimmed '\n' || String.contains trimmed ' ' || String.contains trimmed '\t' then
    in_parens code_str
  else code_str

let is_complicated code_str =
  let trimmed = String.trim code_str in
  String.contains trimmed '\n' || String.length trimmed > 15

(* Add ind in front of every line *)
let code_block_add_line_prefix code_str ind =
  ind ^ String.concat ("\n" ^ ind) (String.split_on_char '\n' code_str)

let code_block code ind print_right_separator =
  (* We reuse code_str to make sure to evaluate `code` only once. *)
  let code_str = code "" in
  if is_complicated code_str then
    "\n" ^
    ind ^ "  begin\n" ^
    (code_block_add_line_prefix code_str (ind ^ "    ")) ^ "\n" ^
    ind ^ "  end" ^
    if print_right_separator then "\n" ^ ind else ""
  else
    " " ^ String.trim code_str ^
    if print_right_separator then " " else ""

let term_needs_entropy t =
  match t.t_desc with
  | FunApp(f, _) -> f.f_impl_ent
  | _ -> false

(* returns set of free and set of bound variables
  get_iprocess_bv returns the set of variables bound not under a replication
  The set of variables bound under a replication is stored in
  bound_vars_under_repl.
  The set of variables bound inside a term is stored in bound_vars_terms.
  The set of free variables is stored in free_vars.
*)

let empty_bv = BinderSet.empty

let bound_bv = BinderSet.singleton

let add_bv = BinderSet.union

let add_list_bv f l = List.fold_left (fun x y -> add_bv x (f y)) empty_bv l

let get_binderref_binder ext (b, l) =
  if Terms.is_args_at_creation b l then b
  else
    Parsing_helper.input_error
      "There should not be any find variable (implementation)" ext

let free_vars = ref BinderSet.empty

(* Contains all free variables; may contain some variables that are also bound *)

let bound_vars_under_repl = ref BinderSet.empty

let bound_vars_terms = ref BinderSet.empty


type prepared_queries = {
  public_vars: binder list;
    (* We will create a reveal query for these variables:
       x:T is always under replication, field st_x: Map.t T in state variables,
         initially empty, reveal query q_x(n: SidMap.sidt) = return st_x[n]
       store x in state variable st_x when it is assigned *)
  secret_vars: (binder * q_secret_opt) list;
    (* We will create queries depending on the secrecy option for these variables
       - RealOrRandom false (several sessions):
         Add field stqueried_x: Map.t T initially empty
         Real side: reveal query
         Ideal side:
           q_x(n): if st_x[n] contains no value, return None;
           otherwise, if stqueried_x[n] contains a value, return it;
           otherwise, choose a random value, store it in stqueried_x[n],
           return it.
       - RealOrRandom true (one-session):
         Add field stqueried_x: bool initially false
         Real side: reveal query that can be called once (answer None when
           stqueried_x is already set, and set stqueried_x)
         Ideal side: q_x(n:SidMap.sidt) = if stqueried_x, then return None,
           else st_x_queried <- true;
                if st_x[n] contains no value, return None
                else return Some(random value).
       - Reachability false (several sessions):
         Add field stqueried_x: Map.t () initially empty
         Reveal query q_x(n: SidMap.sidt) = if st_x[n] contains no value, return None;
            otherwise, add n -> () to stqueried_x; return st_x[n]
         Test query: qtest_x(n: SidMap.sidt, y:T) =
            if stqueried_x[n] contains a value, then return;
            if st_x[n] contains a value and y = st_x[n], then assert false
            else return.
       - Reachability true (one-session):
         Test query qtest_x(n: SidMap.sidt, y:T) =
            if st_x[n] contains a value and y = st_x[n], then assert false
            else return.
 *)
  has_diff_equiv: bool;
  has_equiv: bool;
  corresp: ((bool(*true when injective*) * term) list * qterm) list
}

type fstar_vars =
  | FSBool of string
  | FSVar of string * string
      
type oracle_def =
| Top of inputprocess
  (* nb_of_return, free variables, input process *)
| NonTop of (int * binder list * inputprocess) list

type oracle_data = {
  oracle_name: string;
  under_repl: bool;
  mutable has_queried_var: bool;
  previous_oracle: string option;
  input_type: typet list;
  mutable output_type: typet list option;
  mutable following_oracles: string list option;
  mutable list_of_returns: (int * (string * binder list) list * process) list;
  mutable list_of_defs: oracle_def;
  mutable seen_returns: int
}

let oracle_only_yields od =
  match od.output_type with
  (* this is if all branches yield *)
  | None -> true
  (* this is if at least one other branch returns/has an output *)
  | Some _ -> false

let oracles = ref (StringMap.empty:oracle_data StringMap.t)

let empty_prepared_queries =
  { public_vars = [];
    secret_vars = [];
    has_diff_equiv = false;
    has_equiv = false;
    corresp = []
 }
    
let prepared_queries_ref = ref empty_prepared_queries
    
let tables = ref []

let events = ref []

let built_in_types = [Settings.t_bitstring; Settings.t_bitstringbot;
                      Settings.t_bool; Settings.t_unit; Settings.t_interv]

let types = ref []
let eq_types = ref []
let serde_types = ref []
let random_types = ref []

let built_in_constants = [Settings.c_true; Settings.c_false]
let built_in_functions = [Settings.f_and; Settings.f_or; Settings.f_not;
  Settings.f_plus; Settings.f_mul; Settings.f_comp Equal Settings.t_bool Settings.t_bool;
  Settings.f_comp Diff Settings.t_bool Settings.t_bool;
  Settings.f_comp Equal Settings.t_bitstring Settings.t_bitstring;
  Settings.f_comp Diff Settings.t_bitstring Settings.t_bitstring;
  Settings.f_comp Equal Settings.t_bitstringbot Settings.t_bitstringbot;
  Settings.f_comp Diff Settings.t_bitstringbot Settings.t_bitstringbot]
let functions = ref []
let inv_functions = ref []

let add_bv_term bs = bound_vars_terms := BinderSet.union bs !bound_vars_terms

let free_var b =
  free_vars := BinderSet.add b !free_vars;
  BinderSet.empty

let rec get_pattern_bv = function
  | PatVar b -> bound_bv b
  | PatTuple (fs, pl) -> add_list_bv get_pattern_bv pl
  | PatEqual t ->
      add_bv_term (get_term_bv t);
      empty_bv

and get_iprocess_bv p =
  match p.i_desc with
  | Nil -> empty_bv
  | OracleDecl (o, patl, p) -> add_bv (add_list_bv get_pattern_bv patl) (get_oprocess_bv p)
  | Par (p1, p2) -> add_bv (get_iprocess_bv p1) (get_iprocess_bv p2)
  | Repl (b, p1) ->
      let bv = get_iprocess_bv p1 in
      bound_vars_under_repl := BinderSet.union !bound_vars_under_repl bv;
      empty_bv

and get_term_bv t =
  match t.t_desc with
  | Var (b, tl) -> free_var (get_binderref_binder t.t_loc (b, tl))
  | FunApp (fs, tl) -> add_list_bv get_term_bv tl
  | TestE (t1, t2, t3) ->
      add_bv (add_bv (get_term_bv t1) (get_term_bv t2)) (get_term_bv t3)
  | LetE (pat, t1, t2, t3) ->
      add_bv (get_pattern_bv pat)
        (add_bv (get_term_bv t1)
           (add_bv (get_term_bv t2)
              (match t3 with None -> empty_bv | Some t -> get_term_bv t)))
  | ResE (b, t) -> add_bv (bound_bv b) (get_term_bv t)
  | ReplIndex _ -> empty_bv
      (* Replication indices may occur in events and variables *)
  | EventAbortE _ -> empty_bv
  | EventE (t, p) -> add_bv (get_term_bv t) (get_term_bv p)
  | FindE _ ->
      Parsing_helper.input_error "Find not supported (implementation)" t.t_loc
  | GetE (tbl, patl, topt, p1, p2, _) ->
      List.fold_right add_bv
        (List.map get_pattern_bv patl)
        (add_bv
           (match topt with Some t -> get_term_bv t | None -> empty_bv)
           (add_bv (get_term_bv p1) (get_term_bv p2)))
  | InsertE (tbl, tl, p) ->
      List.fold_right add_bv (List.map get_term_bv tl) (get_term_bv p)

and get_oprocess_bv p =
  match p.p_desc with
  | Yield | EventAbort _ -> empty_bv
  | Restr (b, p) -> add_bv (bound_bv b) (get_oprocess_bv p)
  | Test (t, p1, p2) ->
      add_bv_term (get_term_bv t);
      add_bv (get_oprocess_bv p1) (get_oprocess_bv p2)
  | Return (tl, p) ->
      List.iter (fun t -> add_bv_term (get_term_bv t)) tl;
      get_iprocess_bv p
  | Let (pat, t, p1, p2) ->
      add_bv_term (get_term_bv t);
      add_bv (get_pattern_bv pat)
        (add_bv (get_oprocess_bv p1) (get_oprocess_bv p2))
  | EventP (t, p) ->
      add_bv_term (get_term_bv t);
      get_oprocess_bv p
  | Find (fl, ep, _) -> error "Find not supported"
  | Get (tbl, patl, topt, p1, p2, _) ->
      (match topt with Some t -> add_bv_term (get_term_bv t) | None -> ());
      List.fold_right add_bv
        (List.map get_pattern_bv patl)
        (add_bv (get_oprocess_bv p1) (get_oprocess_bv p2))
  | Insert (tbl, tl, p) ->
      List.iter (fun t -> add_bv_term (get_term_bv t)) tl;
      get_oprocess_bv p

let display_vars (a, b) =
  print_string "Free vars : ";
  BinderSet.iter (fun x -> print_string ((get_binder_name x) ^ " ")) a;
  print_newline ();
  print_string "Bound vars : ";
  BinderSet.iter (fun x -> print_string ((get_binder_name x) ^ " ")) b;
  print_newline ()

let impl_get_vars p =
  free_vars := BinderSet.empty;
  bound_vars_under_repl := BinderSet.empty;
  bound_vars_terms := BinderSet.empty;
  let bv_no_repl = get_iprocess_bv p in
  let bv_repl = !bound_vars_under_repl in
  let bv_terms = !bound_vars_terms in
  (* This way of computing free variables is ok because the variables
     are renamed to distinct names *)
  let fv =
    BinderSet.diff !free_vars
      (BinderSet.union bv_no_repl (BinderSet.union bv_repl bv_terms))
  in
  free_vars := BinderSet.empty;
  bound_vars_under_repl := BinderSet.empty;
  bound_vars_terms := BinderSet.empty;
  (fv, bv_no_repl, bv_repl, bv_terms)

let rt = ref 0

let create_fresh_number () =
  rt := !rt + 1;
  !rt

let create_fresh_name prefix = prefix ^ string_of_int (create_fresh_number ())

let rec create_fresh_names prefix = function
  | 0 -> []
  | i -> create_fresh_name prefix :: create_fresh_names prefix (i - 1)

let create_local_name prefix i = prefix ^ string_of_int i

let rec create_local_names prefix curr = function
  | 0 -> []
  | i -> create_local_name prefix curr :: create_local_names prefix (curr + 1) (i - 1)

let check_oracle_compatibility name (rt, o) (rt', o') =
  if rt <> rt' then
    match !Settings.front_end with
    | Settings.Channels ->
        error
          ( "The outputs following inputs on channel " ^ name
            ^ " do not have the same type" )
    | Settings.Oracles ->
        error
          ( "The oracle " ^ name
            ^ " does not have the same return types everywhere" )
  else if o <> o' then
    match !Settings.front_end with
    | Settings.Channels ->
        error
          ( "The input channels after outputs after inputs on channel " ^ name
            ^ " are not the same everywhere" )
    | Settings.Oracles ->
        error
          ( "The oracle " ^ name
            ^ " does not have the same next oracles everywhere" )

let check_argument_type_compatibility name at at' =
  if at <> at' then
    match !Settings.front_end with
    | Settings.Channels ->
        error
          ( "The messages of inputs on channel " ^ name
            ^ " do not have the same types everywhere" )
    | Settings.Oracles ->
        error
          ( "The arguments of oracle " ^ name
            ^ " do not have the same types everywhere" )

let type_append =
  StringMap.fold (fun name (s, at) acc ->
      try
        let s', at' = StringMap.find name acc in
        check_argument_type_compatibility name at at';
        match (s, s') with
        | Some (rt, o), Some (rt', o') ->
            check_oracle_compatibility name (rt, o) (rt', o');
            acc
        | None, Some (rt, o) -> acc
        | Some (rt, o), None ->
            StringMap.add name (Some (rt, o), at) (StringMap.remove name acc)
        | None, None -> acc
      with Not_found -> StringMap.add name (s, at) acc)

(** Returns the next oracles.
 *  b represents if the oracle is under replication (false) or not (true)
**)
let rec get_next_oracles b p =
  match p.i_desc with
  | Nil -> []
  | OracleDecl (o, patl, p) -> [ (b, o.ooracle.oname, patl, p) ]
  | Par (p1, p2) -> get_next_oracles b p1 @ get_next_oracles b p2
  | Repl (b, p) -> get_next_oracles false p

let rec get_oracle_types_oprocess name args_types p =
  match p.p_desc with
  | Yield | EventAbort _ ->
      StringMap.add name (None, args_types) StringMap.empty
  | Restr (b, p) -> get_oracle_types_oprocess name args_types p
  | Test (t, p1, p2) ->
      type_append
        (get_oracle_types_oprocess name args_types p1)
        (get_oracle_types_oprocess name args_types p2)
  | Return (tl, p) -> (
      let r = get_oracle_types_process p in
      let o = List.map (fun (b, n, _, _) -> (b, n)) (get_next_oracles true p) in
      let ra = List.map (fun t -> t.t_type) tl in
      try
        let s, a' = StringMap.find name r in
        check_argument_type_compatibility name a' args_types;
        match s with
        | Some (ra', o') ->
            check_oracle_compatibility name (ra, o) (ra', o');
            r
        | None ->
            type_append
              (StringMap.add name (Some (ra, o), args_types) StringMap.empty)
              r
      with Not_found -> StringMap.add name (Some (ra, o), args_types) r )
  | Let (pat, t, p1, p2) ->
      type_append
        (get_oracle_types_oprocess name args_types p1)
        (get_oracle_types_oprocess name args_types p2)
  | EventP (t, p) -> get_oracle_types_oprocess name args_types p
  | Find (_, _, _) -> error "Find not supported"
  | Get (tbl, patl, topt, p1, p2, _) ->
      type_append
        (get_oracle_types_oprocess name args_types p1)
        (get_oracle_types_oprocess name args_types p2)
  | Insert (tbl, tl, p) -> get_oracle_types_oprocess name args_types p

and get_oracle_types_process p =
  match p.i_desc with
  | Nil -> StringMap.empty
  | OracleDecl (o, patl, p) ->
      get_oracle_types_oprocess o.ooracle.oname (List.map Terms.get_type_for_pattern patl) p
  | Par (p1, p2) ->
      type_append (get_oracle_types_process p1) (get_oracle_types_process p2)
  | Repl (b, p) -> get_oracle_types_process p

let make_id s =
  if String.contains s ' ' && not (s.[0] = '(' && s.[String.length s - 1] = ')') then "(" ^ s ^ ")" else s

let get_type_name t =
  match t.timplname with
  | Some s -> make_id s
  | None ->
      match t.tcat with
      | Interv _ ->
          (* translate replication indices *)
          "SidMap.sidt"
      | _ -> error ("Type name required for type " ^ t.tname)

let preamble module_name =
  "module " ^ !protocol_name_allcaps ^ "." ^ (!current_side) ^ module_name ^ "\n\n" ^
  "open CVTypes\n" ^
  "open Random\n" ^
  "open State\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Types\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Functions\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Tables\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Events\n" ^
  "open " ^ !protocol_name_allcaps ^ "." ^ (!current_side) ^ "Sessions\n" ^
  "open " ^ !protocol_name_allcaps ^ "." ^ (!current_side) ^ "Protocol\n\n"

let get_table_file tbl =
  match tbl.tblfile with None -> tbl.tblname | Some f -> f

let get_table_field t = "table_" ^ alphabetize_string (get_table_file t)
let get_table_name t = "tn_" ^ alphabetize_string (get_table_file t)
let get_table_entry_name t = "te_" ^ alphabetize_string (get_table_file t)
let get_table_print_entry t = "printentry_" ^ alphabetize_string (get_table_file t)

(* In F*, the field tpredicate is not used:
   the full specification can be included in the type (field timplname),
   using a dependent type to include a predicate if needed *)

let get_read_serial ty =
  match ty.tserial with
  | Some (_, s) -> make_id s
  | None ->
      match ty.tcat with
      | Interv _ -> "SidMap.deserialize_sidt"
      | _ -> error ("Deserialization for type " ^ ty.tname ^ " required")

let get_write_serial ty =
  match ty.tserial with
  | Some (s, _) -> make_id s
  | None ->
      match ty.tcat with
      | Interv _ -> "SidMap.serialize_sidt"
      | _ -> error ("Serialization for type " ^ ty.tname ^ " required")

let get_deser_ok_lemma ty =
  match ty.tserial_lemmas with
  | Some (s,_,_) -> s
  | None ->
      match ty.tcat with
      | Interv _ -> "SidMap.lemma_deser_ok_sidt"
      | _ -> error ("Serialization lemmas for type " ^ ty.tname ^ " required")

let get_deser_rev_lemma ty =
  match ty.tserial_lemmas with
  | Some (_,_,s) -> s
  | None ->
      match ty.tcat with
      | Interv _ -> "SidMap.lemma_deser_rev_sidt"
      | _ -> error ("Serialization lemmas for type " ^ ty.tname ^ " required")
	    
let get_random t =
  match t.trandom with
  | Some r -> make_id r
  | None ->
      error
        ( "Random generation function required for type " ^ t.tname ^ "." )

let random b ind =
  let rand = get_random b.btype in
  "\n" ^
  (* rand already is in parentheses if needed *)
  ind ^ "let " ^ state_variable ^ ", " ^ get_binder_name b ^ " = " ^ call_with_entropy ^ " " ^ state_variable ^ " " ^ rand ^ " in\n"

let equal t =
  match t.tequal with
  | Some eq ->
      make_id eq
  | None ->
      error
        ( "Equality test function required for type " ^ t.tname ^ "." )

let yield_transl od ind =
  if oracle_only_yields od then "\n" ^ ind ^ state_variable ^ ", ()"
  else "\n" ^ ind ^ state_variable ^ ", None"

let store_var b =
  List.memq b (!prepared_queries_ref).public_vars ||
  List.exists (fun (b',_) -> b' == b) (!prepared_queries_ref).secret_vars

let def_var b ind =
  if store_var b then
    let bname = get_binder_name b in
    ind ^ "let " ^ state_variable ^ " = State.var_def " ^ state_variable ^ " " ^
    !protocol_name_allcaps^".Variables.vnst"^bname ^ " " ^ session_id_variable ^ " " ^ bname ^ " in\n"
  else
    ""

let rec needs_state t =
  match t.t_desc with
  | Var _ -> false (* indices are always the current indices, and not encoded *)
  | FunApp(f, tl) -> f.f_impl_ent || f.f_impl_needs_state || List.exists needs_state tl
  | TestE(t1,t2,t3) -> needs_state t1 || needs_state t2 || needs_state t3
  | ReplIndex _ -> false
  | EventAbortE _ -> Parsing_helper.input_error "event_abort not supported in F* implementation" t.t_loc
  | EventE _ | GetE _ | InsertE _ | ResE _ -> true
  | FindE _ -> Parsing_helper.input_error "Find not supported (implementation)" t.t_loc
  | LetE (pat, t1, t2, topt) ->
      needs_state_pat pat ||
      needs_state t1 || needs_state t2 ||
      (match topt with
         None -> false
       | Some t3 -> needs_state t3)

and needs_state_pat = function
  | PatVar b -> store_var b
  | PatEqual(t) -> needs_state t
  | PatTuple(f, pl) -> List.exists needs_state_pat pl


let decompos_pattuple ind f pl =
  if f.f_name = "" then
    (fun arg -> (get_tuple_dec_fun_name (List.map Terms.get_type_for_pattern pl)) ^ " " ^ arg), false
  else
    (
       match (f.f_impl_inv) with
         Some(finv) -> (fun arg -> finv ^ " " ^ arg), f.f_options land Settings.fopt_BIJECTIVE != 0
       | _ -> error ("Inverse function of " ^ f.f_name ^ " not registered. Add a line 'implementation fun " ^ f.f_name ^ "=\"f1\" [inverse = \"f2\"].' in the source.")
    )


let rec translate_termlist_ns sep tl ind = match tl with
  | [] -> "()"
  | [a] -> translate_term_ns a ind
  | a::b -> (translate_term_ns a ind) ^ sep ^ (translate_termlist_ns sep b ind)

(* ns = no state *)
and translate_term_ns t ind =
  (* This wraps everything except Var into parentheses, so everything that might contain spaces.
     This is important because translate_termlist_ns is used for event and function parameters. *)
  match t.t_desc with
  | Var(b, tl) -> debug_comment "translate_term_ns Var" ^ get_binder_name (get_binderref_binder t.t_loc (b, tl))
  | FunApp(f, tl) ->
      if f.f_name = "" then
        "(Tuples.compos "^(get_tuple_tag_name (List.map (fun t -> t.t_type) tl)) ^ " [" ^
        (string_list_sep
          ";"
          (List.map
            (fun t -> (get_write_serial t.t_type) ^ " " ^(translate_term_ns t ind))
            tl)) ^
        "])"
      else
        (match get_impl f with
         | Func x ->
             assert (not f.f_impl_ent);
             debug_comment "translate_term_ns FunApp/Func" ^
	     if tl = [] then
	       x
	     else
               "(" ^ x ^ " " ^ (translate_termlist_ns " " tl ind) ^ ")"
         | Const x ->
             debug_comment "translate_term_ns FunApp/Const" ^
             x
         | SepFun ->
             assert (not f.f_impl_needs_state);
             "(" ^ (!letfun_prefix) ^ (get_letfun_name f) ^ " " ^ (translate_termlist_ns " " tl ind) ^ ")"
         | FuncEqual ->
             let eq = equal (List.hd tl).t_type in
             "(" ^ debug_comment "translate_term_ns FunApp/FuncEqual" ^
             eq ^ " " ^ (translate_termlist_ns " " tl ind) ^ ")"
         | FuncDiff ->
             let eq = equal (List.hd tl).t_type in
             "(" ^ debug_comment "translate_term_ns FunApp/FuncDiff" ^
             "not (" ^ eq ^ " " ^ (translate_termlist_ns " " tl ind) ^ "))"
         | No_impl -> missing_impl f
        )
  | TestE(t1,t2,t3) ->
      "(if " ^ (translate_term_ns t1 ind) ^
      debug_comment "translate_term_ns TestE" ^
      " then " ^
      (translate_term_ns t2 ind) ^
      " else " ^
      (translate_term_ns t3 ind) ^ " )"
  | ReplIndex _ ->
      (* if we end up here, this means that the CV model contains
         not all replication indices, or not in the right order. *)
      Parsing_helper.input_error "Replication indices should occur only inside variables and events, and only as tuple of all replication indices leading to this program point (implementation)" t.t_loc
  | EventAbortE _ ->
      Parsing_helper.input_error "event_abort not supported in F* implementation" t.t_loc
  | EventE _ -> assert false  (* These need state and if we end up here *)
  | GetE _ -> assert false    (* it is an error in the code and so *)
  | InsertE _ -> assert false (* we assert it does not happen. *)
  | ResE _ -> assert false
  | FindE _ ->
      Parsing_helper.input_error "Find not supported (implementation)" t.t_loc
  | LetE (pat, t1, t2, topt) ->
      "(" ^ debug_comment "translate_term_ns LetE" ^
      (match_pattern_ns [] pat (translate_term_ns t1 ind) (translate_term_ns t2)
         (match topt with
            Some t3 ->
              translate_term_ns t3
          | None ->
              (fun ind ->
                Parsing_helper.internal_error "else branch of let called but not defined"))
         true ind) ^
      ")"

and match_pattern_complex_ns opt pat s p1 p2 in_term ind =
  (* decomposition of every function *)
  let rec decompos = function
    (* On the first layer, we will not find PatVar or PatEqual -- it will have been captured
       by match_pattern_ns, already. *)
    | PatVar(b) ->
        let bname = get_binder_name b in
	assert (store_var b = false);
        ([], bname, [], [bname])
    | PatEqual(t) ->
        let n = create_fresh_name "bvar_" in
        let eq = equal t.t_type in
        ([], n, [(eq, n, translate_term_ns t ind)], [])
    | PatTuple(f, pl) ->
        let n = create_fresh_name "bvar_" in
        let (func, bij) = decompos_pattuple ind f pl in
        let decompos_list = List.map decompos pl in
        (
          (* func: (parameter n, func, bij, variable list) *) (n, func, bij, List.map (fun (_, y, _, _) -> y) decompos_list)::
            (List.concat (List.map (fun (x, _, _, _) -> x) decompos_list)),
          (* name *) n,
          (* tests *) List.concat (List.map (fun (_, _, z, _) -> z) decompos_list),
          (* all_binders *) List.concat (List.map (fun (_, _, _, t) -> t) decompos_list)
        )
  in
  let rec andlist = function
    | [] -> "true"
    | [x] -> x
    | x::y -> x ^ " && " ^ (andlist y)
  in
  let (func, name, tests, all_binders) = decompos pat in
  let aux ovar p1 p2_str =
    let p2_block = code_block (fun _ -> p2_str) (ind ^ "  ") false in
    ind ^ "  let " ^ name ^ " = " ^ s ^ " in\n" ^
    (* decompos functions *)
    (match ovar with Some m -> ind ^ "let " ^ m ^ " =\n" | _ -> "") ^
    (List.fold_left (^) ""
       (List.map
          (fun (n, f, bij, vl) -> 
	    if bij then
	      ind ^ "  let (" ^ (string_list_sep "," vl) ^ ") = " ^ f n ^ " in\n"
	    else
              ind ^ "  match " ^ (f n) ^ " with\n" ^
              ind ^ "  | None ->" ^ code_block_add_line_prefix p2_block "  " ^ "\n" ^
              ind ^ "  | Some (" ^ (string_list_sep "," vl) ^ ") ->\n") func)
    ) ^
    (* tests *)
    (if tests == [] then code_block p1 (ind ^ "  ") true
    else
      ind ^ "  if " ^ (andlist (List.map (fun (eq, n, s) -> eq ^ " " ^ n ^ " " ^ s) tests)) ^ " then" ^
      code_block p1 (ind ^ "  ") true ^
      "else" ^
      p2_block ^ "\n") ^ (match ovar with Some m -> ind ^ "in\n" | _ -> "")
  in
  (* We reuse p2_str to make sure p2 is only evaluated once. p1 is evaluated only once anyway. *)
  let p2_str = p2 "" in
  if is_complicated p2_str then
    (*the result of the match needs to be assigned to a new variable.*)
    let m = create_fresh_name "bvar_" in
    debug_comment "match_pattern_complex_ns" ^
    (aux (Some m) (fun s -> "Some (" ^ (string_list_sep "," all_binders) ^ ")") "None") ^ "\n" ^
    ind ^ "match " ^ m ^ " with\n" ^
    ind ^ "| None ->\n" ^
    ind ^ "  " ^ (code_block_add_line_prefix p2_str (ind ^ "      ")) ^ "\n" ^
    ind ^ "| Some (" ^ (string_list_sep "," all_binders) ^ ") ->\n" ^
    ind ^ "  " ^ (p1 (ind ^ "      "))
  else
    debug_comment "match_pattern_complex_ns" ^
    aux None p1 p2_str

(*
  The meaning of the parameters is the following:
  let pat = var in ( p1 ) else ( p2 )
*)
and match_pattern_ns opt (pat:Types.pattern) (var:string) (p1:string -> string) (p2: string -> string) (in_term:bool) ind =
  match pat with
  | PatVar(b) ->
      assert (store_var b = false);
      ind ^
      "let " ^ (get_binder_name b) ^ " = " ^ var ^
      debug_comment "match_pattern_ns PatVar" ^
      " in\n" ^
      (p1 ind)
  | PatEqual(t) ->
      let eq = equal t.t_type in
      ind ^
      "if " ^ eq ^ " " ^ (translate_term_ns t ind) ^ " " ^ var ^ " then" ^
      debug_comment "match_pattern_ns PatEqual" ^
      (code_block p1 ind true) ^
      "else" ^
      (code_block p2 ind true)
  | _ -> match_pattern_complex_ns opt pat var p1 p2 in_term ind

let rec translate_oprocess od p ind =
  match p.p_desc with
  | Yield -> yield_transl od ind
  | EventAbort _ ->
      Parsing_helper.input_error "event_abort not supported in F* implementation" p.p_loc
  | Restr(b, p) ->
      (random b ind) ^
      (def_var b ind) ^
      (translate_oprocess od p ind)
  | Test(t, p1, p2) ->
      let (prefix, valt) = translate_term_get_val t ind in
      "\n" ^
      prefix ^ "\n" ^
      ind ^ "if " ^ valt ^ " then\n" ^
      ind ^ "begin" ^
      (translate_oprocess od p1 (ind ^ "  ")) ^ "\n" ^
      ind ^ "end\n" ^
      ind ^ "else\n" ^
      ind ^ "begin" ^
      (translate_oprocess od p2 (ind ^ "  ")) ^ "\n" ^
      ind ^ "end"
  | Return(tl,p1) ->
      (try
        let (nr, fol, _) = List.find (fun (_, _, pr) -> pr == p) od.list_of_returns in
        (if fol == [] then ""
        else
          "let sel = [" ^
          String.concat "; "
            (List.map
              (fun (fon, bl) ->
                "R" ^ string_of_int nr ^ "_" ^ alphabetize_string fon ^
                List.fold_left
                  (fun s b -> s ^ " " ^ get_binder_name b)
                  ""
                  bl
              )
              fol) ^ "] in\n" ^
        "let " ^ state_variable ^ " = state_add_to_session " ^ state_variable ^ " " ^ session_id_variable ^ " sel in\n") ^
        let (prefix, valt) = translate_term_to_output tl ind in
        "\n" ^ prefix ^ "(" ^
        state_variable ^ ", Some (" ^
        (if (fol != [] || od.has_queried_var) && od.under_repl then session_id_variable ^ ", " else "") ^
        valt ^ "))\n"
      with Not_found ->
        Parsing_helper.internal_error ("Expected exactly one list entry corresponding to the process, when treating an output of oracle " ^ od.oracle_name)
      )
  | Let(pat,t,p1,p2) ->
      let (prefix, valt) = translate_term_get_val t ind in
      let (prefixpat, valpat) = match_pattern [] pat valt (translate_oprocess od p1) (translate_oprocess od p2) false ind in
      "\n" ^ prefix ^ prefixpat ^
      "\n" ^ ind ^ valpat
  | EventP(t,p)->
      "\n"^ind^(translate_event t ind)^
      (translate_oprocess od p ind)
  | Find(_,_,_) ->
      Parsing_helper.input_error "Find not supported (implementation)" p.p_loc
  | Get(tbl,patl,topt,p1,p2,find_info) ->
      translate_get (fun b -> Terms.refers_to_oprocess b p1) [] tbl patl topt (translate_oprocess od p1) (translate_oprocess od p2) find_info ind
  | Insert(tbl,tl,p) ->
      let (prefixtl, valtl) = translate_termlist tl ind in
      prefixtl ^ "let state = " ^ table_insert ^ " " ^ state_variable ^ " " ^
      get_table_name tbl ^ " (" ^ 
      (string_list_sep ", "
         (List.map in_parens_if_needed valtl)
      ) ^ ") in\n" ^
      (translate_oprocess od p ind)

(* p1 and p2 are functions that take the indentation level
   and return the string corresponding to the program.
   Meaning of parameters: get tbl(patl) suchthat topt (p1) else (p2)
*)
and translate_get used opt tbl patl topt p1 p2 find_info ind =
  let pat_vars = Terms.vars_from_pat_list [] patl in
  let used_vars = List.filter used pat_vars in
  let match_res = "("^(string_list_sep "," (List.map get_binder_name used_vars))^")" in
  let tvars = create_fresh_names "tvar_" (List.length tbl.tbltype) in
  let filter_needs_state = match topt with | Some t -> needs_state t | None -> false in
  let (prefix, matchfilt) =
    match_pattern_list
      opt patl tvars
      (fun ind -> (* p1 *)
        match topt with
           | Some t ->
               if filter_needs_state then
                 let prefixf, valf = translate_term_get_val t ind in
                 ind ^ prefixf ^ "if (" ^ valf ^ ") then " ^ state_variable ^ ", Some " ^ match_res ^ " else " ^ state_variable ^ ", None"
               else
                 ind ^ "if (" ^ (translate_term_ns t ind) ^ ") then Some " ^ match_res ^ " else None"
           | None -> ind ^ "Some " ^ match_res)
      (fun ind -> "None") (* p2 *)
      false
      (ind ^ "      ")
  in
  let filterfun =
    ind ^ "  (fun " ^ (if filter_needs_state then "(" ^ state_variable ^ ":"  ^ (state_type ()) ^ ") " else "") ^ "(te:" ^ get_table_entry_name tbl ^ ") -> " ^
    ind ^ "let (" ^ (string_list_sep ", " tvars) ^ ") = te in\n" ^
    ind ^ "    begin\n" ^
    (*  *)
    matchfilt ^ "\n" ^
    ind ^ "    end\n" ^
    ind ^ "  )"
  in
  if used_vars = [] then
    "\n" ^
    ind^"begin\n"^
    prefix ^
    (if filter_needs_state then
      ind ^ "let " ^ state_variable ^ ", b = " ^ table_entry_exists_full ^ " " ^ state_variable ^ " " ^ get_table_name tbl ^ "\n" ^ filterfun ^ " in\n" ^
      ind ^ "if b then"
    else
      ind ^ "if " ^ table_entry_exists ^ " " ^ state_variable ^ " " ^ get_table_name tbl ^ "\n" ^ filterfun ^
      ind ^ "then") ^
    (code_block p1 ind true) ^
    "else" ^
    (code_block p2 ind true) ^
    "end"
  else if find_info = Unique then
    "\n" ^
    ind ^ "begin\n"^
    prefix ^
    ind ^ "match " ^ (if filter_needs_state then table_get_unique_full else table_get_unique) ^ " " ^ state_variable ^ " " ^ get_table_name tbl ^ "\n" ^
    filterfun ^ "\n" ^
    ind ^ "with\n"^
    ind ^ "| " ^ (if filter_needs_state then state_variable ^ ", " else "") ^ "None ->" ^
    (code_block p2 ind false) ^ "\n" ^
    ind ^ "| " ^ (if filter_needs_state then state_variable ^ ", " else "") ^ "Some " ^ match_res ^ " ->" ^
    (code_block p1 ind true) ^
    "end"
  else
    "\n" ^
    ind ^ "begin\n"^
    prefix ^
    ind ^ "match " ^ table_get ^ " " ^
    state_variable ^ " " ^ get_table_name tbl ^
    " (TableFilter" ^ (if filter_needs_state then "Full" else "Simple") ^ " \n" ^
    filterfun ^ ")\n" ^
    ind ^ "with\n"^
    ind ^ "| " ^ state_variable ^ ", None ->" ^
    (code_block p2 ind false) ^ "\n" ^
    ind ^ "| " ^ state_variable ^ ", Some " ^ match_res ^ " ->" ^
    (code_block p1 ind true) ^
    "end"

and translate_event t ind =
  match t.t_desc with
  | FunApp (event_function, repl_indices :: paraml) ->
      let is_session_id = match repl_indices.t_desc with
        (* first boolean: if it matches, second boolean: if we need serialization.
           We need serialization if the event was declared bitstring as type for
           a tuple of replication indices. *)
        | FunApp (_, []) -> (fun t -> false, false)
        | FunApp (_, [repl_index]) -> (fun t -> Terms.equal_terms t repl_index, false)
        | FunApp (_, _) -> (fun t -> Terms.equal_terms t repl_indices, true)
        | _ -> Parsing_helper.internal_error "Expected FunApp for event replication index list (translate_event)"
      in
      let translate_term_get_val_event t ind =
        match is_session_id t with
        | true, true -> ("", "(SidMap.serialize_sidt " ^ session_id_variable ^ ")")
        | true, false -> ("", session_id_variable)
        (* if the model uses only _some_ replication indices, or in the wrong order,
           or not as top-level parameter of the event but nested inside a parameter,
           we will end up in this last case, and then translate_term_get_val will
           complain about any replication indices. *)
        | _ -> translate_term_get_val t ind
      in
      let rec translate_termlist_event l ind =
        match l with
          [] -> ("", [])
        | [a] -> let (prefix, vala) = translate_term_get_val_event a ind in (prefix, [vala])
        | a::l ->
            let (prefixa, vala) = translate_term_get_val_event a ind in
            let (prefixl, vall) = translate_termlist_event l ind in
            prefixa ^ prefixl, vala :: vall
      in
      let prefix, vall = translate_termlist_event paraml "  " in
      prefix ^
      "let ev = Event_" ^ event_function.f_name ^ " " ^ (String.concat " " vall) ^ " in\n" ^
      "let " ^ state_variable ^ " = state_add_event " ^ state_variable ^ " ev in\n"
  | _ -> Parsing_helper.internal_error "Expected FunApp for event (translate_event)"

and translate_termlist l ind =
  match l with
    [] -> ("", [])
  | [a] -> let (prefix, vala) = translate_term_get_val a ind in (prefix, [vala])
  | a::l ->
      let (prefixa, vala) = translate_term_get_val a ind in
      let (prefixl, vall) = translate_termlist l ind in
      prefixa ^ prefixl, vala :: vall

and translate_term_get_val t ind =
  if not (needs_state t) then
    ("", translate_term_ns t ind)
  else
    begin
      match t.t_desc with
      | Var _ | ReplIndex _ -> assert false (* should be translated by the version without state *)
      | FunApp(f, tl) when not f.f_impl_ent ->
          let prefix, valtl = translate_termlist tl ind in
          if f.f_name = "" then
            prefix,
            "(compos [" ^
            (string_list_sep
              ";"
              (List.map2
                (fun t valt -> (get_write_serial t.t_type) ^ " " ^ valt)
                tl
                valtl)) ^
            "])"
          else
            (match get_impl f with
             | Func x ->
                 prefix,
                 "(" ^ debug_comment "translate_term_get_val FunApp/Func" ^
                 x ^ " " ^ (list_args valtl) ^ ")"
             | Const x -> assert false (* should be translated by the version without state *)
             | SepFun ->
                 let v = create_fresh_name "v" in
                 prefix ^
                 "let " ^
                 (if f.f_impl_needs_state then state_variable ^ ", " else "") ^ v ^
                 " = " ^
                 (!letfun_prefix) ^ (get_letfun_name f) ^ " " ^
                 (if f.f_impl_needs_state then state_variable ^ " " else "") ^
                 (if f.f_impl_needs_state && valtl = [] then "" else (list_args valtl)) ^ " in\n", v
             | FuncEqual ->
                 let eq = equal (List.hd tl).t_type in
                 prefix,
                 "(" ^ debug_comment "translate_term_get_val FunApp/FuncEqual" ^
                 eq ^ " " ^ (list_args valtl) ^ ")"
             | FuncDiff ->
                 let eq = equal (List.hd tl).t_type in
                 prefix,
                 "(" ^ debug_comment "translate_term_get_val FunApp/FuncDiff" ^
                 "not (" ^ eq ^ " " ^ (list_args valtl) ^ "))"
             | No_impl -> missing_impl f
            )
      | EventAbortE _ ->
          Parsing_helper.input_error "event_abort not supported in F* implementation" t.t_loc
      | FindE _ ->
          Parsing_helper.input_error "Find not supported (implementation)" t.t_loc
      | EventE(t,p)->
          let (prefix, valp) = translate_term_get_val p ind in
          (translate_event t ind) ^ prefix, valp
      | InsertE(tbl, tl, p) ->
          let (prefixtl, valtl) = translate_termlist tl ind in
          let (prefix, valp) = translate_term_get_val p ind in
          prefixtl ^ "let state = " ^ table_insert ^ " " ^ state_variable ^ " " ^
          get_table_name tbl ^ " (" ^ 
          (string_list_sep ", "
             (List.map in_parens_if_needed valtl)
          ) ^ ") in\n" ^ prefix, valp
      | ResE (b, p) ->
          let (prefix, valp) = translate_term_get_val p ind in
          (* random is producing prefix-like code. *)
          debug_comment "translate_term_get_val ResE" ^
          (random b ind) ^ (def_var b ind) ^ prefix, valp
      | TestE(t1,t2,t3) when not(needs_state t2 || needs_state t3) ->
          let (prefix1, v1) = translate_term_get_val t1 ind in
          prefix1,
          "(if " ^ v1 ^
          debug_comment "translate_term_get_val TestE" ^
          " then " ^ (translate_term_ns t2 ind) ^
          " else " ^ (translate_term_ns t3 ind) ^ ")"
      | _ ->
          let v = create_fresh_name "v" in
          "let " ^ state_variable ^ ", " ^ v ^ " = " ^ (translate_term_get_pair t ind) ^ " in\n", v
    end

and  translate_term_get_pair t ind =
  if not (needs_state t) then
    "("^ state_variable ^ ", " ^ (translate_term_ns t ind) ^ ")"
  else
    begin
      match t.t_desc with
      | Var _ | ReplIndex _ -> assert false (* should be translated by the version without state *)
      | FunApp(f, tl) when f.f_impl_ent ->
          let prefix, valtl = translate_termlist tl ind in
          assert (f.f_name <> ""); (* tuples should not need entropy *)
          (match get_impl f with
           | Func x ->
               let f_code =
                 "(" ^
                 debug_comment "translate_term_get_pair FunApp/Func" ^
                 x ^
                 (if List.length valtl > 0 then " " ^ (list_args valtl) else "") ^
                 ")" in
               prefix ^ call_with_entropy ^ " " ^ state_variable ^ " " ^ f_code
           | Const x -> assert false (* should be translated by the version without state *)
           | SepFun | FuncEqual | FuncDiff -> assert false (* should not need entropy *)
           | No_impl -> missing_impl f
          )
      | TestE(t1,t2,t3) when needs_state t2 || needs_state t3 ->
          let (prefix1, v1) = translate_term_get_val t1 ind in
          "(" ^ prefix1 ^
          " if " ^ v1 ^
          debug_comment "translate_term_get_pair TestE" ^
          " then " ^ (translate_term_get_pair t2 ind) ^
          " else " ^ (translate_term_get_pair t3 ind) ^ " )"
      | EventAbortE _ ->
          Parsing_helper.input_error "event_abort not supported in F* implementation" t.t_loc
      | EventE(t,p)->
          "(" ^ (translate_event t ind) ^
          (translate_term_get_pair p ind) ^ ")"
      | GetE(tbl,patl,topt,p1,p2, find_info) ->
          translate_get (fun b -> Terms.refers_to b p1) [] tbl patl topt (translate_term_get_pair p1) (translate_term_get_pair p2) find_info ind
      | InsertE(tbl, tl, p) ->
          let (prefixtl, valtl) = translate_termlist tl ind in
          prefixtl ^ "(let state = " ^ table_insert ^ " " ^ state_variable ^ " " ^ 
          get_table_name tbl ^ " (" ^
          (string_list_sep ", "
             (List.map in_parens_if_needed valtl)
          ) ^ ") in\n" ^
          (translate_term_get_pair p ind) ^ ")"
      | FindE _ -> Parsing_helper.input_error "Find not supported (implementation)" t.t_loc
      | ResE (b, t) ->
          "(" ^ debug_comment "translate_term_get_pair ResE" ^
          (random b ind) ^ (def_var b ind) ^ (translate_term_get_pair t ind) ^ ")"
      | LetE (pat, t1, t2, topt) ->
          let (prefix1, val1) = translate_term_get_val t1 ind in
          let (prefixpat, valpat) =
          match_pattern [] pat val1 (translate_term_get_pair t2)
                       (match topt with
                          Some t3 -> translate_term_get_pair t3
                        | None -> (fun ind -> Parsing_helper.internal_error "else branch of let called but not defined"))
              true ind
          in
          "(" ^ debug_comment "translate_term_get_pair LetE" ^
          prefix1 ^ prefixpat ^ valpat ^
          ")"
      | _ ->
          let (prefix, valt) = translate_term_get_val t ind in
          prefix ^ "(" ^ state_variable ^ ", " ^ valt ^ ")"
    end

and translate_term_to_output tl ind =
     if tl = [] then "", "()" else 
     let prefix, valtl = translate_termlist tl ind in
     prefix, (string_list_sep "," valtl)

and match_pattern_complex opt patl sl p1 p2 in_term ind =
  (* decomposition of every function *)
  let rec decompos = function
    | PatVar(b) ->
        let bname = get_binder_name b in
        ([], bname, [], [b])
    | PatEqual(t) ->
        let n = create_fresh_name "bvar_" in
        let eq = equal t.t_type in
        ([], n, [(eq, n, translate_term_get_val t ind)], [])
    | PatTuple(f, pl) ->
        let n = create_fresh_name "bvar_" in
        let (func, bij) = decompos_pattuple ind f pl in
        let decompos_list = List.map decompos pl in
        (
          (n,func, bij, List.map (fun (x,y,z,_)->y) decompos_list)::
            (List.concat (List.map (fun (x,y,z,_)->x) decompos_list)),
          n,
          List.concat (List.map (fun (x,y,z,_)->z) decompos_list),
          List.concat (List.map (fun (_,_,_,t)->t) decompos_list)
        )
  in
  let rec andlist = function
    | [] -> "true"
    | [x] -> x
    | x::y -> x^" && "^(andlist y)
  in
  let decompos_list = List.map decompos patl in
  let func = List.concat (List.map (fun (x,y,z,t)->x) decompos_list) in
  let assign = List.map2 (fun (x,y,z,t) s -> (y,s)) decompos_list sl in
  let tests = List.concat (List.map (fun (x,y,z,t)-> z) decompos_list) in
  let all_binders = List.concat (List.map (fun (_,_,_,t)->t) decompos_list) in
  let prefix = String.concat "" (List.map (fun (eq, n, (prefix, s)) -> prefix) tests) in
  let success =
    String.concat "" (List.map (fun b -> def_var b ind) all_binders) ^
    code_block p1 (ind ^ "  ") true
  in
  let aux ovar p1 p2_str =
    let p2_block = code_block (fun _ -> p2_str) (ind ^ "  ") false in
    "\n" ^
    (String.concat
      ""
      (List.map
        (fun (name, s) ->
          ind ^ "let " ^ name ^ " = " ^ s ^ " in\n") assign)) ^
    (* decompos functions *)
    (match ovar with Some m -> "let " ^ m ^ " =\n" | _ -> "") ^
    (List.fold_left (^) ""
       (List.map
          (fun (n, f, bij, vl) -> 
	    if bij then
	      ind ^ "  let (" ^ (string_list_sep "," vl) ^ ") = " ^ f n ^ " in\n"
	    else
              ind ^ "  match " ^ (f n) ^ " with\n" ^
              ind ^ "  | None ->" ^ code_block_add_line_prefix p2_block "  " ^ "\n" ^
              ind ^ "  | Some (" ^ (string_list_sep "," vl) ^ ") ->\n") func)
    ) ^
    (* tests *)
    (if tests == [] then success
     else
      ind ^ "  if " ^ (andlist (List.map (fun (eq, n, (prefix, s)) -> eq ^ " " ^ n ^ " " ^ s) tests)) ^ " then" ^
      success ^
      "else" ^
      p2_block ^ "\n") ^ (match ovar with Some m -> ind ^ "in\n" | _ -> "") ^
    ind ^ ""
  in
  (* We reuse p2_str to make sure p2 is only evaluated once. p1 is evaluated only once anyway. *)
  let p2_str = p2 "" in
  if is_complicated p2_str then
    let all_binders = List.map get_binder_name all_binders in
    (* the result of the match needs to be assigned to a new variable. *)
    let m = create_fresh_name "bvar_" in
    prefix,
    debug_comment "match_pattern_complex" ^
    (aux (Some m) (fun s -> "Some (" ^ (string_list_sep "," all_binders) ^ ")") "None") ^ "\n" ^
    (* the new variable needs to be used here. *)
    ind ^ "match " ^ m ^ " with\n" ^
    ind ^ "| None ->\n" ^
    ind ^ "  " ^ (code_block_add_line_prefix p2_str (ind ^ "      ")) ^ "\n" ^
    ind ^ "| Some (" ^ (string_list_sep "," all_binders) ^ ") ->\n" ^
    ind ^ "  " ^ (p1 (ind ^ "      "))
  else
    prefix,
    debug_comment "match_pattern_complex" ^
    aux None p1 p2_str

and match_pattern opt (pat:Types.pattern) (var:string) (p1:string -> string) (p2: string -> string) (in_term:bool) ind =
  match pat with
  | PatVar(b) ->
      "",
      debug_comment "match_pattern PatVar" ^ "\n" ^
      ind ^ "let " ^ (get_binder_name b) ^ " = " ^ var ^ " in\n" ^
      def_var b ind ^
      (p1 ind)
  | PatEqual(t) ->
      let (prefixt, valt) = translate_term_get_val t ind in
      let eq = equal t.t_type in
      prefixt,
      ind ^ "if " ^ eq ^ " " ^ valt ^ " " ^ var ^ " then" ^
      debug_comment "match_pattern PatEqual" ^
      (code_block p1 ind true) ^
      "else" ^
      (code_block p2 ind true)
  | _ -> match_pattern_complex opt [pat] [var] p1 p2 in_term ind

and match_pattern_list opt patl vars (p1:string -> string) (p2:string -> string) in_term ind =
  match patl, vars with
  | [pat], [var] ->
      match_pattern opt pat var p1 p2 in_term ind
  | _ ->
      match_pattern_complex opt patl vars p1 p2 in_term ind

and match_pattern_from_input opt pl (vars:string list) (next:string -> string) od ind =
     let (prefix, matchfilt) = match_pattern_list opt pl vars next (yield_transl od) false ind in
     prefix ^ matchfilt

let needs_sid od =
  (od.following_oracles <> None && od.following_oracles <> (Some [])) || od.has_queried_var   

let get_interface_spec module_name opt oracles =
  preamble (module_name ^ ".Spec") ^
  StringMap.fold
    (fun on od s -> s ^ 
      let args = create_fresh_names "input_" (List.length od.input_type) in
      let takes_id =
	match od.list_of_defs with
	| Top _ -> false
        | NonTop _ -> true
      in
      (* name of the function *)
      "let " ^ get_oracle_function_name on ^ " " ^
      (* we take a session id as input in some cases *)
      (if takes_id then "(" ^ session_id_variable ^ ": SidMap.sidt) " else "") ^
      (* names for the explicit oracle parameters *)
      (string_list_sep " " (List.map2 (fun arg t -> "("^arg^": "^ get_type_name t^")") args od.input_type)) ^
      (* we always take the current state as input *)
      " (" ^ state_declaration() ^ ") =\n" ^
      (* first line ends here *)
      (* if this continues a session, retrieve the entry *)
      (match od.list_of_defs with
      | Top p ->
          (match p.i_desc with
          | OracleDecl (_, patl, p1) ->
              (if needs_sid od then
                "let " ^ state_variable ^ ", " ^ session_id_variable ^ " = state_reserve_session_id " ^ state_variable ^ " in\n"
              else "") ^
              match_pattern_from_input opt patl args (translate_oprocess od p1) od ("      ")
          | _ -> Parsing_helper.internal_error "expected input process"
          )
      | NonTop dl ->
          "match " ^
          (if od.under_repl then "get_session_entry "
          else "get_and_remove_session_entry ") ^
          state_variable ^ " " ^
          get_oracle_name od.oracle_name ^ " " ^ session_id_variable ^ " with\n" ^
          "| " ^ state_variable ^ ", None -> " ^ state_variable ^ ", None\n" ^
          "| " ^ state_variable ^ ", Some (se:" ^ !protocol_name ^ "_session_entry) ->\n" ^
          (if od.under_repl && needs_sid od then
            "  let " ^ state_variable ^ ", " ^ session_id_variable ^ " = state_reserve_session_id " ^ state_variable ^ " in\n"
          else "") ^
            "  match se with\n" ^
          (* And now we treat all definitions of the oracle/returns of the previous oracle.
             We know that we are in a NonTop case because we are in takes_id. *)
          (List.fold_left
             (fun s (nr, bl, p) -> s ^
               "    | R" ^ string_of_int nr ^ "_" ^ alphabetize_string od.oracle_name ^
               (List.fold_left
                  (fun s b ->
                    s ^ " " ^ get_binder_name b)
                  ""
                  bl
               ) ^ " ->\n" ^
               (match p.i_desc with
               | OracleDecl (_, patl, p1) ->
                   match_pattern_from_input opt patl args (translate_oprocess od p1) od ("      ")
               | _ -> Parsing_helper.internal_error "expected input process"
               )
             )
             ""
             dl
          )
	    
      ) ^ "\n\n" ^

      (* name of the function *)
      "val lemma" ^ get_oracle_function_name on ^ " " ^
      (* we take a session id as input in some cases *)
      (if takes_id then "(" ^ session_id_variable ^ ": SidMap.sidt) " else "") ^
      (* names for the explicit oracle parameters *)
      (string_list_sep " " (List.map2 (fun arg t -> "("^arg^": "^ get_type_name t^")") args od.input_type)) ^
      (* we always take the current state as input *)
      " (" ^ state_declaration() ^ ") :\n" ^
      "  Lemma (requires (" ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas.inv' " ^ state_variable ^
      (if takes_id then " /\\ is_local " ^ session_id_variable ^ " " ^ state_variable else "") ^ "))\n"^
      "    (ensures (" ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas.rel' " ^ state_variable ^
      " (" ^ get_oracle_function_name on ^ " " ^
      (if takes_id then session_id_variable ^ " " else "") ^
      (string_list_sep " " args) ^ " " ^ state_variable ^ ")._1))\n\n"
    )
    oracles
    ""

let get_implementation_spec module_name opt oracles =
  preamble (module_name ^ ".Spec") ^
  "(* PLEASE ADD PROOFS FOR THE LEMMAS *)\n\n" ^
  StringMap.fold
    (fun on od s -> s ^ 
      let args = create_fresh_names "input_" (List.length od.input_type) in
      let takes_id =
	match od.list_of_defs with
	| Top _ -> false
        | NonTop _ -> true
      in
      (* name of the function *)
      "let lemma" ^ get_oracle_function_name on ^ " " ^
      (* we take a session id as input in some cases *)
      (if takes_id then "(" ^ session_id_variable ^ ": SidMap.sidt) " else "") ^
      (* names for the explicit oracle parameters *)
      (string_list_sep " " (List.map2 (fun arg t -> "("^arg^": "^ get_type_name t^")") args od.input_type)) ^
      (* we always take the current state as input *)
      " (" ^ state_declaration() ^ ")  = ()\n\n"
    )
    oracles
    ""

let cvst_args module_name od =
  let takes_id =
    match od.list_of_defs with
    | Top _ -> false
    | NonTop _ -> true
  in
  "(" ^
  (match od.output_type with
  | None -> "unit" (* We come here if all branches yield *)
  | Some tl -> "option (" ^
      (* return session id if needed *)
      (if needs_sid od && od.under_repl then "SidMap.sidt * " else "") ^
      (* output types *)
      (match tl with
      | [] -> "unit"
      | _ -> String.concat " * " (List.map get_type_name tl)) ^
      ")"
  ) ^
  ") " ^ (if takes_id then "(fun s1 -> is_local n s1)" else "true_pre") ^ " (fun s1 res s2 -> (s2, res) == " ^ !protocol_name_allcaps ^ "." ^ (!current_side) ^ module_name ^ ".Spec." ^ get_oracle_function_name od.oracle_name ^
       (match od.list_of_defs with
        | Top _ -> ""
        | NonTop _ -> " n") ^
       (fst (List.fold_left
         (fun (s,n) t ->
            (s ^ " x" ^ string_of_int n, n+1))
         ("",1)
         od.input_type)) ^
       " s1" ^ (if needs_sid od && od.under_repl then " /\\ (match res with | Some(sid" ^
       (match od.output_type with
       | None | Some [] -> ",_"
       | Some tl -> String.concat "" (List.map (fun _ -> ",_") tl)) ^
       ") -> is_local sid s2 | None -> True)" else "") ^ ")"
    

let get_interface module_name opt oracles =
  preamble module_name ^
  "open " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Effect\n\n"^
  StringMap.fold
    (fun on od s -> s ^
      "val " ^ get_oracle_function_name on ^ ": " ^
       (match od.list_of_defs with
        | Top _ ->
	    (* Add unit argument if the function would have no argument at all *)
	    if od.input_type = [] then "unit -> " else ""
        | NonTop _ -> "(n:SidMap.sidt) -> ") ^
       (fst (List.fold_left
         (fun (s,n) t ->
            (s ^ "(x" ^ string_of_int n ^ ": "^ get_type_name t ^ ") -> ",
             n+1))
         ("",1)
         od.input_type)) ^
       "CVST "^ cvst_args module_name od ^ "\n\n"
    )
    oracles
    ""

let get_implementation module_name opt oracles =
  preamble module_name ^
  "friend " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Effect\n\n"^
  StringMap.fold
    (fun on od s -> s ^
      let args_typed =
	" " ^
	(match od.list_of_defs with
        | Top _ ->
	    (* Add unit argument if the function would have no argument at all *)
	    if od.input_type = [] then "() " else ""
        | NonTop _ -> "(n:SidMap.sidt) ") ^
	(fst (List.fold_left
		(fun (s,n) t ->
		  (s ^ "(x" ^ string_of_int n ^ ": "^ get_type_name t ^ ") ",
		   n+1))
		("",1)
		od.input_type))
      in
      let args =
	(match od.list_of_defs with
        | Top _ -> ""
        | NonTop _ -> " n") ^
	(fst (List.fold_left
		(fun (s,n) t ->
		  (s ^ " x" ^ string_of_int n, n+1))
		("",1)
		od.input_type))
      in
      let args_with_unit =
	match od.list_of_defs with
        | Top _ ->
	    (* Add unit argument if the function would have no argument at all *)
	    if od.input_type = [] then " ()" else args
        | NonTop _ -> args
      in
      "val repr"^get_oracle_function_name on ^ args_typed ^ ": repr_inv " ^ cvst_args module_name od ^ "\n" ^
      "let repr"^get_oracle_function_name on ^ args_with_unit ^ " = fun s1 -> 
  " ^ !protocol_name_allcaps ^ "." ^ (!current_side) ^ module_name ^ ".Spec.lemma" ^ get_oracle_function_name on ^ args ^ " s1;
  " ^ !protocol_name_allcaps ^ "." ^ (!current_side) ^ module_name ^ ".Spec." ^ get_oracle_function_name on ^ args ^ " s1\n\n" ^ 
      "let " ^ get_oracle_function_name on ^ args_typed ^
       "= cvst_reflect (repr"^get_oracle_function_name on ^ args_with_unit ^ ")\n\n"
    )
    oracles
    ""

let get_letfun_implementation module_name letfuns =
  preamble module_name ^
  (string_list_sep "\n"
     (List.map
        (fun (f,bl,res) ->
          f.f_impl_needs_state <- needs_state res;

	  (* val *)
           "val " ^ (get_letfun_name f) ^ " : " ^
           (if f.f_impl_needs_state then (state_type ()) ^ " -> " else "") ^
           (if bl = [] then
              if (not f.f_impl_needs_state) then "unit -> " else ""
            else
              (string_list_sep " -> " (List.map (fun b -> get_type_name b.btype) bl)) ^ " -> "
           ) ^
           (if f.f_impl_needs_state then (state_type ()) ^ " * " else "") ^
           (get_type_name res.t_type) ^ "\n" ^

	  (* let *)
           "let " ^ (get_letfun_name f) ^ " " ^
           (if f.f_impl_needs_state then in_parens (state_declaration ()) ^ " " else "") ^
           (if bl = [] then
              if (not f.f_impl_needs_state) then "()" else ""
            else
              (string_list_sep " "
                 (List.map
                    (fun b -> ("(" ^ (get_binder_name b) ^ " : " ^ (get_type_name b.btype) ^ ")")) bl)
              )
           ) ^
           " =\n" ^
           (if f.f_impl_needs_state then
             translate_term_get_pair res "  "
           else
             translate_term_ns res "  ") ^
          "\n"
        )
        letfuns
     )
  )

let check_no_module_name_clash (impl_letfuns, impl_processes) =
  let basic_modules = [
    "CVTypes"; "Tuples"; "SidMap"; "Random"; "Crypto"; "State"; "StateLemmas";
    "Types"; "Functions"; "Tables"; "Sessions"; "Events"; "Protocol"; "Effect"; 
    "Variables"; "Queries"; "Equations" ] in
  let reserved_modules =
    if impl_letfuns <> [] then letfun_module :: basic_modules else basic_modules
  in
  let rec check_clash = function
    | [] -> ()
    | (x, _, _) :: rest ->
        List.iter
          (fun m ->
             if x = m then error ("Module " ^ x ^ " clashes with reserved module");
             if String.uppercase_ascii x = String.uppercase_ascii m then
               error
                 ( "Module " ^ x ^ " clashes with reserved module " ^ m
                   ^ " (filenames are case-insensitive on Windows)" ))
          reserved_modules;
        List.iter
          (fun (x', _, _) ->
             if String.uppercase_ascii x = String.uppercase_ascii x' then
               error
                 ( "Module " ^ x ^ " clashes with module " ^ x'
                   ^ " (filenames are case-insensitive on Windows)" ))
          rest;
        check_clash rest
  in
  check_clash impl_processes

let user_defined_type t =
  (not (List.memq t built_in_types)) &&
  (match t.tcat with | Interv _ -> false | _ -> true)
let user_defined_function f =
  (not (List.memq f built_in_functions)) &&
  (not (List.memq f built_in_constants))
let in_scope_function f =
  match get_impl f with
  | No_impl -> false
  | _ -> true

let add_type t =
  if user_defined_type t && not (List.memq t !types) then types := t::!types
let add_eq_type t =
  if user_defined_type t && not (List.memq t !eq_types) then eq_types := t::!eq_types
let add_serde_type t =
  if user_defined_type t && not (List.memq t !serde_types) then serde_types := t::!serde_types
let add_random_type t =
  if user_defined_type t && not (List.memq t !random_types) then random_types := t::!random_types

let start_tag = 1
let tuple_types = ref []
let add_tuple_type tl = 
  if not (List.exists (Terms.equal_lists (==) tl) (!tuple_types)) then tuple_types := tl :: !tuple_types

let add_table tab =
  if not (List.memq tab !tables) then tables := tab::!tables

let add_event e =
  if not (List.memq e !events) then events := e::!events

let add_inv_function f =
  if user_defined_function f && not (List.memq f !inv_functions) then inv_functions := f::!inv_functions

let add_function f =
  if user_defined_function f && not (List.memq f !functions) then
    begin
      functions := f::!functions;
      match f.f_cat with
      | Proj(finv,_) -> add_inv_function finv
      | _ -> ()
    end

let rec get_iprocess_oracles p under_repl previous_oracle nb_of_return =
  match p.i_desc with
  | Nil -> []
  | OracleDecl (o, patl, p1)  ->
     List.iter get_pattern_types patl;
     let input_type = List.map Terms.get_type_for_pattern patl in
     List.iter add_serde_type input_type;
     let (fv, bv_no_repl, _, _) = impl_get_vars p in
     let fv = BinderSet.elements fv in
     let has_queried_var =
       List.exists (fun b -> BinderSet.mem b bv_no_repl) (!prepared_queries_ref).public_vars ||
       List.exists (fun (b,_) -> BinderSet.mem b bv_no_repl) (!prepared_queries_ref).secret_vars
     in
     (try
       let oracle = StringMap.find o.ooracle.oname !oracles in
       oracle.has_queried_var <- has_queried_var || oracle.has_queried_var;
       if oracle.oracle_name <> o.ooracle.oname then
         error ("Oracle name " ^ oracle.oracle_name ^
           " expected to be " ^ o.ooracle.oname ^
           " at occ " ^ string_of_int p.i_occ ^ ".")
       else if oracle.under_repl <> under_repl then
         error ("Oracle " ^ oracle.oracle_name ^
           " expected to be "^ (if under_repl then "" else "not ") ^
           " under replication at occ " ^ string_of_int p.i_occ ^ ".")
       else if oracle.previous_oracle <> previous_oracle then
         error ("Oracle " ^ oracle.oracle_name ^
           " does not have the expected same previous oracle as " ^
           "other occurrences of this oracle, at occ " ^
           string_of_int p.i_occ ^ ".")
       else
         if not (Terms.equal_lists (==) oracle.input_type input_type) then
           error ("Oracle " ^ oracle.oracle_name ^
           " has different input type than other occurrences of this oracle" ^
           ", at occ " ^ string_of_int p.i_occ ^ ".")
       else
         match oracle.list_of_defs with
         | Top p2 ->
             error ("Oracle " ^ oracle.oracle_name ^
               " is a top-level oracle and should not occur multiple times, " ^
               "but does so at occ " ^ string_of_int p.i_occ ^ ".")
         | NonTop l ->
             match previous_oracle with
             | None ->
                 error ("Oracle " ^ oracle.oracle_name ^
                   " is not a top-level oracle but does not have a previous oracle," ^
                   " at occ " ^ string_of_int p.i_occ ^ ".")
             | Some _ -> (
                 oracle.list_of_defs <- NonTop ((nb_of_return, fv, p)::l);
                 get_oprocess_oracles p1 oracle)
     with Not_found ->
       let defl = match previous_oracle with
         | None -> Top p
         | Some prev -> NonTop [(nb_of_return, fv, p)] in
       let oracle = {
         oracle_name = o.ooracle.oname;
         under_repl = under_repl;
         has_queried_var = has_queried_var;
         previous_oracle = previous_oracle;
         input_type = input_type;
         output_type = None;
         following_oracles = None;
         list_of_returns = [];
         list_of_defs = defl;
         seen_returns = 0
       } in
     oracles := StringMap.add o.ooracle.oname oracle !oracles;
     get_oprocess_oracles p1 oracle
	 );
      [o.ooracle.oname, fv]
  | Par (p1, p2) ->
     (get_iprocess_oracles p1 under_repl previous_oracle nb_of_return) @
     (get_iprocess_oracles p2 under_repl previous_oracle nb_of_return)
  | Repl (_, p1) ->
      get_iprocess_oracles p1 true previous_oracle nb_of_return

and get_oprocess_oracles p0 oracle =
  match p0.p_desc with
  | Yield | EventAbort _ -> ()
  | EventP (e, p) -> get_event_types e; get_oprocess_oracles p oracle
  | Restr (b, p) ->
      add_type b.btype;
      add_random_type b.btype;
      get_oprocess_oracles p oracle
  | Let (pat, t, p1, p2) ->
      get_pattern_types pat;
      get_term_types t;
      get_oprocess_oracles p1 oracle; get_oprocess_oracles p2 oracle
  | Test (t, p1, p2) ->
      get_term_types t;
      get_oprocess_oracles p1 oracle; get_oprocess_oracles p2 oracle
  | Get (tab, patl, topt, p1, p2, _) ->
      add_table tab;
      List.iter add_serde_type tab.tbltype;
      List.iter get_pattern_types patl;
      (match topt with
      | Some t -> get_term_types t
      | None -> ());
      get_oprocess_oracles p1 oracle; get_oprocess_oracles p2 oracle
  | Insert (tab, tl, p) ->
      add_table tab;
      List.iter add_serde_type tab.tbltype;
      List.iter get_term_types tl;
      get_oprocess_oracles p oracle
  | Find (fl, ep, _) -> error "Find not supported"
  | Return (tl, p) ->
      List.iter get_term_types tl;
      let ot = List.map (fun t -> t.t_type) tl in
      List.iter add_serde_type ot;
      oracle.seen_returns <- oracle.seen_returns + 1;
      (match oracle.output_type with
      | None -> oracle.output_type <- Some ot
      | Some l ->
         if not (Terms.equal_lists (==) l ot) then
           error ("Oracle " ^ oracle.oracle_name ^
           " has different output type than other occurrences of this oracle" ^
           ", at occ " ^ string_of_int p.i_occ ^ ".") );

      let fo = get_iprocess_oracles p false (Some oracle.oracle_name) oracle.seen_returns in
      (match oracle.following_oracles with
      | None -> oracle.following_oracles <- Some (List.map fst fo)
      | Some l ->
      	  if l <> (List.map fst fo) then
            error ("Oracle " ^ oracle.oracle_name ^
              " has different following oracles than other occurrences of this oracle" ^
              ", at occ " ^ string_of_int p.i_occ ^ ".") );
      oracle.list_of_returns <- (oracle.seen_returns, fo, p0) :: oracle.list_of_returns

and get_term_types t =
  match t.t_desc with
  | Var (b, tl) -> add_type b.btype; List.iter get_term_types tl
  | ReplIndex _ -> ()
  | FunApp (f, tl) ->
      (if f.f_name = "" then (
        List.iter (fun t -> add_serde_type t.t_type) tl;
        add_tuple_type (List.map (fun t -> t.t_type) tl)
      ) else (
        add_function f;
        (* return type *)
        add_type (snd f.f_type);
        (* input types *)
        List.iter add_type (fst f.f_type);
        (match f.f_impl with | FuncEqual | FuncDiff -> add_eq_type (List.hd tl).t_type | _ -> ())
      ));
      List.iter get_term_types tl;
  | TestE (t1, t2, t3) ->
      get_term_types t1;
      get_term_types t2;
      get_term_types t3;
  | FindE _ -> Parsing_helper.input_error "Find not supported (implementation)" t.t_loc
  | LetE (pat, t1, t2, topt) ->
      get_pattern_types pat;
      get_term_types t1;
      get_term_types t2;
      (match topt with
      | Some t3 -> get_term_types t3;
      | None -> ())
  | ResE (b, t) ->
      add_type b.btype;
      add_random_type b.btype;
      get_term_types t
  | EventAbortE _ -> Parsing_helper.input_error "event_abort not supported in F* implementation" t.t_loc
  | EventE (t, p) ->
      get_event_types t;
      get_term_types p
  | InsertE (tbl, tl, t) ->
      add_table tbl;
      List.iter add_serde_type tbl.tbltype;
      List.iter get_term_types tl;
      get_term_types t
  | GetE (tbl, patl, topt, t1, t2, _) ->
      add_table tbl;
      List.iter add_serde_type tbl.tbltype;
      List.iter get_pattern_types patl;
      (match topt with
      | Some t -> get_term_types t
      | None -> ());
      get_term_types t1;
      get_term_types t2

and get_event_types e =
  add_event e;
  match e.t_desc with
  | FunApp (ef, repl_indices :: param_list) ->
      List.iter add_serde_type (List.tl (fst ef.f_type));
      List.iter get_term_types param_list
  | _ -> Parsing_helper.internal_error "Expected FunApp for event (do_implementation)"

and get_pattern_types pat =
  match pat with
  | PatVar b -> add_type b.btype
  | PatTuple (f, pl) ->
      (* As we are in a pattern, it is actually the inverse function that we need. *)
      if f.f_name = "" then
        begin
          List.iter (fun p -> 
            let ty = Terms.get_type_for_pattern p in
            add_serde_type ty) pl;
          add_tuple_type (List.map Terms.get_type_for_pattern pl)
        end
      else
        begin
          add_function f;
          add_inv_function f;
          add_type (snd f.f_type);
          List.iter add_type (fst f.f_type);
          (* We are generating lemmas for inverse functions, so we need equality test. *)
          add_eq_type (snd f.f_type);
          List.iter add_eq_type (fst f.f_type)
        end;
      List.iter get_pattern_types pl
  | PatEqual t -> add_eq_type t.t_type; get_term_types t

let rec descend l =
  let foldfun s t = s && term_contains_no_library_defs t in
  List.fold_left foldfun true l

and descend_pattern l =
  let foldfun s t = s && pattern_contains_no_library_defs t in
  List.fold_left foldfun true l

and term_contains_no_library_defs t =
  match t.t_desc with
  | Var (b, tl) -> descend tl
  | ReplIndex _ -> true
  | FunApp (f, tl) ->
      if f.f_name = "" then
        descend tl
      else
        if not (in_scope_function f)
        then (info ("1 equation will not be translated because there is no implementation for " ^ f.f_name); false)
        else descend tl
  | TestE (t1, t2, t3) ->
      term_contains_no_library_defs t1 &&
      term_contains_no_library_defs t2 &&
      term_contains_no_library_defs t3
  | FindE _ -> Parsing_helper.input_error "Find not supported (implementation)" t.t_loc
  | LetE (pat, t1, t2, topt) ->
      pattern_contains_no_library_defs pat &&
      term_contains_no_library_defs t1 &&
      term_contains_no_library_defs t2 &&
      (match topt with
      | Some t3 -> term_contains_no_library_defs t3
      | None -> true)
  | ResE (b, t) ->
      term_contains_no_library_defs t
  | EventAbortE _ -> Parsing_helper.input_error "event_abort not supported in F* implementation" t.t_loc
  | EventE (t, p) ->
      event_contains_no_library_defs t &&
      term_contains_no_library_defs p
  | InsertE (tbl, tl, t) ->
      descend tl &&
      term_contains_no_library_defs t
  | GetE (tbl, patl, topt, t1, t2, _) ->
      descend_pattern patl &&
      (match topt with
      | Some t -> term_contains_no_library_defs t
      | None -> true) &&
      term_contains_no_library_defs t1 &&
      term_contains_no_library_defs t2

and event_contains_no_library_defs e =
  match e.t_desc with
  | FunApp (ef, repl_indices :: param_list) ->
      descend param_list
  | _ -> Parsing_helper.internal_error "Expected FunApp for event (do_implementation)"

and pattern_contains_no_library_defs p =
  match p with
  | PatVar b -> true
  | PatTuple (f, pl) ->
      if f.f_name = "" then
        descend_pattern pl
      else
        if not (in_scope_function f) then false else
        descend_pattern pl
  | PatEqual t -> term_contains_no_library_defs t

(* translate built-in equational theories *)
let get_fun_equations first_nr =

  let header i =
    "val lemma_" ^ string_of_int i ^ " " 
  in

  (* f is always a binary function with both inputs of the same type
     in the currently supported builtin equational theories. *)
  let typename f =
    (get_type_name (List.hd (fst f.f_type))) in

  let eq_result f =
    equal (snd f.f_type) in

  let commut f x =
    "(a b:" ^ typename f ^ ") : Lemma (" ^
    eq_result f ^ " (" ^ x ^ " a b) (" ^ x ^ " b a))\n\n" in

  let assoc f x =
    "(a b c :" ^ typename f ^ ") : Lemma (" ^
    eq_result f ^ " (" ^ x ^ " a (" ^ x ^ " b c)) (" ^ x ^ " (" ^ x ^ " a b) c))\n\n" in

  let neutral f x n =
    "(a:" ^ typename f ^ ") : Lemma ((" ^
    eq_result f ^ " (" ^ x ^ " a " ^ n ^ ") a) /\\ (" ^
    eq_result f ^ "(" ^ x ^ " " ^ n ^ " a) a))\n\n" in

  let cancel f x n =
    "(a:" ^ typename f ^ ") : Lemma (" ^
    eq_result f ^ " (" ^ x ^ " a a) " ^ n ^ ")\n\n" in

  let inverse f x n inv =
    "(a:" ^ typename f ^ ") : Lemma (" ^
    eq_result f ^ " (" ^ x ^ " a (" ^ inv ^ " a)) " ^ n ^ " /\\ " ^
    eq_result f ^ " (" ^ x ^ " (" ^ inv ^ " a) a) " ^ n ^ ")\n\n" in

  let is_fstar_eq t x =
    "(a b:" ^ t ^ ") : Lemma ((" ^ x ^ " a b) <==> (a == b))\n\n" in
(*
  let refl t x =
    "(a:" ^ t ^ ") : Lemma (" ^ x ^ " a a)\n\n" in

  let sym t x =
    "(a b:" ^ t ^ ") : Lemma (requires (" ^ x ^ " a b)) (ensures (" ^ x ^ " b a))\n\n" in

  let trans t x =
    "(a b c:" ^ t ^ ") : Lemma (requires ((" ^ x ^ " a b) /\\ (" ^ x ^ " b c)))\n" ^
    "  (ensures (" ^ x ^ " a c))\n\n" in
*)
  let translate_eq_th f x s i =
    (match f.f_eq_theories with
    | NoEq -> s, i
    | Commut ->
        s ^ debug_comment "Commut" ^
        header i ^
        commut f x
        , i + 1
    | Assoc ->
        s ^ debug_comment "Assoc" ^
        header i ^
        assoc f x
        , i + 1
    | AssocCommut ->
        s ^ debug_comment "AssocCommut" ^
        header i ^
        commut f x ^
        header (i + 1) ^
        assoc f x
        , i + 2
    | AssocN (_, n) ->
        (match n.f_impl with
          | Const const ->
              s ^ debug_comment "AssocN" ^
              header i ^
              assoc f x ^
              header (i + 1) ^
              neutral f x const
              , i + 2
          | SepFun | Func _ | FuncEqual | FuncDiff ->
              Parsing_helper.internal_error "Expected a constant as second parameter to assocU."
          | No_impl ->
              info ("  1 builtin equation will not be translated because there is no implementation for " ^ n.f_name);
              s, i)
    | AssocCommutN (_, n) ->
        (match n.f_impl with
          | Const const ->
              s ^ debug_comment "AssocCommutN" ^
              header i ^
              commut f x ^
              header (i + 1) ^
              assoc f x ^
              header (i + 2) ^
              neutral f x const
              , i + 3
          | SepFun | Func _ | FuncEqual | FuncDiff ->
              Parsing_helper.internal_error "Expected a constant as second parameter to ACU."
          | No_impl ->
              info ("  1 builtin equation will not be translated because there is no implementation for " ^ n.f_name);
              s, i)
    | Group (_, inv, n) ->
        (match n.f_impl with
          | Const const ->
              (match get_impl inv with
                | Func invx ->
                    s ^ debug_comment "Group" ^
                    header i ^
                    assoc f x ^
                    header (i + 1) ^
                    neutral f x const ^
                    header (i + 2) ^
                    inverse f x const invx
                    , i + 3
                | Const _ | SepFun | FuncEqual | FuncDiff -> Parsing_helper.internal_error "Expected a non-constant function as second parameter to group."
                | No_impl ->
                    info ("  1 builtin equation will not be translated because there is no implementation for " ^ inv.f_name);
                    s, i)
          | SepFun | Func _ | FuncEqual | FuncDiff ->
              Parsing_helper.internal_error "Expected a constant as third parameter to group."
          | No_impl ->
              info ("  1 builtin equation will not be translated because there is no implementation for " ^ n.f_name);
              s, i)
    | CommutGroup (_, inv, n) ->
        (match n.f_impl with
          | Const const ->
              (match get_impl inv with
                | Func invx ->
                    s ^ debug_comment "CommutGroup" ^
                    header i ^
                    assoc f x ^
                    header (i + 1) ^
                    commut f x ^
                    header (i + 2) ^
                    neutral f x const ^
                    header (i + 3) ^
                    inverse f x const invx
                    , i + 4
                | Const _ | SepFun | FuncEqual | FuncDiff -> Parsing_helper.internal_error "Expected a non-constant function as second parameter to commut_group."
                | No_impl ->
                    info ("  1 builtin equation will not be translated because there is no implementation for " ^ inv.f_name);
                    s, i)
          | SepFun | Func _ | FuncEqual | FuncDiff ->
              Parsing_helper.internal_error "Expected a constant as third parameter to commut_group."
          | No_impl ->
              info ("  1 builtin equation will not be translated because there is no implementation for " ^ n.f_name);
              s, i)
    | ACUN (_, n) ->
        (match n.f_impl with
          | Const const ->
              s ^ debug_comment "ACUN" ^
              header i ^
              commut f x ^
              header (i + 1) ^
              assoc f x ^
              header (i + 2) ^
              neutral f x const ^
              header (i + 3) ^
              cancel f x const
              , i + 4
          | SepFun | Func _ | FuncEqual | FuncDiff ->
              Parsing_helper.internal_error "Expected a constant as second parameter to ACUN."
          | No_impl ->
              info ("  1 builtin equation will not be translated because there is no implementation for " ^ n.f_name);
              s, i)
    ) in

  let generated_eq = ref [] in
  let lemma_equal ty eq s i =
    if List.mem eq (!generated_eq) then (s,i) else
    begin
      generated_eq := eq :: (!generated_eq);
      s ^ debug_comment "Equallemma" ^
      header i ^
      is_fstar_eq ty eq, i + 1
      (* In the future, it might be good to allow some equality functions
	 to differ from the F* equality ==, while still being a congruence.
	 That's more complex to deal with for the user.
	 Which option we choose for each equality function might depend
	 on its name.
	 The following code generates lemmas for the equivalence relation;
	 the preservation by function application is still to be written.
      header i ^
      refl ty eq ^
      header (i+1) ^
      sym ty eq ^
      header (i+2) ^
      trans ty eq, i+3 *)
    end
  in
  
  List.fold_left
    (fun (s, i) f ->
      (* We ignore f_plus and f_mul explicitly because they cannot have an implementation
         and we do not want a useless warning about that. *)
      if f.f_eq_theories <> NoEq && f <> Settings.f_plus && f <> Settings.f_mul then (
        match f.f_impl with
        | Func x -> translate_eq_th f x s i
        | Const _ | SepFun -> Parsing_helper.internal_error "Expected a non-constant non-letfun function as subject of a builtin equation."
        | FuncEqual | FuncDiff ->
            let eq = equal (List.hd (fst f.f_type)) in
	    lemma_equal (typename f) eq s i
        | No_impl ->
            info ("  1 builtin equation will not be translated because there is no implementation for " ^ f.f_name);
            s, i)
      else s, i)
    ("", first_nr)
    (List.append built_in_functions !functions)

(* Tables module *)

let rec fold_enum l s f i =
  match l with
  | [] -> Parsing_helper.internal_error "Table without entries."
  | [t] -> f t i
  | t::r -> f t i ^ s ^ fold_enum r s f (i+1)

let gen_str_tables_impl () =
  if !tables = [] then
    "module " ^ !protocol_name_allcaps ^ ".Tables\n\n" ^
    "open CVTypes\n" ^
    "open " ^ !protocol_name_allcaps ^ ".Types\n" ^
    "open State\n\n" ^
    "type t = unit\n\n" ^
    "val init: t\n" ^
    "let init = ()\n\n" ^
    "val print: #stt:state_type{tt_of stt == t} -> state stt -> FStar.All.ML unit\n" ^
    "let print st = ()\n"
  else

    let str_table_entries =
      String.concat ""
	 (List.map (fun t ->
	   "type " ^ get_table_entry_name t ^ " = " ^
	   (String.concat " * " (List.map (fun ty -> get_type_name ty) t.tbltype)) ^
	   "\n"
		   ) (!tables))
    in

    let str_table_record = "noeq type t = {\n" ^
      (String.concat ""
	 (List.map (fun t ->
	   "  " ^ get_table_field t ^ ": table " ^ get_table_entry_name t ^ ";\n"
		   ) !tables)) ^
      "}\n"
    in

    let str_table_init = "let init = {\n" ^
      (String.concat ""
	 (List.map (fun t ->
	   "  " ^ get_table_field t ^ " = table_empty " ^ get_table_entry_name t ^ ";\n"
		   ) !tables)) ^
      "}\n"
    in

    let str_table_names =
      String.concat ""
	(List.map (fun t ->
	  "val " ^ get_table_name t ^ ": table_name t " ^ get_table_entry_name t ^ "\n\n" ^
	  "let " ^ get_table_name t ^ " =\n" ^
	  "  { get = (fun tabs -> tabs." ^ get_table_field t ^ ");\n" ^
	  "    set = (fun tabs -> fun tab -> { tabs with " ^ get_table_field t ^ " = tab }) }\n\n"
		  ) !tables)
    in

    let str_table_print_entry =
      String.concat ""
	(List.map (fun t ->
	  "val " ^ get_table_print_entry t ^ ": " ^ get_table_entry_name t ^ " -> FStar.All.ML unit\n\n" ^ 
	  "let " ^ get_table_print_entry t ^ " (" ^
	  (fold_enum t.tbltype ", " (fun t i -> "var" ^ string_of_int i) 0) ^
	  ") =\n" ^
	  "  IO.print_string \"{\"; IO.print_newline ();\n" ^
	  (fold_enum t.tbltype "" (fun t i ->
	    "  print_label_bytes \"" ^ get_type_name t ^
	    "\" (" ^ get_write_serial t ^ " var" ^ string_of_int i ^ ") true;\n") 0) ^
	  "  IO.print_string \"}\"; IO.print_newline ()\n\n"
		  ) !tables)
    in
    
    let str_table_print = "let print st =\n" ^
      (String.concat ";\n"
	 (List.map (fun t ->
	   "  print_table st " ^ get_table_name t ^ " \"" ^
	   t.tblname ^ "\" " ^ get_table_print_entry t
		   ) !tables))
    in

    "module " ^ !protocol_name_allcaps ^ ".Tables\n\n" ^
    "open CVTypes\n" ^
    "open " ^ !protocol_name_allcaps ^ ".Types\n" ^
    "open State\n\n" ^
    str_table_entries ^ "\n" ^
    str_table_record ^ "\n" ^
    "val init: t\n\n" ^
    str_table_init ^ "\n" ^
    str_table_names ^ "\n" ^
    (if !Settings.fstar_debug then
      str_table_print_entry ^ "\n" ^
      "\nval print: #stt:state_type{tt_of stt == t} -> state stt -> FStar.All.ML unit\n" ^
      str_table_print ^ "\n"
    else
      "")

(* Events module *)
		      
let gen_str_events_impl () =
  let str_events =
    if !events = [] then
      "type " ^ !protocol_name ^ "_event = unit\n"
    else
      "noeq type " ^ !protocol_name ^ "_event =\n" ^
      List.fold_left
      (fun s e ->
        match e.t_desc with
        | FunApp (ef, _) -> s ^ "  | Event_" ^ ef.f_name ^ ": " ^
            (List.fold_left
              (fun s t -> s ^ get_type_name t ^ " -> ")
              ""
             (* ignore the first element from e and s: it is the bitstring corresponding to the replication indices. *)
             (List.tl (fst ef.f_type))) ^
            !protocol_name ^ "_event\n"
        | _ -> Parsing_helper.internal_error "Expected FunApp for event (do_implementation)"
      )
      ""
      !events in

  let str_events_print = "val " ^ !protocol_name ^ "_print_event: " ^ !protocol_name ^ "_event -> FStar.All.ML unit" in

  let str_events_print_let = "let " ^ !protocol_name ^ "_print_event e =\n" ^
    if !events = [] then
      "  ()\n"
    else
      "  match e with\n" ^
      List.fold_left
      (fun s e ->
        match e.t_desc with
        | FunApp (ef, _) ->
	    let tyl = List.tl (fst ef.f_type) in
	    if tyl = [] then
	      s ^ "  | Event_" ^ ef.f_name ^ " ->\n" ^
	      "    IO.print_string \"" ^ ef.f_name ^ "\"; IO.print_newline ()\n"
	    else
	      s ^ "  | Event_" ^ ef.f_name ^ " " ^
              fold_enum tyl " " (fun e i -> "var" ^ string_of_int i) 0 ^ " ->\n" ^
              "    IO.print_string \"" ^ ef.f_name ^ ": { \";\n" ^
              fold_enum tyl "" (fun e i -> "    print_label_bytes " ^ " \"" ^ get_type_name e ^ "\" (" ^ get_write_serial e ^ " var" ^ string_of_int i ^ ") true;\n") 0 ^
              "    IO.print_string \"}\"; IO.print_newline ()\n"
        | _ -> Parsing_helper.internal_error "Expected FunApp for event (do_implementation)"
      )
      ""
      !events in

  "module " ^ !protocol_name_allcaps ^ ".Events\n\n" ^
  "open CVTypes\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Types\n\n" ^
  str_events ^ "\n" ^
  (if !Settings.fstar_debug then
    str_events_print ^ "\n" ^
    str_events_print_let
  else
    "")

(* Sessions module *)
    
let foreach_oracle oracles_for_modules f init =
  List.fold_left
    (fun s (_, _, oracles) -> StringMap.fold f oracles s)
    init
    oracles_for_modules

let foreach_following_oracle oracles_for_modules f init =
  foreach_oracle oracles_for_modules
    (fun _ od s ->
      List.fold_left
        (fun s (nr, ol, _) ->
          List.fold_left
            (fun s (name, bl) -> f s nr name bl)
            s
            ol)
        s
        od.list_of_returns)
  init

let has_following_oracles oracles_for_modules =
  List.exists (fun (_, _, oracles) ->
    StringMap.exists (fun _ od ->
      List.exists (fun (nr, ol, _) -> ol <> []) od.list_of_returns
		     ) oracles
	      ) oracles_for_modules


let gen_str_sessions_interface oracles_for_modules =
  (* Generate types for session states *)
  let str_oracle_name =
    foreach_oracle oracles_for_modules (fun name od s -> s ^ "  | " ^ get_oracle_name name ^ "\n")
      ("type " ^ (!protocol_name) ^ "_oracle_name: eqtype =\n") in

  let following_oracles = has_following_oracles oracles_for_modules in

  let str_session_entry =
    if following_oracles then
      foreach_following_oracle oracles_for_modules
	(fun s nr name bl ->
          s ^ "  | R" ^ string_of_int nr ^ "_" ^ alphabetize_string name ^ ": " ^
          (List.fold_left
             (fun s b ->
               s ^ get_binder_name b ^ ":" ^ (add_serde_type b.btype; get_type_name b.btype) ^ " -> ")
             ""
             bl) ^
          (!protocol_name) ^ "_session_entry\n")
	("noeq type " ^ (!protocol_name) ^ "_session_entry =\n")
    else
      "noeq type " ^ (!protocol_name) ^ "_session_entry =\n" ^
      "  | R0_dummy: " ^ (!protocol_name) ^ "_session_entry\n"
  in

  let str_session_entry_to_name =
    if following_oracles then
      foreach_following_oracle oracles_for_modules
	(fun s nr name bl ->
          s ^ "  | R" ^ string_of_int nr ^ "_" ^ alphabetize_string name ^ " " ^
          (List.fold_left
             (fun s b ->
               s ^ "_ ")
             ""
             bl) ^
          "-> " ^ get_oracle_name name ^ "\n")
	("let " ^ (!protocol_name) ^ "_session_entry_to_name se =\n" ^
	 "  match se with\n")
    else
      (* Always return the name of the first oracle, as a dummy
	 function *)
      let (_,_,oracles) = List.hd oracles_for_modules in
      let (oname,_) = StringMap.find_first (fun _ -> true) oracles in
      "let " ^ (!protocol_name) ^ "_session_entry_to_name se =\n" ^
      "  " ^ (get_oracle_name oname) ^"\n"
  in

  let str_following_oracles =
    foreach_oracle oracles_for_modules
      (fun name od s ->
        s ^ "  | " ^ get_oracle_name name ^ " -> [" ^
        (match od.following_oracles with
        | None | Some [] -> ""
        | Some fol ->
            (List.fold_left
              (fun s name -> (match s with | "" -> "" | _ -> s ^ "; ") ^ get_oracle_name name)
              ""
              fol)) ^
        "]\n")
      ("let " ^ (!protocol_name) ^ "_following_oracles on =\n" ^
       "  match on with\n") in

  let str_session_rest =
    (if !Settings.fstar_debug then 
      "val " ^ !protocol_name ^ "_print_session_entry: " ^ !protocol_name ^ "_session_entry -> FStar.All.ML unit\n\n"
    else "") ^
    "let " ^ !protocol_name ^ "_session_type = SessionType " ^
    !protocol_name ^ "_oracle_name " ^ !protocol_name ^ "_session_entry " ^
    !protocol_name ^ "_following_oracles " ^ !protocol_name ^ "_session_entry_to_name\n\n"
  in

  "module " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Sessions\n\n" ^
  "open CVTypes\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Types\n" ^
  "open State\n\n" ^
  str_oracle_name ^ "\n\n" ^
  str_session_entry ^ "\n\n" ^
  str_session_entry_to_name ^ "\n\n" ^
  str_following_oracles ^ "\n\n" ^
  str_session_rest


let gen_str_sessions_impl oracles_for_modules =
  let following_oracles = has_following_oracles oracles_for_modules in
  let str_session_print_entry =
    if !Settings.fstar_debug then
      if following_oracles then
	foreach_following_oracle oracles_for_modules
	  (fun s nr name bl ->
            s ^ "  | R" ^ string_of_int nr ^ "_" ^ alphabetize_string name ^ " " ^
            (List.fold_left
               (fun s b ->
		 s ^ get_binder_name b ^ " ")
               ""
               bl) ^
            " ->\n" ^
            "    IO.print_string \"R" ^ string_of_int nr ^ "_" ^ name ^ ": {\\n\";\n" ^
            (List.fold_left
               (fun s b ->
		 s ^ "    print_label_bytes \"" ^ get_binder_name b ^ "\" (" ^ get_write_serial b.btype ^ " " ^ get_binder_name b ^ ") true;\n")
               ""
               bl) ^
            "    IO.print_string \"}\\n\"\n"
	      )
	  ("let " ^ (!protocol_name) ^ "_print_session_entry se =\n" ^
	   "  match se with\n")
      else
	"let " ^ (!protocol_name) ^ "_print_session_entry se = ()\n"
    else
      ""
  in

  "module " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Sessions\n\n" ^
  "open CVTypes\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Types\n" ^
  "open State\n\n" ^
  str_session_print_entry

(* Protocol module *)
    
let gen_str_protocol () =
  "module " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Protocol\n\n" ^
  "open CVTypes\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Types\n" ^
  "open " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Sessions\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Events\n" ^
  "open State\n\n" ^
  (* state_type *)
  "let " ^ !protocol_name ^ "_state_type = StateType " ^
  !protocol_name_allcaps ^ ".Tables.t " ^
  !protocol_name_allcaps ^ ".Variables.t " ^
  !protocol_name ^ "_session_type " ^
  !protocol_name ^ "_event " ^
  (if !Settings.fstar_debug then "true" else "false") ^ "\n\n" ^
  (* state *)
  "type " ^ state_type () ^ " = state " ^
  !protocol_name ^ "_state_type\n\n" ^
  (* is_init_state *)
  "let is_init_state (s0: " ^ state_type() ^ ") = 
  sessions_of s0 == init_sessions (st_of " ^ !protocol_name ^ "_state_type) /\\
  events_of s0 == [] /\\
  tables_of s0 == " ^ !protocol_name_allcaps ^ ".Tables.init /\\
  variables_of s0 == " ^ !protocol_name_allcaps ^ ".Variables.init /\\
  local_of s0 == Set.empty #SidMap.sidt\n"

(* Invariant module *)

let gen_str_invariant () =
  "module " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Invariant\n\n" ^
  "open CVTypes\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Types\n" ^
  "open " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Sessions\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Events\n" ^
  "open " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Protocol\n" ^
  "open State\n\n" ^

  "val inv : " ^ state_type () ^ " -> prop\n" ^
  "(* PLEASE DEFINE THE INVARIANT OF YOUR CHOICE *)\n" ^ 
  "let inv s = True\n\n" ^
  "val rel : " ^ state_type () ^ " -> " ^ state_type () ^ " -> prop\n" ^
  "(* PLEASE DEFINE THE RELATION OF YOUR CHOICE *)\n" ^ 
  "let rel s0 s1 = True\n\n"

(* InvariantLemmas module *)

let gen_str_invariant_lemmas_intf () =
  "module " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas\n\n" ^
  "open CVTypes\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Types\n" ^
  "open " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Sessions\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Events\n" ^
  "open " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Protocol\n" ^
  "open State\n\n" ^
 
"let inv' (s: " ^ state_type () ^ ") = 
  (" ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Invariant.inv (remove_local s)) /\\
  (forall (x: SidMap.sidt). is_local x s ==> is_session x s)
   
let rel' (s0 s1: " ^ state_type () ^ ") = 
  " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Invariant.rel s0 s1 /\\ FStar.Set.subset (local_of s0) (local_of s1) /\\ inv' s1

val initial (s: " ^ state_type () ^ "):
  Lemma (requires (is_init_state s)) (ensures (" ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Invariant.inv s))

let initial' (s: " ^ state_type () ^ "):
  Lemma (requires (is_init_state s)) (ensures (inv' s)) 
  [SMTPat (is_init_state s)] =
  initial s

val reflexivity (s: " ^ state_type () ^ "):
  Lemma (requires (inv' s)) (ensures (" ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Invariant.rel s s))

let reflexivity' (s: " ^ state_type () ^ "):
  Lemma (requires (inv' s)) (ensures (rel' s s)) =
  reflexivity s

val transitivity (s0 s1 s2: " ^ state_type () ^ "):
  Lemma (requires (inv' s0 /\\ " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Invariant.rel s0 s1 /\\ " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Invariant.rel s1 s2))
    (ensures (" ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Invariant.rel s0 s2))

let transitivity' (s0 s1 s2: " ^ state_type () ^ "):
  Lemma (requires (inv' s0 /\\ rel' s0 s1 /\\ rel' s1 s2))
    (ensures (rel' s0 s2)) =
  transitivity s0 s1 s2

let inverse (s: " ^ state_type () ^ "):
  Lemma (requires (inv' s)) (ensures (inv' (inverse_local s))) = ()
  (* Uses [State.lemma_inverse] via [SMTPat] *)\n"

let gen_str_invariant_lemmas_impl() =
   "module " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas\n\n" ^
  "open CVTypes\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Types\n" ^
  "open " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Sessions\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Events\n" ^
  "open " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Protocol\n" ^
  "open State\n\n" ^

"(* PLEASE ADD PROOFS FOR THE LEMMAS *)
let initial s = ()

let reflexivity s = ()

let transitivity s0 s1 s2 = ()\n"

(* Types interface *)
    
let gen_str_types () =
  let str_types = List.fold_left
    (fun s t ->
      s ^
       match t.timplsize with
       | None -> "val " ^ get_type_name t ^ ": Type0\n"
       | Some l -> "" (* not generating anything for fixed types *))
    ""
    !types
  in

  let str_eq_types = List.fold_left
    (fun s t ->
      s ^
      (match t.timplsize with
      | None -> "val " ^ equal t ^ ": " ^ get_type_name t ^ " -> " ^ get_type_name t ^ " -> bool\n\n"
      | Some _ -> "")
    )
    ""
    !eq_types
  in

  let str_serde_types = List.fold_left
    (fun s t ->
      s ^
      (match t.timplsize with
      | None -> "val " ^ get_write_serial t ^ ": serialize_type " ^ get_type_name t ^ "\n" ^
                "val " ^ get_read_serial t ^ ": deserialize_type " ^get_type_name t ^ "\n\n"
      | Some _ -> "")
    )
    ""
    !serde_types
  in

  let str_tuple_types = String.concat "" (List.mapi
      (fun n tl -> 
        "let " ^ (get_tuple_tag_name tl)^" : tagt = tag_from_nat "^(string_of_int (n+start_tag))^"\n\n"^
	"let " ^ get_tuple_dec_fun_name tl ^ " (buf: bytes) =\n" ^
	"  match Tuples.check_tag "^(get_tuple_tag_name tl)^" buf with\n" ^
        "  | None -> None\n" ^
	"  | Some pos ->\n" ^
	let return_vars = create_fresh_names "cvar_" (List.length tl) in
	(String.concat "" (List.map2 (fun rv t -> 
	  "  match " ^(get_read_serial t)^ " buf pos with\n" ^
	  "  | DSTooShort _ | DSFailure -> None\n" ^
	  "  | DSOk(" ^rv^ ", pos) ->\n"
			     ) return_vars tl)) ^
	"     if pos = Seq.length buf then\n" ^
	"       Some(" ^ (String.concat "," return_vars) ^ ")\n" ^
	"     else\n"  ^
	"       None\n\n"
	)
      !tuple_types)
  in

  let str_random_types = List.fold_left
    (fun s t ->
      s ^
      (match t.timplsize with
      | None -> "val " ^ get_random t ^ ": entropy -> entropy * " ^ get_type_name t ^ "\n\n"
      | Some _ -> "")
    )
    ""
    !random_types
  in

  "module " ^ !protocol_name_allcaps ^ ".Types\n\n" ^
  "open CVTypes\n" ^
  "open Random\n\n" ^
  str_types ^ "\n\n" ^
  str_eq_types ^ "\n\n" ^
  str_serde_types ^ "\n\n" ^
  str_tuple_types ^ "\n\n" ^
  str_random_types

(* Functions interface *)
    
let gen_str_functions () =
  let str_functions =
    List.fold_left
      (fun s f ->
        s ^
	match f.f_cat with
	| Proj _ -> "" (* Projections are defined just after their bijective inverse *)
	| _ -> 
        match f.f_impl with
        | Func x ->
            "val " ^ x ^ ": " ^
            (* parameter types *)
            (List.fold_left
              (fun s t -> s ^ get_type_name t ^ " -> ")
              ""
              (fst f.f_type)) ^
            (* additional types when needing entropy *)
            (if f.f_impl_ent then "entropy -> entropy * " else "") ^
            (* return type *)
            get_type_name (snd f.f_type) ^ "\n\n" ^

            (* inverse *)
            (if List.memq f !inv_functions then
              (match f.f_impl_inv with
              | None ->
                  (* if we have this error message here, we would no longer need it in match_pattern_complex(_ns) *)
                  error ("Inverse function of " ^ f.f_name ^ " not registered. Add a line 'implementation fun " ^ f.f_name ^ "=\"f1\" [inverse = \"f2\"].' in the source.")
              | Some inv ->
		  let input_type =
		    String.concat " * " (List.map
					   (fun t -> get_type_name t)
					   (fst f.f_type))
		  in
                  "val " ^ inv ^ ": " ^
                  (* parameter is the return type *)
                  get_type_name (snd f.f_type) ^ " -> " ^
                  (* return type is the input input when f is bijective,
		     option of the input type when f is only injective *)
		  (if f.f_options land Settings.fopt_BIJECTIVE != 0 then
		    input_type
		  else
                    "option (" ^ input_type ^ ")")^ "\n\n" ^
		  if f.f_options land Settings.fopt_BIJECTIVE != 0 then
		    (* Define all the projection functions *)
		    String.concat "" (List.mapi (fun i t ->
		      "let " ^ get_proj_name f (i+1)^" (x: " ^
		      get_type_name (snd f.f_type) ^ ") =\n  let ("^
		      (String.concat ", " (List.mapi (fun j _ ->
			if j = i then "y" else "_") (fst f.f_type)))^
		      ") = "^inv^" x in y\n\n"
						       ) (fst f.f_type))
		    
		  else "")
             else "")

        | Const x ->
            "val " ^ x ^ ": " ^
            get_type_name (snd f.f_type) ^ "\n\n"
        | SepFun | FuncEqual | FuncDiff -> ""
        | No_impl -> error ("Function or constant not registered:" ^ f.f_name))
      ""
      !functions
  in

  "module " ^ !protocol_name_allcaps ^ ".Functions\n\n" ^
  "open CVTypes\n" ^
  "open Random\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Types\n\n" ^
  str_functions

(* Effect module *)
    
let gen_str_effect_interface() =
"module " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Effect

open State

type stt = " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Protocol." ^ !protocol_name ^ "_state_type

type pre_t = state stt -> Type0
type post_t (a:Type) = state stt -> a -> state stt -> Type0

val repr (a:Type) (pre:pre_t) (post:post_t a) : Type0

val return (a:Type) (x:a) : repr a (fun _ -> True) (fun s0 r s1 -> r == x /\\ s0 == s1)

/// bind bakes in the weakening of f's post to compose it with g's pre

val bind (a:Type) (b:Type)
  (pre_f:pre_t) (post_f:post_t a) (pre_g:a -> pre_t) (post_g:a -> post_t b)
  (f:repr a pre_f post_f) (g:(x:a -> repr b (pre_g x) (post_g x)))
: repr b
  (fun s0 -> pre_f s0 /\\ (forall (x:a) (s1:state stt). post_f s0 x s1 ==> pre_g x s1))
  (fun s0 y s2 -> exists (x:a) (s1:state stt). pre_f s0 /\\ post_f s0 x s1 /\\ post_g x s1 y s2)

/// sub comp rule

val subcomp (a:Type)
  (pre_f:pre_t) (post_f:post_t a)
  (pre_g:pre_t) (post_g:post_t a)
  (f:repr a pre_f post_f)
: Pure (repr a pre_g post_g)
  (requires
    (forall (s:state stt). pre_g s ==> pre_f s) /\\
    (forall (s0 s1:state stt) (x:a). (pre_g s0 /\\ post_f s0 x s1) ==> post_g s0 x s1))
  (ensures fun _ -> True)

let if_then_else (a:Type)
  (pre_f:pre_t) (post_f:post_t a)
  (pre_g:pre_t) (post_g:post_t a)
  (f:repr a pre_f post_f)
  (g:repr a pre_g post_g)
  (p:bool)
  : Type
= repr a
  (fun s -> (p ==> pre_f s) /\\ ((~ p) ==> pre_g s))
  (fun s0 r s1 -> (p ==> post_f s0 r s1) /\\ ((~ p) ==> post_g s0 r s1))

reifiable
reflectable
effect {
  CVSTPre (a:Type) (pre:pre_t) (post:post_t a)
  with {repr; return; bind; subcomp; if_then_else}
}\n\n" ^
(if !Settings.fstar_concurrent then 
"effect CVST (a:Type) (pre: pre_t) (post:post_t a) =
  CVSTPre a (fun s0 -> " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas.inv' s0 /\\ pre s0) (fun s0 r s1 ->
      exists si. " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas.rel' (inverse_local s0) (inverse_local si) /\\
        local_of s0 == local_of si /\\
	post si r s1 /\\
        " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas.rel' si s1)\n\n"
else
"effect CVST (a:Type) (pre: pre_t) (post:post_t a) =
  CVSTPre a (fun s0 -> " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas.inv' s0 /\\ pre s0) (fun s0 r s1 ->
	post s0 r s1 /\\
        " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas.rel' s0 s1)\n\n") ^
"let true_pre (s: state stt) = True

effect CVSTId (a: Type) = CVSTPre a true_pre (fun s0 _ s1 -> s0 == s1)

/// We now define a lift from PURE. This is needed to use total computations/function calls
/// inside of CVST computations

unfold
let pure_p (#a:Type) (wp: pure_wp a) : pre_t = fun _ -> as_requires wp

unfold
let pure_q (#a:Type) (wp: pure_wp a) : post_t a =
  fun s0 x s1 -> s0 == s1 /\\ as_ensures wp x

val lift_pure (a:Type) (wp:pure_wp a) (f:eqtype_as_type unit -> PURE a wp) : repr a (pure_p wp) (pure_q wp)

sub_effect PURE ~> CVSTPre = lift_pure

/// A helper function, returning the current state. The returned state is erased,
/// thus only usable inside proofs.
val get_state () : CVST (Ghost.erased (state stt)) true_pre (fun s0 r s1 -> s0 == s1 /\\ Ghost.reveal r == s1)

/// We do not want to lift the entire ALL effect, as we do not have an ML heap in CVST. We instead
/// provide an explicit coercion from ML to CVST, which will allow the use of printing functions
val ml_to_CVST (#a: Type) (f: eqtype_as_type unit -> FStar.All.ML a) : CVSTId a

/// Lifting print functions to CVST
val print_string: string -> CVSTId unit
val print_bytes: CVTypes.bytes -> CVSTId unit
val print_sid: SidMap.sidt -> CVSTId unit
val print_bool: bool -> CVSTId unit

val print_label_bytes: string -> CVTypes.bytes -> bool -> CVSTId unit
val print_label_sid: string -> SidMap.sidt -> bool -> CVSTId unit
val print_label_bool: string -> bool -> bool -> CVSTId unit\n\n" ^
(if !Settings.fstar_debug then 
"// DEBUG ONLY: These functions may print secrets!
// TODO comment out in final version
val " ^ !protocol_name ^ "_print_session: SidMap.sidt -> CVSTId unit
val " ^ !protocol_name ^ "_print_tables: unit -> CVSTId unit
val " ^ !protocol_name ^ "_print_events: unit -> CVSTId unit\n\n"
else "") ^
"effect CVSTInit (a:Type) =
  CVSTPre a " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Protocol.is_init_state (fun _ _ _ -> True)

val init_and_run (#a:Type) (f: unit -> CVSTInit a) : FStar.All.ML a
"

let gen_str_effect_impl() =
"module " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Effect

open State

let repr (a:Type) (pre:pre_t) (post:post_t a) : Type0 =
  s: state stt { pre s } -> FStar.All.ML (r: (state stt * a) { post s r._2 r._1 })

let return (a:Type) (x:a)
: repr a (fun _ -> True) (fun s0 r s1 -> r == x /\\ s0 == s1)
= fun s -> (s,x)

/// bind bakes in the weakening of f's post to compose it with g's pre

let bind (a:Type) (b:Type)
  (pre_f:pre_t) (post_f:post_t a) (pre_g:a -> pre_t) (post_g:a -> post_t b)
  (f:repr a pre_f post_f) (g:(x:a -> repr b (pre_g x) (post_g x)))
: repr b
  (fun s0 -> pre_f s0 /\\ (forall (x:a) (s1:state stt). post_f s0 x s1 ==> pre_g x s1))
  (fun s0 y s2 -> exists (x:a) (s1:state stt). pre_f s0 /\\ post_f s0 x s1 /\\ post_g x s1 y s2)
= fun s0 ->
  let (s1,x) = f s0 in
  g x s1

/// sub comp rule

let subcomp (a:Type)
  (pre_f:pre_t) (post_f:post_t a)
  (pre_g:pre_t) (post_g:post_t a)
  (f:repr a pre_f post_f)
: Pure (repr a pre_g post_g)
  (requires
    (forall (s:state stt). pre_g s ==> pre_f s) /\\
    (forall (s0 s1:state stt) (x:a). (pre_g s0 /\\ post_f s0 x s1) ==> post_g s0 x s1))
  (ensures fun _ -> True)
= fun (x:state stt {pre_g x}) -> f x

let repr_inv (a:Type) (pre:pre_t) (post:post_t a) : Type0 =
  repr a (fun s0 -> " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas.inv' s0 /\\ pre s0) (fun s0 r s1 ->
	post s0 r s1 /\\
	" ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas.rel' s0 s1)

val cvst_reflect (#a: Type) (#pre: pre_t) (#post:post_t a) (f: repr_inv a pre post): CVST a pre post\n\n" ^
(if !Settings.fstar_concurrent then
"let cvst_reflect f = 
   CVST?.reflect (fun s -> 
     " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas.inverse s;
     " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas.reflexivity' (inverse_local s);
     f s)\n\n"
else
"let cvst_reflect f = CVST?.reflect f\n\n") ^
"/// We now define a lift from PURE. This is needed to use total computations/function calls
/// inside of CVST computations

let lift_pure a wp f = fun s ->
  FStar.Monotonic.Pure.elim_pure_wp_monotonicity wp;
  let x = f () in
  s, x

let get_repr : repr_inv (Ghost.erased (state stt)) true_pre
  (fun s0 r s1 -> s0 == s1 /\\ Ghost.reveal r == s1) =
  fun s -> 
    " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas.reflexivity' s;
    s, Ghost.hide s

let get_state () = cvst_reflect get_repr

val ml_to_CVST_repr (#a: Type) (f: eqtype_as_type unit -> FStar.All.ML a) : repr a true_pre (fun s0 _ s1 -> s0 == s1)

let ml_to_CVST_repr #a f = fun s ->
  s, f ()

let ml_to_CVST #a f = CVST?.reflect (ml_to_CVST_repr f)

let print_string s = ml_to_CVST (fun _ -> FStar.IO.print_string s)
let print_bytes b = ml_to_CVST (fun _ -> CVTypes.print_bytes b)
let print_sid n = ml_to_CVST (fun _ -> SidMap.print_sid n)
let print_bool b = ml_to_CVST (fun _ -> CVTypes.print_bool b)

let print_label_bytes l b separator = ml_to_CVST (fun _ -> CVTypes.print_label_bytes l b separator)
let print_label_sid l n separator = ml_to_CVST (fun _ -> SidMap.print_label_sid l n separator)
let print_label_bool l b separator = ml_to_CVST (fun _ -> CVTypes.print_label_bool l b separator)\n\n" ^
(if !Settings.fstar_debug then
"// Functions that print the internal state

val print_to_CVST_repr (f: state stt -> FStar.All.ML unit) : repr unit true_pre (fun s0 _ s1 -> s0 == s1)

let print_to_CVST_repr (f: state stt -> FStar.All.ML unit) = fun s1 -> 
  f s1;
  (s1, ())

val print_to_CVST (f: state stt -> FStar.All.ML unit) : CVSTId unit

let print_to_CVST (f: state stt -> FStar.All.ML unit) = CVST?.reflect (print_to_CVST_repr f)

// print_session
let print_session_internal (sid: SidMap.sidt) (st: state stt) : FStar.All.ML unit =
  print_session #stt st " ^ !protocol_name_allcaps ^ "." ^ (!current_side) ^ "Sessions." ^ !protocol_name ^ "_print_session_entry sid
  
let " ^ !protocol_name ^ "_print_session (sid:SidMap.sidt) = print_to_CVST (print_session_internal sid)

// print_tables
let " ^ !protocol_name ^ "_print_tables() = print_to_CVST " ^ !protocol_name_allcaps ^ ".Tables.print

// print_events
let print_events_internal (st: state stt) : FStar.All.ML unit =
  print_events #stt st " ^ !protocol_name_allcaps ^ ".Events." ^ !protocol_name ^ "_print_event

let " ^ !protocol_name ^ "_print_events () = print_to_CVST print_events_internal\n\n"
else "") ^
"// init_and_run
val init_and_run_repr (#a:Type) (f: repr a " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Protocol.is_init_state (fun _ _ _ -> True)) : FStar.All.ML a

let init_and_run_repr f =
  let ent = Lib.RandomSequence.entropy0 in
  let s = init_state stt ent " ^ !protocol_name_allcaps ^ ".Tables.init " ^ !protocol_name_allcaps ^ ".Variables.init in
  let s', r = f s in
  let _ = finalize_state s' in
  r

let init_and_run f = init_and_run_repr (reify (f ()))
"

(* Equations module *)
    
let gen_str_equations translatable_equations =

  let is_trivial_equation t =
    match t.t_desc with
    | FunApp (f, _) -> f = Settings.c_true
    | _ -> false
  in

  let (str_equations, last_nr) = List.fold_left
    (fun (s, i) (_, bl, t, t_if, _, _) ->
      (let binders = (List.fold_left
          (fun s b ->
            s ^ "(" ^ get_binder_name b ^ ":" ^ get_type_name b.btype ^ ") ")
          ""
          bl) in
      let equation = (* side condition *)
        (if is_trivial_equation t_if then "" else
          ("  (" ^ (translate_term_ns t_if "  ") ^ "\n" ^
          "  ) =>\n")) ^
        (* main equation *)
        ("  " ^ translate_term_ns t "  ") in

      s ^

      (* Not using this style at the moment because it is hard
         to control the SMTPat for general equations:
         We want to avoid using `not` in an SMTPat, for example.
      "val lemma_" ^ string_of_int i ^ " " ^
      (if bl = [] then "(_:unit) " else binders) ^
      " : Lemma (\n" ^
      equation ^ ")\n" ^
      " [SMTPat (" ^ equation ^ ")]\n\n" *)

      "val lemma_" ^ string_of_int i ^ " " ^ 
      (* Binder List *)
      (if bl = [] then "(_:unit) " else binders) ^ " : " ^
      "Lemma (\n" ^
      equation ^ ")\n\n"
      , i + 1)
    )
    ("", 0)
    translatable_equations
  in

  let (str_inv_equations, last_nr) = List.fold_left
    (fun (s, i) f ->
      (match f.f_impl with
      | Func x when f.f_name <> "" ->
          (match f.f_impl_inv with
          | Some inv ->
              (* inv (f a) = Some (a) *)
              let inv_f =
                let binders = create_local_names "x" 0 (List.length (fst f.f_type)) in
                let binders_pat = create_local_names "y" 0 (List.length (fst f.f_type)) in
                let str_parameters = (List.fold_left2
                  (fun s b t ->
                    s ^ "(" ^ b ^ ":" ^ get_type_name t ^ ") ")
                  ""
                  binders
                  (fst f.f_type)) in
                let function_call = inv ^ " (" ^ x ^ " " ^
                  (if binders = [] then "()" else
                  (String.concat " " binders)) ^ ")" in
		let equality =
		  String.concat " /\\\n      " (List.mapi
                    (fun i t ->
                      (equal t) ^ " " ^ (List.nth binders i) ^ " " ^ (List.nth binders_pat i))
                    (fst f.f_type)
					       )
		in
                let equation =
                  (* main equation *)
		  if f.f_options land Settings.fopt_BIJECTIVE != 0 then
		    if binders = [] then "" else
		    "  let (" ^ (String.concat ", " binders_pat) ^ ") = " ^
		    function_call ^ " in\n  " ^ equality
		  else
                    "  match " ^ function_call ^ " with\n" ^
                    "  | Some (" ^
                    (String.concat ", " binders_pat) ^ ") ->\n" ^
                    "      " ^
                    (if binders = [] then "True" else equality) ^ "\n" ^
                    "  | None -> False"
                in
                "val lemma_" ^ string_of_int i ^ " " ^
                (if binders = [] then "(_:unit) " else str_parameters) ^
                ": Lemma (" ^
                equation ^ ")\n" ^
                "[SMTPat (" ^
                function_call ^
                ")]\n\n"
              in
              (* f (inv a) = a *)
              let f_inv =
                let returns_unit = ((snd f.f_type) = Settings.t_unit) in
                let binders_pat = create_local_names "y" 0 (List.length (fst f.f_type)) in
                let str_parameters = "(x:" ^ get_type_name (snd f.f_type) ^ ")" in
                let function_call = inv ^ (if returns_unit then " () " else " x ") in
		let equality =
		  (if returns_unit then "True" else
                  (equal (snd f.f_type)) ^ " (" ^ x ^ " " ^ (if binders_pat = [] then "()" else String.concat " " binders_pat) ^ ") x") ^ "\n"
		in
                let equation =
		  if f.f_options land Settings.fopt_BIJECTIVE != 0 then
		    "let (" ^ (String.concat ", " binders_pat) ^ ") = " ^ function_call ^ " in\n" ^
		    equality
		  else
                    "  match " ^ function_call ^ "  with\n" ^
                    "  | Some (" ^ (String.concat ", " binders_pat) ^ ") ->\n" ^
                    "      " ^ equality ^
                    "  | None -> " ^
                    (if binders_pat = [] then "True" else
                    "~ (exists " ^
                    (List.fold_left2
                       (fun s b t -> s ^ "(" ^ b ^ ":" ^ get_type_name t ^ ") ")
                       ""
                       binders_pat
                       (fst f.f_type)
                    ) ^
                    ". " ^
                    (equal (snd f.f_type)) ^ " (" ^ x ^ " " ^ (if binders_pat = [] then "()" else String.concat " " binders_pat) ^ ") x)") in

                "val lemma_" ^ string_of_int (i + 1) ^ " " ^
                (if returns_unit then "(_:unit) " else str_parameters) ^
                ": Lemma (" ^
                equation ^
                ")\n" ^
                "[SMTPat (" ^
                function_call ^
                ")]\n\n"
              in
              s ^ inv_f ^ f_inv
              , i + 2
          | None ->
              (* if we have this error message here, we would no longer need it in match_pattern_complex(_ns) *)
              error ("Inverse function of " ^ f.f_name ^ " not registered. Add a line 'implementation fun " ^ f.f_name ^ "=\"f1\" [inverse = \"f2\"].' in the source.")
          )
      | Func x (*when f.f_name = ""*) -> s, i (* We currently do not translate the tuple/detuple functions *)
      | Const _ | SepFun | FuncEqual | FuncDiff -> Parsing_helper.internal_error "Only non-constant non-letfun functions expected as functions that have an inverse."
      | No_impl -> error ("Function or constant not registered:" ^ f.f_name)
    ))
    ("", last_nr)
    !inv_functions in

  let (str_serde_equations, last_nr) =  List.fold_left
    (fun (s, i) t ->
      (match t.timplsize with
      | None ->
	  t.tserial_lemmas <- Some ("lemma_" ^(string_of_int i),"lemma_" ^(string_of_int (i+1)),"lemma_" ^(string_of_int (i+2)));
	  s ^
"val lemma_" ^(string_of_int i) ^ " 
    : deser_ok " ^(get_type_name t) ^ " serialize_" ^(get_type_name t) ^ " deserialize_" ^(get_type_name t) ^ "

val lemma_" ^(string_of_int (i+1)) ^ "
    : deser_too_short " ^(get_type_name t) ^ " serialize_" ^(get_type_name t) ^ " deserialize_" ^(get_type_name t) ^ "

val lemma_" ^(string_of_int (i+2)) ^ "
    : deser_rev " ^(get_type_name t) ^ " serialize_" ^(get_type_name t) ^ " deserialize_" ^(get_type_name t) ^ "\n\n"
          , i + 3
      | Some _ -> s, i)
    )
    ("", last_nr)
    !serde_types in

  let (str_fun_equations, last_nr) = get_fun_equations last_nr in

  let (str_tuple_lemmas, _) = List.fold_left (fun (s, i) tl ->
    let x_vars = create_fresh_names "x_" (List.length tl) in
    let return_vars = create_fresh_names "cvar_" (List.length tl) in
    let ser_list x_vars tl =
      "[" ^ (String.concat "; " (List.map2 (fun x t ->
	(get_write_serial t) ^ " " ^ x) x_vars tl))^"]"
    in
    (s ^
    "let lemma_" ^ (string_of_int i) ^
    (String.concat "" (List.map2 (fun x t ->
      " ("^x^": "^get_type_name t^")"
				     ) x_vars tl))^":\n"^
    "  Lemma ("^ get_tuple_dec_fun_name tl ^ " (compos "^
    get_tuple_tag_name tl ^ " "^ser_list x_vars tl^") == Some("^
    (String.concat ", " x_vars)^"))\n"^
    "  =\n"^
    "  let l = "^ser_list x_vars tl^" in\n"^
    "  let buf = compos "^get_tuple_tag_name tl^" l in\n"^
    "  lemma_check_tag "^get_tuple_tag_name tl^" l buf;\n"^
    "  match check_tag "^get_tuple_tag_name tl^" buf with\n"^
    "  | None -> ()\n"^
    "  | Some pos ->\n"^
    let rec aux x_vars return_vars tl =
      match x_vars, return_vars, tl with
      | [], [], [] ->
	  "  ()\n\n"
      | x::x_vars_rest, cvar::return_vars_rest, t::tl_rest ->
	  "  lemma_append_ok (compos_rec "^
	  ser_list x_vars tl^") ("^get_write_serial t^" "^x^") (compos_rec "^
	  ser_list x_vars_rest tl_rest^") buf pos;\n"^
	  "  "^get_deser_ok_lemma t^" "^x^" buf pos;\n"^
	  "  match "^get_read_serial t^" buf pos with\n"^
	  "  | DSTooShort _ | DSFailure -> ()\n"^
	  "  | DSOk("^cvar^", pos) ->\n"^
	  aux x_vars_rest return_vars_rest tl_rest
      | _ -> assert false
    in
    aux x_vars return_vars tl^

    let pos_vars = create_fresh_names "pos_" (List.length tl) in
    "let lemma_" ^ (string_of_int (i+1)) ^ " (buf: bytes) :\n"^
    "  Lemma (match "^get_tuple_dec_fun_name tl^" buf with\n"^
    "  | None -> True\n" ^
    "  | Some("^(String.concat ", " x_vars)^") -> buf == compos "^
    get_tuple_tag_name tl^" "^ser_list x_vars tl^")\n"^
    "  =\n"^
    "  lemma_check_tag_rev "^get_tuple_tag_name tl^" buf;\n"^
    "  match Tuples.check_tag "^get_tuple_tag_name tl^" buf with\n"^
    "  | None -> ()\n"^
    "  | Some pos_0 ->\n"^
    let rec aux1 in_pos pos_vars return_vars tl =
      match pos_vars, return_vars, tl with
      | [], [], [] ->
	  "  if "^in_pos^" = Seq.length buf then\n"
      | out_pos::pos_vars_rest, cvar::return_vars_rest, t::tl_rest ->
	  "  "^get_deser_rev_lemma t^" buf "^in_pos^";\n"^
	  "  match "^get_read_serial t^" buf "^in_pos^" with\n"^
	  "  | DSTooShort _ | DSFailure -> ()\n"^
	  "  | DSOk("^cvar^", "^out_pos^") ->\n"^
	  aux1 out_pos pos_vars_rest return_vars_rest tl_rest
      | _ -> assert false
    in
    aux1 "pos_0" pos_vars return_vars tl^
    "  begin\n"^
    let rec aux2 in_pos pos_vars return_vars tl =
      match pos_vars, return_vars, tl with
      | [], [], [] ->
          ""
      | out_pos::pos_vars_rest, cvar::return_vars_rest, t::tl_rest ->
	  aux2 out_pos pos_vars_rest return_vars_rest tl_rest^
	  "    lemma_append_rev ("^get_write_serial t^" "^cvar^") (compos_rec "^
	  ser_list return_vars_rest tl_rest^") buf "^in_pos^";\n"
      | _ -> assert false
    in
    aux2 "pos_0" pos_vars return_vars tl^
    "    lemma_append_rev (char4_of_int "^get_tuple_tag_name tl^") (compos_rec "^ser_list return_vars tl^") buf 0\n"^
    "  end\n"^
    "  else ()\n\n"), i+2
      ) ("",last_nr) (!tuple_types)
  in

  "module " ^ !protocol_name_allcaps ^ ".Equations\n\n" ^
  "open CVTypes\n" ^
  "open Tuples\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Types\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Functions\n\n" ^
  str_equations ^
  str_inv_equations ^
  str_serde_equations ^
  str_fun_equations ^
  str_tuple_lemmas

(* Variables module *)

let fstar_vars (b, secr_opt) =
  match secr_opt with
  | RealOrRandom false ->
      [ FSVar ("st"^get_binder_name b, get_type_name b.btype);
        FSVar ("stqueried"^get_binder_name b, get_type_name b.btype) ]
  | RealOrRandom true ->
      [ FSVar ("st"^get_binder_name b, get_type_name b.btype);
        FSBool ("stqueried"^get_binder_name b) ]
  | Reachability false ->
      [ FSVar ("st"^get_binder_name b, get_type_name b.btype);
        FSVar ("stqueried"^get_binder_name b, "unit") ]
  | Reachability true ->
      [ FSVar ("st"^get_binder_name b, get_type_name b.btype) ]
  | Bit ->
      (* All variables are under replication by the constraints on modules,
	 and bit-secrecy can be proved only for variables not under replication *)
      assert false

let fstar_all_vars prepared_queries =
  (List.map (fun b ->
    FSVar ("st"^get_binder_name b, get_type_name b.btype)
	    ) prepared_queries.public_vars) @
  List.concat (List.map fstar_vars prepared_queries.secret_vars)

let gen_str_variables_impl fstar_variables =
  if fstar_variables = [] then
    "module " ^ !protocol_name_allcaps ^ ".Variables\n\n" ^
    "type t = unit\n\n" ^
    "val init: t\n\n" ^
    "let init = ()\n\n"
  else

  let str_variables_record = "noeq type t = {\n" ^
    (String.concat ""
       (List.map (function
	 | FSBool v -> "  "^v^": bool;\n"
	 | FSVar(v,t) -> "  "^v^": var "^t^";\n"
		 ) fstar_variables)) ^
    "}\n"
  in

  let str_variables_init = "let init = {\n" ^
    (String.concat ""
       (List.map (function
	 | FSBool v -> "  "^v^" = false;\n"
	 | FSVar(v,t) -> "  "^v^" = var_empty "^t^";\n"
		 ) fstar_variables)) ^
    "}\n"
  in

  let str_variables_names =
    String.concat ""
      (List.map (fun fsv ->
	let s, v =
	  match fsv with
	  | FSBool v -> "accessor t bool", v
	  | FSVar(v,t) -> "var_name t "^t, v
	in
	"val vn" ^ v ^ ": " ^ s ^ "\n" ^
	"let vn" ^ v ^ " =\n" ^
	"  { get = (fun vars -> vars." ^ v ^ ");\n" ^
	"    set = (fun vars -> fun var -> { vars with " ^ v ^ " = var }) }\n\n"
		) fstar_variables)
  in

  "module " ^ !protocol_name_allcaps ^ ".Variables\n\n" ^
  "open CVTypes\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Types\n" ^
  "open State\n\n" ^
  str_variables_record ^ "\n" ^
  "val init: t\n\n" ^
  str_variables_init ^ "\n" ^
  str_variables_names ^ "\n"

(* Queries *)

(* There are two kinds of oracles for queries:

   - Those that return a value (test queries for indistinguishability
     secrecy, reveal queries for reachability secrecy). The returned
     result is computed by [get_call_return], the arguments of the
     CVST effect are computed by [cvst_args_return] (they express that
     the result is computed using [get_call_return]), and the name of
     the function is computed by [fun_name_return].

   - Those that compare with a received value (test queries for
     (one-session) reachability secrecy). The compared result is
     computed by [get_call_eq], the arguments of the CVST effect are
     computed by [cvst_args_eq] (they express that the state does not
     change, the result is unit, and the result of [get_call_eq] is
     guaranteed to be different from the received value), and the name
     of the function is computed by [fun_name_eq].
*)
			  
let get_call_return (b, opt) =
  let bname = get_binder_name b in
  match opt with
  | RealOrRandom onesession -> 
      if onesession then
	if !current_side = side1^"." (* Real *) then
	  "State.indist_one_session_real s1 vnst"^bname^" vnstqueried"^bname^" n"
	else (* Ideal *)
	  begin
	    assert(!current_side = side2^".");
	    "State.indist_one_session_ideal s1 vnst"^bname^" vnstqueried"^bname^" "^get_random b.btype^" n"
	  end
      else
	if !current_side = side1^"." (* Real *) then
	  "State.indist_real s1 vnst"^bname^" n"
	else (* Ideal *)
	  begin
	    assert(!current_side = side2^".");
	    "State.indist_ideal s1 vnst"^bname^" vnstqueried"^bname^" "^get_random b.btype^" n"
	  end
  | Reachability false ->
      "State.reach_reveal s1 vnst"^bname^" vnstqueried"^bname^" n"
  | Reachability true -> raise Not_found
  | Bit -> assert false

let cvst_args_return (b,opt) = 
  "(option " ^ get_type_name b.btype ^ ") true_pre (fun s1 res s2 ->
  (s2, res) == " ^ get_call_return (b,opt) ^ ")"

let fun_name_return (b,opt) = 
  let bname = get_binder_name b in
  match opt with
  | RealOrRandom _ -> "indist"^bname
  | Reachability false -> "reachreveal"^bname
  | Reachability true -> raise Not_found
  | Bit -> assert false
					 
let get_call_eq (b,opt) = 
  let bname = get_binder_name b in
  match opt with
  | RealOrRandom _ -> raise Not_found
  | Reachability false ->
      "State.reach_test s1 vnst"^bname^" vnstqueried"^bname^" n"
  | Reachability true ->
      "State.var_get s1 vnst"^bname^" n"
  | Bit -> assert false

let cvst_args_eq (b,opt) = 
  "unit true_pre (fun s1 res s2 ->
    (s2 == s1) /\\ (res == ()) /\\
        ~(" ^ get_call_eq (b,opt) ^ " == Some v))"

let fun_name_eq (b,opt) = 
  let bname = get_binder_name b in
  "reach"^bname
    
type fqterm =
  | FQEvent of string option
  | FQTerm
  | FQAnd of fqterm * fqterm
  | FQOr of fqterm * fqterm
  | FQNot of fqterm

let rec build_fqterm = function
  | QEvent(inj,_) ->
      FQEvent(if inj then Some (Terms.fresh_id "f") else None)
  | QTerm _ -> FQTerm
  | QNot(QEvent(false,_) as t) -> FQNot(build_fqterm t)
  | QNot _ | QExists _ | QForall _ -> assert false
  | QAnd(t1,t2) -> FQAnd(build_fqterm t1, build_fqterm t2)
  | QOr(t1,t2) -> FQOr(build_fqterm t1, build_fqterm t2)

let rec collect_functions accu = function
  | FQEvent(Some f) -> f :: accu
  | FQEvent None | FQTerm -> accu
  | FQAnd(t1,t2) | FQOr(t1,t2) ->
      collect_functions (collect_functions accu t1) t2
  | FQNot t -> collect_functions accu t

let translate_eterm t =
  match t.t_desc with
  | FunApp(f, _::l) ->
      "(Event_" ^ f.f_name ^ " " ^ (String.concat " " (List.map (fun t -> translate_term_ns t "") l)) ^ ")"
  | _ -> assert false
	
let targs s vars =
  (String.concat "" (List.map (fun b -> "("^s^(get_binder_name b)^": "^(get_type_name b.btype) ^ ") ") vars))
    
let quantify q l s =
  if l = [] then s else "      ("^q^" "^(targs "" l) ^ ".\n" ^ s ^ ")"

let rec translate_qterm forall t ft args =
  match t, ft with
  | QEvent(inj, t), FQEvent(fopt) ->
      if inj then
	"(occursat e ("^(Option.get fopt)^" "^args^") "^(translate_eterm t)^")"
      else
	"(mem "^(translate_eterm t)^" e)"
  | QTerm t, FQTerm ->
      translate_term_ns t ""
  | QAnd(t1,t2), FQAnd(ft1,ft2) ->
      "(" ^ translate_qterm forall t1 ft1 args ^ " /\\ " ^ translate_qterm forall t2 ft2 args ^ ")"
  | QOr(t1,t2), FQOr(ft1,ft2) ->
      "(" ^ translate_qterm forall t1 ft1 args ^ " \\/ " ^ translate_qterm forall t2 ft2 args ^ ")"
  | QNot(QEvent(false,t)), FQNot _ ->
      let forallev =
	List.filter (fun b -> Terms.refers_to b t) forall
      in
      quantify "forall" forallev 
	("        (~(mem "^(translate_eterm t)^" e))")
  | _ -> assert false
	
let translate_corresp (t1,t2) =
  let (forall, exists, forall2) = Terms.collect_vars_corresp t1 t2 in
  let corresp_base_name =
    match t1 with
    | (_,{t_desc = FunApp(f,_)}) :: _ -> f.f_name
    | _ -> "corresp"
  in
  let corresp_name = Terms.fresh_id corresp_base_name in
  let ft2 = build_fqterm t2 in
  let step_functions = collect_functions [] ft2 in
  let targf s =
    (String.concat "" (List.mapi (fun n _ -> "("^s^"tau"^(string_of_int (n+1))^": interv e) ") t1)) ^
    (targs s forall)
  in    
  let argf s = 
    (String.concat "" (List.mapi (fun n _ -> s^"tau"^(string_of_int (n+1))^" ") t1)) ^
    (String.concat "" (List.map (fun b -> s^(get_binder_name b)^" ") forall))
  in
  let interface = 
    (if List.exists (fun (inj,_) -> inj) t1 then
      (* Injective correspondence *)
      "type ft_"^corresp_name^" (e:evseq) = "^
      (String.concat "" (List.map (fun _ -> "interv e -> ") t1)) ^
      (String.concat "" (List.map (fun b -> (get_type_name b.btype) ^ " -> ") forall)) ^
      "intervopt e\n\n" ^
      "type inj_"^corresp_name^" (e:evseq) (f: ft_"^corresp_name^" e) = \n"^
      "  forall "^ (targf "x") ^ (targf "y") ^ ".\n" ^
      "    ((f "^ (argf "x") ^ "== f " ^(argf "y")^ ") /\\ ~(f " ^ (argf "x") ^ "== None)) ==> (" ^
      let first = ref true in
      (String.concat "" (List.mapi (fun n (inj, _) ->
	if inj then
	  let prefix = if !first then "" else " /\\ " in
	  first := false;
	  let argn = string_of_int (n+1) in
	  prefix ^ "xtau"^argn^" == ytau"^argn
	else
	  "") t1)) ^ ")\n\n" ^
      "type corresp_"^corresp_name^" (e: evseq) = \n"^
      "  exists " ^ (String.concat "" (List.map (fun f -> "(" ^ f ^ ": ft_" ^ corresp_name ^ " e)") step_functions)) ^ ".\n" ^
      "    "^(String.concat " /\\ " (List.map (fun f -> "inj_"^corresp_name^" e "^f) step_functions))^" /\\\n"^
      "    (forall " ^ (targf "") ^ ".\n" ^
      "      ("^(String.concat " /\\ " (List.mapi (fun n (inj,t) ->
	"(Seq.index e tau"^(string_of_int (n+1))^" == "^translate_eterm t^")") t1))^
      ") ==>\n"^
      (quantify "exists" exists
	    ("        "^translate_qterm forall2 t2 ft2 (argf "")))^
      ")\n\n"
    else
      begin
	(* Non-injective correspondence: step functions not needed *)
	assert (step_functions = []);
	"type corresp_"^corresp_name^" (e: evseq) = \n"^
	(if forall = [] then "" else "  forall " ^ (targs "" forall) ^ ".\n")^
	"    ("^(String.concat " /\\ " (List.map (fun (inj,t) ->
	  assert(inj = false);
	  "(mem "^translate_eterm t^" e)") t1))^
	") ==>\n"^
	(quantify "exists" exists
	   ("        "^translate_qterm forall2 t2 ft2 ""))^"\n\n"
      end)^
    "val lemma_"^corresp_name^": unit -> CVST unit true_pre (fun s1 _ s2 -> s1 == s2 /\\ corresp_"^corresp_name^" (Seq.seq_of_list (State.events_of s2)))\n\n"
  in
  let implem =
    "let lemma_"^corresp_name^"() = admit()\n\n"
  in
  (interface, implem)


let cvst_args_public b =
  let bname = get_binder_name b in
  "(option " ^ get_type_name b.btype ^ ") true_pre (fun s1 res s2 ->
  (s2 == s1) /\\ res == State.var_get s1 vnst"^bname ^ " n)"
  
let gen_str_queries_interface corresp =
  "module " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Queries\n\n"^
  "open CVTypes\n" ^
  "open Random\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Types\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Variables\n" ^
  "open " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Effect\n\n" ^
  
  (String.concat "" (List.map (fun b ->
    let bname = get_binder_name b in
    "val public"^bname ^ ": (n:SidMap.sidt) -> CVST " ^ cvst_args_public b ^ "\n\n"
			   ) ((!prepared_queries_ref).public_vars))) ^
  (String.concat "" (List.map (fun (b,opt) ->
    (try 
      "val " ^ fun_name_return (b,opt) ^ ": (n: SidMap.sidt) -> CVST " ^ cvst_args_return (b, opt) ^ "\n\n"
    with Not_found ->
      "") ^
    (try
      "val " ^ fun_name_eq (b, opt) ^ ": (n:SidMap.sidt) -> (v:"^get_type_name b.btype ^") -> CVST " ^ cvst_args_eq (b, opt) ^ "\n\n"
    with Not_found ->
      "")
  ) ((!prepared_queries_ref).secret_vars))) ^
  (if (!prepared_queries_ref).corresp = [] then "" else
  "open " ^ !protocol_name_allcaps ^ ".Events\n\n" ^
  "type evseq = Seq.seq " ^ !protocol_name ^ "_event\n\n" ^
  "type interv (e: evseq) = x: nat { x < Seq.length e }\n\n" ^
  "type intervopt (e: evseq) = option (interv e)\n\n" ^
  "type mem (evt: " ^ !protocol_name ^ "_event) (e: evseq) = exists (tau: interv e). (Seq.index e tau == evt)\n\n"^
  "let occursat (e: evseq) (tauopt:intervopt e) (evt: nsl_event) =
  match tauopt with
  | None -> False
  | Some tau -> Seq.index e tau == evt\n\n"^
  String.concat "" (List.map fst corresp))
		       
let gen_str_queries_impl corresp =
  "module " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Queries\n\n" ^
  "open CVTypes\n" ^
  "open Random\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Types\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Variables\n" ^
  "friend " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Effect\n\n" ^
  "(* We call sure_diff x x' when x <> x' has been proved by CryptoVerif,
   so we admit that x <> x' has been proved on the F* side *)
let sure_diff (#t: Type0) (x: t) (x':t) : Lemma (ensures (~(x == x'))) =
  admit()\n\n" ^
  (String.concat "" (List.map (fun b ->
    let bname = get_binder_name b in
    "val reprpublic"^bname ^ " (n:SidMap.sidt): repr_inv " ^ cvst_args_public b ^ "\n" ^
    "let reprpublic"^bname ^ " n = fun s1 ->
  " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas.reflexivity' s1;
  s1, State.var_get s1 vnst"^bname ^ " n\n\n" ^
    "let public"^bname ^ " (n:SidMap.sidt) = cvst_reflect (reprpublic"^bname ^ " n)\n\n"
			   ) ((!prepared_queries_ref).public_vars))) ^
  
  (String.concat "" (List.map (fun (b,opt) ->
    (try 
      let fun_name = fun_name_return (b,opt) in
      "val repr"^fun_name ^ " (n:SidMap.sidt): repr_inv " ^ cvst_args_return (b,opt) ^ "\n" ^
      "let repr"^fun_name ^ " n = fun s1 ->\n  " ^
      "  " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "QueryLemmas.lemma"^fun_name^ " n s1;\n   " ^ 
      get_call_return (b, opt) ^ "\n\n" ^
      "let "^fun_name ^" (n:SidMap.sidt) = cvst_reflect (repr"^fun_name ^ " n)\n\n"
    with Not_found ->
      "") ^
    (try
      let fun_name = fun_name_eq (b,opt) in
      "val repr"^fun_name ^ " (n:SidMap.sidt) (v:"^get_type_name b.btype^"): repr_inv " ^ cvst_args_eq (b,opt) ^ "\n" ^
      "let repr"^fun_name ^ " n v = fun s1 ->\n  " ^
      !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas.reflexivity' s1;
  sure_diff (" ^ get_call_eq (b,opt) ^ ") (Some v);\n  (s1, ())\n\n" ^
      "let "^fun_name^" (n:SidMap.sidt) (v:"^get_type_name b.btype^") = cvst_reflect (repr"^fun_name^" n v)\n\n"
    with Not_found -> 
      "")
      ) ((!prepared_queries_ref).secret_vars))) ^
  (String.concat "" (List.map snd corresp))
  

let gen_str_querylemmas_interface () =
  "module " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "QueryLemmas\n\n"^
  "open CVTypes\n" ^
  "open Random\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Types\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Variables\n" ^
  "open " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Protocol\n\n" ^
  
  (String.concat "" (List.map (fun (b,opt) ->
    try 
      "val lemma"^fun_name_return (b,opt)^ " (n: SidMap.sidt) (s1: "^state_type()^"):\n" ^
      "  Lemma (requires (" ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas.inv' s1))\n" ^
      "    (ensures (" ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "InvariantLemmas.rel' s1 (" ^ get_call_return (b,opt) ^ ")._1))\n\n"
    with Not_found ->
      ""
  ) ((!prepared_queries_ref).secret_vars))) 
		       
let gen_str_querylemmas_impl () =
  "module " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "QueryLemmas\n\n" ^
  "open CVTypes\n" ^
  "open Random\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Types\n" ^
  "open " ^ !protocol_name_allcaps ^ ".Variables\n" ^
  "open " ^ !protocol_name_allcaps ^ "." ^ !current_side ^ "Protocol\n\n" ^
  (String.concat "" (List.map (fun (b,opt) ->
    try 
      "(* PLEASE ADD PROOF FOR THE FOLLOWING LEMMA *)\n"^
      "let lemma"^fun_name_return (b,opt) ^ " (n: SidMap.sidt) (s1: "^state_type()^") = ()\n\n"
    with Not_found ->
      ""
	) ((!prepared_queries_ref).secret_vars)))

(* Print a string to a file *)	
    
let print_to_file filename content =
  let f = open_out (Filename.concat !Settings.out_dir filename) in
  output_string f content;
  close_out f

(* Remove redundant queries *)

let includes l l' =
  List.for_all (fun b -> List.memq b l') l

let implies_query q q' =
  match q, q' with
  | QDiffEquivalence pub_vars, QDiffEquivalence pub_vars' ->
      includes pub_vars' pub_vars
  | QEquivalence(_,pub_vars,_), QEquivalence(_,pub_vars',_) ->
      includes pub_vars' pub_vars
  | QSecret(x, pub_vars, opt), QSecret(x', pub_vars', opt') ->
      (x == x') && (includes pub_vars' pub_vars) &&
      (match opt, opt' with
      | _ when opt = opt' -> true
      | Bit, _ -> true
      | RealOrRandom false, _ -> true
      | RealOrRandom true, Bit -> true
          (* Bit secrecy is equivalent to real-or-random secrecy and
	     to one-session real-or-random secrecy, because the bit must
	     be chosen once. There may be a constant probability loss. *)
      | _, Bit -> false
      | _, RealOrRandom false -> false
      | _, Reachability true -> true
      | Reachability true, _ -> false
      | _ -> false
	    )
  | QEventQ(t1,t2,pub_vars),  QEventQ(t1',t2',pub_vars') ->
      false
      (* We could be more precise here:
	 (includes pub_vars' pub_vars) && (matches (t1,t2) (t1',t2'))
	 but the computation of [matches] is complicated,
	 and it is not essential, so we just give up for simplicity. *)
  | _ -> false

let remove_redundant_queries queries =
  let rec aux seen = function
    | [] -> seen
    | a::l ->
	let test_fun q = implies_query q a in
	if List.exists test_fun seen
        || List.exists test_fun l then
	  aux seen l
	else
	  let filter_fun q = not (implies_query a q) in
	  let seen' = List.filter filter_fun seen in
	  let l' = List.filter filter_fun l in
	  aux (a::seen') l'
  in
  aux [] queries

(* Check that the public variables are coherent *)

let check_coherent_public_vars queries =
  let public_vars = Settings.get_public_vars0 queries in
  List.iter (fun q ->
    let public_vars_q =
      match q with
      | QSecret (b', pub_vars, Reachability true) ->
      (* For one-session reachability secrecy, the variable for which we
	 prove secrecy is not considered public. *)
	  pub_vars
      | QSecret (b',pub_vars,_) ->
	  b'::pub_vars
      | QEventQ (_,_,pub_vars)
      | QEquivalence(_,pub_vars,_)
      | QEquivalenceFinal(_,pub_vars)
      | QDiffEquivalence(pub_vars) ->
	  pub_vars
      | AbsentQuery -> []
    in
    if not (Terms.equal_lists_sets_q public_vars public_vars_q) then
      let display_pubvars pubvars =
	if pubvars = [] then
	  Display.print_string "no public variable"
	else
	  begin
	    Display.print_string "public variable(s) ";
	    Display.display_list Display.display_binder pubvars
	  end
      in
      let s = Display.string_out (fun () ->
	Display.print_string "The query ";
	Display.display_query3 q;
	Display.print_string " has ";
	display_pubvars public_vars_q;
	Display.print_string ". This is not coherent with the other queries, which have ";
	display_pubvars public_vars;
	Display.print_string "."
	  )
      in
      error s
	) queries;
  public_vars

(* Prepare the queries *)

let prepare_queries public_secret_vars queries =
  let prepared_queries = ref empty_prepared_queries in
  List.iter (function
    | QSecret(x, pub_vars, opt) ->
	(* There can be at most one secrecy query for each variable,
	   by the elimination of redundant queries and the constraints on
	   public variables. *)
	assert (not (List.exists (fun (b,_) -> b == x) (!prepared_queries.secret_vars)));
	prepared_queries := { !prepared_queries with secret_vars = (x, opt) :: (!prepared_queries).secret_vars }
    | QEventQ(t1,t2,pub_vars) ->
	prepared_queries := { !prepared_queries with corresp = (t1,t2) :: (!prepared_queries).corresp }
    | QEquivalence _ ->
	prepared_queries := { !prepared_queries with has_equiv = true }
    | QDiffEquivalence _ ->
	prepared_queries := { !prepared_queries with has_diff_equiv = true }
    | QEquivalenceFinal _ -> assert false
    | AbsentQuery -> ()
	     ) queries;
  { !prepared_queries with
    public_vars = List.filter (fun x -> not (List.exists (fun (b,_) -> b == x) (!prepared_queries.secret_vars))) public_secret_vars }


(* Check that two implementations have the same oracles in the same modules *)

let compatible_oracle_data oracle1 oracle2 =
  (* Compatibility is tested only between elements that have the same
     key in the map, so they have the same name *)
  assert (oracle1.oracle_name = oracle2.oracle_name);
  if not (oracle1.under_repl = oracle2.under_repl) then
    begin
      let get_under_repl o =
	if o.under_repl then
	  " is under replication"
	else
	  " is not under replication"
      in
      error ("Oracle "^oracle1.oracle_name^(get_under_repl oracle1)^" on the real side and"^(get_under_repl oracle2)^" on the ideal side.\nIt should be either under replication on both sides and not under replication on both sides.")
    end;
  if not (oracle1.previous_oracle = oracle2.previous_oracle) then
    begin
      let get_previous o =
	match o.previous_oracle with
	| None -> " has no previous oracle"
	| Some op -> " has previous oracle "^op
      in
      error ("Oracle "^oracle1.oracle_name^(get_previous oracle1)^" on the real side and"^(get_previous oracle2)^" on the ideal side.\nIt should have the same previous oracle on both sides.")
    end;
  let get_type tl =
    String.concat ", " (List.map (fun t -> t.tname) tl)
  in
  if not (Terms.equal_lists (==) oracle1.input_type oracle2.input_type) then
    error ("Oracle "^oracle1.oracle_name^" has input type "^(get_type oracle1.input_type)^" on the real side and "^(get_type oracle2.input_type)^" on the ideal side.\nIt should have the same input type on both sides.");
  begin
    match oracle1.output_type, oracle2.output_type with
    | None,_ | _,None -> ()
    | Some tl1, Some tl2 ->
	if not (Terms.equal_lists (==) tl1 tl2) then
	  error ("Oracle "^oracle1.oracle_name^" has output type "^(get_type tl1)^" on the real side and "^(get_type tl2)^" on the ideal side.\nIt should have the same output type on both sides.")
  end;
  true

let same_module_oracles (mod1, _, oracles1) (mod2, _, oracles2) =
  if mod1 = mod2 then
    begin
      if not (StringMap.equal compatible_oracle_data oracles1 oracles2) then
	begin
	  let get_oracles om =
	    StringMap.fold (fun o _ s ->
	      if s = "" then o else s ^ ", " ^ o) om ""
	  in
	  error ("Module "^mod1^ " contains oracle(s) "^(get_oracles oracles1)^" on the real side and "^(get_oracles oracles2)^" on the ideal side.\nIt should contain the same oracles on both sides.")
	end;
      true
    end
  else
    false

let check_same_modules_oracles oracles_for_modules1 oracles_for_modules2 =
  if not (Terms.equal_lists_sets same_module_oracles oracles_for_modules1 oracles_for_modules2) then
    error "When two F* implementations are generated, they should contain the same modules, with the same oracles inside each module"

(* Main function for generating implementations *)

let do_implementation filename impl equations proved_queries =
  let proved_queries = remove_redundant_queries proved_queries in
  let public_secret_vars = check_coherent_public_vars proved_queries in
  let prepared_queries = prepare_queries public_secret_vars proved_queries in
  let has_real_or_random_secrecy =
    List.exists (function
      | (_, (RealOrRandom _ | Bit)) -> true
      | _ -> false) prepared_queries.secret_vars
  in
  (* We revise the provided implementation(s) depending on whether
     equivalence is proved and whether real-or-random secrecy queries
     are present. *)
  let impl_list =
    match impl with
    | Impl0 -> assert false
    | Impl1 impl1 ->
	assert (prepared_queries.has_diff_equiv = false &&
		prepared_queries.has_equiv = false);
	if has_real_or_random_secrecy then
	  [(side1^".", impl1); (side2^".", impl1)]
	else
	  ["",impl1]
    | Impl2 (impl1, impl2) ->
	assert (not (prepared_queries.has_diff_equiv && prepared_queries.has_equiv));
	if prepared_queries.has_diff_equiv || prepared_queries.has_equiv then
	  [(side1^".", impl1); (side2^".", impl2)]
	else
	  begin
	    print_string "Warning: equivalence not proved; generating F* implementation for the first process only (supposed to be the protocol).\n";
	    if has_real_or_random_secrecy then
	      [(side1^".", impl1); (side2^".", impl1)]
	    else
	      ["",impl1]
	  end
  in

  let fstar_variables = fstar_all_vars prepared_queries in
  prepared_queries_ref := prepared_queries;

  set_protocol_name filename;

  List.iter (fun (side_prefix,impl) ->
    check_no_module_name_clash impl
      ) impl_list;

  (* Set translations for some built-in types *)
  Settings.t_bitstring.timplname <- Some "bytes";
  Settings.t_bitstring.tequal <- Some "eq_bytes";
  Settings.t_bitstring.tserial <- Some ("serialize_bytes", "deserialize_bytes");
  Settings.t_bitstring.tserial_lemmas <- Some ("lemma_deser_ok_bytes", "lemma_deser_too_short_bytes", "lemma_deser_rev_bytes");
  Settings.t_bitstringbot.timplname <- Some "option bytes";
  Settings.t_bitstringbot.tequal <- Some "eq_obytes";
  Settings.t_bitstringbot.tserial <- Some ("serialize_obytes", "deserialize_obytes");
  Settings.t_bitstringbot.tserial_lemmas <- Some ("lemma_deser_ok_obytes", "lemma_deser_too_short_obytes", "lemma_deser_rev_obytes");  
  Settings.t_bool.tserial <- Some ("serialize_bool", "deserialize_bool");
  Settings.t_bitstringbot.tserial_lemmas <- Some ("lemma_deser_ok_bool", "lemma_deser_too_short_bool", "lemma_deser_rev_bool");
  Settings.t_bool.trandom <- Some "gen_bool";
  Settings.f_or.f_impl <- Func "( || )";

  let impl_list =
    List.map (fun (side_prefix,(impl_letfuns, impl_processes)) ->
      let oracles_for_modules =
	List.map
	  (fun (x, opt, p) ->
            oracles := StringMap.empty;
            ignore(get_iprocess_oracles p true None 0);
            (x, opt, !oracles)
	      ) impl_processes
      in
      (side_prefix, impl_letfuns, impl_processes, oracles_for_modules)
	) impl_list
  in

  (* When we generate two implementations, check that they have the same
     oracles in the same modules *)
  begin
    match impl_list with
    | [(_,_,_, oracles_for_modules1); (_,_,_, oracles_for_modules2)] ->
	check_same_modules_oracles oracles_for_modules1 oracles_for_modules2
    | _ -> ()
  end;

  let translatable_equations = List.filter
    (fun (_, _, t, t_if, _, _) ->
      term_contains_no_library_defs t && term_contains_no_library_defs t_if)
    equations
  in

  if List.length equations <> List.length translatable_equations then (
    info ("Number of translatable equations: " ^ string_of_int (List.length translatable_equations) ^
          " out of " ^ string_of_int (List.length equations) ^ " equations.")
  );

  (* collect types, functions, etc, that are needed to implement all translatable equations *)
  List.iter
    (fun (_, _, t, t_if, _, _) -> get_term_types t; get_term_types t_if)
    translatable_equations;

  (* The order of generation of modules is important: for example, generating
     the module Sessions may add serialization/deserialization to some types,
     which adds functions in module Types. *)
  List.iter (fun (side_prefix, impl_letfuns, impl_processes, oracles_for_modules) ->
    if impl_letfuns <> [] then
      begin
	current_side := side_prefix;
	print_string ("Generating "^side_prefix^"Letfun module ...\n");

	List.iter
	  (fun (_,_,t) -> get_term_types t)
	  impl_letfuns;

        (* Inside the Letfun module, we do not need to explicitly use the namespace. *)
	letfun_prefix := "";
	print_to_file (!protocol_name_allcaps ^ "." ^ side_prefix ^ letfun_module ^ ".fst") (get_letfun_implementation letfun_module impl_letfuns)
      end
	) impl_list;

  print_string ("Generating Functions module ...\n");
  print_to_file (!protocol_name_allcaps ^ ".Functions.fsti") (gen_str_functions ());

  print_string ("Generating Tables module ...\n");
  print_to_file (!protocol_name_allcaps ^ ".Tables.fst") (gen_str_tables_impl ());

  print_string ("Generating Variables module ...\n");
  print_to_file (!protocol_name_allcaps ^ ".Variables.fst") (gen_str_variables_impl fstar_variables);

  print_string ("Generating Event module ...\n");
  print_to_file (!protocol_name_allcaps ^ ".Events.fst") (gen_str_events_impl ());

  List.iter (fun (side_prefix, impl_letfuns, impl_processes, oracles_for_modules) ->
    current_side := side_prefix;
    (* In all modules except Letfun, we call explicitly into the Letfun namespace. *)
    letfun_prefix := !protocol_name_allcaps ^ "." ^ side_prefix ^ letfun_module ^ ".";

    print_string ("Generating "^side_prefix^"Sessions module ...\n");
    print_to_file (!protocol_name_allcaps ^ "." ^ side_prefix ^ "Sessions.fsti") (gen_str_sessions_interface oracles_for_modules);
    print_to_file (!protocol_name_allcaps ^ "." ^ side_prefix ^ "Sessions.fst") (gen_str_sessions_impl oracles_for_modules);

    print_string ("Generating "^side_prefix^"Protocol module ...\n");
    print_to_file (!protocol_name_allcaps ^ "." ^ side_prefix ^ "Protocol.fst") (gen_str_protocol ());

    let invariant_name = !protocol_name_allcaps ^ "." ^ side_prefix ^ "Invariant.fst" in
    if Sys.file_exists (Filename.concat !Settings.out_dir invariant_name) then
      print_string ("Module "^side_prefix^"Invariant left unchanged.\n")
    else
      print_to_file invariant_name (gen_str_invariant ());
    
    print_string ("Generating "^side_prefix^"InvariantLemmas module ...\n");
    print_to_file (!protocol_name_allcaps ^ "." ^ side_prefix ^ "InvariantLemmas.fsti") (gen_str_invariant_lemmas_intf ());
    let invariant_lemmas_impl_name = !protocol_name_allcaps ^ "." ^ side_prefix ^ "InvariantLemmas.fst" in
    if not (Sys.file_exists (Filename.concat !Settings.out_dir invariant_lemmas_impl_name)) then
      print_to_file invariant_lemmas_impl_name (gen_str_invariant_lemmas_impl ());

    print_string ("Generating "^side_prefix^"Effect module ...\n");
    print_to_file (!protocol_name_allcaps ^ "." ^ side_prefix ^ "Effect.fsti") (gen_str_effect_interface ());
    print_to_file (!protocol_name_allcaps ^ "." ^ side_prefix ^ "Effect.fst") (gen_str_effect_impl ());


    if prepared_queries.secret_vars != [] then
      begin
 	print_string ("Generating "^side_prefix^"QueryLemmas module ...\n");
	print_to_file (!protocol_name_allcaps ^ "." ^ side_prefix ^ "QueryLemmas.fsti") (gen_str_querylemmas_interface ());
	let querylemmas_impl_name = !protocol_name_allcaps ^ "." ^ side_prefix ^ "QueryLemmas.fst" in
	if not (Sys.file_exists (Filename.concat !Settings.out_dir querylemmas_impl_name)) then
	  print_to_file querylemmas_impl_name (gen_str_querylemmas_impl ());
      end;
    
    if prepared_queries.public_vars != [] ||
       prepared_queries.secret_vars != [] ||
       prepared_queries.corresp != [] then
      begin
	print_string ("Generating "^side_prefix^"Queries module ...\n");
	let corresp = List.map translate_corresp prepared_queries.corresp in
	print_to_file (!protocol_name_allcaps ^ "." ^ side_prefix ^ "Queries.fsti") (gen_str_queries_interface corresp);
	print_to_file (!protocol_name_allcaps ^ "." ^ side_prefix ^ "Queries.fst") (gen_str_queries_impl corresp);
      end
	    ) impl_list;

  print_string ("Generating Types module ...\n");
  print_to_file (!protocol_name_allcaps ^ ".Types.fsti") (gen_str_types ());

  print_string ("Generating Equations module ...\n");
  print_to_file (!protocol_name_allcaps ^ ".Equations.fsti") (gen_str_equations translatable_equations);

  List.iter (fun (side_prefix, impl_letfuns, impl_processes, oracles_for_modules) ->
    current_side := side_prefix;
    (* Modules for oracles *)
    (* In all modules except Letfun, we call explicitly into the Letfun namespace. *)
    letfun_prefix := !protocol_name_allcaps ^ "." ^ side_prefix ^ letfun_module ^ ".";

    List.iter (fun (x, opt, oracles) ->
      print_string ("Generating implementation for module " ^ side_prefix ^ x ^ " ...\n");
      print_to_file (!protocol_name_allcaps ^ "." ^ side_prefix ^ x ^ ".Spec.fsti") (get_interface_spec x opt oracles);
      let spec_impl_name = !protocol_name_allcaps ^ "." ^ side_prefix ^ x ^ ".Spec.fst" in
      if not (Sys.file_exists (Filename.concat !Settings.out_dir spec_impl_name)) then
	print_to_file spec_impl_name (get_implementation_spec x opt oracles);

      print_to_file (!protocol_name_allcaps ^ "." ^ side_prefix ^ x ^ ".fst") (get_implementation x opt oracles);
      print_to_file (!protocol_name_allcaps ^ "." ^ side_prefix ^ x ^ ".fsti") (get_interface x opt oracles)
	      ) oracles_for_modules
	    ) impl_list;

  prepared_queries_ref := empty_prepared_queries

(* -------------------------------------------------------------------- *)
type 'a pp = Format.formatter -> 'a -> unit
type fmt   = Format.formatter

(* -------------------------------------------------------------------- *)
module Format : sig
  include module type of Format with type formatter = Format.formatter

  val tsfprintf :
    formatter -> ('a, formatter, unit) format -> 'a

  val pp_print_list :
    sep:(unit, formatter, unit) format ->
    (formatter -> 'a -> unit) -> formatter -> 'a list -> unit
end = struct
  include Format

  let tsfprintf (fmt : formatter) =
    let mypp fmt =
      let open Format in
      let fof = pp_get_formatter_out_functions fmt () in
      function
      | Output_flush -> fof.out_flush ()
      | Output_newline -> fof.out_newline ()
      | Output_string s -> fof.out_string s 0 (String.length s)
      | Output_spaces n -> fof.out_spaces n
      | Output_indent n -> fof.out_indent n in

    let sob = Format.make_symbolic_output_buffer () in

    let k _fmt =
      let l = Format.flush_symbolic_output_buffer sob in
      let rec unindent_blank_lines l =
        let open Format in
        match l with
        | [] -> []
        | Output_newline :: Output_indent _ :: Output_newline :: l ->
            Output_newline :: unindent_blank_lines (Output_newline :: l)
        | x :: l -> x :: unindent_blank_lines l
      in
      let l = unindent_blank_lines l in
      List.iter (mypp fmt) l
    in

    Format.kfprintf k (Format.formatter_of_symbolic_output_buffer sob)

  let pp_print_list ~sep pp fmt x =
    pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt sep) pp fmt x
end

(* -------------------------------------------------------------------- *)
let pp_bullets pp (fmt : fmt) xs =
  Format.pp_print_list
    ~sep:"@\n" (fun fmt x -> Format.fprintf fmt "- %a" pp x) fmt xs

let pp_option pp (fmt : fmt) x =
  match x with
  | None   -> Format.fprintf fmt "%s" "[none]"
  | Some x -> Format.fprintf fmt "%a" pp x

let pp_mode (fmt : fmt) (the : Types.mode) =
  Format.fprintf fmt "mode:%s"
    (match the with
     | ExistEquiv -> "ExistEquiv"
     | AllEquiv   -> "AllEquiv")

let pp_restropt (fmt : fmt) (the : Types.restropt) =
  Format.fprintf fmt "restropt:%s"
    (match the with
     | NoOpt     -> "NoOpt"
     | Unchanged -> "Unchanged"
     | DontKnow  -> "DontKnow")

let pp_fields (fmt : fmt) fields =
  Format.pp_print_list ~sep:"@\n" (fun fmt (name, pp) ->
      Format.fprintf fmt "@[<hov 2>%s =@\n%t@]" name pp
    ) fmt fields

let pp_repl_index (fmt : fmt) (ri : Types.repl_index) =
  Format.fprintf fmt "%s#%d (%s)"
    ri.Types.ri_sname ri.Types.ri_vname ri.Types.ri_type.tname

let pp_binder (fmt : fmt) (b : Types.binder) =
  Format.fprintf fmt "%s/%d" b.Types.sname b.Types.vname

let pp_abinder (fmt : fmt) ((b, _, r) : Types.binder * Parsing_helper.extent * Types.restropt) =
  Format.fprintf fmt "%a [%a]" pp_binder b pp_restropt r

let pp_ribinder (fmt : fmt) ((b, ri) : Types.binder * Types.repl_index) =
  Format.fprintf fmt "%a {%a}" pp_binder b pp_repl_index ri

let pp_oracle_name (fmt : fmt) (o : Types.oracle) =
  Format.fprintf fmt "$%s" o.oname

let pp_funsymb (fmt : fmt) (f : Types.funsymb) =
  Format.fprintf fmt "^%s" f.Types.f_name

let pp_table (fmt : fmt) (tab : Types.table) =
  Format.fprintf fmt "%s" tab.tblname

let rec pp_term (fmt : fmt) (t : Types.term) =
  match t.Types.t_desc with
  | Var (b, ts) ->
      Format.fprintf fmt "@[<hov 2>term:Var:{@\n%a@]@\n}"
        pp_fields
          [("name", fun fmt -> pp_binder fmt b);
           ("idx" , fun fmt -> pp_bullets pp_term fmt ts)]

  | ReplIndex ri ->
      Format.fprintf fmt "@[<hov 2>term:ReplIndex:%a@]"
        pp_repl_index ri

  | FunApp (f, ts) ->
      Format.fprintf fmt "@[<hov 2>term:FunApp:{@\n%a@]@\n}"
        pp_fields
          [("name", fun fmt -> pp_funsymb fmt f);
           ("idx" , fun fmt -> pp_bullets pp_term fmt ts)]

  | TestE (c, b1, b2) ->
      Format.fprintf fmt "@[<hov 2>term:LetE:{@\n%a@]@\n}"
        pp_fields
          [("condition", fun fmt -> pp_term fmt c );
           ("then"     , fun fmt -> pp_term fmt b1);
           ("else"     , fun fmt -> pp_term fmt b2)]

  | FindE (bs, t, _) ->
      Format.fprintf fmt "@[<hov 2>term:FindE:{@\n%a@]@\n}"
        pp_fields
          [("find", fun fmt -> pp_bullets pp_findbranch fmt bs);
           ("else", fun fmt -> pp_term fmt t)];

  | LetE (p, t1, t2, t3) ->
      Format.fprintf fmt "@[<hov 2>term:LetE:{@\n%a@]@\n}"
        pp_fields
          [("pattern", fun fmt -> pp_pattern fmt p );
           ("matched", fun fmt -> pp_term    fmt t1);
           ("then"   , fun fmt -> pp_term    fmt t2);
           ("else"   , fun fmt -> pp_option pp_term fmt t3)]

  | ResE (b, t) ->
      Format.fprintf fmt "@[<hov 2>term:ResE:{@\n%a@]@\n}"
        pp_fields
          [("binder" , fun fmt -> pp_binder fmt b);
           ("then"   , fun fmt -> pp_term   fmt t)]

  | EventAbortE f ->
      Format.fprintf fmt "@[<hov 2>term:EventAbortE:%a@]"
        pp_funsymb f

  | EventE (t1, t2) ->
      Format.fprintf fmt "@[<hov 2>term:EventE:{@\n%a@]@\n}"
        pp_fields
          [("event", fun fmt -> pp_term fmt t1);
           ("then" , fun fmt -> pp_term fmt t2)]

  (* TODO: print table name? *)
  | InsertE (tab,ts1,t2) ->
     Format.fprintf fmt "@[<hov 2>term:InsertE:{@\n%a@]@\n}"
       pp_fields
       [("table"    , fun fmt -> pp_table fmt tab);
        ("entry" , fun fmt -> Format.pp_print_list ~sep:"@\n" pp_term fmt ts1);
        ("then"  , fun fmt -> pp_term fmt t2)]

  | GetE (tab,ps,cnd,t1,t2,_) ->
     Format.fprintf fmt "@[<hov 2>term:GetE:{@\n%a@]@\n}"
       pp_fields
       [("table"    , fun fmt -> pp_table fmt tab);
        ("patterns" , fun fmt -> Format.pp_print_list ~sep:"@\n" pp_pattern fmt ps);
        ("condition", fun fmt -> pp_option pp_term fmt cnd);
        ("then"     , fun fmt -> pp_term fmt t1);
        ("else"     , fun fmt -> pp_term fmt t2)]


and pp_pattern (fmt : fmt) (p : Types.pattern) =
  match p with
  | PatVar b ->
      Format.fprintf fmt "@[<hov 2>pattern:PatVar:%a@]"
        pp_binder b

  | PatTuple (f, ps) ->
      Format.fprintf fmt "@[<hov 2>pattern:PatTuple:{@\n%a@]@\n}"
        pp_fields
          [("name", fun fmt -> pp_funsymb fmt f);
           ("args", fun fmt -> pp_bullets pp_pattern fmt ps)]

  | PatEqual t ->
      Format.fprintf fmt "@[<hov 2>pattern:PatTerm:{@\n%a@]@\n}"
        pp_fields [("term", fun fmt -> pp_term fmt t)]

and pp_findbranch (fmt : fmt) (b : Types.term Types.findbranch) =
  let (bs, brs, c, b) = b in

  Format.fprintf fmt "%a" pp_fields
    [("binders"  , fun fmt -> pp_bullets pp_ribinder fmt bs);
     ("defined"  , fun fmt -> pp_bullets pp_binderref fmt brs);
     ("condition", fun fmt -> pp_term fmt c);
     ("then"     , fun fmt -> pp_term fmt b)]

and pp_binderref (fmt : fmt) ((b, ts) : Types.binderref) =
  Format.fprintf fmt "@[<hov 2>binderref:{@\n%a@]@\n}"
    pp_fields
      [("binder", fun fmt -> pp_binder fmt b);
       ("index" , fun fmt -> pp_bullets pp_term fmt ts)]

let rec pp_fungroup (fmt : fmt) (group : Types.fungroup) =
  match group with
  | ReplRestr (ri, b, subgroups) ->
      Format.fprintf fmt
        "@[<hov 2>fungroup:ReplRestr:{@\n%a@]}@\n}"
        pp_fields
          [("repl_index", fun fmt -> (pp_option pp_repl_index fmt ri));
           ("binders"   , fun fmt -> (pp_bullets pp_abinder fmt b));
           ("groups"    , fun fmt -> (pp_bullets pp_fungroup fmt subgroups))]

  | Fun (o, b, t, _) ->
      Format.fprintf fmt
        "@[<hov 2>fungroup:Fun:{@\n%a@]@\n}"
        pp_fields
          [("oracle", fun fmt -> (pp_oracle_name fmt o));
           ("binders", fun fmt -> (pp_bullets pp_binder fmt b));
           ("term"   , fun fmt -> (pp_term    fmt t))]

let pp_eqmember1 (fmt : fmt) (group, mode) =
  Format.fprintf fmt "@[<hov 2>eqmember:{@\n%a@]@\n}"
    pp_fields
      [ ("mode"  , fun fmt -> (pp_mode     fmt mode ))
      ; ("groups", fun fmt -> (pp_fungroup fmt group))]

let pp_eqmember (fmt : fmt) (the : Types.eqmember) =
  Format.pp_print_list ~sep:"@\n" pp_eqmember1 fmt the

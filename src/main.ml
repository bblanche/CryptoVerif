open Types

let front_end_set = ref false

let call_m4 input_file output_file =
  let output_file_descr = Unix.openfile output_file [ Unix.O_WRONLY; Unix.O_CREAT; Unix.O_TRUNC ] 0o600 in
  let args = Array.make 3 "m4" in
  args.(1) <- "-DCryptoVerif";
  args.(2) <- input_file;
  let (_,status) = Unix.waitpid [] (Unix.create_process "m4" args Unix.stdin output_file_descr Unix.stderr) in
  Unix.close output_file_descr;
  match status with
  | Unix.WEXITED 0 -> ()
  | _ -> Parsing_helper.user_error ("Error: Preprocessing of " ^ input_file ^ " by m4 failed.\n")
    
let anal_file s0 =
  if !Arg.current < Array.length Sys.argv - 1 then
    Parsing_helper.user_error "Error: The CryptoVerif file to analyze should be the last argument.\nIf you want to analyze several files, please run CryptoVerif once for each file.\n";
  let s =
    (* Preprocess .pcv files with m4 *)
    if StringPlus.case_insensitive_ends_with s0 ".pcv" then
      let s' = Filename.temp_file "cv" ".cv" in
      call_m4 s0 s';
      s'
    else
      s0
  in
  if not (!front_end_set) then
    begin
      (* Use the oracle front-end by default when the file name ends
	 in .ocv *)
      if StringPlus.case_insensitive_ends_with s ".ocv" then Settings.front_end := Settings.Oracles
    end;
  try
    Sys.catch_break true;
    let (statements, collisions, equivs, queries, proof, impl, final_p) = Syntax.read_file s in
    Settings.initial_statements := !Settings.statements;
    List.iter Statements.record_statement (!Settings.statements);
    List.iter Statements.simplify_statement statements;
    Settings.collisions := collisions;
    List.iter Statements.record_collision collisions;
    let (p, queries) = 
      match final_p with
      | SingleProcess p' -> (p', queries)
      | Equivalence(p1,p2,q1,q2,pub_vars) ->
	  let p2 = Terms.move_occ_process p2 in
          Check.check_def_process_main p2;
	  let final_game =
	    { proc = RealProcess p2;
	      expanded = false;
	      game_number = -1;
	      current_queries = [] }
	  in
          (*We put AbsentQuery to make sure that events are preserved in [initial_expand_simplify]*)
	  final_game.current_queries <-
	     ((AbsentQuery,final_game), None, ref ToProve)::(List.map (fun q -> ((q,final_game), None, ref ToProve)) q2);
	  let final_state =
	    { game = final_game;
	      prev_state = None;
	      tag = None }
	  in
          let final_state_after_minimal_transfos =
            Instruct.initial_expand_simplify_success final_state
          in
	  let rec remove_absent_query state =
            state.game.current_queries <-
               List.filter (function ((AbsentQuery,_),_,_) -> false | _ -> true)
		 state.game.current_queries;
            match state.prev_state with
              None -> ()
            | Some(_,_,_,s') -> remove_absent_query s'
	  in
	  remove_absent_query final_state_after_minimal_transfos;
	  (p1, (QEquivalence (final_state_after_minimal_transfos, pub_vars, true), None)::(List.map (fun q -> (q,None)) q1))
    in
    let p = Terms.move_occ_process p in
    Check.check_def_process_main p;
    let prove_queries() =
      let g = { proc = RealProcess p;
		expanded = false;
		game_number = 1;
		current_queries = [] } in
      let var_num_state = Terms.get_var_num_state() in
      let queries = List.map (fun (q, qinit_opt) ->
	Check_corresp.well_formed q;
	((q,g), qinit_opt, ref ToProve)) queries in
      Terms.set_var_num_state var_num_state;
      let queries =
        if List.for_all Terms.is_nonunique_event_query queries then 
	  ((AbsentQuery,g), None, ref ToProve)::queries
        else
	  queries
      in
      g.current_queries <- queries;
      Settings.equivs := equivs;
      
            (*
              List.iter Display.display_statement statements;
              print_newline();
              List.iter Display.display_equiv equivs;
              print_newline();
              Display.display_process p;
            *)
      Instruct.do_proof proof 
	{ game = g; 
	  prev_state = None;
	  tag = None }
    in
    let _ = 
      match !Settings.get_implementation with
      | OCaml ->
	  begin
	    let impl = Delete_ghost.delete_ghost [] impl in
	    match impl with
	    | Impl0 -> assert false
	    | Impl1 impl | Impl2(impl, _) ->
		ImplementationOCaml.do_implementation impl
	  end
      | FStar ->
	  begin
	    let proved_queries = prove_queries() in
	    let impl = Delete_ghost.delete_ghost proved_queries impl in
	    ImplementationFStar.do_implementation s0 impl statements proved_queries
	  end
      | Prove ->
          match !Settings.easy_crypt_convert with
          | Some s ->
              Settings.equivs := equivs;
              EasyCryptConvert.main s
		
          | None -> 
	      ignore (prove_queries())
    in
    (* Remove the preprocessed temporary file when everything went well *)
    if s0 <> s then
      Unix.unlink s
  with
  | End_of_file ->
      print_string "End of file.\n"
  | Sys.Break ->
      print_string "Stopped.\n"
  | Parsing_helper.Error(s, ext) ->
      Parsing_helper.input_error s ext
  | e ->
      Printexc.print_backtrace stdout;
      Parsing_helper.internal_error (Printexc.to_string e)

let _ =
  Arg.parse
    [ "-lib", Arg.String (fun s -> Settings.lib_name := s :: (!Settings.lib_name)),
      "<filename> \tchoose library file";
      "-tex", Arg.String (fun s -> Settings.tex_output := s),
      "<filename> \tchoose TeX output file";
      "-oproof", Arg.String (fun s -> Settings.proof_output := s),
      "<filename> \toutput the proof in this file";
      "-ocommands", Arg.String (fun s -> Settings.command_output := s),
      "<filename> \toutput the interactive commands in this file";
      "-oequiv", Arg.String (fun s -> Settings.equiv_output := s),
      "<filename> \tappend the generated special equivalences to this file";
      "-in", Arg.String (function 
	  "channels" -> Settings.front_end := Settings.Channels
	| "oracles" -> Settings.front_end := Settings.Oracles
	| _ -> Parsing_helper.user_error "Error: Command-line option -in expects argument either \"channels\" or \"oracles\".\n"),
      "channels / -in oracles \tchoose the front-end";
      "-impl",
        Arg.String
          (function
          | "OCaml" -> Settings.get_implementation := OCaml
          | "FStar" -> Settings.get_implementation := FStar
          | _ ->
              Parsing_helper.user_error
                "Error: Command-line option -impl expects argument either \"OCaml\" \
                 or \"FStar\".\n"),
        "\t<language> get implementation of defined modules in the chosen \
         language (OCaml or FStar)";
      "-EasyCryptConvert", Arg.String (fun s -> Settings.easy_crypt_convert := Some s),
      "<filename>:<equiv name> \twrite the EasyCrypt conversion of the given equiv to the given file.";
      "-EasyCryptRemoveTables", Arg.Set Settings.easy_crypt_remove_tables,
      "\tconvert insert/get into find before translating to EasyCrypt.";
      "-o", Arg.String (fun s -> 
                          try 
                            if (Sys.is_directory s) then Settings.out_dir := s
                            else Parsing_helper.user_error "Error: Command-line option -o expects a directory.\n"
                          with Sys_error _ ->
			    Parsing_helper.user_error "Error: Command-line option -o expects a directory.\n"
                       ),
          "<directory> \tthe generated files will be placed in this directory, for -impl, out_game, out_state, and out_facts (Default: .)";
    ]
    anal_file ("Cryptoverif " ^ Version.version ^ ". Cryptographic protocol verifier, by Bruno Blanchet, Pierre Boutry, David Cadé, Christian Doczkal, Aymeric Fromherz, Charlie Jacomme, Benjamin Lipp, and Pierre-Yves Strub\nCopyright ENS, CNRS, Inria, MPI-SP, Ecole Polytechnique, distributed under the CeCILL-B license.\nUsage:\n  cryptoverif [options] <file_to_analyze>\nwhere the options are listed below:")

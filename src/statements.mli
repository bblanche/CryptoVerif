open Types

val record_statement : collision -> unit
val record_collision : collision -> unit
val simplify_statement : statement -> unit

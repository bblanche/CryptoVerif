(* Remove ghost types. before generating an implementation *)

open Types

val delete_ghost : query list -> impl_info -> impl_info
    
val check_top_ghost : inputprocess -> unit

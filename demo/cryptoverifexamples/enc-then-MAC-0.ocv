(* Encrypt-then-MAC example *)

param n.

type mkey [fixed].
type key [fixed].
type macs [bounded].

fun k2b(key):bitstring [data].

(* Shared-key encryption (CPA Stream cipher) *)

proba Penc.

expand IND_CPA_sym_enc(key, bitstring, bitstring, enc, dec, injbot, Z, Penc).

(* The function Z returns for each bitstring, a bitstring
   of the same length, consisting only of zeroes. *)
const Zk:bitstring.
equation forall y:key; 
	Z(k2b(y)) = Zk.

(* Mac *)

proba Pmac.

expand SUF_CMA_det_mac(mkey, bitstring, macs, mac, verify, Pmac).

(* Queries *)

query secret k'' [onesession].
query secret k''.

process 
	Ostart() :=
	k <-R key;
	mk <-R mkey;
	return;
	((foreach i <= n do
	OA() :=
	k' <-R key;
	let e = enc(k2b(k'), k) in
	return(e, mac(e, mk))) |
	(foreach i' <= n do
	OB(e':bitstring, ma':macs) :=
	if verify(e', mk, ma') then
	let injbot(k2b(k'':key)) = dec(e', k) in
	return()))

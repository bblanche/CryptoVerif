all: test

# Boilerplate
# -----------

include Makefile.include

FST_FILES=$(wildcard src/*.fst) $(wildcard src/*.fsti)

ifndef NODEPEND
ifndef MAKE_RESTARTS
.depend: .FORCE
	@mkdir -p obj
	@$(FSTAR) --dep full $(FST_FILES) --extract 'OCaml:-* +NSL +Application' > $@

.PHONY: .FORCE
.FORCE:
endif
endif

include .depend

.PHONY: clean
clean:
	rm -rf obj dist .depend *.exe
	rm -rf tests/test.cmx tests/test.cmi tests/test.o

# Verification
# ------------

hints obj:
	mkdir $@

%.checked: | hints obj
	$(call run-with-log,\
	  $(FSTAR) \
	    --hint_dir hints \
	    $< \
	    && \
	    touch -c $@ \
	  ,[VERIFY] $(notdir $*),$(call to-obj-dir,$@))

%.ml:
	$(call run-with-log,\
	  $(FSTAR) $(notdir $(subst .checked,,$<)) --codegen OCaml \
	    --extract_module $(subst .fst.checked,,$(notdir $<)) \
	  ,[EXTRACT-ML] $(notdir $*),$(call to-obj-dir,$@))

# OCaml
# -----

ifeq ($(OS),Windows_NT)
  export OCAMLPATH := $(FSTAR_HOME)/lib;$(OCAMLPATH)
else
  export OCAMLPATH := $(FSTAR_HOME)/lib:$(OCAMLPATH)
endif

OCAMLOPT = OCAMLFIND_IGNORE_DUPS_IN="`ocamlc -where`/compiler-libs" OCAMLPATH=$(OCAMLPATH) ocamlfind opt -package fstar.lib -package cryptokit -linkpkg -thread -g -I $(HACL_HOME)/obj -I $(CV2FSTAR_HOME)/obj -I obj -w -8-20-26

TAC = $(shell which tac >/dev/null 2>&1 && echo "tac" || echo "tail -r")

ALL_CMX_FILES = $(patsubst %.ml,%.cmx,$(shell echo $(ALL_ML_FILES) | $(TAC)))

.PRECIOUS: obj/%.cmx
%.cmx: %.ml
	$(call run-with-log,\
	  $(OCAMLOPT) -c $< -o $@ \
	  ,[OCAMLOPT-CMX] $(notdir $*),$(call to-obj-dir,$@))

# As test.ml is manually written, we need to enforce extra dependencies to ensure it is not compiled before the
# corresponding F* files were extracted to OCaml
tests/test.cmx: $(ALL_ML_FILES)

libnspk.cmxa: $(ALL_CMX_FILES)
	$(call run-with-log,\
	  OCAMLFIND_IGNORE_DUPS_IN="`ocamlc -where`/compiler-libs" ocamlfind opt -a -o $@ -package fstar.lib -thread -g -I $(HACL_HOME)/obj -I $(CV2FSTAR_HOME)/obj $^ \
	  ,[OCAMLOPT-CMXA] libnspk,$(call to-obj-dir,$@))

# Tests
# -----

.PHONY: test
test: test.exe
	./$<

test.exe: $(HACL_HOME)/obj/libhaclml.cmxa $(CV2FSTAR_HOME)/obj/Lib_RandomSequence.cmx $(CV2FSTAR_HOME)/libcv2fstar.cmxa libnspk.cmxa tests/test.cmx
	$(call run-with-log,\
	  $(OCAMLOPT) -I obj -package fstar.lib $^ -o $@ \
	  ,[OCAMLOPT-EXE] $(notdir $*),$(call to-obj-dir,$@))

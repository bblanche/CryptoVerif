module NSL.Equations

open CVTypes
open NSL.Types
open NSL.Functions
friend NSL.Types
friend NSL.Functions

module SP = FStar.Seq.Properties
module I = Lib.IntTypes
module B = Lib.ByteSequence
module HPKE = Spec.Agile.HPKE

let lemma_0 (var_x1_0:skey) (var_x2_0:pkey) (var_x3_0:bool) (var_x4_0:(lbytes 8)) (var_x5_0:ciphertext) = ()

let lemma_1 var_x_0 = ()

let lemma_2 var_x_0 var_y_0 var_z_0 = ()

let lemma_3 var_x_0 var_y_0 = ()

let lemma_4 (var_z_0:(lbytes 8)) (var_t_0:(lbytes 8)) (var_u_0:address) (var_y2_0:(lbytes 8)) = ()

let lemma_5 (var_y_0:(lbytes 8)) (var_y2_0:(lbytes 8)) (var_z2_0:address) = ()

let msg1_min_length:nat = 11
let msg1_has_min_length (x: lbytes 8) (y: address)
    : Lemma (ensures Seq.length (msg1 x y) >= msg1_min_length) = ()
let msg2_min_length:nat = 19
let msg2_has_min_length (x y: lbytes 8) (z: address)
    : Lemma (ensures Seq.length (msg2 x y z) >= msg2_min_length) = ()
let msg3_length:nat = 9
let msg3_has_length (x: lbytes 8) : Lemma (ensures Seq.length (msg3 x) = msg3_length) = ()

let has_msg1_beginning (msg: bytes{Seq.length msg >= msg1_min_length}) : bool =
  let t = FStar.Seq.index msg 0 in
  B.lbytes_eq #1 (Seq.createL [t]) msg1typel

let msg1_begins_with_typebyte (x: lbytes 8) (y: address)
    : Lemma (ensures has_msg1_beginning (msg1 x y)) =
  ()


let has_msg2_beginning (msg: bytes{Seq.length msg >= msg2_min_length}) : bool =
  let t = FStar.Seq.index msg 0 in
  B.lbytes_eq #1 (Seq.createL [t]) msg2typel

let msg2_begins_with_typebyte (x y: lbytes 8) (z: address)
    : Lemma (ensures has_msg2_beginning (msg2 x y z)) =
  ()

let has_msg3_beginning (msg: bytes{Seq.length msg = msg3_length}) : bool =
  let t = FStar.Seq.index msg 0 in
  B.lbytes_eq #1 (Seq.createL [t]) msg3typel

let msg3_begins_with_typebyte (x: lbytes 8) : Lemma (ensures has_msg3_beginning (msg3 x)) =
  ()

let lemma_6 (var_z_0:(lbytes 8)) (var_t_0:(lbytes 8)) (var_u_0:address) (var_y2_0:(lbytes 8)) (var_z2_0:address) =
  let lem3 (x y: lbytes 8) (z: address) (a: lbytes 8) (b: address)
      : Lemma (ensures not (eq_plaintext (msg2 x y z) (msg1 a b)))
        [SMTPat (eq_plaintext (msg2 x y z) (msg1 a b))] =
    msg1_begins_with_typebyte a b;
    msg2_begins_with_typebyte x y z
  in
  assert (forall (x: lbytes 8) (y: lbytes 8) (z: address) (a: lbytes 8) (b: address).
        (not (eq_plaintext (msg2 x y z) (msg1 a b))))

let lemma_7 (var_x_1:ciphertext) = ()

let lemma_8 (var_x_0:bool) (var_y_0:bool) (var_z_0:bool) = ()

let lemma_9 (var_x_0:bool) = ()

let lemma_10 (var_x_0:bool) = ()

let lemma_11 (var_x_0:bool) = ()

let lemma_12 (var_x_0:bool) = ()

let lemma_13 () = ()

let lemma_14 () = ()

let lemma_15 (var_x_0:bool) (var_y_0:bool) = ()

let lemma_16 (var_x_0:bool) (var_y_0:bool) = ()

let lemma_17 (var_x_0:bool) = ()

let msg2_length (x0 x1: lbytes 8) (x2: address) : nat =
  1 + size_nonce + size_nonce + 2 + Seq.length x2

let msg2_has_expected_length (x0 x1: lbytes 8) (x2: address)
    : Lemma (ensures Seq.length (msg2 x0 x1 x2) = msg2_length x0 x1 x2) = ()

let lemma_18 (x0 x1: lbytes 8) (x2: address) =
  (* the lengths match *)
  msg2_has_expected_length x0 x1 x2;
  (* slice and append are inverses with the right parameters *)
  SP.append_slices (B.nat_to_bytes_be #I.SEC 2 (Seq.length x2)) x2;
  SP.append_slices x1 (Seq.append (B.nat_to_bytes_be #I.SEC 2 (Seq.length x2)) x2);
  SP.append_slices x0 (Seq.append x1 (Seq.append (B.nat_to_bytes_be #I.SEC 2 (Seq.length x2)) x2));
  SP.append_slices msg2typel
    (Seq.append x0 (Seq.append x1 (Seq.append (B.nat_to_bytes_be #I.SEC 2 (Seq.length x2)) x2)));
  match inv_msg2 (msg2 x0 x1 x2) with
  | Some (y0, y1, y2) ->
    assert (eq_lbytes x0 y0);
    assert (eq_lbytes x1 y1);
    assert (eq_addr x2 y2)
  | None -> ()

#push-options "--z3rlimit 50"
let lemma_19 (x: plaintext) =
  match inv_msg2 x with
  | Some (y0, y1, y2) ->
    msg2_has_expected_length y0 y1 y2;
    SP.lemma_split x 1;
    let msgtype, bytes1 = SP.split x 1 in
    SP.lemma_split bytes1 size_nonce;
    let na, bytes2 = SP.split bytes1 size_nonce in
    SP.lemma_split bytes2 size_nonce;
    let nb, bytes3 = SP.split bytes2 size_nonce in
    SP.lemma_split bytes3 2;
    let lenc, addr = SP.split bytes3 2 in
    B.lemma_nat_from_to_bytes_be_preserves_value #I.SEC lenc 2
  | None -> ()
#pop-options

let lemma_20 sk pk trust n ct = ()

let lemma_21 x = ()

let msg3_has_expected_length (x: lbytes 8) : Lemma (ensures Seq.length (msg3 x) = msg3_length) = ()

let lemma_22 n =
  msg3_has_expected_length n;
  SP.append_slices msg3typel n;
  ()

let lemma_23 x =
  match inv_msg3 x with
  | Some y ->
    msg3_has_expected_length y;
    SP.lemma_split x 1;
    ()
  | None -> ()

let lemma_24 x = ()

let lemma_25 x = ()

let msg1_length (x0: lbytes 8) (x1: address) : nat = 1 + size_nonce + 2 + Seq.length x1

let msg1_has_expected_length (x0: lbytes 8) (x1: address)
    : Lemma (ensures Seq.length (msg1 x0 x1) = msg1_length x0 x1) = ()

let lemma_26 x0 x1 =
  msg1_has_expected_length x0 x1;
  SP.append_slices (B.nat_to_bytes_be #I.SEC 2 (Seq.length x1)) x1;
  SP.append_slices x0 (Seq.append (B.nat_to_bytes_be #I.SEC 2 (Seq.length x1)) x1);
  SP.append_slices msg1typel
    (Seq.append x0 (Seq.append (B.nat_to_bytes_be #I.SEC 2 (Seq.length x1)) x1))

let lemma_27 x =
  match inv_msg1 x with
  | Some (y0, y1) ->
    msg1_has_expected_length y0 y1;
    SP.lemma_split x 1;
    let msgtype, bytes1 = SP.split x 1 in
    SP.lemma_split bytes1 size_nonce;
    let na, bytes2 = SP.split bytes1 size_nonce in
    SP.lemma_split bytes2 2;
    let lenc, addr = SP.split bytes2 2 in
    B.lemma_nat_from_to_bytes_be_preserves_value #I.SEC lenc 2
  | None -> ()

let lemma_28 x = ()

let lemma_29 x = ()

let lemma_30 x0 x1 = ()

let lemma_31 x = ()

let lemma_32 ct buf pos =
  let pk, c = ct in
  let ser = serialize_ciphertext ct in
  lemma_append_ok ser (serialize_pkey pk) (serialize_bbytes #(Spec.Agile.AEAD.cipher_max_length aead_alg) c) buf pos;
  lemma_deser_ok_lbytes #(HPKE.size_dh_public cs) pk buf pos;
  lemma_deser_ok_bbytes #(Spec.Agile.AEAD.cipher_max_length aead_alg) c buf (pos + Seq.length (serialize_pkey pk)) (*;
  match deserialize_ciphertext buf pos with
  | DSOk ((pk', c'), pos') ->
    assert (eq_pkey pk pk');
    assert (eq_bytes c c')
  | DSTooShort _ -> ()
  | DSFailure -> ()*)

let lemma_33 ct buf pos =
  let pk, c = ct in
  let ser = serialize_ciphertext ct in
  if Seq.length buf < pos + Seq.length (serialize_pkey pk) then
    begin
      lemma_append_too_short1 ser (serialize_pkey pk) (serialize_bbytes #(Spec.Agile.AEAD.cipher_max_length aead_alg) c) buf pos;
      lemma_deser_too_short_lbytes #(HPKE.size_dh_public cs) pk buf pos
    end
  else
    begin
      lemma_append_too_short2 ser (serialize_pkey pk) (serialize_bbytes #(Spec.Agile.AEAD.cipher_max_length aead_alg) c) buf pos;    
      lemma_deser_ok_lbytes #(HPKE.size_dh_public cs) pk buf pos;
      lemma_deser_too_short_bbytes #(Spec.Agile.AEAD.cipher_max_length aead_alg) c buf (pos + Seq.length (serialize_pkey pk))
    end

let lemma_34 buf pos =
  match deserialize_ciphertext buf pos with
  | DSOk((pk,ct), pos') ->
      lemma_deser_rev_lbytes #(HPKE.size_dh_public cs) buf pos;
      lemma_deser_rev_bbytes #(Spec.Agile.AEAD.cipher_max_length aead_alg) buf (pos + Seq.length (serialize_pkey pk));
      lemma_append_rev (serialize_pkey pk) (serialize_bbytes #(Spec.Agile.AEAD.cipher_max_length aead_alg) ct) buf pos
  | DSTooShort _ -> ()
  | DSFailure -> ()

let lemma_35 x buf pos = lemma_deser_ok_lbytes #(HPKE.size_dh_key cs) x buf pos

let lemma_36 x buf pos = lemma_deser_too_short_lbytes #(HPKE.size_dh_key cs) x buf pos

let lemma_37 buf pos = lemma_deser_rev_lbytes #(HPKE.size_dh_key cs) buf pos

let lemma_38 x buf pos = lemma_deser_ok_lbytes #(HPKE.size_dh_public cs) x buf pos

let lemma_39 x buf pos = lemma_deser_too_short_lbytes #(HPKE.size_dh_public cs) x buf pos

let lemma_40 buf pos = lemma_deser_rev_lbytes #(HPKE.size_dh_public cs) buf pos

let lemma_41 x buf pos = lemma_deser_ok_bbytes #max_size_addr x buf pos

let lemma_42 x buf pos = lemma_deser_too_short_bbytes #max_size_addr x buf pos

let lemma_43 buf pos = lemma_deser_rev_bbytes #max_size_addr buf pos

let lemma_44 (a:bool) (b:bool) = ()

let lemma_45 (a:bool) (b:bool) (c:bool) = ()

let lemma_46 (a:bool) = ()

let lemma_47 (a:bool) (b:bool) = ()

let lemma_48 (a:bool) (b:bool) (c:bool) = ()

let lemma_49 (a:bool) = ()

let lemma_50 (a b:bool) = ()

let lemma_51 (a b:bytes) = ()

let lemma_52 (a b:option bytes) = ()

let lemma_53 (a b:ciphertext_opt) = ()

let lemma_54 (a b:plaintext) = ()

let lemma_55 (a b:msg1res) =
    match a, b with
    | Some (sk1, pk1, trust1, n1, c1), Some (sk2, pk2, trust2, n2, c2) ->
      lemma_eq_bytes_equal n1 n2
    | _, _ -> ()


module CVTypes

open Lib.IntTypes
module B = Lib.ByteSequence
open Lib.Sequence
open Lib.PrintSequence

(* Equality *)

#push-options "--fuel 1 --ifuel 1 --z3rlimit 100"
let rec _eq_bytes (blocks_left blocks_right: FStar.Seq.seq uint8)
    : Pure bool
      (requires
        FStar.Seq.length blocks_left = FStar.Seq.length blocks_right /\
        FStar.Seq.length blocks_left >= Lib.IntTypes.max_size_t)
      (ensures fun b -> b = true <==> blocks_left == blocks_right)
      (decreases (FStar.Seq.length blocks_left)) =
  let block_left, rem_left = Lib.UpdateMulti.split_block Lib.IntTypes.max_size_t blocks_left 1 in
  let block_right, rem_right = Lib.UpdateMulti.split_block Lib.IntTypes.max_size_t blocks_right 1 in
  assert (FStar.Seq.length block_left = FStar.Seq.length block_right);
  assert (FStar.Seq.length rem_left = FStar.Seq.length rem_right);
  let front = B.lbytes_eq #(Seq.length block_left) block_left block_right in
  if (FStar.Seq.length rem_left <= Lib.IntTypes.max_size_t)
  then front && B.lbytes_eq #(Seq.length rem_left) rem_left rem_right
  else front && _eq_bytes rem_left rem_right
#pop-options

let eq_bytes b1 b2 =
  if not (Seq.length b1 = Seq.length b2)
  then false
  else
    if (Seq.length b1 <= Lib.IntTypes.max_size_t)
    then B.lbytes_eq #(Seq.length b1) b1 b2
    else _eq_bytes b1 b2

let eq_obytes ob1 ob2 =
  match ob1, ob2 with
  | None, None -> true
  | Some b1, Some b2 -> eq_bytes b1 b2
  | _ -> false

let eq_lbytes #l b1 b2 = B.lbytes_eq #l b1 b2

(* Lemmas for Equality *)

let rec lemma__eq_bytes_is_equal
      (b1: bytes)
      (b2: bytes{Seq.length b1 = Seq.length b2 /\ Seq.length b1 >= Lib.IntTypes.max_size_t})
    : Lemma (ensures _eq_bytes b1 b2 <==> b2 == b1) (decreases Seq.length b1) =
  let block_left, rem_left = Lib.UpdateMulti.split_block Lib.IntTypes.max_size_t b1 1 in
  let block_right, rem_right = Lib.UpdateMulti.split_block Lib.IntTypes.max_size_t b2 1 in
  assert (FStar.Seq.length block_left = FStar.Seq.length block_right);
  assert (FStar.Seq.length rem_left = FStar.Seq.length rem_right);
  if Seq.length rem_left <= Lib.IntTypes.max_size_t
  then
    (* in this case we rely on the property of lbytes_eq *)
    ()
  else lemma__eq_bytes_is_equal rem_left rem_right

let lemma_eq_bytes_equal b1 b2 =
  if not (Seq.length b1 = Seq.length b2)
  then ()
  else if (Seq.length b1 <= Lib.IntTypes.max_size_t) then () else lemma__eq_bytes_is_equal b1 b2

let lemma_eq_obytes_equal b1 b2 = ()

(* Serialization *)

open FStar.Mul

val char4_of_int (n: uint32) : Tot (B.lbytes 4)
let char4_of_int n = B.uint_to_bytes_le n

val int_of_char4 (s: B.bytes) (i: nat{Seq.length s >= i + 4}) : Tot (uint32)
let int_of_char4 s i =
  let length_bytes = FStar.Seq.slice s i (i + 4) in
  B.uint_from_bytes_le length_bytes

val serialize_bytes_rec (b: bytes): Tot bytes (decreases (Seq.length b))
let rec serialize_bytes_rec b =
  let len = Seq.length b in
  if len < maxint U32 then
    Seq.append (char4_of_int (u32 len)) b
  else
    let fst, rem = Seq.split b (maxint U32 - 1) in
    Seq.append (Seq.append (char4_of_int (u32 (maxint U32))) fst) (serialize_bytes_rec rem)

let serialize_bytes b = serialize_bytes_rec b

val deserialize_bytes_rec (buf: bytes) (pos: nat { pos <= Seq.length buf }):
  Tot (r: deserialize_res bytes { deser_is_ok bytes buf pos r }) (decreases (Seq.length buf - pos))
let rec deserialize_bytes_rec buf pos =
  if Seq.length buf < pos + 4 then
    DSTooShort (pos + 4 - Seq.length buf)
  else
    let len = uint_v (int_of_char4 buf pos) in
    if len = maxint U32 then
    begin
      let len' = maxint U32 - 1 in
      if Seq.length buf < pos + 4 + len' then
        DSTooShort (pos + 4 + len' - Seq.length buf)
      else
        let fst = Seq.slice buf (pos + 4) (pos + 4 + len') in
	match deserialize_bytes_rec buf (pos + 4 + len') with
	| DSOk(rem, pos') ->
	    if Seq.length rem = 0 then
	      DSFailure
	    else 
	      DSOk(Seq.append fst rem, pos')
	| DSTooShort l -> DSTooShort l
	| DSFailure -> DSFailure
    end  
    else
      if Seq.length buf < pos + 4 + len then
        DSTooShort (pos + 4 + len - Seq.length buf)
      else
        let fst = Seq.slice buf (pos + 4) (pos + 4 + len) in
        DSOk (fst, pos + 4 + len)

let deserialize_bytes buf pos = deserialize_bytes_rec buf pos 

let serialize_lbytes lb = lb
let deserialize_lbytes l buf pos =
  if Seq.length buf >= pos + l then
    DSOk(Seq.slice buf pos (pos+l), pos+l)
  else
    DSTooShort(pos+l-Seq.length buf)

let obytes_none:uint8 = u8 0x00
let obytes_some:uint8 = u8 0x01
let obytes_none_l:lbytes 1 = (Seq.createL [obytes_none])
let obytes_some_l:lbytes 1 = (Seq.createL [obytes_some])

let serialize_obytes ob =
  match ob with
  | None -> obytes_none_l
  | Some b ->
      Seq.append obytes_some_l (serialize_bytes b)

let deserialize_obytes buf pos =
  if Seq.length buf < pos + 1
  then DSTooShort 1
  else
    let fst = Seq.slice buf pos (pos+1) in
    if Lib.ByteSequence.lbytes_eq #1 obytes_none_l fst
    then DSOk(None, pos+1)
    else if Lib.ByteSequence.lbytes_eq #1 obytes_some_l fst then
      match deserialize_bytes buf (pos+1) with
      | DSOk(b, pos') -> DSOk(Some b, pos')
      | DSTooShort l -> DSTooShort l
      | DSFailure -> DSFailure
    else DSFailure

inline_for_extraction
let size_bytes_bool:size_nat = 1
let bytes_true_list:l: list uint8 {List.Tot.length l == size_bytes_bool} =
  [@@ inline_let ]let l = [u8 0xFF] in
  assert_norm (List.Tot.length l == size_bytes_bool);
  l
let bytes_true:lbytes size_bytes_bool = Seq.createL bytes_true_list

let bytes_false_list:l: list uint8 {List.Tot.length l == size_bytes_bool} =
  [@@ inline_let ]let l = [u8 0x00] in
  assert_norm (List.Tot.length l == size_bytes_bool);
  l
let bytes_false:lbytes size_bytes_bool = Seq.createL bytes_false_list


let serialize_bool b =
  match b with
  | true -> bytes_true
  | false -> bytes_false

let deserialize_bool buf pos =
  if Seq.length buf < pos + 1
  then
    DSTooShort 1
  else
    let fst = Seq.slice buf pos (pos+1) in
    if B.lbytes_eq fst bytes_true
    then DSOk(true, pos+1)
    else if B.lbytes_eq fst bytes_false
    then DSOk(false, pos+1) else DSFailure

let serialize_bbytes b =
  let len = Seq.length b in
  Seq.append (char4_of_int (u32 len)) b

let deserialize_bbytes l buf pos =
  if Seq.length buf < pos + 4 then
    DSTooShort (pos + 4 - Seq.length buf)
  else
    let len = uint_v (int_of_char4 buf pos) in
    if len <= l then
      let end_pos = pos + 4 + len in
      if Seq.length buf < end_pos then
        DSTooShort (end_pos - Seq.length buf)
      else
        let b = Seq.slice buf (pos + 4) end_pos in
        DSOk (b, end_pos)
    else
      DSFailure

let lemma_append_ok sapp s1 s2 buf pos =
  Seq.append_slices s1 s2

let lemma_append_too_short1 sapp s1 s2 buf pos =
  Seq.append_slices s1 s2

let lemma_append_too_short2 sapp s1 s2 buf pos =
  let l1 = Seq.length s1 in
  Seq.slice_slice buf pos (Seq.length buf) 0 l1;
  // Seq.slice buf pos (pos+l1) == Seq.slice (Seq.slice buf pos (Seq.length buf)) 0 l1
  Seq.slice_slice buf pos (Seq.length buf) l1 (Seq.length buf - pos);
  // Seq.slice buf (pos+l1) (Seq.length buf) == Seq.slice (Seq.slice buf pos (Seq.length buf)) l1 (Seq.length buf - pos)
  Seq.slice_slice sapp 0 (Seq.length buf - pos) 0 l1;
  // Seq.slice sapp 0 l1 == Seq.slice (Seq.slice sapp 0 (Seq.length buf - pos)) 0 l1
  Seq.slice_slice sapp 0 (Seq.length buf - pos) l1 (Seq.length buf - pos);
  // Seq.slice sapp l1 (Seq.length buf - pos) == Seq.slice (Seq.slice sapp 0 (Seq.length buf - pos)) l1 (Seq.length buf - pos)
  Seq.append_slices s1 s2;
  // Seq.slice sapp 0 l1 == s1
  // Seq.slice sapp l1 (Seq.length sapp) == s2
  Seq.slice_slice sapp l1 (Seq.length sapp) 0 (Seq.length buf - pos - l1)
  // Seq.slice (Seq.slice sapp l1 (Seq.length sapp)) 0 (Seq.length buf - pos - l1) == Seq.slice sapp l1 (Seq.length buf - pos)
  // so
  // Seq.slice buf (pos+l1) (Seq.length buf)
  // == Seq.slice (Seq.slice buf pos (Seq.length buf)) l1 (Seq.length buf - pos)
  // == Seq.slice (Seq.slice sapp 0 (Seq.length buf - pos)) l1 (Seq.length buf - pos)
  // == Seq.slice sapp l1 (Seq.length buf - pos)
  // == Seq.slice (Seq.slice sapp 1 (Seq.length sapp)) 0 (Seq.length buf - pos - 1)
  // == Seq.slice s2 0 (Seq.length buf - pos - l1)
  // hence
  // Seq.slice buf (pos+l1) (Seq.length buf) == Seq.slice s2 0 (Seq.length buf - pos - l1)

let lemma_append_rev s1 s2 buf pos =
  let l1 = Seq.length s1 in
  let l2 = Seq.length s2 in
  let pos' = pos + l1 + l2 in
  Seq.slice_slice buf pos pos' 0 l1;
  // Seq.slice buf pos (pos + l1) == Seq.slice (Seq.slice buf pos pos') 0 l1
  Seq.slice_slice buf pos pos' l1 (l1 + l2);
  // Seq.slice buf (pos + l1) pos' == Seq.slice (Seq.slice buf pos pos') l1 (l1 + l2)
  Seq.lemma_split (Seq.slice buf pos pos') l1
  
val lemma_deser_ok_bytes_rec (x: bytes)
  (buf: bytes) (pos: nat { pos <= Seq.length buf }) 
    : Lemma
        (requires (Seq.length buf >= pos + Seq.length (serialize_bytes_rec x) /\
	   Seq.slice buf pos (pos + Seq.length (serialize_bytes_rec x)) == serialize_bytes_rec x))
        (ensures (deserialize_bytes_rec buf pos == DSOk(x, pos + Seq.length (serialize_bytes_rec x))))
        (decreases (Seq.length buf - pos))      
let rec lemma_deser_ok_bytes_rec x buf pos =
  let len = Seq.length x in
  if len < maxint U32 then
    begin
      let ser = serialize_bytes_rec x in
      lemma_append_ok ser (char4_of_int (u32 len)) x buf pos;
      B.lemma_uint_to_from_bytes_le_preserves_value #U32 #SEC (u32 len)
    end
  else
    begin
      let len' = maxint U32 - 1 in
      let fstx, remx = Seq.split x len' in
      let fst = Seq.append (char4_of_int (u32 (maxint U32))) fstx in
      let ser = serialize_bytes_rec x in
      lemma_append_ok ser fst (serialize_bytes_rec remx) buf pos;
      lemma_append_ok fst (char4_of_int (u32 (maxint U32))) fstx buf pos;
      B.lemma_uint_to_from_bytes_le_preserves_value #U32 #SEC (u32 (maxint U32));
      lemma_deser_ok_bytes_rec remx buf (pos + 4 + len');
      Seq.lemma_split x len'
    end

let lemma_deser_ok_bytes x buf pos = lemma_deser_ok_bytes_rec x buf pos

val lemma_deser_too_short_bytes_rec (x: bytes)
  (buf: bytes) (pos: nat { pos <= Seq.length buf }) :
  Lemma (requires (Seq.length buf < pos + Seq.length (serialize_bytes_rec x) /\
                   Seq.slice buf pos (Seq.length buf) == Seq.slice (serialize_bytes_rec x) 0 (Seq.length buf - pos)))
    (ensures (match deserialize_bytes_rec buf pos with
              | DSTooShort n -> n <= pos + Seq.length (serialize_bytes_rec x) - Seq.length buf
	      | _ -> False))
    (decreases (Seq.length buf - pos))
let rec lemma_deser_too_short_bytes_rec x buf pos =
  if Seq.length buf < pos + 4 then
    ()
  else
    let ser = serialize_bytes_rec x in
    if Seq.length x >= maxint U32 then
    begin
      let len' = maxint U32 - 1 in
      let fst, rem = Seq.split x len' in
      let rem_seq = serialize_bytes_rec rem in
      let after_len = Seq.append fst rem_seq in
      Seq.append_assoc (char4_of_int (u32 (maxint U32))) fst rem_seq;
      lemma_append_too_short2 ser (char4_of_int (u32 (maxint U32))) after_len buf pos;
      B.lemma_uint_to_from_bytes_le_preserves_value #U32 #SEC (u32 (maxint U32));
      if Seq.length buf < pos + 4 + len' then
        ()
      else
        begin
          lemma_append_too_short2 after_len fst rem_seq buf (pos + 4);
	  lemma_deser_too_short_bytes_rec rem buf (pos + 4 + len')
	end
    end  
    else
    begin
      let len = Seq.length x in
      lemma_append_too_short2 ser (char4_of_int (u32 len)) x buf pos;
      B.lemma_uint_to_from_bytes_le_preserves_value #U32 #SEC (u32 len)
    end

let lemma_deser_too_short_bytes x buf pos = lemma_deser_too_short_bytes_rec x buf pos

val lemma_deser_rev_bytes_rec
  (buf: bytes) (pos: nat { pos <= Seq.length buf }) :
  Lemma
    (ensures (match deserialize_bytes_rec buf pos with
    | DSOk(x, end_pos) -> serialize_bytes_rec x == Seq.slice buf pos end_pos
    | _ -> True))
    (decreases (Seq.length buf - pos))
let rec lemma_deser_rev_bytes_rec buf pos =
  if Seq.length buf < pos + 4 then
    ()
  else
    let len = uint_v (int_of_char4 buf pos) in
    if len = maxint U32 then
    begin
      let len' = maxint U32 - 1 in
      if Seq.length buf < pos + 4 + len' then
        ()
      else
        let fst = Seq.slice buf (pos + 4) (pos + 4 + len') in
	match deserialize_bytes_rec buf (pos + 4 + len') with
	| DSOk(rem, pos') ->
	    if Seq.length rem = 0 then
	      ()
	    else
              let lenseq = Seq.slice buf pos (pos + 4) in
	      let remseq = Seq.slice buf (pos + 4 + len') pos' in
	      // returns DSOk(Seq.append fst rem, pos')
	      lemma_deser_rev_bytes_rec buf (pos + 4 + len');
	      // serialize_bytes_rec rem == Seq.slice buf (pos + 4 + len') pos'
              B.lemma_uint_from_to_bytes_le_preserves_value #U32 #SEC lenseq;
	      lemma_append_rev lenseq fst buf pos;
	      lemma_append_rev (Seq.append lenseq fst) remseq buf pos;
	      Seq.append_slices fst rem
	      // serialize_bytes_rec (Seq.append fst rem) == Seq.slice buf pos pos'
	| DSTooShort l -> ()
	| DSFailure -> ()
    end  
    else
      if Seq.length buf < pos + 4 + len then
        ()
      else
        let lenseq = Seq.slice buf pos (pos + 4) in
        let fst = Seq.slice buf (pos + 4) (pos + 4 + len) in
	// returns DSOk (fst, pos + 4 + len)
        B.lemma_uint_from_to_bytes_le_preserves_value #U32 #SEC lenseq;
	lemma_append_rev lenseq fst buf pos

let lemma_deser_rev_bytes buf pos = lemma_deser_rev_bytes_rec buf pos

let lemma_deser_ok_lbytes x buf pos = ()

let lemma_deser_too_short_lbytes x buf pos = ()

let lemma_deser_rev_lbytes buf pos = ()

let lemma_deser_ok_obytes x buf pos =
  match x with
  | Some b ->
      lemma_append_ok (serialize_obytes x) obytes_some_l (serialize_bytes b) buf pos;
      lemma_deser_ok_bytes b buf (pos+1)
  | None -> ()

let lemma_deser_too_short_obytes x buf pos =
  // (Seq.length buf < pos + Seq.length (serialize_obytes x) /\
  // Seq.slice buf pos (Seq.length buf) == Seq.slice (serialize_obytes x) 0 (Seq.length buf - pos))
  match x with
  | Some b ->
     let some_s2 = serialize_obytes x in
     let s2 = serialize_bytes b in
     Seq.lemma_len_append obytes_some_l s2;
     // Seq.length some_s2 == 1 + Seq.length s2
     if Seq.length buf < pos + 1 then
       ()
     else
       begin
         lemma_append_too_short2 some_s2 obytes_some_l s2 buf pos;
         lemma_deser_too_short_bytes b buf (pos+1)
       end
  | None ->
      ()

let lemma_deser_rev_obytes buf pos = 
  if Seq.length buf < pos + 1
  then ()
  else
    let fst = Seq.slice buf pos (pos+1) in
    if Lib.ByteSequence.lbytes_eq #1 obytes_none_l fst
    then
      // DSOk(None, pos+1)
      // show serialize_obytes None == fst
      ()
    else if Lib.ByteSequence.lbytes_eq #1 obytes_some_l fst then
      match deserialize_bytes buf (pos+1) with
      | DSOk(b, pos') ->
          let b' = serialize_bytes b in
          // DSOk(Some b, pos')
	  // show serialize_obytes (Some b) == Seq.slice buf pos pos'
	  lemma_deser_rev_bytes buf (pos+1);
	  // serialize_bytes b == Seq.slice buf (pos+1) pos'
	  lemma_append_rev obytes_some_l b' buf pos
      | DSTooShort l -> ()
      | DSFailure -> ()
    else
      ()

let lemma_deser_ok_bool x buf pos = ()

let lemma_deser_too_short_bool x buf pos = ()

let lemma_deser_rev_bool buf pos = ()

let lemma_deser_ok_bbytes #l x buf pos = 
(* (Seq.length buf >= pos + Seq.length (serialize_bbytes x) /\
   Seq.slice buf pos (pos + Seq.length (serialize_bbytes x)) == serialize_bbytes x)) *)
  let len = Seq.length x in
  lemma_append_ok (serialize_bbytes x) (char4_of_int (u32 len)) x buf pos;
  B.lemma_uint_to_from_bytes_le_preserves_value #U32 #SEC (u32 len)
  // uint_from_bytes_le (uint_to_bytes_le (u32 len)) == len
  // len == uint_v (int_of_char4 buf pos)

let lemma_deser_too_short_bbytes x buf pos =
  // (Seq.length buf < pos + Seq.length (serialize_bbytes x) /\
  // Seq.slice buf pos (Seq.length buf) == Seq.slice (serialize_bbytes x) 0 (Seq.length buf - pos))
  let len = Seq.length x in
  Seq.lemma_len_append (char4_of_int (u32 len)) x;
  // Seq.length (serialize_bbytes x) = 4 + Seq.length x = 4 + len
  if Seq.length buf < pos + 4 then
    ()
  else
    begin
      lemma_append_too_short2 (serialize_bbytes x) (char4_of_int (u32 len)) x buf pos;
      B.lemma_uint_to_from_bytes_le_preserves_value #U32 #SEC (u32 len)
      // uint_from_bytes_le (uint_to_bytes_le (u32 len)) == len
      // len == uint_v (int_of_char4 buf pos)
    end

let lemma_deser_rev_bbytes #l buf pos =
  if Seq.length buf < pos + 4 then
    ()
  else
    let len = uint_v (int_of_char4 buf pos) in
    if len <= l then
      let end_pos = pos + 4 + len in
      if Seq.length buf < end_pos then
        ()
      else
        let lenseq = Seq.slice buf pos (pos + 4) in
        let b = Seq.slice buf (pos + 4) end_pos in
        B.lemma_uint_from_to_bytes_le_preserves_value #U32 #SEC lenseq;
	// char4_of_int (int_of_char4 buf pos) == lenseq
	lemma_append_rev lenseq b buf pos
    else
      ()

(* Printing *)
let bool_to_str b =
  match b with
  | true -> "true"
  | false -> "false"


let print_bytes bytes = List.iter (fun a -> print_uint8_hex_pad a) (to_list bytes)

let print_bool b = IO.print_string (bool_to_str b)

let print_label_bytes l b separator =
  IO.print_string ("  " ^ l ^ ": ");
  print_bytes b;
  IO.print_string (if separator then "," else "");
  IO.print_newline ()

let print_label_bool l b separator =
  IO.print_string ("  " ^ l ^ ": ");
  IO.print_string (bool_to_str b);
  IO.print_string (if separator then "," else "");
  IO.print_newline ()


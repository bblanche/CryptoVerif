module CVTypes

let bytes = Lib.ByteSequence.bytes
let lbytes (l: Lib.IntTypes.size_nat) = Lib.ByteSequence.lbytes l

(* Tags for types *)

open Lib.IntTypes

type tagt = uint32
let tag_from_nat = u32

(* Equality *)

val eq_bytes: bytes -> bytes -> bool
val eq_obytes: option bytes -> option bytes -> bool
val eq_lbytes: #l: Lib.IntTypes.size_nat -> (b1: lbytes l) -> (b2: lbytes l) -> b: bool{b <==> b1 == b2}

(* Lemmas for Equality *)

val lemma_eq_bytes_equal (b1 b2: bytes)
    : Lemma (eq_bytes b1 b2 <==> b1 == b2) [SMTPat (eq_bytes b1 b2)]

val lemma_eq_obytes_equal (b1 b2: option bytes)
    : Lemma (eq_obytes b1 b2 <==> b1 == b2) [SMTPat (eq_obytes b1 b2)]

(* Serialization *)

type deserialize_res (a: Type0) =
  | DSOk of a * nat
  | DSTooShort of (l:nat {l > 0})
  | DSFailure

let deser_is_ok (a: Type0) (b: bytes) (pos: nat) (x: deserialize_res a) =
  match x with
  | DSOk(y, new_pos) -> pos <= new_pos /\ new_pos <= Seq.length b
  | _ -> True

type serialize_type (a: Type0) = a -> bytes
type deserialize_type (a: Type0) = (b: bytes) -> (pos: nat { pos <= Seq.length b }) -> (r: deserialize_res a { deser_is_ok a b pos r })

val serialize_bytes: serialize_type bytes
val deserialize_bytes: deserialize_type bytes

val serialize_lbytes: #l: Lib.IntTypes.size_nat -> serialize_type (lbytes l)
val deserialize_lbytes: l: Lib.IntTypes.size_nat -> deserialize_type (lbytes l)

val serialize_obytes: serialize_type (option bytes)
val deserialize_obytes: deserialize_type (option bytes)

val serialize_bool: serialize_type bool
val deserialize_bool: deserialize_type bool

let bbytes (l: Lib.IntTypes.size_nat)  = (x: bytes { Seq.length x <= l })

val serialize_bbytes: #l: Lib.IntTypes.size_nat -> serialize_type (bbytes l)
val deserialize_bbytes: l: Lib.IntTypes.size_nat -> deserialize_type (bbytes l)

(* Lemmas for (de)serialization *)

let deser_ok (a: Type0) (ser: serialize_type a) (deser: deserialize_type a) = 
  (x: a) -> (buf: bytes) -> (pos: nat { pos <= Seq.length buf }) ->
  Lemma (requires (Seq.length buf >= pos + Seq.length (ser x) /\
                   Seq.slice buf pos (pos + Seq.length (ser x)) == ser x))
    (ensures (deser buf pos == DSOk(x, pos + Seq.length (ser x))))

let deser_too_short (a: Type0) (ser: serialize_type a) (deser: deserialize_type a) =
  (x: a) -> (buf: bytes) -> (pos: nat { pos <= Seq.length buf}) ->
  Lemma (requires (Seq.length buf < pos + Seq.length (ser x) /\
                   Seq.slice buf pos (Seq.length buf) == Seq.slice (ser x) 0 (Seq.length buf - pos)))
    (ensures (match deser buf pos with
              | DSTooShort n -> n <= pos + Seq.length (ser x) - Seq.length buf
	      | _ -> False))

let deser_rev (a: Type0) (ser: serialize_type a) (deser: deserialize_type a) =
  (buf: bytes) -> (pos: nat { pos <= Seq.length buf}) -> 
  Lemma (match deser buf pos with
  | DSOk(x, end_pos) -> ser x == Seq.slice buf pos end_pos
  | _ -> True)

(* Intermediate lemmas useful when serialization is done by appending *)

val lemma_append_ok (sapp s1 s2 buf: bytes) (pos: nat { pos <= Seq.length buf }) :
  Lemma (requires (sapp == Seq.append s1 s2 /\
                   Seq.length buf >= pos + Seq.length sapp /\
                   Seq.slice buf pos (pos + Seq.length sapp) == sapp))
        (ensures (Seq.length buf >= pos + Seq.length s1 + Seq.length s2 /\
	          Seq.slice buf pos (pos + Seq.length s1) == s1 /\
		  Seq.slice buf (pos + Seq.length s1) (pos + Seq.length s1 + Seq.length s2) == s2))

val lemma_append_too_short1 (sapp s1 s2 buf: bytes) (pos: nat { pos <= Seq.length buf }) :
  Lemma (requires (sapp == Seq.append s1 s2 /\
         Seq.length buf < pos + Seq.length s1 /\
         Seq.slice buf pos (Seq.length buf) == Seq.slice sapp 0 (Seq.length buf - pos)))
	(ensures (Seq.slice buf pos (Seq.length buf) == Seq.slice s1 0 (Seq.length buf - pos)))

val lemma_append_too_short2 (sapp s1 s2 buf: bytes) (pos: nat { pos <= Seq.length buf }) :
  Lemma (requires (sapp == Seq.append s1 s2 /\
         Seq.length buf < pos + Seq.length sapp /\ Seq.length buf >= pos + Seq.length s1 /\
         Seq.slice buf pos (Seq.length buf) == Seq.slice sapp 0 (Seq.length buf - pos)))
	(ensures (Seq.slice buf pos (pos + Seq.length s1) == s1 /\
	          Seq.slice buf (pos + Seq.length s1) (Seq.length buf) == Seq.slice s2 0 (Seq.length buf - pos - Seq.length s1)))

val lemma_append_rev (s1 s2 buf: bytes) (pos: nat { pos <= Seq.length buf }) :
  Lemma (requires (Seq.length buf >= pos + Seq.length s1 + Seq.length s2 /\
                  s1 == Seq.slice buf pos (pos + Seq.length s1) /\
	          s2 == Seq.slice buf (pos + Seq.length s1) (pos + Seq.length s1 + Seq.length s2)))
        (ensures (Seq.append s1 s2 == Seq.slice buf pos (pos + Seq.length (Seq.append s1 s2))))
        
val lemma_deser_ok_bytes : deser_ok bytes serialize_bytes deserialize_bytes 

val lemma_deser_too_short_bytes : deser_too_short bytes serialize_bytes deserialize_bytes

val lemma_deser_rev_bytes : deser_rev bytes serialize_bytes deserialize_bytes

val lemma_deser_ok_lbytes (#l: Lib.IntTypes.size_nat)
    : deser_ok (lbytes l) (serialize_lbytes #l) (deserialize_lbytes l)

val lemma_deser_too_short_lbytes (#l: Lib.IntTypes.size_nat) 
    : deser_too_short (lbytes l) (serialize_lbytes #l) (deserialize_lbytes l) 

val lemma_deser_rev_lbytes (#l: Lib.IntTypes.size_nat) 
    : deser_rev (lbytes l) (serialize_lbytes #l) (deserialize_lbytes l) 

val lemma_deser_ok_obytes 
    : deser_ok (option bytes) serialize_obytes deserialize_obytes 

val lemma_deser_too_short_obytes 
    : deser_too_short (option bytes) serialize_obytes deserialize_obytes

val lemma_deser_rev_obytes
    : deser_rev (option bytes) serialize_obytes deserialize_obytes

val lemma_deser_ok_bool 
    : deser_ok bool serialize_bool deserialize_bool 

val lemma_deser_too_short_bool
    : deser_too_short bool serialize_bool deserialize_bool

val lemma_deser_rev_bool
    : deser_rev bool serialize_bool deserialize_bool

val lemma_deser_ok_bbytes (#l: Lib.IntTypes.size_nat)
    : deser_ok (bbytes l) (serialize_bbytes #l) (deserialize_bbytes l)

val lemma_deser_too_short_bbytes (#l: Lib.IntTypes.size_nat)
    : deser_too_short (bbytes l) (serialize_bbytes #l) (deserialize_bbytes l)

val lemma_deser_rev_bbytes (#l: Lib.IntTypes.size_nat)
    : deser_rev (bbytes l) (serialize_bbytes #l) (deserialize_bbytes l)

(* Printing *)

val bool_to_str: bool -> string

val print_bytes: bytes -> FStar.All.ML unit
val print_bool: bool -> FStar.All.ML unit

(* label, value, boolean indicating if separator should be printed *)
val print_label_bytes: string -> bytes -> bool -> FStar.All.ML unit
val print_label_bool: string -> bool -> bool -> FStar.All.ML unit


module SidMap

open CVTypes
open Lib.IntTypes
open Lib.Sequence
open Lib.PrintSequence

(* Type for session ids *)
type sidt = nat

(* Serialization for replication indices *)

val pow2log (n:nat{ n > 0 }) :
   Lemma(pow2((FStar.Math.Lib.log_2 n) + 1) >= n +1)

let rec pow2log n =
  if n = 1 then () else
  pow2log (n / 2)

let serialize_sidt n =
  let len = if n > 0 then (FStar.Math.Lib.div (FStar.Math.Lib.log_2 n) 8) + 1 else 0 in
  if n > 0 then
    begin
      FStar.Math.Lemmas.pow2_le_compat (op_Multiply 8 len) ((FStar.Math.Lib.log_2 n) + 1);
      pow2log n
    end;
  assert (n < pow2 (op_Multiply 8 len));
  serialize_bytes (Lib.ByteSequence.nat_to_intseq_be #U8 #SEC len n)

let deserialize_sidt buf pos =
  match deserialize_bytes buf pos with
  | DSOk(b, new_pos) -> 
    let n = Lib.ByteSequence.nat_from_intseq_be #U8 #SEC b in
    let len = if n > 0 then (FStar.Math.Lib.div (FStar.Math.Lib.log_2 n) 8) + 1 else 0 in
    if len = Seq.length b then
      DSOk(n, new_pos)
    else
      DSFailure
  | DSTooShort n -> DSTooShort n
  | DSFailure -> DSFailure

(* Lemmas for (de)serialization *)

let lemma_deser_ok_sidt n buf pos =
  let len = if n > 0 then (FStar.Math.Lib.div (FStar.Math.Lib.log_2 n) 8) + 1 else 0 in
  if n > 0 then
    begin
      FStar.Math.Lemmas.pow2_le_compat (op_Multiply 8 len) ((FStar.Math.Lib.log_2 n) + 1);
      pow2log n
    end;
  assert (n < pow2 (op_Multiply 8 len));
  let x = Lib.ByteSequence.nat_to_intseq_be #U8 #SEC len n in
  lemma_deser_ok_bytes x buf pos;
  ()

let lemma_deser_too_short_sidt n buf pos = 
  let len = if n > 0 then (FStar.Math.Lib.div (FStar.Math.Lib.log_2 n) 8) + 1 else 0 in
  if n > 0 then
    begin
      FStar.Math.Lemmas.pow2_le_compat (op_Multiply 8 len) ((FStar.Math.Lib.log_2 n) + 1);
      pow2log n
    end;
  assert (n < pow2 (op_Multiply 8 len));
  let x = Lib.ByteSequence.nat_to_intseq_be #U8 #SEC len n in
  lemma_deser_too_short_bytes x buf pos;
  ()

let lemma_deser_rev_sidt buf pos =
  match deserialize_bytes buf pos with
  | DSOk(b, new_pos) ->
      lemma_deser_rev_bytes buf pos;
      assert (serialize_bytes b == Seq.slice buf pos new_pos);
      assert (new_pos == pos + Seq.length (serialize_bytes b));
      let n = Lib.ByteSequence.nat_from_intseq_be #U8 #SEC b in
      let len = if n > 0 then (FStar.Math.Lib.div (FStar.Math.Lib.log_2 n) 8) + 1 else 0 in
      if len = Seq.length b then
        begin
          let c = Lib.ByteSequence.nat_to_intseq_be #U8 #SEC len n in
          Lib.ByteSequence.nat_from_intseq_be_inj #U8 #SEC b c;
          lemma_deser_ok_bytes b buf pos
        end
      else
        ()
  | DSTooShort n -> ()
  | DSFailure -> ()

(* Printing session ids *)

let print_sid n = IO.print_string (string_of_int n)

let print_label_sid l n separator = print_label_bytes l (serialize_sidt n) separator

(* Map session id -> value: The main type provided by this module *)

module Map = FStar.Map

noeq type t value =
 { sid: nat;
   m: Map.t nat value }

let available t id = (id >= t.sid)

let get t id = if Map.contains t.m id then Some (Map.sel t.m id) else None

let empty v =
  let m = Map.const v in
  { sid = 0;
    m = Map.restrict Set.empty m }

let reserve t = t.sid, { sid = t.sid + 1; m = t.m }

let upd t n v =
  let t' = { sid = t.sid; m = Map.upd t.m n v } in
  (introduce forall (id:nat { id <> n }). get t' id == get t id
  with
     (
     Map.lemma_InDomUpd2 t.m id n v;
     Map.lemma_SelUpd2 t.m id n v
     ));
  t'

let lem_empty v = ()

let lem_reserve t = ()

let lem_upd t n v =
   introduce forall (id:nat). available (upd t n v) id ==> get (upd t n v) id == None
   with
      Map.lemma_InDomUpd2 t.m id n v 


module StateLemmas

open CVTypes
open Random
open State
module Map = SidMap
module Set = FStar.Set


let same_sessions (#stt: state_type) (st0 st1: state stt) =
  (sessions_of st0 == sessions_of st1) /\
  (local_of st0 == local_of st1)

let same_tables (#stt: state_type) (st0 st1: state stt) =
  (tables_of st0 == tables_of st1)

let same_variables (#stt: state_type) (st0 st1: state stt) =
  (variables_of st0 == variables_of st1)

let same_events (#stt: state_type) (st0 st1: state stt) =
  (events_of st0 == events_of st1)

let same_state_except_entropy (#stt: state_type) (st0 st1: state stt) =
  same_sessions st0 st1 /\ same_tables st0 st1 /\ 
  same_variables st0 st1 /\ same_events st0 st1

(* Lemmas about various state functions *)

val call_with_entropy_lemma (#stt: state_type) (#a: Type) (st0: state stt) (f: entropy -> entropy * a)
    : Lemma (same_state_except_entropy st0 (call_with_entropy st0 f)._1)

let call_with_entropy_lemma st0 f = ()

val insert_lemma (#stt: state_type) (#te: Type0) (st0: state stt) (tn: table_name (tt_of stt) te) (x:te)
    : Lemma (same_sessions st0 (insert st0 tn x) /\
      	     same_variables st0 (insert st0 tn x) /\
	     same_events st0 (insert st0 tn x))

let insert_lemma st0 tn x = ()

type table_filter_full_same_state (stt: state_type) (te: Type0) (rt: Type0) =
  filter: table_filter_full stt te rt
  { forall (st0: state stt) (x:te) . same_state_except_entropy st0 (filter st0 x)._1 }

type table_filter_same_state (stt: state_type) (te: Type0) (rt: Type0) =
  f : table_filter stt te rt
  { match f with
    | TableFilterSimple _ -> True
    | TableFilterFull filter ->
        forall (st0: state stt) (x:te) . same_state_except_entropy st0 (filter st0 x)._1 }

val get_inner_full_lemma (#stt: state_type) (#te: Type0)
      (#rt: Type0)
      (st0: state stt)
      (matches: Seq.seq rt)
      (tab: table te)
      (filter: table_filter_full_same_state stt te rt)
      : Lemma
      (ensures (same_state_except_entropy st0 (get_inner_full st0 matches tab filter)._1))
      (decreases tab)

let rec get_inner_full_lemma st matches tab f = 
  match tab with
  | [] -> ()
  | x :: r -> 
    match f st x with
    | st, Some e -> get_inner_full_lemma st (Seq.cons e matches) r f
    | st, None -> get_inner_full_lemma st matches r f

val get_lemma (#stt: state_type) (#te: Type0)
      (#rt: Type0)
      (st0: state stt)
      (tn: table_name (tt_of stt) te)
      (f: table_filter_same_state stt te rt)
      : Lemma (same_state_except_entropy st0 (get st0 tn f)._1)

let get_lemma st tn tf =
  let tab = return_table st tn in
  match tf with
  | TableFilterSimple _ -> ()
  | TableFilterFull f -> get_inner_full_lemma st Seq.empty tab f

val get_unique_inner_full_lemma (#stt: state_type) (#te: Type0)
      (#rt: Type0)
      (st0: state stt)
      (tab: table te)
      (filter: table_filter_full_same_state stt te rt)
      : Lemma 
      (ensures (same_state_except_entropy st0 (get_unique_inner_full st0 tab filter)._1))
      (decreases tab)

let rec get_unique_inner_full_lemma st tab f =
  match tab with
  | [] -> ()
  | x :: r -> 
    match f st x with
    | st, Some e -> ()
    | st, None -> get_unique_inner_full_lemma st r f

val get_unique_full_lemma
    (#stt: state_type) (#te: Type0) (#rt: Type0)
    (st0: state stt) (tn: table_name (tt_of stt) te)
    (filter: table_filter_full_same_state stt te rt)
    : Lemma (same_state_except_entropy st0 (get_unique_full st0 tn filter)._1)

let get_unique_full_lemma st tn f =
  let tab = return_table st tn in
  get_unique_inner_full_lemma st tab f

val entry_exists_full_lemma
    (#stt: state_type) (#te: Type0)
    (#rt: Type0)
    (st0: state stt)
    (tn: table_name (tt_of stt) te)
    (filter: table_filter_full_same_state stt te rt)
    : Lemma (same_state_except_entropy st0 (entry_exists_full st0 tn filter)._1)

let entry_exists_full_lemma st tn f = 
  get_unique_full_lemma st tn f


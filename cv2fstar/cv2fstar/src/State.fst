module State

open CVTypes
open Random
open FStar.Ghost

module Map = SidMap
module Set = FStar.Set

(* Preliminary declarations for sessions *)

noeq
type session_type =
  (* oracle name *)
  (* session state entry *)
  (* following_oracles *)
  (* session_entry_to_name *)
  | SessionType : on: eqtype -> se: Type0 -> fo: (on -> list on) -> se2on: (se -> on)
    -> session_type

let oracle_name (st: session_type) = match st with | SessionType on _ _ _ -> on

let session_entry (st: session_type) = match st with | SessionType _ se _ _ -> se

let is_valid_entry (#st: session_type) (on: oracle_name st) (se: session_entry st) =
  match st with | SessionType _ _ _ se2on -> (se2on se = on)

let is_valid_next_entry (#st: session_type) (on: oracle_name st) (se: session_entry st) =
  match st with
  | SessionType _ _ fo se2on ->
    let nsn = se2on se in
    FStar.List.Tot.Base.mem nsn (fo on)

let session_entry_to_name (#st: session_type) (se: session_entry st) : oracle_name st =
  match st with | SessionType _ _ _ se2on -> se2on se

val sessions (st: session_type) : Type0
let sessions (st: session_type) = Map.t (list (session_entry st))

val init_sessions (st: session_type) : sessions st
let init_sessions st = Map.empty []

// each protocol instantiates this type and uses it with state
noeq
type state_type =
  | StateType : tt: Type0 -> vt: Type0 -> st: session_type -> event_type: Type0 -> debug: bool -> state_type

let tt_of (stt: state_type) : Type0 = match stt with | StateType tt _ _ _ _ -> tt

let vt_of (stt: state_type) : Type0 = match stt with | StateType _ vt _ _ _ -> vt

let st_of (stt: state_type) : session_type = match stt with | StateType _ _ st _ _ -> st

let et_of (stt: state_type) : Type0 = match stt with | StateType _ _ _ et _ -> et

let db_of (stt: state_type) : bool = match stt with | StateType _ _ _ _ db -> db

(* Erase if not debug *)

let opt_erased (stt: state_type) (ty: Type0) : Type0 =
  if db_of stt then ty else Ghost.erased ty

let opt_hide (stt: state_type) (#ty: Type0) (v: ty) : opt_erased stt ty =
  if db_of stt then v else Ghost.hide v

let opt_reveal (stt: state_type) (#ty: Type0) (v: opt_erased stt ty) : GTot ty =
  if db_of stt then v else Ghost.reveal v

noeq
type state (stt: state_type) =
  { ent: entropy;
    tabs: tt_of stt;
    vars: vt_of stt;
    s: sessions (st_of stt);
    evs: opt_erased stt (list (et_of stt));
    local: erased (Set.set SidMap.sidt) }

val entropy_of: #stt: state_type -> state stt -> entropy
let entropy_of st = st.ent

val tables_of: #stt: state_type -> state stt -> tt_of stt
let tables_of st = st.tabs

val variables_of: #stt: state_type -> state stt -> vt_of stt
let variables_of st = st.vars

val sessions_of: #stt: state_type -> state stt -> sessions (st_of stt)
let sessions_of st = st.s

val events_of: #stt: state_type -> state stt -> GTot (list (et_of stt))
let events_of #stt st = opt_reveal stt st.evs

val local_of: #stt: state_type -> state stt -> GTot (Set.set SidMap.sidt)
let local_of st = st.local

(* Sessions *)

val state_upd_sessions (#stt: state_type) (st0: state stt) (s: sessions (st_of stt)) : state stt
let state_upd_sessions st sessions = { st with s = sessions }

let _get_remove_session_entry #stt st sn id rm_flag =
  let seso = Map.get (sessions_of st) id in
  match seso with
  | None -> st, None
  | Some ses ->
    let seo = List.Tot.find (fun se -> is_valid_entry sn se) ses in
    if rm_flag
    then
      // construct a list without the entry
      (let ses_new = List.Tot.filter (fun se -> not (is_valid_entry sn se)) ses in
        (let m = Map.upd (sessions_of st) id ses_new in
          state_upd_sessions #stt st m, seo))
    else (st, seo)

val get_session_entry (#stt: state_type) (st: state stt) (on: oracle_name (st_of stt)) (id: SidMap.sidt)
    : Tot (option (s: (session_entry (st_of stt)){is_valid_entry on s}))

let get_session_entry #stt st sn id =
  let _, seo = _get_remove_session_entry #stt st sn id false in
  seo

val get_and_remove_session_entry
      (#stt: state_type)
      (st: state stt)
      (on: oracle_name (st_of stt))
      (id: SidMap.sidt)
    : Tot (state stt * option (s: (session_entry (st_of stt)){is_valid_entry on s}))

let get_and_remove_session_entry #stt st sn id = _get_remove_session_entry #stt st sn id true

val state_reserve_session_id (#stt: state_type) (st0: state stt) : state stt * SidMap.sidt

let state_reserve_session_id st0 =
  let m = sessions_of st0 in
  let id, m = Map.reserve m in
  { st0 with s = m; local = Set.add id (local_of st0) }, id

val state_add_to_session
      (#stt: state_type)
      (st0: state stt)
      (id: SidMap.sidt)
      (ses: list (session_entry (st_of stt)))
    : Tot (state stt)

let state_add_to_session st id ses =
  let m = sessions_of st in
  match Map.get m id with
  | None ->
    let m = Map.upd m id ses in
    state_upd_sessions st m
  | Some old_ses ->
    let new_ses = List.Tot.append old_ses ses in
    let m = Map.upd m id new_ses in
    state_upd_sessions st m

(* There is no way to iterate over all keys on which the map is defined,
   so we require the caller to tell us which session id they want. *)
val print_session
      (#stt: state_type)
      (st: state stt)
      (pe: (session_entry (st_of stt) -> FStar.All.ML unit))
      (id: SidMap.sidt)
    : FStar.All.ML unit

let print_session st print_entry id =
  SidMap.print_sid id;
  IO.print_string ".";
  let seso = Map.get (sessions_of st) id in
  match seso with
  | None
  | Some [] ->
    IO.print_string "session does not exist";
    IO.print_newline ()
  | Some ses -> List.iter (fun se -> print_entry se) ses

(* Entropy *)

val call_with_entropy: #stt: state_type -> #a: Type -> st0: state stt -> (entropy -> entropy * a)
  -> state stt * a

let call_with_entropy st0 f =
  let ent0 = entropy_of st0 in
  let ent1, res = f ent0 in
  { st0 with ent = ent1 }, res

(* For tables and variables *)

noeq
type accessor (r: Type0) (tv: Type0) =
  { get: r -> tv; set: r -> tv -> r } 

(* Tables *)

// Abbreviations:
// tt: table type
// tn: table name
// tns: table names
// te: table entry

val table (t: Type0) : Type0
let table t = list t

val table_empty (t: Type0) : table t
let table_empty t = []

type table_name (r: Type0) (te: Type0) = accessor r (table te)

let return_table (#stt: state_type) (#te: Type0) (st: state stt) (tn: table_name (tt_of stt) te) : table te =
  tn.get (tables_of st)

type table_filter_simple (te: Type0) (rt: Type0) = 
   te -> option rt

type table_filter_full (stt: state_type) (te: Type0) (rt: Type0) = 
   state stt -> te -> state stt * option rt

noeq
type table_filter (stt: state_type) (te: Type0) (rt: Type0) =
  | TableFilterSimple : table_filter_simple te rt -> table_filter stt te rt
  | TableFilterFull : table_filter_full stt te rt -> table_filter stt te rt
  
val insert:
    #stt: state_type -> #te: Type0 ->
    st0: state stt ->
    tn: table_name (tt_of stt) te ->
    te
  -> state stt

let insert #stt st tn te =
  let tab = return_table st tn in
  let new_tab = te :: tab in
  { st with tabs = tn.set (tables_of st) new_tab }

val get_inner_simple
      (#te: Type0)
      (#rt: Type0)
      (matches: Seq.seq rt)
      (tab: table te)
      (filter: table_filter_simple te rt)
    : Tot (Seq.seq rt) (decreases tab)

let rec get_inner_simple matches tab f =
  match tab with
  | [] -> matches
  | x :: r -> 
    match f x with
    | Some e -> get_inner_simple (Seq.cons e matches) r f
    | None -> get_inner_simple matches r f

val get_inner_full
      (#stt: state_type) (#te: Type0)
      (#rt: Type0)
      (st0: state stt)
      (matches: Seq.seq rt)
      (tab: table te)
      (filter: table_filter_full stt te rt)
    : Tot (state stt * Seq.seq rt)
      (decreases tab)

let rec get_inner_full st matches tab f =
  match tab with
  | [] -> st, matches
  | x :: r -> 
    match f st x with
    | st, Some e -> get_inner_full st (Seq.cons e matches) r f
    | st, None -> get_inner_full st matches r f

val get:
    #stt: state_type -> #te: Type0 ->
    #rt: Type0 ->
    st0: state stt ->
    tn: table_name (tt_of stt) te ->
    table_filter stt te rt
  -> state stt * option rt

let get #stt st tn tf =
  let tab = return_table st tn in
  let st, matches =
    (match tf with
      | TableFilterSimple f -> st, get_inner_simple Seq.empty tab f
      | TableFilterFull f -> get_inner_full st Seq.empty tab f)
  in
  match Seq.length matches with
  | 0 -> st, None
  | 1 -> st, Some (Seq.head matches)
  | _ ->
    let st, rand_i = call_with_entropy st (gen_nat (Seq.length matches - 1)) in
    st, Some (Seq.index matches rand_i)

val get_unique_inner_simple
      (#te: Type0)
      (#rt: Type0)
      (tab: table te)
      (filter: table_filter_simple te rt)
    : Tot (option rt) (decreases tab)

let rec get_unique_inner_simple tab f =
  match tab with
  | [] -> None
  | x :: r -> 
    match f x with
    | Some e -> Some e
    | None -> get_unique_inner_simple r f

val get_unique_inner_full
      (#stt: state_type) (#te: Type0)
      (#rt: Type0)
      (st0: state stt)
      (tab: table te)
      (filter: table_filter_full stt te rt)
    : Tot (state stt * option rt)
      (decreases tab)

let rec get_unique_inner_full st tab f =
  match tab with
  | [] -> st, None
  | x :: r -> 
    match f st x with
    | st, Some e -> st, Some e
    | st, None -> get_unique_inner_full st r f

val get_unique_full:
    #stt: state_type -> #te: Type0 ->
    #rt: Type0 ->
    st0: state stt ->
    tn: table_name (tt_of stt) te ->
    table_filter_full stt te rt
  -> state stt * option rt

let get_unique_full #stt st tn f =
  let tab = return_table st tn in
  (* CryptoVerif proves that it is not possible to have multiple matches, so we
     can return after finding a first (and thus the only) match. *)
  get_unique_inner_full st tab f

(* get[unique] with a simple filter does not use randomness and thus does not need entropy. *)
val get_unique:
    #stt: state_type -> #te: Type0 ->
    #rt: Type0 ->
    st0: state stt ->
    tn: table_name (tt_of stt) te ->
    table_filter_simple te rt 
  -> option rt

let get_unique #stt st tn f =
  let tab = return_table st tn in
  (* CryptoVerif proves that it is not possible to have multiple matches, so we
     can return after finding a first (and thus the only) match. *)
  get_unique_inner_simple tab f

(* if on the CV side we do not use the entry but only its existence *)
val entry_exists_full:
    #stt: state_type -> #te: Type0 ->
    #rt: Type0 ->
    st0: state stt ->
    tn: table_name (tt_of stt) te ->
    table_filter_full stt te rt
  -> state stt * bool

let entry_exists_full st tn f =
  match get_unique_full st tn f with
  | st, None -> st, false
  | st, Some _ -> st, true

val entry_exists:
    #stt: state_type -> #te: Type0 ->
    #rt: Type0 ->
    state stt ->
    tn: table_name (tt_of stt) te ->
    table_filter_simple te rt
  -> bool

let entry_exists st tn f =
  match get_unique st tn f with
  | None -> false
  | Some _ -> true

val print_table:
    #stt: state_type -> #te: Type0 ->
    state stt ->
    tn: table_name (tt_of stt) te ->
    tns: string ->
    (te -> FStar.All.ML unit)
  -> FStar.All.ML unit

let print_table #stt st tn tns print_entry =
  let tab = return_table st tn in
  IO.print_string (tns ^ " [");
  IO.print_newline ();
  List.iter print_entry tab;
  IO.print_string "]";
  IO.print_newline ();
  IO.print_newline ()

(* Variables *)

val var (t: Type0) : Type0
let var t = FStar.PartialMap.t SidMap.sidt t

val var_empty (t: Type0) : var t
let var_empty t = FStar.PartialMap.empty SidMap.sidt t

type var_name (r: Type0) (te: Type0) = accessor r (var te)

val var_bool_set : (#stt: state_type) -> state stt -> (accessor (vt_of stt) bool) -> state stt
let var_bool_set st vn =
  { st with vars = vn.set (variables_of st) true }
  
val var_def (#stt: state_type) (#t: Type0) (st: state stt) (vn: var_name (vt_of stt) t) (idx: SidMap.sidt) (v: t) : state stt
let var_def st vn idx v =
  let vars = variables_of st in
  { st with vars = vn.set vars (FStar.PartialMap.upd (vn.get vars) idx v) }

val var_bool_get : (#stt: state_type) -> state stt -> (accessor (vt_of stt) bool) -> bool
let var_bool_get st vn =
  vn.get (variables_of st)

val var_get (#stt: state_type) (#t: Type0) (st: state stt) (vn: var_name (vt_of stt) t) (idx:SidMap.sidt) : option t
let var_get st vn idx =
  let m = vn.get (variables_of st) in
  FStar.PartialMap.sel m idx

val indist_real (#stt: state_type) (#t: Type0) (st: state stt) (vn: var_name (vt_of stt) t) (idx:SidMap.sidt) : state stt * option t
let indist_real st vn idx =
  (st, var_get st vn idx)

val indist_ideal (#stt: state_type) (#t: Type0) (st: state stt) (vn: var_name (vt_of stt) t) (vnqueried: var_name (vt_of stt) t) (rand: entropy -> entropy * t) (idx:SidMap.sidt) : state stt * option t
let indist_ideal st vn vnqueried rand idx =
  match var_get st vn idx with
  | None -> (st, None)
  | Some _ -> 
      match var_get st vnqueried idx with
      | Some v -> (st, Some v)
      | None ->
          let (st', v) = call_with_entropy st rand in
          let st'' = var_def st' vnqueried idx v in
          (st'', Some v)

val indist_one_session_real (#stt: state_type) (#t: Type0) (st: state stt) (vn: var_name (vt_of stt) t) (vnqueried: accessor (vt_of stt) bool) (idx:SidMap.sidt) : state stt * option t
let indist_one_session_real st vn vnqueried idx =
  if var_bool_get st vnqueried then
    (* already queried *) (st, None)
  else
    let st' = var_bool_set st vnqueried in
    (st', var_get st' vn idx)

val indist_one_session_ideal (#stt: state_type) (#t: Type0) (st: state stt) (vn: var_name (vt_of stt) t) (vnqueried: accessor (vt_of stt) bool) (rand: entropy -> entropy * t) (idx:SidMap.sidt) : state stt * option t
let indist_one_session_ideal st vn vnqueried rand idx =
  if var_bool_get st vnqueried then
    (* already queried *) (st, None)
  else
    let st' = var_bool_set st vnqueried in
    match var_get st' vn idx with
    | Some _ ->
        let (st'', v) = call_with_entropy st' rand in
        (st'', Some v)
    | None -> (st', None)

val reach_reveal (#stt: state_type) (#t: Type0) (st: state stt) (vn: var_name (vt_of stt) t) (vnqueried: var_name (vt_of stt) unit) (idx: SidMap.sidt) : state stt * option t
let reach_reveal st vn vnqueried idx =
  match var_get st vn idx with
  | None -> (st, None)
  | Some v ->
      let st' = var_def st vnqueried idx () in
      (st', Some v)

val reach_test (#stt: state_type) (#t: Type0) (st: state stt) (vn: var_name (vt_of stt) t) (vnqueried: var_name (vt_of stt) unit) (idx: SidMap.sidt) : option t
let reach_test st vn vnqueried idx =
  match var_get st vnqueried idx with
  | Some _ -> (* already revealed *) None
  | None -> var_get st vn idx 
      
(* Events *)

val state_add_event (#stt: state_type) (st: state stt) (ev: et_of stt) : Tot (r: state stt
 { (sessions_of r == sessions_of st) /\
   (local_of r == local_of st) /\
   (tables_of r == tables_of st) /\
   (variables_of r == variables_of st) /\
   (events_of r == List.append (events_of st) [ev]) })
let state_add_event #stt st ev =
  if db_of stt then
    { st with evs = List.append st.evs [ev] }
  else
    { st with evs = Ghost.bind st.evs (fun l -> Ghost.hide (List.append l [ev])) }
    
val print_events (#stt: state_type) (st: state stt) (pe: (et_of (stt) -> FStar.All.ML unit))
    : FStar.All.ML unit
let print_events #stt st print_event =
  if db_of stt then
    begin
      IO.print_string "Events: [\n";
      List.iter (fun e -> print_event e) st.evs;
      IO.print_string "]"
    end
  else
    IO.print_string "Events can be printed only in debug mode"
    
(* Local *)

val is_local: #stt: state_type -> SidMap.sidt -> state stt -> GTot bool
let is_local sid st = Set.mem sid (local_of st)

val is_session: #stt: state_type -> SidMap.sidt -> state stt -> bool
let is_session sid st = not (SidMap.available (sessions_of st) sid)

val remove_local: #stt: state_type -> state stt -> state stt
let remove_local st = { st with local = Set.empty #SidMap.sidt }

val inverse_local: #stt: state_type -> state stt -> state stt
let inverse_local st = { st with local = Set.intersect
  (Set.intension (fun sid -> is_session sid st))
  (Set.complement (local_of st)) }

let lemma_inverse (#stt: state_type) (sid: SidMap.sidt) (st: state stt):
  Lemma (requires (is_local sid (inverse_local st))) (ensures (is_session sid (inverse_local st)))
  [SMTPat (is_local sid (inverse_local st))]
 = Set.mem_intersect sid (Set.intension (fun sid -> is_session sid st)) (Set.complement (local_of st));
   Set.mem_intension sid (fun sid -> is_session sid st)
  

(* Init and finalize state *)

val init_state: stt: state_type -> Lib.RandomSequence.entropy -> tt_of stt -> vt_of stt -> state stt
let init_state stt ent init_tables init_vars =
  { ent = initialize_entropy ent;
    tabs = init_tables;
    vars = init_vars;
    s = init_sessions (st_of stt);
    evs = opt_hide stt [];
    local = Set.empty #SidMap.sidt }

val finalize_state: #stt: state_type -> state stt -> Lib.RandomSequence.entropy
let finalize_state state = finalize_entropy (entropy_of state)



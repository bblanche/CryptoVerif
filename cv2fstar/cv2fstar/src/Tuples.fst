module Tuples

open CVTypes

module List = FStar.List.Tot
module String = FStar.String
module Char = FStar.Char
module B = Lib.ByteSequence
open FStar.Mul
open Lib.IntTypes

#set-options "--z3rlimit 25"

val char4_of_int (n: uint32) : Tot (B.lbytes 4)
let char4_of_int n = B.uint_to_bytes_le n


val int_of_char4 (s: B.bytes) (i: nat{Seq.length s >= i + 4}) : Tot (uint32)
let int_of_char4 s i =
  let length_bytes = Seq.slice s i (i + 4) in
  B.uint_from_bytes_le length_bytes

val compos_rec: list bytes -> bytes
let rec compos_rec l =
  match l with
  | [] -> B.bytes_empty
  | e :: ql ->
      Seq.append e (compos_rec ql)

val compos: tagt -> list bytes -> bytes
let compos tag l =
  Seq.append (char4_of_int tag) (compos_rec l)

val check_tag: tagt -> (b:bytes) ->
  option (new_pos:nat { new_pos <= Seq.length b })
let check_tag tag b =
  if Seq.length b >= 4 then
    if uint_v tag = uint_v (int_of_char4 b 0)
    then Some 4
    else None
  else None

val lemma_check_tag (t: tagt) (l: list bytes) (buf: bytes) :
  Lemma (requires (buf == compos t l))
        (ensures (match check_tag t buf with
         | None -> False
	 | Some pos -> 
             Seq.length buf == pos + Seq.length (compos_rec l) /\
	     Seq.slice buf pos (pos + Seq.length (compos_rec l)) == compos_rec l))
let lemma_check_tag t l buf =
  Seq.append_slices (char4_of_int t) (compos_rec l);
  B.lemma_uint_to_from_bytes_le_preserves_value #U32 #SEC t

val lemma_check_tag_rev (t:tagt) (buf: bytes) :
  Lemma (match check_tag t buf with
         | None -> True
	 | Some pos ->
	     Seq.slice buf 0 pos == char4_of_int t)
let lemma_check_tag_rev t buf =
  match check_tag t buf with
  | None -> ()
  | Some pos ->
      let tagseq = Seq.slice buf 0 pos in
      B.lemma_uint_from_to_bytes_le_preserves_value #U32 #SEC tagseq 

(* Lemmas *)

val get_tag (b:bytes) : option tagt
let get_tag b =
  if Seq.length b < 4 then
    None
  else
    Some (int_of_char4 b 0)

val lemma_get_tag_compos (t: tagt) (l: list bytes) :
  Lemma (get_tag (compos t l) == Some t)
let lemma_get_tag_compos t l =
  Seq.append_slices (char4_of_int t) (compos_rec l);
  B.lemma_uint_to_from_bytes_le_preserves_value #U32 #SEC t

val lemma_diff_compos (t1: tagt) (t2: tagt) (l1: list bytes) (l2: list bytes):
  Lemma (requires (t1 =!= t2))
        (ensures (compos t1 l1 =!= compos t2 l2))
let lemma_diff_compos t1 t2 l1 l2 =
  lemma_get_tag_compos t1 l1;
  lemma_get_tag_compos t2 l2


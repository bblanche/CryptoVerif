module SidMap

open CVTypes

(* Type for session ids *)
val sidt: eqtype

(* Serialization for replication indices *)
val serialize_sidt: serialize_type sidt
val deserialize_sidt: deserialize_type sidt

(* Lemmas for (de)serialization *)
val lemma_deser_ok_sidt: deser_ok sidt serialize_sidt deserialize_sidt

val lemma_deser_too_short_sidt: deser_too_short sidt serialize_sidt deserialize_sidt

val lemma_deser_rev_sidt: deser_rev sidt serialize_sidt deserialize_sidt

(* Printing session ids *)
val print_sid: sidt -> FStar.All.ML unit
val print_label_sid: string -> sidt -> bool -> FStar.All.ML unit

(* Map session id -> value: The main type provided by this module *)
val t (value: Type u#a) : Type u#a

val available : #value: Type -> t value -> id: sidt -> Tot bool

(* [get m k] returns [Some v] when [m] maps [k] to [v];
   [None] when [m] has no mapping for [k] *)
val get: #value: Type -> t value -> sidt -> Tot (option value)

(* empty v : A map with empty domain. v is needed as a dummy value. *)
val empty: #value: Type -> value ->
  Tot (m: t value { forall (id: sidt). available m id })

(* reserve m returns an available entry and the new map *)
val reserve: #value: Type -> m: t value ->
  Tot (r:(sidt * t value) {available m r._1 /\ (forall (id: sidt). available r._2 id <==> id =!= r._1 /\ available m id) /\ (forall (id: sidt). get r._2 id == get m id) })

(* upd m k v : A map identical to `m` except mapping `k` to `v` *)
val upd: #value: Type -> m: t value -> x: sidt -> v: value -> Tot (m': t value { (forall (id: sidt). available m' id <==> available m id) /\ (get m' x == Some v) /\ (forall (id:sidt { id =!= x }). get m' id == get m id) })

type inv (#value: Type) (m: t value) =
  (forall (id:sidt). available m id ==> get m id == None)

val lem_empty (#value: Type) (v: value):
  Lemma (inv (empty v))
  
val lem_reserve (#value: Type) (m: t value { inv m }) :
  Lemma (inv (reserve m)._2)

val lem_upd (#value: Type) (m: t value { inv m }) (n: sidt { ~(available m n) }) (v: value) :
  Lemma (inv (upd m n v))


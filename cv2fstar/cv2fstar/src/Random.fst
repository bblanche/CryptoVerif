module Random

open CVTypes

(* Accuracy parameter for generating random integers.
   The greater the closer the distribution to uniform. *)
let accuracy = 10

module B = Lib.ByteSequence
open Lib.Sequence
open Lib.RandomSequence

type entropy = Lib.RandomSequence.entropy 

let initialize_entropy ent = ent

let finalize_entropy ent = ent

val crypto_random_unbounded: entropy -> len: nat
  -> Tot (entropy * b: bytes{Seq.length b = len})
      (decreases len)

let rec crypto_random_unbounded ent len =
  if len > Lib.IntTypes.max_size_t
  then
    let ent1, bytes1 = crypto_random ent Lib.IntTypes.max_size_t in
    let ent2, bytes2 = crypto_random_unbounded ent1 (len - Lib.IntTypes.max_size_t) in
    ent2, Seq.append bytes1 bytes2
  else
    let ent3, bytes3 = crypto_random ent len in
    ent3, bytes3

val log_2_lt (x: pos) : Lemma (FStar.Math.Lib.log_2 x < x)
let rec log_2_lt x =
  match x with
  | 1 -> ()
  | 2 -> ()
  | _ -> log_2_lt (x / 2)

val pow2log (n:nat{ n > 0 }) :
   Lemma(pow2((FStar.Math.Lib.log_2 n) + 1) >= n +1)

let rec pow2log n =
  if n = 1 then () else
  pow2log (n / 2)

let gen_nat max ent =
  log_2_lt max;
  let k_num_bits:nat = 1 + FStar.Math.Lib.log_2 max in
  let k_num_bytes:nat = k_num_bits / 8 + (if k_num_bits % 8 > 0 then 1 else 0) in
  assert (op_Multiply 8 k_num_bytes >= k_num_bits);
  FStar.Math.Lemmas.pow2_le_compat (op_Multiply 8 k_num_bytes) k_num_bits;
  pow2log max;
  assert (pow2 (op_Multiply 8 k_num_bytes) - 1 >= max);
  (* The accuracy is to reduce skew in the distribution. *)
  let num_bytes = k_num_bytes + accuracy in
  let ent2, r = crypto_random_unbounded ent num_bytes in
  let n_raw = Lib.ByteSequence.nat_from_bytes_le #Lib.IntTypes.SEC r in
  let n = n_raw % (max + 1) in
  (ent2, n)

let gen_lbytes num ent =
  crypto_random ent num 

let gen_bool ent =
  let ent2, byte = crypto_random ent 1 in
  let n = Lib.ByteSequence.nat_from_bytes_le #Lib.IntTypes.SEC byte in
  ent2, n % 2 <> 0

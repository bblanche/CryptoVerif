module Crypto

open CVTypes

module HPKE = Spec.Agile.HPKE
module DH = Spec.Agile.DH
module AEAD = Spec.Agile.AEAD
module Hash = Spec.Agile.Hash
module HKDF = Spec.Agile.HKDF

open Lib.IntTypes
module B = Lib.ByteSequence
open Lib.Sequence
open Lib.PrintSequence
open FStar.All

let aead_alg = AEAD.CHACHA20_POLY1305
let cs:HPKE.ciphersuite = (DH.DH_Curve25519, Hash.SHA2_256, HPKE.Seal aead_alg, Hash.SHA2_256)

(* Types *)

let plaintext = AEAD.plain aead_alg
let pkey = HPKE.key_dh_secret_s cs
let skey = HPKE.key_dh_public_s cs
let ciphertext = HPKE.key_dh_public_s cs * AEAD.cipher aead_alg
let keypair = pkey * skey

#set-options "--z3rlimit 75"

(* Asymmetric encryption / decryption *)

val get_pk: skey -> pkey

let get_pk sk = match DH.secret_to_public DH.DH_Curve25519 sk with | Some pk -> pk

val enc: plaintext -> pkey -> skey -> option ciphertext

let enc plain pk skE =
  match HPKE.sealBase cs skE pk B.lbytes_empty B.lbytes_empty plain with
  | None -> None
  | Some (pkE, c) -> Some (pkE, c)

val dec: ciphertext -> skey -> option bytes

let dec ciphertext sk =
  let enc, c = ciphertext in
  match HPKE.openBase cs enc sk B.lbytes_empty B.lbytes_empty c with
  | None -> None
  | Some p -> Some p

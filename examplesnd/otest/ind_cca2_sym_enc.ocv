(* Trivial running example for conference paper. *)

param N.
param N2.
param N3.

type key [fixed,large].
type seed [fixed].

fun keyToBitstring(key):bitstring [data].

(* Shared-key encryption (CPA Stream cipher) *)

proba Penc.

expand sym_enc_all_args(key, bitstring, bitstring, seed, enc, enc_r, dec, injbot).
expand IND_CCA2_prop_sym_enc(key, bitstring, bitstring, seed, enc, enc_r, enc_r', dec, dec', injbot, Z, Penc).

(* The function Z returns for each bitstring, a bitstring
   of the same length, consisting only of zeroes. *)
const Zkey:bitstring.
equation forall y:key; 
	Z(keyToBitstring(y)) = Zkey.

(* Queries *)

query secret k2 [cv_onesession].
query secret k3 [cv_onesession].

query secret k2.
query secret k3.





process 
	Ostart() :=
Kab <-R key;
return();
((
  foreach iA <= N do
  OA() :=
  k2 <-R key;
  ea1: bitstring <- enc(keyToBitstring(k2), Kab);
  return(ea1)
) | (
  foreach iB <= N do
  OB(ea: bitstring) :=
  let injbot(keyToBitstring(k3: key)) = dec(ea, Kab) in
  return()
))



(* EXPECTED
Warning: Foreach uses the same parameter N with oracle OB as foreach at line 42, characters 17-18 with oracle OA. Avoid reusing parameters for multiple foreach with different oracles to avoid losing precision in the probability bound.
RESULT Could not prove secrecy of k3; one-session secrecy of k3.
0.049s (user 0.049s + system 0.000s), max rss 20344K
END *)

(*
When performing several merges together, with a common condition
(typically, if diff_bit then ... else ...), I could compute the
"ok_arrays_first/second_branch" together for all merges with the same
condition, instead of separately for each merge. That would allow
more renamings of array variables.

That would allow merging this example without first moving
"if b then" to the top. 
*)


proof {
insert after "b <-R" "if b then";
simplify;
merge_branches;
success
}

param N2.

type v [bounded, large].

query secret b [cv_bit].

let O1(b0: bool) =
	f1() :=
		if b0 then
			find u = i <= N2 suchthat defined(v_2[i]) then
				return(v_2[u])
			else
				v_1 <-R v;
				return(v_1)
		else
			find u = i <= N2 suchthat defined(v_3[i]) then
				return(v_3[u])
			else
				v_4 <-R v;
				return(v_4).

let O2(b0: bool) =
	foreach i2 <= N2 do
	f2() :=
		if b0 then
			if defined(v_1) then
				return(v_1)
			else
				v_2 <-R v;
				return(v_2)
		else
			if defined(v_4) then
				return(v_4)
			else
				v_3 <-R v;
				return(v_3).

process
	Ostart() :=
	b <-R bool;
	return;
	(run O1(b) | run O2(b))

(* EXPECTED
All queries proved.
0.047s (user 0.039s + system 0.008s), max rss 20056K
END *)

(* CryptoVerif cannot prove this correspondence
and this is what we want: when we know that e1(f_zero(x))
is executed, x can actually have any value, so the
correspondence would require that e2(x) is executed for all x,
which is not true, it is executed for one x. *)

fun f_zero(bitstring):bitstring.
const zero : bitstring.

(* equation forall x: bitstring; f_zero(x) = zero. *)

event e1(bitstring).
event e2(bitstring).

query x: bitstring; event(e1(f_zero(x))) ==> event(e2(x)).

channel c.

process
	in(c, x: bitstring);
	event e2(x);
	event e1(f_zero(x));
	out(c, x)

(* EXPECTED
Warning: could not prove that the term x in the conclusion of a correspondence is determined by the hypothesis of the correspondence: it might take several different values for the same events in the hypothesis
RESULT Could not prove forall x: bitstring; event(e1(f_zero(x))) ==> event(e2(x)).
0.046s (user 0.038s + system 0.008s), max rss 20160K
END *)

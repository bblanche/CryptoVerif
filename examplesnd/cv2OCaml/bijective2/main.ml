open Base
open Crypto

let _ = 
  let f = WLSK_Init.init() in
  let (), r = f("NONCEXXX") in
  if r = "A" then print_string "CORRECT: " else print_string "ERROR: ";
  print_string r;
  print_newline()

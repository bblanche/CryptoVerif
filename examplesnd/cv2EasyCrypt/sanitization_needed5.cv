(* EasyCrypt requires ids starting with a lowercase character for variables *)

def IND_CCA2_public_key_enc_all_args_EC(keyseed, pkey, skey, cleartext, ciphertext, enc_seed, skgen, skgen2, pkgen, pkgen2, enc, enc_r, enc_r2, dec, dec2, injbot, Z, Penc, Penccoll) {

param N, N', N2, N3, N4.

fun enc_r(cleartext, pkey, enc_seed): ciphertext.
fun skgen(keyseed):skey.
fun pkgen(keyseed):pkey.
fun dec(ciphertext, skey): bitstringbot.

fun enc_r2(cleartext, pkey, enc_seed): ciphertext.
fun skgen2(keyseed):skey.
fun pkgen2(keyseed):pkey.
fun dec2(ciphertext, skey): bitstringbot.

letfun enc(m: cleartext, pk: pkey) =
       r <-R enc_seed; enc_r(m,pk,r).

fun injbot(cleartext):bitstringbot [data].

(* The function Z returns for each bitstring, a bitstring
   of the same length, consisting only of zeroes. *)
fun Z(cleartext):cleartext.

equation forall m:cleartext, r:keyseed, r2:enc_seed; 
	dec(enc_r(m, pkgen(r), r2), skgen(r)) = injbot(m).
equation forall m:cleartext, r:keyseed, r2:enc_seed; 
	dec2(enc_r2(m, pkgen2(r), r2), skgen2(r)) = injbot(m).


(* Version with N enc oracle calls *)

equiv(eqec)
       r <-R keyseed; (
           Opk() := return(pkgen(r)) |
	   foreach i2 <= N2 do Odec(c:ciphertext) := return(dec(c, skgen(r))) |
           foreach i <= N do r1 <-R enc_seed; Oenc(m:cleartext) [useful_change] := return(enc_r(m, pkgen(r),r1)))
     <=(N * Penc(time + (N-1) * time(enc_r, maxlength(m)), N2))=> [manual]
       r <-R keyseed; (
           Opk() := return(pkgen(r)) |
	   foreach i2 <= N2 do Odec(c:ciphertext) :=
                find J <= N suchthat defined(c1[J],m[J]) && c = c1[J] then return(injbot(m[J])) else
		return(dec(c, skgen(r))) |
	   foreach i <= N do r1 <-R enc_seed; Oenc(m:cleartext) :=
			c1:ciphertext <- enc_r2(Z(m), pkgen(r), r1);
			return(c1)).

}

(* Usage of the macro *)

type pkey [bounded].
type skey [bounded].
type keyseed [large,fixed].
type cleartext.
type ciphertext.
type enc_seed [bounded].

proba Penc.
proba Penccoll.

expand IND_CCA2_public_key_enc_all_args_EC(keyseed, pkey, skey, cleartext, ciphertext, enc_seed, skgen, skgen2, pkgen, pkgen2, enc, enc_r, enc_r2, dec, dec2, injbot, Z, Penc, Penccoll).

process 0


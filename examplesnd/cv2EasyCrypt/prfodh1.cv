def PRF_ODH1EC(G, Z, prf_in, prf_out, g, exp, exp', mult, prf, pPRF_ODH) {

fun prf(G, prf_in): prf_out.

(* The PRF-ODH assumption *)

event ev_abort.

param na, na1, na2, nb, nb1, nb2, naDH9, nbDH9. 

table prf_dh_val(na, nb, prf_in, prf_out).

equiv(eqec)
    foreach ia <= na do a <-R Z; (
      OA() := return(exp(g,a)) | 
      Oa() [10] := return(a) |
      foreach ia2 <= na2 do Oa2(jb <= nb, xa2: prf_in) [useful_change] := return(prf(exp(g, mult(b[jb], a)), xa2)) |
      foreach iaDH9 <= naDH9 do ODHa9(x:Z) [2] := return(exp(g, mult(a, x)))
    ) |
    foreach ib <= nb do b <-R Z; (
      OB() := return(exp(g,b)) |
      Ob() [10] := return(b) |
      foreach ib2 <= nb2 do Ob2(ja <= na, xb2: prf_in) := return(prf(exp(g, mult(a[ja], b)), xb2)) |
      foreach ibDH9 <= nbDH9 do ODHb9(x:Z) [2] := return(exp(g, mult(b, x)))
    )
<=(na * nb * pPRF_ODH(time +
     (na + nb + #Oa2 + #Ob2 - 3 + #ODHa9 + #ODHb9)*time(exp)+
     (#Oa2 + #Ob2 - 1)*time(prf, max(maxlength(xa2), maxlength(xb2))),
     na2 + nb2))=>
    foreach ia <= na do a <-R Z; (
      OA() := return(exp'(g,a)) |
      Oa() :=
      	 (* Abort when a must not be compromised *)
	 get prf_dh_val(=ia, jb, x, c) in event_abort ev_abort else
         let ka:bool = true in return(a) |
      foreach ia2 <= na2 do Oa2(jb <= nb, xa2: prf_in) :=
	 if defined(kb[jb]) then (* b[jb] compromised *) return(prf(exp'(g, mult(b[jb], a)), xa2)) else
         if defined(ka) then (* a compromised *) return(prf(exp'(g, mult(b[jb], a)), xa2)) else
	 (* At this point, a and b[jb] are not compromised, and must never be compromised in the future *)
	 get[unique] prf_dh_val(=ia, =jb, =xa2, c) in return(c) else
         ca2 <-R prf_out;
	 insert prf_dh_val(ia, jb, xa2, ca2);
	 return(ca2) |
      foreach iaDH9 <= naDH9 do ODHa9(x:Z) := return(exp'(g, mult(a, x)))
    ) |
    foreach ib <= nb do b <-R Z; (
      OB() := return(exp'(g,b)) |
      Ob() :=
      	 (* Abort when b must not be compromised *)
	 get prf_dh_val(ja, =ib, x, c) in event_abort ev_abort else
         let kb:bool = true in return(b) |
      foreach ib2 <= nb2 do Ob2(ja <= na, xb2: prf_in) :=
	 if defined(ka[ja]) then (* a[ja] compromised *) return(prf(exp'(g, mult(a[ja], b)), xb2)) else
         if defined(kb) then (* b compromised *) return(prf(exp'(g, mult(a[ja], b)), xb2)) else
	 (* At this point, b and a[ja] are not compromised, and must never be compromised in the future *)
	 get[unique] prf_dh_val(=ja, =ib, =xb2, c) in return(c) else
         cb2 <-R prf_out;
	 insert prf_dh_val(ja, ib, xb2, cb2);
	 return(cb2) |
      foreach ibDH9 <= nbDH9 do ODHb9(x:Z) := return(exp'(g, mult(b, x)))
    ).
	 
}

(* Usage of the macro *)

type Z [large, bounded, nonuniform].
type elt [large, bounded, nonuniform].
type extracted [large, fixed].

expand DH_basic(elt, Z, G, exp, exp', mult).

proba pPRF_ODH.
expand PRF_ODH1EC(elt, Z, extracted, extracted, G, exp, exp', mult, PRF, pPRF_ODH).

process 0

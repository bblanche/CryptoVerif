module Application.Ideal

open CVTypes
open EQUIV.Types
friend EQUIV.Types
open State
open EQUIV.Functions
open EQUIV.Tables
open EQUIV.Ideal.Sessions
open EQUIV.Events
open EQUIV.Ideal.Protocol
open EQUIV.Ideal.Enc
open EQUIV.Ideal.Effect

// These are only needed for the definition of addrA and addrB
open Lib.IntTypes
open Lib.ByteSequence


inline_for_extraction
let size_block:size_nat = 16
let block_list:l: list uint8 {List.Tot.length l == size_block} =
  [@@ inline_let ]let l =
    [
      u8 0x41; u8 0x40; u8 0x6c; u8 0x6f; u8 0x63; u8 0x61; u8 0x6c; u8 0x68;
      u8 0x6f; u8 0x73; u8 0x74; u8 0x6f; u8 0x63; u8 0x61; u8 0x6c; u8 0x68
    ]
  in
  assert_norm (List.Tot.length l == size_block);
  l
let block:lbytes size_block = Seq.createL block_list

val testrun: unit -> CVSTInit unit 
let testrun() =
      print_string "Ideal\n";
      print_string "## Tables\n\n";
      equiv_print_tables();
      print_string "\n";
      match oracle_Oenc1 block block with
      | None ->
        print_string "ERROR Oenc fail\n"
      | Some (result) ->
        // Because we use dummy functions
     	if eq_lbytes result zero then
	  print_string "CORRECT\n"
	else
  	  print_string "ERROR Oenc bad result\n"

let run =
  init_and_run testrun


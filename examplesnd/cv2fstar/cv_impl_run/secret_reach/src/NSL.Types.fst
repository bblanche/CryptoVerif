module NSL.Types

open CVTypes

let size_nonce = 8

let _eq_nonce n1 n2 = eq_lbytes #size_nonce n1 n2


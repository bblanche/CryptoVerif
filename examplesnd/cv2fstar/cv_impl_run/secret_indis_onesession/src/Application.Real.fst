module Application.Real

open CVTypes
open EQUIV.Types
friend EQUIV.Types
open State
open EQUIV.Functions
open EQUIV.Tables
open EQUIV.Real.Sessions
open EQUIV.Events
open EQUIV.Real.Protocol
open EQUIV.Real.Gen
open EQUIV.Real.Effect
open EQUIV.Real.Queries

// These are only needed for the definition of addrA and addrB
open Lib.IntTypes
open Lib.ByteSequence


inline_for_extraction
let size_block:size_nat = 16
let block_list:l: list uint8 {List.Tot.length l == size_block} =
  [@@ inline_let ]let l =
    [
      u8 0x41; u8 0x40; u8 0x6c; u8 0x6f; u8 0x63; u8 0x61; u8 0x6c; u8 0x68;
      u8 0x6f; u8 0x73; u8 0x74; u8 0x6f; u8 0x63; u8 0x61; u8 0x6c; u8 0x68
    ]
  in
  assert_norm (List.Tot.length l == size_block);
  l
let block:lbytes size_block = Seq.createL block_list

val testrun: unit -> CVSTInit unit
let testrun() =
  print_string "Real\n";
  match oracle_Ogen() with
  | None ->
      print_string "ERROR Ogen fail\n"
  | Some (sid, key_id, key) ->
      print_string "## Tables After Calling Ogen\n\n";
      equiv_print_tables();
      print_string "\n";
      match indistvar_k0_0 sid with
      | None ->
        print_string "ERROR k0 read fail\n"
      | Some (result) ->
          if eq_lbytes result key then
	    begin
            match indistvar_k0_0 sid with
            | None -> 	  
                print_string "CORRECT\n"
            | Some (result2) ->
	        print_string "ERROR second query should not be accepted for onesession\n"
	    end
	  else
	    print_string "ERROR Should get k0 because of dummy xor\n"
	
let run =
  init_and_run testrun

(* This file shows that AES128-CFB128 is IND-CPA, when the IV is
computed as IV = E(k, 0; IVdata) provided the IVdata arguments are
pairwise distinct. 
It shows that the encryption of the messages is indistinguishable
from the encryption of zero-messages of the same length,
for 3 messages of 1, 2, 3 blocks respectively.
*)

set fstarDebug = true.

(* Do not assume by default that all constants are 
   different. We shall say it explicitly when they are different *)

set diffConstants = false.

type enc_key [fixed]. (* 128 bits AES key *)
type key_id [fixed, large].

(* Properties of XOR *)
expand Xor(enc_key, xor, Zero).

table keys(key_id, enc_key).

param Nk, Ne.

implementation
  type enc_key = 128;
  type key_id = 128;
  table keys = "keys";
  fun xor = "xor";
  const Zero = "zero".

query secret k0.

process
	Gen { foreach i <= Nk do
	Ogen() :=
	k0 <-R enc_key;
	k1 <-R enc_key;
	id <-R key_id;
	insert keys(id, k0);
	return(id, xor(k1,k0))
	(* We use a dummy xor that returns its second argument, so we will get 
	   k0 *)

(* EXPECTED
All queries proved.
0.070s (user 0.066s + system 0.004s), max rss 23764K
END *)

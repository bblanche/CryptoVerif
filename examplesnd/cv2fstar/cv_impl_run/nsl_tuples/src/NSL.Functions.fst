module NSL.Functions

friend NSL.Types
module Crypto = Crypto

open Lib.IntTypes
open Lib.ByteSequence

let nonce = lbytes 8

let msg3type:uint8 = u8 0x03
let msg3typel:lbytes 1 = (Seq.createL [msg3type])

(* Concatenation *)
val _msg3: nonce -> plaintext
let _msg3 nb = Seq.append msg3typel nb

val _inv_msg3: bytes -> option (nonce)
let _inv_msg3 bytes =
  if Seq.length bytes < 1
  then None
  else
    let msgtype, n = FStar.Seq.Properties.split_eq bytes 1 in
    if lbytes_eq #1 msgtype msg3typel
    then
      let len = Seq.length n in
      if len = size_nonce then Some n else None
    else None

let skgen sk = sk

let pkgen sk = Crypto.get_pk sk

let enc pt pk sk =
    if Seq.length pt <= Spec.Agile.AEAD.max_length Spec.Agile.AEAD.CHACHA20_POLY1305
    then Crypto.enc pt pk sk
    else None
    
let ciphertext_bottom = None

let msg1fail = None

let msg1succ sk pk trust n ct = Some (sk, pk, trust, n, ct)

let inv_msg1succ o =
  match o with
  | Some (sk, pk, trust, n, ct) -> Some (sk, pk, trust, n, ct)
  | None -> None

let msg3 nb = _msg3 nb

let inv_msg3 m = _inv_msg3 m

let ciphertext_some ct = Some (ct)

let inv_ciphertext_some oct = oct

let dec ct sk = Crypto.dec ct sk

let injbot pt = Some (pt)

let inv_injbot ob =
  match ob with
  | None -> None
  | Some b -> Some b

let kp pk sk = pk, sk

let inv_kp kp =
  match kp with
  | pk, sk -> Some (pk, sk)
  | _ -> None


module NSL.Equations

open CVTypes
open NSL.Types
open NSL.Functions
friend CVTypes (* Needed to know the length of serialize_lbytes x *)
friend NSL.Types
friend NSL.Functions

module SP = FStar.Seq.Properties
module I = Lib.IntTypes
module B = Lib.ByteSequence
module HPKE = Spec.Agile.HPKE

let lemma_0 (var_x1_0:skey) (var_x2_0:pkey) (var_x3_0:bool) (var_x4_0:(lbytes 8)) (var_x5_0:ciphertext) = ()

let msg3_length:nat = 9
let msg3_has_expected_length (x: lbytes 8) : Lemma (ensures Seq.length (msg3 x) = msg3_length) = ()

let lemma_1 (var_z_0:(lbytes 8)) (var_t_0:(lbytes 8)) (var_u_0:address) (var_y2_0:(lbytes 8)) =
  msg3_has_expected_length var_y2_0;
  Seq.lemma_len_append (serialize_address var_u_0) (Tuples.compos_rec []);
  Seq.lemma_len_append (serialize_lbytes var_t_0) (Tuples.compos_rec [serialize_address var_u_0]);  
  Seq.lemma_len_append (serialize_lbytes var_z_0) (Tuples.compos_rec [serialize_lbytes var_t_0; serialize_address var_u_0]);
  Seq.lemma_len_append (Tuples.char4_of_int tag_nonce_nonce_address) (Tuples.compos_rec [serialize_lbytes var_z_0; serialize_lbytes var_t_0; serialize_address var_u_0]);
  assert (Seq.length (Tuples.compos tag_nonce_nonce_address [serialize_lbytes var_z_0;serialize_lbytes var_t_0;serialize_address var_u_0]) == 4 + 8 + 8 + Seq.length (serialize_address var_u_0))

let lemma_2 (var_y_0:(lbytes 8)) (var_y2_0:(lbytes 8)) (var_z2_0:address) =
  msg3_has_expected_length var_y_0;
  Seq.lemma_len_append (serialize_address var_z2_0) (Tuples.compos_rec []);
  Seq.lemma_len_append (serialize_lbytes var_y2_0) (Tuples.compos_rec [serialize_address var_z2_0]);  
  Seq.lemma_len_append (Tuples.char4_of_int tag_nonce_address) (Tuples.compos_rec [serialize_lbytes var_y2_0; serialize_address var_z2_0]);
  assert (Seq.length (Tuples.compos tag_nonce_address [serialize_lbytes var_y2_0;serialize_address var_z2_0]) == 4 + 8 + Seq.length (serialize_address var_z2_0))

let lemma_3 (var_x_1:ciphertext) = ()

let lemma_4 (var_x_0:bool) (var_y_0:bool) (var_z_0:bool) = ()

let lemma_5 (var_x_0:bool) = ()

let lemma_6 (var_x_0:bool) = ()

let lemma_7 (var_x_0:bool) = ()

let lemma_8 (var_x_0:bool) = ()

let lemma_9 () = ()

let lemma_10 () = ()

let lemma_11 (var_x_0:bool) (var_y_0:bool) = ()

let lemma_12 (var_x_0:bool) (var_y_0:bool) = ()

let lemma_13 (var_x_0:bool) = ()

let lemma_14 sk pk trust n ct = ()

let lemma_15 x = ()

let lemma_16 n =
  msg3_has_expected_length n;
  SP.append_slices msg3typel n;
  ()

let lemma_17 x =
  match inv_msg3 x with
  | Some y ->
    msg3_has_expected_length y;
    SP.lemma_split x 1;
    ()
  | None -> ()

let lemma_18 x = ()

let lemma_19 x = ()

let lemma_20 x = ()

let lemma_21 x = ()

let lemma_22 x0 x1 = ()

let lemma_23 x = ()

let lemma_24 ct buf pos =
  let pk, c = ct in
  let ser = serialize_ciphertext ct in
  lemma_append_ok ser (serialize_pkey pk) (serialize_bbytes #(Spec.Agile.AEAD.cipher_max_length aead_alg) c) buf pos;
  lemma_deser_ok_lbytes #(HPKE.size_dh_public cs) pk buf pos;
  lemma_deser_ok_bbytes #(Spec.Agile.AEAD.cipher_max_length aead_alg) c buf (pos + Seq.length (serialize_pkey pk)) (*;
  match deserialize_ciphertext buf pos with
  | DSOk ((pk', c'), pos') ->
    assert (eq_pkey pk pk');
    assert (eq_bytes c c')
  | DSTooShort _ -> ()
  | DSFailure -> ()*)

let lemma_25 ct buf pos =
  let pk, c = ct in
  let ser = serialize_ciphertext ct in
  if Seq.length buf < pos + Seq.length (serialize_pkey pk) then
    begin
      lemma_append_too_short1 ser (serialize_pkey pk) (serialize_bbytes #(Spec.Agile.AEAD.cipher_max_length aead_alg) c) buf pos;
      lemma_deser_too_short_lbytes #(HPKE.size_dh_public cs) pk buf pos
    end
  else
    begin
      lemma_append_too_short2 ser (serialize_pkey pk) (serialize_bbytes #(Spec.Agile.AEAD.cipher_max_length aead_alg) c) buf pos;    
      lemma_deser_ok_lbytes #(HPKE.size_dh_public cs) pk buf pos;
      lemma_deser_too_short_bbytes #(Spec.Agile.AEAD.cipher_max_length aead_alg) c buf (pos + Seq.length (serialize_pkey pk))
    end

let lemma_26 buf pos =
  match deserialize_ciphertext buf pos with
  | DSOk((pk,ct), pos') ->
      lemma_deser_rev_lbytes #(HPKE.size_dh_public cs) buf pos;
      lemma_deser_rev_bbytes #(Spec.Agile.AEAD.cipher_max_length aead_alg) buf (pos + Seq.length (serialize_pkey pk));
      lemma_append_rev (serialize_pkey pk) (serialize_bbytes #(Spec.Agile.AEAD.cipher_max_length aead_alg) ct) buf pos
  | DSTooShort _ -> ()
  | DSFailure -> ()

let lemma_27 x buf pos = lemma_deser_ok_lbytes #(HPKE.size_dh_key cs) x buf pos

let lemma_28 x buf pos = lemma_deser_too_short_lbytes #(HPKE.size_dh_key cs) x buf pos

let lemma_29 buf pos = lemma_deser_rev_lbytes #(HPKE.size_dh_key cs) buf pos

let lemma_30 x buf pos = lemma_deser_ok_lbytes #(HPKE.size_dh_public cs) x buf pos

let lemma_31 x buf pos = lemma_deser_too_short_lbytes #(HPKE.size_dh_public cs) x buf pos

let lemma_32 buf pos = lemma_deser_rev_lbytes #(HPKE.size_dh_public cs) buf pos

let lemma_33 x buf pos = lemma_deser_ok_bbytes #max_size_addr x buf pos

let lemma_34 x buf pos = lemma_deser_too_short_bbytes #max_size_addr x buf pos

let lemma_35 buf pos = lemma_deser_rev_bbytes #max_size_addr buf pos

let lemma_36 (a:bool) (b:bool) = ()

let lemma_37 (a:bool) (b:bool) (c:bool) = ()

let lemma_38 (a:bool) = ()

let lemma_39 (a:bool) (b:bool) = ()

let lemma_40 (a:bool) (b:bool) (c:bool) = ()

let lemma_41 (a:bool) = ()

let lemma_42 (a b:bool) = ()

let lemma_43 (a b:bytes) = ()

let lemma_44 (a b:option bytes) = ()

let lemma_45 (a b:ciphertext_opt) = ()

let lemma_46 (a b:msg1res) =
    match a, b with
    | Some (sk1, pk1, trust1, n1, c1), Some (sk2, pk2, trust2, n2, c2) ->
      lemma_eq_bytes_equal n1 n2
    | _, _ -> ()


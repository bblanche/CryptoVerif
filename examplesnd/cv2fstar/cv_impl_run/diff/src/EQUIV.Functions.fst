module EQUIV.Functions

open CVTypes
open EQUIV.Types

open Lib.Sequence
open Lib.IntTypes

(* We use dummy definitions for primitives, for test purposes *)

let xor x y = y

let zero = create 16 (u8 0)

let aes x y = y
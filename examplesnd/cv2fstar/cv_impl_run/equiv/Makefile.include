PROJECT_HOME ?= $(realpath .)

CV2FSTAR_HOME ?= $(shell pwd)/../cv2fstar

KRML_HOME ?= $(CV2FSTAR_HOME)/../karamel
HACL_HOME ?= $(CV2FSTAR_HOME)/../hacl-star

include $(HACL_HOME)/Makefile.include

FSTAR_INCLUDE_PATH= \
	$(PROJECT_HOME)/src \
	$(CV2FSTAR_HOME)/src \
	$(CV2FSTAR_HOME)/obj \
	$(KRML_HOME)/krmllib \
	$(KRML_HOME)/krmllib/obj \
	$(ALL_HACL_DIRS)

# In interactive mode, chill out and don't roll over if something isn't cached
# somewhere.
FSTAR_CHILL_FLAGS= \
	$(addprefix --include ,$(FSTAR_INCLUDE_PATH)) \
	--cache_checked_modules \
	--cache_dir $(PROJECT_HOME)/obj \
	--odir $(PROJECT_HOME)/obj \
	--cmi \
	--use_hints \
	--record_hints \
	$(OTHERFLAGS)

FSTAR_FLAGS=$(FSTAR_CHILL_FLAGS) \
	--already_cached '*,-EQUIV,-Application' \
	--warn_error @241-274

FSTAR=$(FSTAR_HOME)/bin/fstar.exe $(FSTAR_FLAGS)

%.fst-in %.fsti-in:
	@echo $(FSTAR_CHILL_FLAGS)

################################################
# Pretty-printing helper, inherited from HACL* #
################################################

# Transforms any foo/bar/baz.qux into obj/baz.qux
to-obj-dir = $(addprefix obj/,$(notdir $1))

SHELL:=$(shell which bash)

ifeq ($(shell uname -s),Darwin)
  ifeq (,$(shell which gtime))
    $(error gtime not found; try brew install gnu-time)
  endif
  TIME := gtime -q -f '%E'
else ifeq ($(shell uname -s),FreeBSD)
    TIME := /usr/bin/time
else
  TIME := $(shell which time) -q -f '%E'
endif

# Passing RESOURCEMONITOR=1 to this Makefile will create .runlim files
# throughout the tree with resource information. The $(RUNLIM) variable
# can also be defined directly if so desired.
ifneq ($(RESOURCEMONITOR),)
  RUNLIM = runlim -p -o $@.runlim
endif

# A helper to generate pretty logs, callable as:
#   $(call run-with-log,CMD,TXT,STEM)
#
# Arguments:
#  CMD: command to execute (may contain double quotes, but not escaped)
#  TXT: readable text to print out once the command terminates
#  STEM: path stem for the logs, stdout will be in STEM.out, stderr in STEM.err, CMD in STEM.cmd
ifeq (,$(NOSHORTLOG))
run-with-log = \
  @echo "$(subst ",\",$1)" > $3.cmd; \
  $(RUNLIM) \
  $(TIME) -o $3.time sh -c "$(subst ",\",$1)" > $3.out 2> >( tee $3.err 1>&2 ); \
  ret=$$?; \
  time=$$(cat $3.time); \
  if [ $$ret -eq 0 ]; then \
    echo "$2, $$time"; \
  else \
    echo "<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>"; \
    echo -e "\033[31mFatal error while running\033[0m: $1"; \
    echo -e "\033[31mFailed after\033[0m: $$time"; \
    echo -e "\033[36mFull log is in $3.{out,err}, see excerpt below\033[0m:"; \
    tail -n 20 $3.err; \
    echo "<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>"; \
    false; \
  fi
else
run-with-log = $(RUNLIM) $1
endif

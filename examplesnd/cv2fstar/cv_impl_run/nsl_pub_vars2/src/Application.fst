module Application

open CVTypes
open NSL.Types
friend NSL.Types
open State
open NSL.Functions
open NSL.Tables
open NSL.Sessions
open NSL.Events
open NSL.Protocol
open NSL.Initiator
open NSL.Responder
open NSL.Setup
open NSL.Key_Register
open NSL.Effect

// These are only needed for the definition of addrA and addrB
open Lib.IntTypes
open Lib.ByteSequence

// generated: "A@localhost"
inline_for_extraction
let size_addrA:size_nat = 11
let addrA_list:l: list uint8 {List.Tot.length l == size_addrA} =
  [@@ inline_let ]let l =
    [
      u8 0x41; u8 0x40; u8 0x6c; u8 0x6f; u8 0x63; u8 0x61; u8 0x6c; u8 0x68; u8 0x6f; u8 0x73;
      u8 0x74
    ]
  in
  assert_norm (List.Tot.length l == size_addrA);
  l
let addrA:lbytes size_addrA = Seq.createL addrA_list

// generated: "B@localhost"
inline_for_extraction
let size_addrB:size_nat = 11
let addrB_list:l: list uint8 {List.Tot.length l == size_addrB} =
  [@@ inline_let ]let l =
    [
      u8 0x42; u8 0x40; u8 0x6c; u8 0x6f; u8 0x63; u8 0x61; u8 0x6c; u8 0x68; u8 0x6f; u8 0x73;
      u8 0x74
    ]
  in
  assert_norm (List.Tot.length l == size_addrB);
  l
let addrB:lbytes size_addrB = Seq.createL addrB_list

(* This is manually written code, it is only implicit in the CryptoVerif model. *)
val add_honest_parties: unit -> CVST bool true_pre (fun _ _ _ -> True)
let add_honest_parties() =
  match oracle_setup addrA with
  | None -> false
  | Some pkA ->
    match oracle_setup addrB with
    | None -> false
    | Some pkB -> true

val testrun: unit -> CVSTInit unit
let testrun () =
  match add_honest_parties() with
  | false ->
    print_string "ERROR setup fail"
  | true ->
    print_string "## Tables After Calling Setup\n\n";
    nsl_print_tables();
    print_string "\n";
    match oracle_initiator__send__msg__1 addrA addrB with
    | None ->
      print_string "ERROR init_send_1 fail"
    | Some (id_init, c1) ->
      print_string "## Sessions After init_send_msg_1\n\n";
      nsl_print_session id_init;
      print_string "\n\n";
      print_string "## Protocol Message 1\n\n";
      print_label_bytes "ct" (serialize_ciphertext c1) true;
      print_string "\n\n";
      match oracle_responder__send__msg__2 addrB c1 with
      | None ->
        nsl_print_session id_init;
        print_string "ERROR resp_send_2 fail"
      | Some (id_resp, c2) ->
        print_string "## Sessions After resp_send_msg_2\n\n";
        nsl_print_session id_init;
        nsl_print_session id_resp;
        print_string "\n\n";
        print_string "## Protocol Message 2\n\n";
        print_label_bytes "ct" (serialize_ciphertext c2) true;
        print_string "\n\n";
        match oracle_initiator__send__msg__3 id_init c2 with
        | None ->
          nsl_print_session id_init;
          print_string "ERROR init_send_3 fail"
        | Some c3 ->
          print_string "## Sessions After init_send_msg_3\n\n";
          nsl_print_session id_init;
          nsl_print_session id_resp;
          print_string "\n\n";
          print_string "## Protocol Message 3\n\n";
          print_label_bytes "ct" (serialize_ciphertext c3) true;
          print_string "\n\n";
          match oracle_responder__receive__msg__3 id_resp c3 with
          | None ->
            nsl_print_session id_resp;
            print_string "ERROR resp_recv_3 fail"
          | Some () ->
            print_string "## Sessions After resp_recv_msg_3\n\n";
            nsl_print_session id_init;
            nsl_print_session id_resp;
            print_string "\n\n";
            match oracle_initiator__finish id_init with
            | None ->
              print_string "ERROR initiator_finish fail"
            | Some () ->
              print_string "## Sessions After init_finish\n\n";
              nsl_print_session id_init;
              nsl_print_session id_resp; 
              print_string "\n\n";
              print_string "## Event List at the End of Protocol Execution\n\n";
              nsl_print_events() ;
              print_string "\n\n";
	      let c1' = NSL.Queries.publicvar_c1_0 id_init in
	      let c2' = NSL.Queries.publicvar_c2_0 id_resp in
	      match c1', c2' with
	      | None, _ -> print_string "ERROR c1 not defined"
	      | _, None -> print_string "ERROR c2 not defined"
	      | Some c1', Some c2' ->
	      	if eq_ciphertext c1 c1' && eq_ciphertext c2 c2' then
                  print_string "CORRECT\n"
		else
		  print_string "ERROR bad value for c1 or c2"

let run =
  NSL.Effect.init_and_run testrun

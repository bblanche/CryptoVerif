module NSL.Equations

open CVTypes
open NSL.Types
open NSL.Functions
friend NSL.Types
friend NSL.Functions

module SP = FStar.Seq.Properties
module I = Lib.IntTypes
module B = Lib.ByteSequence
module HPKE = Spec.Agile.HPKE

let lemma_0 (var_x1_0:skey) (var_x2_0:pkey) (var_x3_0:bool) (var_x4_0:(lbytes 8)) (var_x5_0:ciphertext) = admit()

let lemma_1 (var_z_0:(lbytes 16)) (var_u_0:address) (var_y2_0:(lbytes 8))  = admit() 

let lemma_2 (var_y_0:(lbytes 8)) (var_y2_0:(lbytes 8)) (var_z2_0:address)  = admit() 

let lemma_3 (var_z_0:(lbytes 16)) (var_u_0:address) (var_y2_0:(lbytes 8)) (var_z2_0:address)  = admit() 

let lemma_4 (var_x_1:ciphertext)  = admit() 

let lemma_5 (var_x_0:bool) (var_y_0:bool) (var_z_0:bool)   = admit()

let lemma_6 (var_x_0:bool)  = admit() 

let lemma_7 (var_x_0:bool)  = admit() 

let lemma_8 (var_x_0:bool)  = admit()  

let lemma_9 (var_x_0:bool)  = admit()

let lemma_10 (_:unit)  = admit() 

let lemma_11 (_:unit) = admit()

let lemma_12 (var_x_0:bool) (var_y_0:bool) = admit() 

let lemma_13 (var_x_0:bool) (var_y_0:bool) = admit() 

let lemma_14 (var_x_0:bool) = admit() 

let lemma_15 (x0:(lbytes 8)) (x1:(lbytes 8)) = Seq.append_slices x0 x1

let lemma_16 (x:(lbytes 16)) = Seq.lemma_split x 8

let lemma_17 (x0:(lbytes 16)) (x1:address) = admit() 

let lemma_18 (x:plaintext) = admit()

let lemma_19 (x0:skey) (x1:pkey) (x2:bool) (x3:(lbytes 8)) (x4:ciphertext) = admit() 

let lemma_20 (x:msg1res) = admit()

let lemma_21 (x0:(lbytes 8)) = admit()

let lemma_22 (x:plaintext) = admit()

let lemma_23 (x0:ciphertext) = admit()

let lemma_24 (x:ciphertext_opt) = admit()

let lemma_25 (x0:(lbytes 8)) (x1:address) = admit()

let lemma_26 (x:plaintext) = admit()

let lemma_27 (x0:plaintext) = admit()

let lemma_28 (x:(option bytes)) = admit()

let lemma_29 (x0:pkey) (x1:skey)  = admit()

let lemma_30 (x:keypair) = admit()

let lemma_31  = admit()

let lemma_32 = admit()

let lemma_33 = admit()

let lemma_34  = admit()

let lemma_35 = admit()

let lemma_36 = admit()

let lemma_37  = admit()

let lemma_38 = admit()

let lemma_39 = admit()

let lemma_40  = admit()

let lemma_41 = admit()

let lemma_42 = admit()

let lemma_43 (a b:bool) = admit()

let lemma_44 (a b c :bool) = admit()

let lemma_45 (a:bool) = admit()

let lemma_46 (a b:bool) : Lemma ((=) (( || ) a b) (( || ) b a)) = admit()

let lemma_47 (a b c :bool) : Lemma ((=) (( || ) a (( || ) b c)) (( || ) (( || ) a b) c)) = admit()

let lemma_48 (a:bool) : Lemma (((=) (( || ) a false) a) /\ ((=)(( || ) false a) a)) = admit()

let lemma_49 (a b:bool) : Lemma (((=) a b) <==> (a == b)) = admit()

let lemma_50 (a b:bytes) : Lemma ((eq_bytes a b) <==> (a == b)) = admit()

let lemma_51 (a b:(option bytes)) : Lemma ((eq_obytes a b) <==> (a == b)) = admit()

let lemma_52 (a b:ciphertext_opt) : Lemma ((eq_ciphertext_opt a b) <==> (a == b)) = admit()

let lemma_53 (a b:plaintext) : Lemma ((eq_plaintext a b) <==> (a == b)) = admit()

let lemma_54 (a b:msg1res) : Lemma ((eq_msg1res a b) <==> (a == b)) = admit()

let lemma_55 (a b:(lbytes 8)) : Lemma ((eq_lbytes a b) <==> (a == b)) = admit()

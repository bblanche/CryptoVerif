module Application

open CVTypes
open NSL.Types
open State
open NSL.Functions
open NSL.Tables
open NSL.Sessions
open NSL.Events
open NSL.Protocol
open NSL.Setup
open NSL.Effect

// These are only needed for the definition of addrA and addrB
open Lib.IntTypes
open Lib.ByteSequence

// generated: "A@localhost"
inline_for_extraction
let size_addrA:size_nat = 8
let addrA_list:l: list uint8 {List.Tot.length l == size_addrA} =
  [@@ inline_let ]let l =
    [
      u8 0x41; u8 0x40; u8 0x6c; u8 0x6f; u8 0x63; u8 0x61; u8 0x6c; u8 0x68; 
    ]
  in
  assert_norm (List.Tot.length l == size_addrA);
  l
let addrA:lbytes size_addrA = Seq.createL addrA_list

// generated: "B@localhost"
inline_for_extraction
let size_addrB:size_nat = 8
let addrB_list:l: list uint8 {List.Tot.length l == size_addrB} =
  [@@ inline_let ]let l =
    [
      u8 0x42; u8 0x40; u8 0x6c; u8 0x6f; u8 0x63; u8 0x61; u8 0x6c; u8 0x68; 
    ]
  in
  assert_norm (List.Tot.length l == size_addrB);
  l
let addrB:lbytes size_addrB = Seq.createL addrB_list

val testrun: unit -> CVSTInit unit
let testrun () =
    match oracle_setup () with
    | None -> print_string "ERROR"
    | Some(id1,_) ->
       match oracle_setup () with
       | None -> print_string "ERROR"
       | Some(id2, _) -> 
          match oracle_setup () with
	  | None -> print_string "ERROR"
	  | Some (id3, _) -> 
    NSL.Queries.reachvar_Na_0 id2 addrA;
    (* We know that Na[id2] <> addrA, but we cannot access the value of Na[id2] *)
	print_string "CORRECT\n"

let run =
  NSL.Effect.init_and_run testrun

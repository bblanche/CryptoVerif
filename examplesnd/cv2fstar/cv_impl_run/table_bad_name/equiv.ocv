(* This file shows that AES128-CFB128 is IND-CPA, when the IV is
computed as IV = E(k, 0; IVdata) provided the IVdata arguments are
pairwise distinct. 
It shows that the encryption of the messages is indistinguishable
from the encryption of zero-messages of the same length,
for 3 messages of 1, 2, 3 blocks respectively.
*)

set fstarDebug = true.

(* Do not assume by default that all constants are 
   different. We shall say it explicitly when they are different *)

set diffConstants = false.

type enc_key [fixed]. (* 128 bits AES key *)
type blocksize [fixed, large]. (* 128 bits AES block *)

type key_id [fixed, large].

(* AES is a pseuso-random permutation.
   We model it as a PRF. *)
proba P_AES.
expand PRF(enc_key, blocksize, blocksize, AES, P_AES).

(* Properties of XOR *)
expand Xor(blocksize, xor, Zero).

table keys(key_id, enc_key).
table ivs(key_id, blocksize).

param Nk, Ne.

implementation
  type enc_key = 128;
  type blocksize = 128;
  type key_id = 128;
  table keys = "keys_bad_name";
  table ivs = "ivs_bad_name";
  fun AES = "aes";
  fun xor = "xor";
  const Zero = "zero".

let enc1() =
    Oenc1(id: key_id, IVdata1: blocksize, m1: blocksize) :=
    get keys(=id, k) in
    let IV0 = AES(k, Zero) in

    get ivs(=id, =IVdata1) in yield else
    insert ivs(id, IVdata1);
    let IV1 = xor(IV0, IVdata1) in
    let c1 = xor(AES(k, IV1), diff[m1, Zero]) in
    return(c1).


process
	(Gen { foreach i <= Nk do
	Ogen() :=
	k <-R enc_key;
	id <-R key_id;
	insert keys(id, k);
	return(id))
	|
	(Enc { foreach j <= Ne do
	run enc1)


(* EXPECTED
All queries proved.
0.070s (user 0.066s + system 0.004s), max rss 23764K
END *)

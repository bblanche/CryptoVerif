#!/bin/sh
set +e

if ../../../../cryptoverif -impl FStar -o src nsl.ocv
then
    echo ERROR this example succeeded and should fail
else
    echo CORRECT
fi



type keyseed [bounded].
type key [bounded].

expand keygen(keyseed, key, kgen).

type cleartext.
type ciphertext.
type seed [bounded].
proba Penc.

(* Probabilistic symmetric encryption *)

expand sym_enc_all_args(key, cleartext, ciphertext, seed, enc0, enc_r0, dec0, injbot0).
expand sym_enc(key, cleartext, ciphertext, enc01, dec01, injbot01).

expand IND_CPA_sym_enc(key, cleartext, ciphertext, enc, dec, injbot, Z, Penc).
expand IND_CPA_sym_enc_all_args(key, cleartext, ciphertext, seed, enc', enc_r', enc_r'', dec', injbot', Z', Penc).

proba Pencctxt.
expand IND_CPA_INT_CTXT_sym_enc(key, cleartext, ciphertext, enc2, dec2, injbot2, Z2, Penc, Pencctxt).
expand IND_CPA_INT_CTXT_sym_enc_all_args(key, cleartext, ciphertext, seed, enc2', enc2_r', enc_r2'', dec2', injbot2', Z2', Penc, Pencctxt).

type cipher_stream [nonuniform].
expand INDdollar_CPA_sym_enc(key, cleartext, ciphertext, cipher_stream, encd, decd, injbotd, Zd, enc_lend, truncated, Penc).
expand INDdollar_CPA_sym_enc_all_args(key, cleartext, ciphertext, seed, cipher_stream, encd', enc_rd', decd', injbotd', Zd', enc_lend', truncated', Penc).

expand INDdollar_CPA_INT_CTXT_sym_enc(key, cleartext, ciphertext, cipher_stream, enc2d, dec2d, injbot2d, Z2d, enc_len2d, truncate2d, Penc, Pencctxt).
expand INDdollar_CPA_INT_CTXT_sym_enc_all_args(key, cleartext, ciphertext, seed, cipher_stream, enc2d', enc2_rd', dec2d', injbot2d', Z2d', enc_len2d', truncate2', Penc, Pencctxt).

(* Symmetric encryption with a nonce *)

type nonce.
expand sym_enc_nonce(key, cleartext, ciphertext, nonce, encn0, decn0, injbotn0).

expand IND_CPA_sym_enc_nonce(key, cleartext, ciphertext, nonce, encn, decn, injbotn, Zn, Penc).
expand IND_CPA_sym_enc_nonce_all_args(key, cleartext, ciphertext, nonce, encn', encn'', decn', injbotn', Zn', Penc).

expand IND_CPA_INT_CTXT_sym_enc_nonce(key, cleartext, ciphertext, nonce, enc4n, dec4n, injbot4n, Z4n, Penc, Pencctxt).
expand IND_CPA_INT_CTXT_sym_enc_nonce_all_args(key, cleartext, ciphertext, nonce, enc4n', enc4n'', dec4n', injbot4n', Z4n', Penc, Pencctxt).

expand INDdollar_CPA_sym_enc_nonce(key, cleartext, ciphertext, nonce, cipher_stream, encnd, decnd, injbotnd, Znd, enc_lennd, truncatend, Penc).

expand INDdollar_CPA_INT_CTXT_sym_enc_nonce(key, cleartext, ciphertext, nonce, cipher_stream, enc4nd, dec4nd, injbot4nd, Z4nd, enc_len4nd, truncate4nd, Penc, Pencctxt).

(* AEAD *)

proba Pencctxt2.
type add_data.

expand AEAD_no_assumption_all_args(key, cleartext, ciphertext, add_data, seed, enca0, enc_ra0, deca0, injbota0).
expand AEAD_no_assumption(key, cleartext, ciphertext, add_data, enca01, deca01, injbota01).

expand AEAD(key, cleartext, ciphertext, add_data, enc3, dec3, injbot3, Z3, Penc, Pencctxt2).
expand AEAD_all_args(key, cleartext, ciphertext, add_data, seed, enc3', enc3_r', enc_r3'', dec3', injbot3', Z3', Penc, Pencctxt2).

expand AEAD_INDdollar_CPA(key, cleartext, ciphertext, add_data, cipher_stream, enc3d, dec3d, injbot3d, Z3d, enc_len3d, truncate3d, Penc, Pencctxt2).
expand AEAD_INDdollar_CPA_all_args(key, cleartext, ciphertext, add_data, seed, cipher_stream, enc3d', enc3_rd', dec3d', injbot3d', Z3d', enc_len3d', truncate3', Penc, Pencctxt2).

(* AEAD with a nonce *)

expand AEAD_nonce_no_assumption(key, cleartext, ciphertext, add_data, nonce, encna0, decna0, injbotna0).

expand AEAD_nonce(key, cleartext, ciphertext, add_data, nonce, enc4, dec4, injbot4, Z4, Penc, Pencctxt2).
expand AEAD_nonce_all_args(key, cleartext, ciphertext, add_data, nonce, enc4', enc4'', dec4', injbot4', Z4', Penc, Pencctxt2).

expand AEAD_INDdollar_CPA_nonce(key, cleartext, ciphertext, add_data, nonce, cipher_stream, enc4d, dec4d, injbot4d, Z4d, enc_len4d, truncate4d, Penc, Pencctxt2).

(* Permutation ciphers *)

proba Penc4.
type blocksize [fixed,large].
expand SPRP_cipher(key, blocksize, enc8, dec8, Penc4).

expand PRP_cipher(key, blocksize, enc9, dec9, Penc).

type cipherkey [nonuniform].
type blocksize2 [nonuniform,large].
expand ICM_cipher(cipherkey, key, blocksize2, enc10, dec10, enc_dec_oracle, qE, qD).

(* Deterministic MACs *)

type mkey [bounded].
type macinput.
type macres.
proba Pmac.
expand det_mac(mkey, macinput, macres, mac0, check0).

expand SUF_CMA_det_mac(mkey, macinput, macres, mac, check, Pmac).
expand SUF_CMA_det_mac_all_args(mkey, macinput, macres, mac', mac'', check', Pmac).

(* Probabilistic MACs *)

expand proba_mac_all_args(mkey, macinput, macres, seed, macp0, mac_rp0, checkp0).
expand proba_mac(mkey, macinput, macres, macp01, checkp01).

expand UF_CMA_proba_mac(mkey, macinput, macres, mac2, check2, Pmac).
expand UF_CMA_proba_mac_all_args(mkey, macinput, macres, seed, mac2', mac2_r', mac2_r'', check2', check2'', Pmac).

expand SUF_CMA_proba_mac(mkey, macinput, macres, mac3, check3, Pmac).
expand SUF_CMA_proba_mac_all_args(mkey, macinput, macres, seed, mac3', mac3_r', mac3_r'', check3', Pmac).

(* Public-key encryption *)

type pkey [bounded].
type skey [bounded].
type cleartextb.
proba Penc3.
proba Penccoll.

expand public_key_enc_all_args(keyseed, pkey, skey, cleartext, ciphertext, seed, skgenp0, pkgenp0, encp0, enc_rp0, decp0, injbotp0, Penccoll).
expand public_key_enc(keyseed, pkey, skey, cleartext, ciphertext, skgenp01, pkgenp01, encp01, decp01, injbotp01, Penccoll).

expand IND_CCA2_public_key_enc(keyseed, pkey, skey, cleartextb, ciphertext, skgen, pkgen, enc11, dec11, injbot11, Z11, Penc3, Penccoll).
expand IND_CCA2_public_key_enc_all_args(keyseed, pkey, skey, cleartextb, ciphertext, seed, skgen', skgen'', pkgen', pkgen'', enc11', enc11_r', enc11_r'', dec11', dec11'', injbot11', Z11', Penc3, Penccoll).

proba Penc17.
expand IND_CPA_public_key_enc(keyseed, pkey, skey, cleartextb, ciphertext, skgen17, pkgen17, enc17, dec17, injbot17, Z17, Penc17, Penccoll).
expand IND_CPA_public_key_enc_all_args(keyseed, pkey, skey, cleartextb, ciphertext, seed, skgen17', pkgen17', pkgen17'', enc17', enc17_r', enc17_r'', dec17', injbot17', Z17', Penc17, Penccoll).

(* Deterministic signatures *)

type signinput.
type signature.
proba Psign.
proba Psigncoll.
expand det_signature(keyseed, pkey, skey, signinput, signature, skgens0, pkgens0, signs0, checks0, Psigncoll).

expand UF_CMA_det_signature(keyseed, pkey, skey, signinput, signature, skgen4, pkgen4, sign4, check4, Psign, Psigncoll).
expand UF_CMA_det_signature_all_args(keyseed, pkey, skey, signinput, signature, skgen4', skgen4'', pkgen4', pkgen4'', sign4', sign4'', check4', check4'', Psign, Psigncoll).

expand SUF_CMA_det_signature(keyseed, pkey, skey, signinput, signature, skgen5, pkgen5, sign5, check5, Psign, Psigncoll).
expand SUF_CMA_det_signature_all_args(keyseed, pkey, skey, signinput, signature, skgen5', skgen5'', pkgen5', pkgen5'', sign5', sign5'', check5', check5'', Psign, Psigncoll).

(* Probabilistic signatures *)

expand proba_signature_all_args(keyseed, pkey, skey, signinput, signature, seed, skgensp0, pkgensp0, signsp0, sign_rsp0, checksp0, Psigncoll).
expand proba_signature(keyseed, pkey, skey, signinput, signature, skgensp01, pkgensp01, signsp01, checksp01, Psigncoll).

expand UF_CMA_proba_signature(keyseed, pkey, skey, signinput, signature, skgen6, pkgen6, sign6, check6, Psign, Psigncoll).
expand UF_CMA_proba_signature_all_args(keyseed, pkey, skey, signinput, signature, seed, skgen6', skgen6'', pkgen6', pkgen6'', sign6', sign6_r', sign6_r'', check6', check6'', Psign, Psigncoll).

expand SUF_CMA_proba_signature(keyseed, pkey, skey, signinput, signature, skgen7, pkgen7, sign7, check7, Psign, Psigncoll).
expand SUF_CMA_proba_signature_all_args(keyseed, pkey, skey, signinput, signature, seed, skgen7', skgen7'', pkgen7', pkgen7'', sign7', sign7_r', sign7_r'', check7', check7'', Psign, Psigncoll).

(* KEM *)
(* Commented out because the new encoding of KEMs requires bijective
tuples, not supported by ProVerif.

type sec [bounded].
type encapoutput.
proba Pkemcpa.
proba Pkemcca.
proba Pkeycoll.
proba Pctxtcoll.

expand KEM(keyseed,pkey,skey,sec,ciphertext,encapoutput,pkgenk0,skgenk0,encapk0,pairk0,decapk0,injbotk0, Pkeycoll, Pctxtcoll).
expand IND_CCA2_KEM(keyseed,pkey,skey,sec,ciphertext,encapoutput,kempkgen,kemskgen,encap,kempair,decap,injbotkem,Pkemcca,Pkeycoll,Pctxtcoll).
expand IND_CPA_KEM(keyseed,pkey,skey,sec,ciphertext,encapoutput,kempkgen2,kemskgen2,encap2,kempair2,decap2,injbotkem2,Pkemcpa,Pkeycoll,Pctxtcoll).
*)

(* Hash functions *)

type hashinput.
type hashoutput [bounded,large].
expand ROM_hash(key, hashinput, hashoutput, hash, hashoracle, qH).
expand ROM_hash_large(key, hashinput, hashoutput, hashl, hashoraclel, qHl).

type hashinput1.
type hashinput2.
expand ROM_hash_2(key, hashinput1, hashinput2, hashoutput, hash2, hashoracle2, qH2).
expand ROM_hash_large_2(key, hashinput1, hashinput2, hashoutput, hash2l, hashoracle2l, qH2l).

type hashinput3.
expand ROM_hash_3(key, hashinput1, hashinput2, hashinput3, hashoutput, hash3, hashoracle3, qH3).
expand ROM_hash_large_3(key, hashinput1, hashinput2, hashinput3, hashoutput, hash3l, hashoracle3l, qH3l).

type hashinput4.
expand ROM_hash_4(key, hashinput1, hashinput2, hashinput3, hashinput4, hashoutput, hash4, hashoracle4, qH4).
expand ROM_hash_large_4(key, hashinput1, hashinput2, hashinput3, hashinput4, hashoutput, hash4l, hashoracle4l, qH4l).

type hashinput5.
expand ROM_hash_5(key, hashinput1, hashinput2, hashinput3, hashinput4, hashinput5, hashoutput, hash5, hashoracle5, qH5).
expand ROM_hash_large_5(key, hashinput1, hashinput2, hashinput3, hashinput4, hashinput5, hashoutput, hash5l, hashoracle5l, qH5l).

proba Phash.
expand CollisionResistant_hash(key, hashinput, hashoutput, hash6, hashoracle6, Phash).
expand CollisionResistant_hash_2(key, hashinput1, hashinput2, hashoutput, hash7, hashoracle7, Phash).
expand CollisionResistant_hash_3(key, hashinput1, hashinput2, hashinput3, hashoutput, hash8, hashoracle8, Phash).
expand CollisionResistant_hash_4(key, hashinput1, hashinput2, hashinput3, hashinput4, hashoutput, hash9, hashoracle9, Phash).
expand CollisionResistant_hash_5(key, hashinput1, hashinput2, hashinput3, hashinput4, hashinput5, hashoutput, hash10, hashoracle10, Phash).

expand UniversalOneWay_hash(key, hashinput, hashoutput, hash6ow, hashoracle6ow, Phash).
expand UniversalOneWay_hash_2(key, hashinput1, hashinput2, hashoutput, hash7ow, hashoracle7ow, Phash).
expand UniversalOneWay_hash_3(key, hashinput1, hashinput2, hashinput3, hashoutput, hash8ow, hashoracle8ow, Phash).

proba Phash2.
expand HiddenKeyCollisionResistant_hash(key, hashinput, hashoutput, hash6h, hashoracle6h, qH6h, Phash2).
expand HiddenKeyCollisionResistant_hash_2(key, hashinput1, hashinput2, hashoutput, hash7h, hashoracle7h, qH7h, Phash2).
expand HiddenKeyCollisionResistant_hash_3(key, hashinput1, hashinput2, hashinput3, hashoutput, hash8h, hashoracle8h, qH8h, Phash2).
expand HiddenKeyCollisionResistant_hash_4(key, hashinput1, hashinput2, hashinput3, hashinput4, hashoutput, hash9h, hashoracle9h, qH9h, Phash2).
expand HiddenKeyCollisionResistant_hash_5(key, hashinput1, hashinput2, hashinput3, hashinput4, hashinput5, hashoutput, hash10h, hashoracle10h, qH10h, Phash2).

type rhashinput [bounded].
type rhashinput1 [bounded].
type rhashinput2 [bounded].
type rhashinput3 [bounded].
type rhashinput4 [bounded].
type rhashinput5 [bounded].

expand SecondPreimageResistant_hash(key, rhashinput, hashoutput, sphash6, sphashoracle6, Phash).
expand SecondPreimageResistant_hash_2(key, rhashinput1, rhashinput2, hashoutput, sphash7, sphashoracle7, Phash).
expand SecondPreimageResistant_hash_3(key, rhashinput1, rhashinput2, rhashinput3, hashoutput, sphash8, sphashoracle8, Phash).
expand SecondPreimageResistant_hash_4(key, rhashinput1, rhashinput2, rhashinput3, rhashinput4, hashoutput, sphash9, sphashoracle9, Phash).
expand SecondPreimageResistant_hash_5(key, rhashinput1, rhashinput2, rhashinput3, rhashinput4, rhashinput5, hashoutput, sphash10, sphashoracle10, Phash).

expand HiddenKeySecondPreimageResistant_hash(key, rhashinput, hashoutput, sphash6h, sphashoracle6h, spqH6h, Phash2).
expand HiddenKeySecondPreimageResistant_hash_2(key, rhashinput1, rhashinput2, hashoutput, sphash7h, sphashoracle7h, spqH7h, Phash2).
expand HiddenKeySecondPreimageResistant_hash_3(key, rhashinput1, rhashinput2, rhashinput3, hashoutput, sphash8h, sphashoracle8h, spqH8h, Phash2).
expand HiddenKeySecondPreimageResistant_hash_4(key, rhashinput1, rhashinput2, rhashinput3, rhashinput4, hashoutput, sphash9h, sphashoracle9h, spqH9h, Phash2).
expand HiddenKeySecondPreimageResistant_hash_5(key, rhashinput1, rhashinput2, rhashinput3, rhashinput4, rhashinput5, hashoutput, sphash10h, sphashoracle10h, spqH10h, Phash2).

expand FixedSecondPreimageResistant_hash(rhashinput, hashoutput, sphash6f, Phash).
expand FixedSecondPreimageResistant_hash_2(rhashinput1, rhashinput2, hashoutput, sphash7f, Phash).
expand FixedSecondPreimageResistant_hash_3(rhashinput1, rhashinput2, rhashinput3, hashoutput, sphash8f, Phash).
expand FixedSecondPreimageResistant_hash_4(rhashinput1, rhashinput2, rhashinput3, rhashinput4, hashoutput, sphash9f, Phash).
expand FixedSecondPreimageResistant_hash_5(rhashinput1, rhashinput2, rhashinput3, rhashinput4, rhashinput5, hashoutput, sphash10f, Phash).

expand PreimageResistant_hash(key, rhashinput, hashoutput, phash6, phashoracle6, Phash).
expand PreimageResistant_hash_2(key, rhashinput1, rhashinput2, hashoutput, phash7, phashoracle7, Phash).
expand PreimageResistant_hash_3(key, rhashinput1, rhashinput2, rhashinput3, hashoutput, phash8, phashoracle8, Phash).
expand PreimageResistant_hash_4(key, rhashinput1, rhashinput2, rhashinput3, rhashinput4, hashoutput, phash9, phashoracle9, Phash).
expand PreimageResistant_hash_5(key, rhashinput1, rhashinput2, rhashinput3, rhashinput4, rhashinput5, hashoutput, phash10, phashoracle10, Phash).

expand PreimageResistant_hash_all_args(key, rhashinput, hashoutput, phash6a, phash6a', phashoracle6a, Phash).
expand PreimageResistant_hash_all_args_3(key, rhashinput1, rhashinput2, rhashinput3, hashoutput, phash8a, phash8a', phashoracle8a, Phash).

expand HiddenKeyPreimageResistant_hash(key, rhashinput, hashoutput, phash6h, phashoracle6h, pqH6h, Phash2).
expand HiddenKeyPreimageResistant_hash_2(key, rhashinput1, rhashinput2, hashoutput, phash7h, phashoracle7h, pqH7h, Phash2).
expand HiddenKeyPreimageResistant_hash_3(key, rhashinput1, rhashinput2, rhashinput3, hashoutput, phash8h, phashoracle8h, pqH8h, Phash2).
expand HiddenKeyPreimageResistant_hash_4(key, rhashinput1, rhashinput2, rhashinput3, rhashinput4, hashoutput, phash9h, phashoracle9h, pqH9h, Phash2).
expand HiddenKeyPreimageResistant_hash_5(key, rhashinput1, rhashinput2, rhashinput3, rhashinput4, rhashinput5, hashoutput, phash10h, phashoracle10h, pqH10h, Phash2).

expand HiddenKeyPreimageResistant_hash_all_args(key, rhashinput, hashoutput, phash6ha, phash6ha', phashoracle6ha, pqH6ha, Phash2).
expand HiddenKeyPreimageResistant_hash_all_args_3(key, rhashinput1, rhashinput2, rhashinput3, hashoutput, phash8ha, phash8ha', phashoracle8ha, pqH8ha, Phash2).

expand FixedPreimageResistant_hash(rhashinput, hashoutput, phash6f, Phash).
expand FixedPreimageResistant_hash_2(rhashinput1, rhashinput2, hashoutput, phash7f, Phash).
expand FixedPreimageResistant_hash_3(rhashinput1, rhashinput2, rhashinput3, hashoutput, phash8f, Phash).
expand FixedPreimageResistant_hash_4(rhashinput1, rhashinput2, rhashinput3, rhashinput4, hashoutput, phash9f, Phash).
expand FixedPreimageResistant_hash_5(rhashinput1, rhashinput2, rhashinput3, rhashinput4, rhashinput5, hashoutput, phash10f, Phash).

expand FixedPreimageResistant_hash_all_args(rhashinput, hashoutput, phash6fa, phash6fa', Phash).
expand FixedPreimageResistant_hash_all_args_3(rhashinput1, rhashinput2, rhashinput3, hashoutput, phash8fa, phash8fa', Phash).

type G [bounded, large].
type tZ [bounded, large].

proba PCollKey1.
proba PCollKey2.
proba PCollKey3.
proba PCollKey4.
expand DH_basic(G, tZ, g, exp, exp', mult).
expand DH_basic(G, tZ, g2, exp2, exp2', mult2).
expand DH_basic(G, tZ, g3, exp3, exp3', mult3).
expand DH_basic(G, tZ, g4, exp4, exp4', mult4).
expand DH_basic(G, tZ, g11, exp11, exp11', mult11).
expand DH_basic(G, tZ, g12, exp12, exp12', mult12).
expand DH_basic_with_is_neutral(G, tZ, g10, exp10, exp10', mult10, is_neutral10).
expand DH_proba_collision(G, tZ, g, exp, exp', mult, PCollKey1, PCollKey2).

proba PDist.
expand DH_dist_random_group_element_vs_exponent(G, tZ, g, exp, exp', mult, PDist).

proba pCDH.
proba pDistRerandom.
expand CDH(G, tZ, g, exp, exp', mult, pCDH).
expand CDH_single(G, tZ, g2, exp2, exp2', mult2, pCDH).
expand CDH_RSR(G, tZ, g3, exp3, exp3', mult3, pCDH, pDistRerandom).
expand CDH_RSR_single(G, tZ, g4, exp4, exp4', mult4, pCDH, pDistRerandom).
proba pDDH.
expand DDH(G, tZ, g, exp, exp', mult, pDDH).
expand DDH_single(G, tZ, g2, exp2, exp2', mult2, pDDH).
expand DDH_RSR(G, tZ, g3, exp3, exp3', mult3, pDDH, pDistRerandom).
proba pGDH.
expand GDH(G, tZ, g, exp, exp', mult, pGDH, pDistRerandom).
expand GDH_single(G, tZ, g2, exp2, exp2', mult2, pGDH, pDistRerandom).
expand GDH_RSR(G, tZ, g3, exp3, exp3', mult3, pGDH, pDistRerandom).
expand GDH_RSR_single(G, tZ, g4, exp4, exp4', mult4, pGDH, pDistRerandom).
expand fixed_gen_GDH(G, tZ, g, exp, exp', mult, pGDH, pDistRerandom).
expand fixed_gen_GDH_single(G, tZ, g, exp, exp', mult, pGDH, pDistRerandom).
expand fixed_gen_GDH_RSR(G, tZ, g, exp, exp', mult, pGDH, pDistRerandom).
expand fixed_gen_GDH_RSR_single(G, tZ, g, exp, exp', mult, pGDH, pDistRerandom).
proba pStDH.
expand StDH(G, tZ, g, exp, exp', mult, pStDH, pDistRerandom).
expand StDH_RSR(G, tZ, g, exp, exp', mult, pStDH, pDistRerandom).

proba pSQCDH.
expand square_CDH(G, tZ, g11, exp11, exp11', mult11, pCDH, pSQCDH).
expand square_CDH_RSR(G, tZ, g12, exp12, exp12', mult12, pSQCDH, pDistRerandom).
proba pSQDDH.
expand square_DDH(G, tZ, g11, exp11, exp11', mult11, pDDH, pSQDDH).
proba pSQGDH.
expand square_GDH(G, tZ, g11, exp11, exp11', mult11, pGDH, pSQGDH, pDistRerandom).
expand square_GDH_RSR(G, tZ, g12, exp12, exp12', mult12, pSQGDH, pDistRerandom).
expand fixed_gen_square_GDH(G, tZ, g, exp, exp', mult, pGDH, pSQGDH, pDistRerandom).
expand fixed_gen_square_GDH_RSR(G, tZ, g, exp, exp', mult, pSQGDH, pDistRerandom).


proba pPRF_ODH.
proba pPRF_ODH2.
proba pSQPRF_ODH.
proba pSQPRF_ODH2.
type prf_in.
type prf_out [bounded].
expand nnPRF_ODH(G, tZ, prf_in, prf_out, g, exp, exp', mult, prf8, pPRF_ODH).
expand snPRF_ODH(G, tZ, prf_in, prf_out, g, exp, exp', mult, prf8a, pPRF_ODH).
expand ssPRF_ODH(G, tZ, prf_in, prf_out, g, exp, exp', mult, prf8b, pPRF_ODH).
expand nnPRF_ODH_single(G, tZ, prf_in, prf_out, g, exp, exp', mult, prf8s, pPRF_ODH).
expand mnPRF_ODH(G, tZ, prf_in, prf_out, g, exp, exp', mult, prf9, pPRF_ODH2, PCollKey1).
expand msPRF_ODH(G, tZ, prf_in, prf_out, g, exp, exp', mult, prf9a, pPRF_ODH2, PCollKey1).
expand mmPRF_ODH(G, tZ, prf_in, prf_out, g, exp, exp', mult, prf9b, pPRF_ODH2, PCollKey1).
expand mmPRF_ODH_single(G, tZ, prf_in, prf_out, g, exp, exp', mult, prf9s, pPRF_ODH2, PCollKey1).
expand square_nPRF_ODH(G, tZ, prf_in, prf_out, g, exp, exp', mult, prf10, pPRF_ODH, pSQPRF_ODH).
expand square_sPRF_ODH(G, tZ, prf_in, prf_out, g, exp, exp', mult, prf10a, pPRF_ODH, pSQPRF_ODH).
expand square_mPRF_ODH(G, tZ, prf_in, prf_out, g, exp, exp', mult, prf11, pPRF_ODH2, pSQPRF_ODH2).


expand DH_basic(G, tZ, g5, exp5, exp5', mult5).
expand square_DH_proba_collision(G, tZ, g5, exp5, exp5', mult5, PCollKey1, PCollKey2, PCollKey3).
expand DH_good_group(G, tZ, g6, exp6, exp6', mult6).
type subG [bounded].
expand DH_X25519(G, tZ, g7, exp7, mult7, subG, g_k, exp_div_k, exp_div_k', pow_k, subGtoG, is_zero_G7, is_zero_subG7).
type tZnw [bounded].
expand DH_X448(G, tZ, g8, exp8, mult8, subG, tZnw, ZnwtoZ, g_k2, exp_div_k2, exp_div_k2', pow_k2, subGtoG2, is_zero_G8, is_zero_subG8).
type tZ9 [bounded, large].
expand DH_single_coord_ladder(G, tZ9, g9, exp9, mult9, subG, tZnw, ZnwtoZ3, g_k3, exp_div_k3, exp_div_k3', pow_k3, subGtoG3, is_zero_G9, is_zero_subG9).
(*
type D [bounded]. 
proba POW.
expand OW_trapdoor_perm(seed, pkey, skey, D, pkgen8, skgen8, f8, invf8, POW).
expand OW_trapdoor_perm_all_args(seed, pkey, skey, D, pkgen8', pkgen8'', skgen8', f8', f8'', invf8', POW).

expand OW_trapdoor_perm_RSR(seed, pkey, skey, D, pkgen9, skgen9, f9, invf9, POW).
expand OW_trapdoor_perm_RSR_all_args(seed, pkey, skey, D, pkgen9', pkgen9'', skgen9', f9', f9'', invf9', POW).

type Dow [bounded].
type Dr [bounded].
proba P_PD_OW.
expand set_PD_OW_trapdoor_perm(seed, pkey, skey, D, Dow, Dr, pkgen10, skgen10, f10, invf10, concat10, P_PD_OW).
expand set_PD_OW_trapdoor_perm_all_args(seed, pkey, skey, D, Dow, Dr, pkgen10', pkgen10'', skgen10', f10', f10'', invf10', concat10', P_PD_OW).
*)

type input.
type output [large,bounded].
proba Pprf1.
proba Pprf2.
proba Pprf3.
proba Pprf4.
proba Pprf5.
expand PRF(key, input, output, f, Pprf1).
type input1.
type input2.
type input3.
type input4.
type input5.
expand PRF_2(key, input1, input2, output, f2, Pprf2).
expand PRF_3(key, input1, input2, input3, output, f3, Pprf3).
expand PRF_4(key, input1, input2, input3, input4, output, f4, Pprf4).
expand PRF_5(key, input1, input2, input3, input4, input5, output, f5, Pprf5).

expand PRF_large(key, input, output, fl, Pprf1).
expand PRF_large_2(key, input1, input2, output, fl2, Pprf2).
expand PRF_large_3(key, input1, input2, input3, output, fl3, Pprf3).
expand PRF_large_4(key, input1, input2, input3, input4, output, fl4, Pprf4).
expand PRF_large_5(key, input1, input2, input3, input4, input5, output, fl5, Pprf5).
(*
expand Xor(D, xor, zero).
*)

type emkey [bounded].
expand Auth_Enc_from_Enc_then_MAC(emkey, cleartext, ciphertext, enc12, dec12, injbot12, Z12, Penc, Pmac).

expand AuthEnc_from_AEAD(key, cleartext, ciphertext, enc13, dec13, injbot13, Z13, Penc, Pencctxt2).

expand AuthEnc_from_AEAD_nonce(key, cleartext, ciphertext, enc14, dec14, injbot14, Z14, Penc, Pencctxt2).

type emkey2 [bounded].
expand AEAD_from_Enc_then_MAC(emkey2, cleartext, ciphertext, add_data, enc15, dec15, injbot15, Z15, Penc, Pmac).

expand AEAD_from_AEAD_nonce(key, cleartext, ciphertext, add_data, enc16, dec16, injbot16, Z16, Penc, Pencctxt2).

channel c1, c2.
process in(c1, ()); out(c2, ())

(* EXPECTED
All queries proved.
0.281s (user 0.261s + system 0.020s), max rss 43608K
END *)

(* EXPECTPV
No query in the input file.
0.044s (user 0.034s + system 0.010s), max rss 14232K
END *)
